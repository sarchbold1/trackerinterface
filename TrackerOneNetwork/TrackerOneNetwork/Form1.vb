
Public Class Form1

    Private WithEvents oAgent As New Agent

    Private Sub oAgent_Progress(ByVal i As Long) Handles oAgent.Progress
        Me.ProgressBar1.Value = i Mod 100
        Me.ProgressBar1.Update()
        Me.Refresh()
        My.Application.DoEvents()
    End Sub

    Private Sub oAgent_Status(ByVal msg As String) Handles oAgent.Status
        Me.ListBox1.Items.Add(msg)
        Me.ListBox1.SetSelected(Me.ListBox1.Items.Count - 1, True)
        Me.ListBox1.Update()
        Me.Refresh()
        My.Application.DoEvents()
    End Sub

    Private Sub Form1_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.Show()

        'REGISTER W/ KIKAPPMONITOR
        KikAppMonitor.Register(OneOutbound.KikAppMonitor.Status.START)


        'start main
        'If UCase(My.Settings.InterfaceType) = "E1 TO ONENETWORK" Then     'SBains Setting to switch between Macola and E1


        If Not oAgent.StartTransfers_E1() Then
            ErrObj.log(1, 0, "TrackerOneForm", "Could not do transfer correctly")
        End If

        'Else
        ''If Not oAgent.StartTransfers() Then
        ''    ErrObj.log(1, 0, "TrackerOneForm", "Could not do transfer correctly")
        ''End If
        'End If

        'If Not Me.MdiChildren Is Nothing Then
        '    Dim pointertoform As System.Windows.Forms.Form
        '    For Each pointertoform In Me.MdiChildren
        '        pointertoform.Close()
        '    Next
        'End If

        'DE-REGISTER W/ KIKAPPMONITOR
        KikAppMonitor.Register(OneOutbound.KikAppMonitor.Status.FINISH)

        'close form
        Me.Close()

    End Sub

    Private Sub Form1_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
    End Sub

End Class
