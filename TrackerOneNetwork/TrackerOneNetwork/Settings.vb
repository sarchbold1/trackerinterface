﻿
Namespace My
    
    'This class allows you to handle specific events on the settings class:
    ' The SettingChanging event is raised before a setting's value is changed.
    ' The PropertyChanged event is raised after a setting's value is changed.
    ' The SettingsLoaded event is raised after the setting values are loaded.
    ' The SettingsSaving event is raised before the setting values are saved.
    Partial Friend NotInheritable Class MySettings

        Function SettingValue(ByRef SettingElement As String) As String
            Dim ReturnVal As String = ""

            Select Case UCase(Trim(SettingElement))
                Case "E1DATABASE"
                    If UCase(Trim(My.Settings.Application_Mode)) = "LIVE" Then
                        ReturnVal = My.Settings.E1Database
                    Else
                        ReturnVal = My.Settings.E1Database_TEST
                    End If
                Case "E1DATABASE_CTL"
                    If UCase(Trim(My.Settings.Application_Mode)) = "LIVE" Then
                        ReturnVal = My.Settings.E1Database_CTL
                    Else
                        ReturnVal = My.Settings.E1Database_CTL_TEST
                    End If
                Case "FTPINBOUNDDIR"
                    If UCase(Trim(My.Settings.Application_Mode)) = "LIVE" Then
                        ReturnVal = My.Settings.FTPInboundDir
                    Else
                        ReturnVal = My.Settings.FTPInboundDir_TEST
                    End If
                Case "FTPPASSWORD"
                    If UCase(Trim(My.Settings.Application_Mode)) = "LIVE" Then
                        ReturnVal = My.Settings.FTPPassword
                    Else
                        ReturnVal = My.Settings.FTPPassword_TEST
                    End If
                Case "FTPREMOTERECEIVEDIR"
                    If UCase(Trim(My.Settings.Application_Mode)) = "LIVE" Then
                        ReturnVal = My.Settings.FTPRemoteReceiveDir
                    Else
                        ReturnVal = My.Settings.FTPRemoteReceiveDir_TEST
                    End If
                Case "FTPSHIPLOCALDIR"
                    If UCase(Trim(My.Settings.Application_Mode)) = "LIVE" Then
                        ReturnVal = My.Settings.FTPShipLocalDir
                    Else
                        ReturnVal = My.Settings.FTPShipLocalDir_TEST
                    End If
                Case "FTPSHIPREMOTEDIR"
                    If UCase(Trim(My.Settings.Application_Mode)) = "LIVE" Then
                        ReturnVal = My.Settings.FTPShipRemoteDir
                    Else
                        ReturnVal = My.Settings.FTPShipRemoteDir_TEST
                    End If
                Case "FTPUSERNAME"
                    If UCase(Trim(My.Settings.Application_Mode)) = "LIVE" Then
                        ReturnVal = My.Settings.FTPUsername
                    Else
                        ReturnVal = My.Settings.FTPUsername_TEST
                    End If
                Case "ONENETWORKCONN"
                    If UCase(Trim(My.Settings.Application_Mode)) = "LIVE" Then
                        ReturnVal = My.Settings.OneNetworkConn
                    Else
                        ReturnVal = My.Settings.OneNetworkConn_Test
                    End If
                Case "ONENETWORKFTP"
                    If UCase(Trim(My.Settings.Application_Mode)) = "LIVE" Then
                        ReturnVal = My.Settings.OneNetWorkFTP
                    Else
                        ReturnVal = My.Settings.OneNetWorkFTP_TEST
                    End If
                Case "TRACKERCONN"
                    If UCase(Trim(My.Settings.Application_Mode)) = "LIVE" Then
                        ReturnVal = My.Settings.TrackerConn
                    Else
                        ReturnVal = My.Settings.TrackerConn_Test
                    End If
                Case "ONENETWORKEMAILDIST"
                    If UCase(Trim(My.Settings.Application_Mode)) = "LIVE" Then
                        ReturnVal = My.Settings.OneNetworkEmailDist
                    Else
                        ReturnVal = My.Settings.OneNetworkEmailDist_TEST
                    End If
            End Select

            Return ReturnVal

        End Function
    End Class

End Namespace

