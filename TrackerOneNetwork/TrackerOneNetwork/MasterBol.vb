Imports System.Data.Odbc

Public Class MasterBol

    Public strConn As String
    Public schema As String

    Sub New(ByVal dbconn, ByVal schemata)

        strconn = dbconn
        schema = schemata

    End Sub

    Public Function GetMovementMasterBol(ByVal movementNo As String) As Long

        Dim masterBol As Long

        Using cnn As New OdbcConnection(strConn)

            cnn.Open()
            Dim ssql As String

            ssql = String.Format("SELECT * FROM {0}.F5547003 WHERE MBURRF = :MOVEMENTNO", schema)
            Using cmd As New OdbcCommand(ssql, cnn)

                cmd.Parameters.Add("MOVEMENTNO", OdbcType.Char).Value = movementNo

                Dim rdr As OdbcDataReader = cmd.ExecuteReader

                If rdr.HasRows Then
                    masterBol = rdr("MBSHPN")
                    If masterBol.Equals(System.DBNull.Value) Then masterBol = 0
                Else
                    masterBol = 0
                End If

                rdr.Close()
                cmd.Dispose()

            End Using

            cnn.Close()
        End Using

        Return masterBol

    End Function

    Public Function GetNextMasterBol() As Long

        Dim masterBol As Object

        Using cnn As New OdbcConnection(strConn)

            cnn.Open()

            Dim ssql As String
            ssql = String.Format("SELECT MAX(MBSHPN) FROM {0}.F5547003", schema)

            Using cmd As New OdbcCommand(ssql, cnn)
                masterBol = cmd.ExecuteScalar()

                If masterBol.Equals(System.DBNull.Value) Then
                    masterBol = 1
                Else
                    masterBol = masterBol + 1
                End If

                cmd.Dispose()
            End Using

            cnn.Close()

        End Using
        Return masterBol
    End Function

    Public Function DeleteMovementMasterBol(ByVal movementNo As String) As Integer

        Dim recAffected As Integer = 0

        Using cnn As New OdbcConnection(strConn)
            cnn.Open()
            Dim ssql As String
            Dim MasterBOL As Integer = 0


            'RETRIEVE MASTER BOL # FROM MASTER BOL TABLE BEFORE WE DELETE IT SO WE CAN SET SDSHPN = 0 ON SALES ORDER
            ssql = String.Format("SELECT MBSHPN FROM {0}.F5547003 WHERE ROWNUM = 1 AND MBURRF = :MOVEMENT", schema)
            Using cmd2 As New OdbcCommand(ssql, cnn)

                cmd2.Parameters.Add("MOVEMENT", OdbcType.Char).Value = movementNo
                MasterBOL = cmd2.ExecuteScalar()
                cmd2.Dispose()

                'If Master BOL is not null then delete entry in Master BOL and clear sales order Master BOL
                If Not (MasterBOL.Equals(System.DBNull.Value)) Then

                    'DELETE ENTRY IN MASTER BOL TABLE 
                    ssql = String.Format("DELETE FROM {0}.F5547003 WHERE MBURRF = :MOVEMENT", schema)
                    Using cmd As New OdbcCommand(ssql, cnn)

                        cmd.Parameters.Add("MOVEMENT", OdbcType.Char).Value = movementNo

                        recAffected = cmd.ExecuteNonQuery()

                        cmd.Dispose()
                    End Using

                    'Clear Sales Order Master BOL
                    ClearSalesOrderMasterBOL(MasterBOL)
                End If
            End Using


            cnn.Close()
        End Using

        Return recAffected
    End Function

    Public Function HasMultipleOrders(ByVal movementNo As String) As Integer
        Dim ssql As String

        Dim numOrders As Integer

        Using cnn As New OdbcConnection(strConn)
            cnn.Open()
            ssql = String.Format("SELECT COUNT(*) FROM {0}.F4201 WHERE SHURRF = :MOVEMENT", schema)

            Using cmd As New OdbcCommand(ssql, cnn)
                cmd.Parameters.Add("MOVEMENT", OdbcType.Char).Value = movementNo
                numOrders = cmd.ExecuteScalar()
                cmd.Dispose()
            End Using

            cnn.Close()
        End Using

        Return numOrders
    End Function

    Public Function InsertMasterBol(ByVal movementNo As String, ByVal masterBol As Integer) As Integer
        Dim ssql As String
        Dim recInserted As Integer
        Using cnn As New OdbcConnection(strConn)
            cnn.Open()

            ssql = String.Format("INSERT INTO {0}.F5547003 (MBURRF,MBSHPN,MBUSER,MBJOBN,MBPID,MBUPMJ,MBUPMT) VALUES (:MOVEMENT,:MASTERBOL,:ONENETWORK,:ONENETWORK,:ONENETWORK,KIKDATETOJUL(CURRENT_DATE),to_char(Current_Date, 'HH24MISS'))", schema)

            Using cmd As New OdbcCommand(ssql, cnn)

                cmd.Parameters.Add("MOVEMENT", OdbcType.Char).Value = movementNo
                cmd.Parameters.Add("MASTERBOL", OdbcType.Int).Value = masterBol
                cmd.Parameters.Add("ONENETWORK", OdbcType.Char).Value = "ONENET"


                recInserted = cmd.ExecuteNonQuery()
                cmd.Dispose()
            End Using
            cnn.Close()
        End Using

        Return recInserted

    End Function

    Public Function ClearSalesOrderMasterBOL(ByVal MasterBOL As Integer) As Integer

        Dim recAffected As Integer = 0

        Using cnn As New OdbcConnection(strConn)
            cnn.Open()

            Dim ssql As String
            ssql = String.Format("UPDATE {0}.F4211 SET SDSHPN = 0 WHERE SDSHPN = :MASTERBOL", schema)
            Using cmd As New OdbcCommand(ssql, cnn)

                cmd.Parameters.Add("MASTERBOL", OdbcType.Char).Value = MasterBOL

                recAffected = cmd.ExecuteNonQuery()

                cmd.Dispose()
            End Using

            cnn.Close()
        End Using

        Return recAffected
    End Function

End Class
