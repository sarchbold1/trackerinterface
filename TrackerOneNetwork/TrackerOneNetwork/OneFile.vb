Imports System.IO
Imports System.xml
Imports System.Xml.Serialization
Imports OneNetworkSchema

Public Class OneFile

    Public Enum OneFileType
        Consolidated
        Payment
        Appointment
        Tracking
        Modified
    End Enum

    Private xContent As String
    Private xFilename As String
    Private xFileType As String
    Private xObject As Object
    Private m_refNo As String

    Private m_Environment As String

    Public ReadOnly Property xmlObject() As Object
        Get
            Return xObject
        End Get
    End Property

    Public ReadOnly Property xmlFilename() As String
        Get
            Return xFilename
        End Get
    End Property

    Public ReadOnly Property xmlFiletype() As OneFileType
        Get
            Return xFileType
        End Get
    End Property

    Public ReadOnly Property RefNo() As String
        Get
            RefNo = m_refNo
        End Get
    End Property

    Public ReadOnly Property Environment() As String
        Get
            Return m_Environment
        End Get
    End Property

    Sub New(ByVal filename As String, ByVal contents As String)
        xContent = contents
        xFilename = filename

        Try
            Serialize()
        Catch ex As Exception
            Throw New Exception("Unable to serialize file " & filename, ex)
        End Try

    End Sub

    Sub Serialize()

        Dim oSer As XmlSerializer
        Dim sReader As StringReader
        Dim rootNode As String

        'initialize member variables
        m_refNo = Nothing
        xObject = Nothing

        'get first node which is the root
        Try
            Dim xDoc As New XPath.XPathDocument(New StringReader(Me.xContent))
            Dim xNav As XPath.XPathNavigator

            'get root element
            xNav = xDoc.CreateNavigator
            xNav.MoveToRoot()
            xNav.MoveToFirstChild()

            rootNode = xNav.LocalName

            sReader = New StringReader(Me.xContent)

        Catch ex As Exception
            Exit Sub
        End Try

        Select Case rootNode
            Case "FGIIntegConsolidationNotification"
                xFileType = OneFileType.Consolidated
                Dim FGCombine As FGIIntegConsolidationNotification

                oSer = New XmlSerializer(GetType(FGIIntegConsolidationNotification))
                FGCombine = oSer.Deserialize(sReader)
                sReader.Close()
                xObject = FGCombine

                'check for shipment information

                'Try
                '    'get previous movement
                '    'm_refNo = FGCombine.PreviousMovementShipmentInfo.ShipmentInfo(0).ShipmentNumber
                '    'Me.ParseEnvironment(m_refNo)

                'Catch ex As Exception
                '    'do nothing
                '    Me.m_Environment = Nothing
                'End Try

                Try
                    'do this if we didn't get the previous
                    If Me.Environment Is Nothing Then
                        m_refNo = FGCombine.CurrentMovementShipmentInfo.ShipmentInfo(0).ShipmentNumber
                        Me.ParseEnvironment(m_refNo)
                    End If
                Catch ex As Exception
                    'do nothing
                    Me.m_Environment = Nothing
                End Try

            Case "FGIIntegPaymentNotification"
                xFileType = OneFileType.Payment
                Dim FGPayment As FGIIntegPaymentNotification
                oSer = New XmlSerializer(GetType(FGIIntegPaymentNotification))
                FGPayment = oSer.Deserialize(sReader)
                sReader.Close()
                xObject = FGPayment

                'need to get the reference #, so we know which environment this is for
                m_refNo = FGPayment.ShipmentListMessage.Shipment(0).ShipmentNumber
                Me.ParseEnvironment(m_refNo)
                m_refNo = FGPayment.ShipmentListMessage.Shipment(0).ShipmentNumber

            Case "FGIIntegApptNotification"
                xFileType = OneFileType.Appointment
                Dim FGAppt As FGIIntegApptNotification
                oSer = New XmlSerializer(GetType(FGIIntegApptNotification))
                FGAppt = oSer.Deserialize(sReader)
                sReader.Close()
                xObject = FGAppt

                'need to get the reference #, so we know which environment this is for
                m_refNo = FGAppt.Shipment.ShipmentNumber
                Me.ParseEnvironment(m_refNo)

            Case "FGIIntegTrackNotification"
                xFileType = OneFileType.Tracking
                Dim FGTrack As FGIIntegTrackNotification
                oSer = New XmlSerializer(GetType(FGIIntegTrackNotification))
                FGTrack = oSer.Deserialize(sReader)
                sReader.Close()
                xObject = FGTrack

                'need to get the reference #, so we know which environment this is for
                m_refNo = FGTrack.ShipmentListMessage.Shipment(0).ShipmentNumber

                Me.ParseEnvironment(m_refNo)

            Case "FGIIntegModshipnot"
                Try
                    xFileType = OneFileType.Modified
                    Dim ModShipNot As FGIIntegModshipnot

                    oSer = New XmlSerializer(GetType(FGIIntegModshipnot))
                    ModShipNot = oSer.Deserialize(sReader)
                    sReader.Close()
                    xObject = ModShipNot

                    'need to get the reference #, so we know which environment this is for
                    m_refNo = ModShipNot.ShipmentListMessage.Shipment.ShipmentNumber
                    Me.ParseEnvironment(m_refNo)
                Catch EX As Exception

                End Try
        End Select

    End Sub

    Sub ParseEnvironment(ByVal shipNumber As String)

        Dim arrShip() As String
        Dim tmpLoc As String

        'see if we got an empty shipment number in
        If shipNumber Is Nothing Then
            Me.m_Environment = Nothing
            Exit Sub
        End If

        'split using dashes
        arrShip = shipNumber.Split("-")

        'need to check length
        If arrShip.Length <> 2 Then
            'must be length of 2
            Me.m_Environment = Nothing
            Exit Sub
        End If

        'get last element, this should be the plant
        tmpLoc = arrShip.GetValue(arrShip.Length - 1)

        'look for unique location
        Dim ssql As String = "SELECT Environment FROM Companies WHERE MfgLoc = ?"
        Using cnn As New Odbc.OdbcConnection(My.Settings.SettingValue("OneNetworkConn"))
            Dim cmd As New Odbc.OdbcCommand(ssql, cnn)
            cmd.Parameters.Add("@mfgloc", Odbc.OdbcType.VarChar).Value = tmpLoc
            cnn.Open()

            Me.m_Environment = cmd.ExecuteScalar
        End Using

    End Sub

End Class
