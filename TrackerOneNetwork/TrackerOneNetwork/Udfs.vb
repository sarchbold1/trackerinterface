Imports System.Data.Odbc
Imports OneNetworkSchema

Public Class Udfs

    Public Enum PickupReasonCodes
        Customer = 116
        Traffic = 117
        Corporate = 118
        Plant = 119
    End Enum

    Public Enum DeliveryReasonCodes
        Customer = 120
        Traffic = 121
        Corporate = 122
        Plant = 123
    End Enum

    'Function ProcessUDFs(ByRef oShipment As ShipmentListMessageShipment, ByRef cnn As OdbcConnection, ByRef txn As OdbcTransaction) As Boolean

    '    Dim oShipAdf As New ShipmentListMessageShipmentShipmentHeaderMDFs
    '    Dim i As Integer

    '    'handling of ADFs
    '    For i = 0 To oShipment.Items.Length - 1

    '        If oShipment.Items(i).GetType.Name = "ShipmentHeaderMDFs" Then
    '            oShipAdf = oShipment.Items(i)
    '        End If

    '    Next

    '    'parse shipmentnumber
    '    Dim ordno, demand As String

    '    ordno = Nothing
    '    demand = Nothing

    '    OneShipments.ParseShipmentNumber(oShipment.ShipmentNumber, ordno, demand)

    '    For Each nvPair As NameValuePair In oShipment.Udfs.NameValuePair

    '        If nvPair.Name = "PO_COMMENTS1" Then
    '            'this field are reason codes
    '            'If IsNumeric(nvPair.Value) Then    

    '            '    'determine ship via value
    '            '    shipvia = IIf(oShipment.FreightControlledBySystem, "DELIVERY", "PICKUP")

    '            '    Dim tmpRcCode() As String
    '            '    Dim iCode As Integer

    '            '    tmpRcCode = nvPair.Value.Split(",")

    '            '    For iCode = 0 To tmpRcCode.Length - 1
    '            '        'insert the reason code in the table
    '            '        InsertReasonCode(cnn, txn, demand, CInt(ordno), oShipAdf.ShipFromSiteIntegRef, CInt(tmpRcCode(iCode)), shipvia, oShipment.LastModifiedUser)
    '            '    Next

    '            'End If
    '        ElseIf nvPair.Name = "PO_COMMENTS2" Then

    '            'Dim ssql As String

    '            'If Not nvPair.Value Is System.DBNull.Value Then

    '            '    Try
    '            '        ssql = "UPDATE Fps SET FPS.APPOINTMENT_DATE = '" & nvPair.Value & "' " _
    '            '                & " FROM FPSShipmentMaster FPS " _
    '            '                & " INNER JOIN TrafficInformationTable Traffic " _
    '            '                & " ON Traffic.shipmentnumber = fps.shipmentnumber " _
    '            '                & " AND Traffic.DemandLocation = fps.demandlocation " _
    '            '                & " WHERE Traffic.ORDER_NO = " & ordno _
    '            '                & " AND Traffic.DemandLocation = '" & demand & "'"

    '            '        DbConn.ExecuteQuery(ssql, My.Settings.SettingValue("TrackerConn"))
    '            '    Catch ex As Exception
    '            '        ErrObj.log(1, 0, "ProcessUDFs", "Could not update appointment date to " & nvPair.Value, ex)
    '            '    End Try

    '            'End If


    '        End If

    '    Next

    '    Return True

    'End Function

    '*********This overloaded function is obsolete ********************
    Function InsertReasonCode(ByRef cnnTracker As OdbcConnection, ByRef txn As OdbcTransaction, ByVal demand As String, ByVal orderNo As String, ByVal mfgLoc As String, _
                            ByVal rcCode As Integer, ByVal shipVia As String, ByVal userId As String, _
                            Optional ByVal itemNo As String = Nothing)


        Dim ssql As String
        Dim dr As DataRow
        Dim da As OdbcDataAdapter = Nothing
        Dim ds As New DataSet

        Try

            Dim dbDateTime As Date
            Dim tempDemand As String
            Dim tempmfgLoc As String

            'CONVERT E1 Demand Location ID to Macola ID
            'ssql = "SELECT KIKMASTER.dbo.GetMacolaLocation('" & demand & "') "
            'tempDemand = DbConn.ExecuteScalar(ssql, My.Settings.SettingValue("OneNetworkConn"))
            tempDemand = Get_Macola_Location(demand)

            'CONVERT E1 MfgLoc Location ID to Macola ID
            'ssql = "SELECT KIKMASTER.dbo.GetMacolaLocation('" & mfgLoc & "') "
            'tempmfgLoc = DbConn.ExecuteScalar(ssql, My.Settings.SettingValue("OneNetworkConn"))
            tempmfgLoc = Get_Macola_Location(mfgLoc)

            dbDateTime = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))

            'Me.ArchiveExistingReasonCodes(demand, mfgLoc, orderNo, rcCode, dbDateTime)   'Sarj Aug 16, 2011
            Me.ArchiveExistingReasonCodes(tempDemand, tempmfgLoc, orderNo, rcCode, dbDateTime)

            'Open table for entry
            ssql = "SELECT * FROM RcTrx WITH(NOLOCK) WHERE 1=0" 'Changed from RCtrx to RCtrx_oneNet Feb27, 2013 Sarchbold
            DbConn.GetUpdateableDataAdapter(ssql, cnnTracker, da, txn)

            da.Fill(ds)

            'create new row
            dr = ds.Tables(0).NewRow

            'fill our data
            dr("REASON_CODE") = rcCode
            dr("DEMAND_LOCATION") = tempDemand
            dr("MFGING_LOCATION") = tempmfgLoc
            dr("ORDER_NO") = orderNo
            dr("SHIP_VIA_CODE") = shipVia
            dr("ITEM_NO") = itemNo
            dr("USER_ID") = userId
            dr("DATE_TIME") = dbDateTime
            dr("ARC_FLAG") = 0
            dr("SC_CHECK_FLAG") = 0

            'add row to dataset
            ds.Tables(0).Rows.Add(dr)
            da.UpdateCommand.CommandTimeout = 60
            da.Update(ds)
            ds.AcceptChanges()

        Catch ex As Exception
            ErrObj.log(1, 0, "InsertReasonCode", "could not insert reason code", ex, orderNo)

        End Try

        Return True

    End Function

    Function InsertReasonCode(ByVal demand As String, ByVal orderNo As String, ByVal mfgLoc As String, _
                            ByVal rcCode As Integer, ByVal shipVia As String, ByVal userId As String, _
                            Optional ByVal itemNo As String = Nothing)


        Dim ssql As String
        Dim dr As DataRow
        Dim da As OdbcDataAdapter = Nothing
        Dim ds As New DataSet


        Using cnnTracker As New OdbcConnection(My.Settings.SettingValue("TrackerConn"))

            Try
                Dim dbDateTime As Date
                Dim tempDemand As String
                Dim tempmfgLoc As String

                'CONVERT E1 Demand Location ID to Macola ID
                'ssql = "SELECT KIKMASTER.dbo.GetMacolaLocation('" & demand & "') "
                'tempDemand = DbConn.ExecuteScalar(ssql, My.Settings.SettingValue("OneNetworkConn"))
                tempDemand = Get_Macola_Location(demand)

                'CONVERT E1 MfgLoc Location ID to Macola ID
                'ssql = "SELECT KIKMASTER.dbo.GetMacolaLocation('" & mfgLoc & "') "
                'tempmfgLoc = DbConn.ExecuteScalar(ssql, My.Settings.SettingValue("OneNetworkConn")) & ""
                tempmfgLoc = Get_Macola_Location(mfgLoc)

                dbDateTime = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))

                'archive any existing reason codes
                'Me.ArchiveExistingReasonCodes(demand, mfgLoc, orderNo, rcCode, dbDateTime)  'changed by Sarj Aug 16, 2011
                Me.ArchiveExistingReasonCodes(tempDemand, tempmfgLoc, orderNo, rcCode, dbDateTime)

                'open connection object
                cnnTracker.Open()

                'open table for entry
                ssql = "SELECT * FROM RcTrx WITH(NOLOCK) WHERE 1=0" 'Changed from RCtrx to RCtrx_oneNet Feb27, 2013 Sarchbold
                DbConn.GetUpdateableDataAdapter(ssql, cnnTracker, da)

                da.Fill(ds)

                'create new row
                dr = ds.Tables(0).NewRow

                'fill our data
                dr("REASON_CODE") = rcCode
                dr("DEMAND_LOCATION") = tempDemand
                dr("MFGING_LOCATION") = tempmfgLoc  'changed by Sarj Aug 16, 2011
                dr("ORDER_NO") = orderNo
                dr("SHIP_VIA_CODE") = shipVia
                dr("ITEM_NO") = itemNo
                dr("USER_ID") = userId
                dr("DATE_TIME") = dbDateTime
                dr("ARC_FLAG") = 0
                dr("SC_CHECK_FLAG") = 0

                'add row to dataset
                ds.Tables(0).Rows.Add(dr)
                'da.UpdateCommand.CommandTimeout = 60
                da.Update(ds)
                ds.AcceptChanges()

            Catch ex As Exception
                ErrObj.log(1, 0, "InsertReasonCode", "could not insert reason code", ex, orderNo)

            End Try
        End Using

        Return True

    End Function

    Function InsertReasonCode_Rerun(ByVal demand As String, ByVal orderNo As String, ByVal mfgLoc As String, _
                             ByVal rcCode As Integer, ByVal shipVia As String, ByVal userId As String, ByVal EnteredDate As DateTime, _
                             Optional ByVal itemNo As String = Nothing)


        Dim ssql As String
        Dim dr As DataRow
        Dim da As OdbcDataAdapter = Nothing
        Dim ds As New DataSet


        Using cnnTracker As New OdbcConnection(My.Settings.SettingValue("TrackerConn"))

            Try
                Dim dbDateTime As Date
                Dim tempDemand As String
                Dim tempmfgLoc As String

                'CONVERT E1 Demand Location ID to Macola ID
                'ssql = "SELECT KIKMASTER.dbo.GetMacolaLocation('" & demand & "') "
                'tempDemand = DbConn.ExecuteScalar(ssql, My.Settings.SettingValue("OneNetworkConn"))
                tempDemand = Get_Macola_Location(demand)

                'CONVERT E1 MfgLoc Location ID to Macola ID
                'ssql = "SELECT KIKMASTER.dbo.GetMacolaLocation('" & mfgLoc & "') "
                'tempmfgLoc = DbConn.ExecuteScalar(ssql, My.Settings.SettingValue("OneNetworkConn")) & ""
                tempmfgLoc = Get_Macola_Location(mfgLoc)

                dbDateTime = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))

                'archive any existing reason codes
                'Me.ArchiveExistingReasonCodes(demand, mfgLoc, orderNo, rcCode, dbDateTime)  'Sarj Aug 16, 2011
                Me.ArchiveExistingReasonCodes(tempDemand, tempmfgLoc, orderNo, rcCode, dbDateTime)

                'open connection object
                cnnTracker.Open()

                'open table for entry
                ssql = "SELECT * FROM RcTrx WITH(NOLOCK) WHERE 1=0" 'Changed from RCtrx to RCtrx_oneNet Feb27, 2013 Sarchbold
                DbConn.GetUpdateableDataAdapter(ssql, cnnTracker, da)

                da.Fill(ds)

                'create new row
                dr = ds.Tables(0).NewRow

                'If orderNo = 866113 Then Stop

                'fill our data
                dr("REASON_CODE") = rcCode
                dr("DEMAND_LOCATION") = tempDemand
                dr("MFGING_LOCATION") = tempmfgLoc
                dr("ORDER_NO") = orderNo
                dr("SHIP_VIA_CODE") = shipVia
                dr("ITEM_NO") = itemNo
                dr("USER_ID") = userId
                dr("DATE_TIME") = EnteredDate
                dr("ARC_FLAG") = 0
                dr("SC_CHECK_FLAG") = 0

                'add row to dataset
                ds.Tables(0).Rows.Add(dr)
                da.UpdateCommand.CommandTimeout = 60
                da.Update(ds)
                ds.AcceptChanges()

            Catch ex As Exception
                ErrObj.log(1, 0, "InsertReasonCode", "could not insert reason code", ex, orderNo)

            End Try
        End Using

        Return True

    End Function


    Class ReasonCode
        Public code As String
        Public description As String
        Public shipvia As String
        Public mfgloc As String
        Public username As String
    End Class

    'MOD notification ReasonCode Function
    Function ParseReasonCode_E1(ByVal freightControlled As Boolean, ByRef oMovement As FGIIntegModshipnotShipmentListMessageShipmentShipmentHeaderMDFs) As ReasonCode
        Dim oRc As ReasonCode = Nothing

        If oMovement IsNot Nothing And oMovement.ReasonCode IsNot Nothing Then
            Try
                Using cnn As New OdbcConnection(My.Settings.SettingValue("OneNetworkConn"))
                    Dim ssql As String
                    Dim shipvia As String
                    Dim description As String

                    'determine shipvia & reason code to look for
                    shipvia = IIf(freightControlled, "DELIVERY", "PICKUP")
                    description = oMovement.ReasonCode
                    ssql = "SELECT * FROM ReasonCodes WHERE shipvia = ? AND DESCRIPTION = ?"
                    Using cmd As New OdbcCommand(ssql, cnn)

                        'add parameters, look for the shipvia & description in the table
                        cmd.Parameters.Add("@shipvia", OdbcType.VarChar).Value = shipvia
                        cmd.Parameters.Add("@description", OdbcType.VarChar).Value = description

                        'execute
                        cnn.Open()
                        Dim rdr As OdbcDataReader = cmd.ExecuteReader

                        'check if rows came back
                        If rdr.HasRows Then
                            oRc = New ReasonCode

                            'assign fields
                            oRc = New ReasonCode
                            oRc.code = rdr("code")
                            oRc.description = rdr("description")
                            oRc.shipvia = rdr("shipvia")

                        End If

                    End Using
                End Using
            Catch ex As Exception
                'if any error, return nothing
                oRc = Nothing
            End Try
        End If

        Return oRc
    End Function

    'Track notification ReasonCode Function
    Function ParseReasonCode_E1(ByVal freightControlled As Boolean, ByRef oMovement As FGIIntegTrackNotificationMovementEvt) As ReasonCode
        Dim oRc As ReasonCode = Nothing

        If oMovement IsNot Nothing And oMovement.ReasonCode IsNot Nothing Then
            Try
                Using cnn As New OdbcConnection(My.Settings.SettingValue("OneNetworkConn"))
                    Dim ssql As String
                    Dim shipvia As String
                    Dim description As String

                    'determine shipvia & reason code to look for
                    shipvia = IIf(freightControlled, "DELIVERY", "PICKUP")
                    description = oMovement.ReasonCode
                    ssql = "SELECT * FROM ReasonCodes WHERE shipvia = ? AND DESCRIPTION = ?"
                    Using cmd As New OdbcCommand(ssql, cnn)

                        'add parameters, look for the shipvia & description in the table
                        cmd.Parameters.Add("@shipvia", OdbcType.VarChar).Value = shipvia
                        cmd.Parameters.Add("@description", OdbcType.VarChar).Value = description

                        'execute
                        cnn.Open()
                        Dim rdr As OdbcDataReader = cmd.ExecuteReader

                        'check if rows came back
                        If rdr.HasRows Then
                            oRc = New ReasonCode

                            'assign fields
                            oRc = New ReasonCode
                            oRc.code = rdr("code")
                            oRc.description = rdr("description")
                            oRc.shipvia = rdr("shipvia")

                        End If

                    End Using
                End Using
            Catch ex As Exception
                'if any error, return nothing
                oRc = Nothing
            End Try
        End If

        Return oRc
    End Function

    '
    'Name: ParseReasonCode Apptnotification Rason code

    '
    'Description:
    'extracts the reason code returned from the oAdf parameter
    '
    'Parameters:
    '
    'freightControlled - delivered or pickup
    'oAdf - object where the reason code data is stored
    ' Function ParseReasonCode_E1(ByVal freightControlled As Boolean, ByRef oAdf As ShipmentHeaderMDFs2) As ReasonCode
    Function ParseReasonCode_E1(ByVal freightControlled As Boolean, ByRef oAdf As FGIIntegApptNotificationShipmentShipmentHeaderMDFs) As ReasonCode

        Dim oRc As ReasonCode = Nothing

        Try
            'see if pickup reason code exists
            If Not freightControlled And oAdf.PickupChangeReasonCode IsNot Nothing Then

                Using cnn As New OdbcConnection(My.Settings.SettingValue("OneNetworkConn"))
                    Dim ssql As String
                    ssql = "SELECT * FROM ReasonCodes WHERE shipvia = 'PICKUP' AND description = ?"
                    Dim rdr As OdbcDataReader
                    Dim cmd As New OdbcCommand(ssql, cnn)
                    cmd.Parameters.Add("@desc", OdbcType.VarChar).Value = oAdf.PickupChangeReasonCode
                    cnn.Open()

                    rdr = cmd.ExecuteReader
                    If rdr.Read Then
                        oRc = New ReasonCode
                        oRc.code = rdr("code")
                        oRc.description = rdr("description")
                        oRc.shipvia = rdr("shipvia")
                        oRc.mfgloc = Get_Macola_Location(oAdf.ShipFromSiteIntegRef)
                    End If

                End Using

                'see if delivery reason code exists
            ElseIf freightControlled And oAdf.DelChangeReasonCode IsNot Nothing Then

                Using cnn As New OdbcConnection(My.Settings.SettingValue("OneNetworkConn"))
                    Dim ssql As String
                    ssql = "SELECT * FROM ReasonCodes WHERE shipvia = 'DELIVERY' AND description = ?"
                    Dim rdr As OdbcDataReader
                    Dim cmd As New OdbcCommand(ssql, cnn)
                    cmd.Parameters.Add("@desc", OdbcType.VarChar).Value = oAdf.DelChangeReasonCode
                    cnn.Open()

                    rdr = cmd.ExecuteReader
                    If rdr.Read Then
                        oRc = New ReasonCode
                        oRc.code = rdr("code")
                        oRc.description = rdr("description")
                        oRc.shipvia = rdr("shipvia")
                        oRc.mfgloc = Get_Macola_Location(oAdf.ShipFromSiteIntegRef)
                    End If

                End Using
            Else
                'invalid combo, don't do anything

            End If
        Catch ex As Exception
            oRc = Nothing
        End Try

        Return oRc

    End Function



    Sub ArchiveExistingReasonCodes(ByVal demand As String, ByVal mfgLoc As String, ByVal orderNo As String, ByVal rcCode As String, ByVal dbDateTime As DateTime)
        Using cnnTracker As New OdbcConnection(My.Settings.SettingValue("TrackerConn"))
            Dim ssql As String

            cnnTracker.Open()

            'need to find out if this code is already used for this order, will need to archive the existing one's
            'Changed from RCtrx to RCtrx_oneNet Feb27, 2013 Sarchbold
            ssql = "UPDATE RcTrx SET ARC_FLAG = 1 WHERE DEMAND_LOCATION = ? AND MFGING_LOCATION = ? AND ORDER_NO = ? AND REASON_CODE = ? AND ITEM_NO IS NULL AND ARC_FLAG = 0 AND DATE_TIME < ?"
            Dim cmdArchive As New OdbcCommand(ssql, cnnTracker)
            With cmdArchive
                .Parameters.Add("@demand", OdbcType.VarChar).Value = demand
                .Parameters.Add("@mfgloc", OdbcType.VarChar).Value = Get_Macola_Location(mfgLoc)
                .Parameters.Add("@orderno", OdbcType.Int).Value = orderNo
                .Parameters.Add("@rccode", OdbcType.Int).Value = rcCode
                .Parameters.Add("@datetime", OdbcType.DateTime).Value = dbDateTime

                .ExecuteNonQuery()

                .Dispose()
            End With

            cnnTracker.Close()
        End Using
    End Sub

    'This function was created because because we have E1 and Macola references to Manufacturing Locations and need to return the Macola value
    Function Get_Macola_Location(ByRef MfgLocation As String) As String
        Dim tempMfgLoc As String = ""
        Dim ssql As String

        ssql = "SELECT KIKMASTER.dbo.GetMacolaLocation_OLD('" & Trim(MfgLocation & "") & "') "
        tempMfgLoc = DbConn.ExecuteScalar(ssql, My.Settings.SettingValue("OneNetworkConn"))

        Return tempMfgLoc
    End Function
End Class


