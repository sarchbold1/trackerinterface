Imports System.Data.Odbc
Imports System.Net.Mail

Public Class ErrObj
    Private Shared ErrBatchDate As Date = Now
    Public Shared HasError As Boolean
    Public Shared ErrLineCounter As Integer

    Public Enum RetProcType
        Success
        Fail
    End Enum

    Public Shared Sub log(ByVal type As Long, ByVal code As Integer, ByVal source As String, ByVal desc As String, _
                            Optional ByVal exc As Exception = Nothing, Optional ByVal srcError As String = "", _
                            Optional ByVal sendEmail As Boolean = False, _
                            Optional ByVal EmailDist As String = "", _
                            Optional ByVal EmailSubject As String = "")
        Dim ssql As String
        Dim SendToEmail As String = My.Settings.HelpDeskEmail

        If EmailDist <> "" Then SendToEmail = EmailDist
        If EmailSubject <> "" Then EmailSubject = " - " & EmailSubject

        'insert error
        Try

            'build error body
            If exc IsNot Nothing Then
                desc = desc & vbCrLf & exc.Message
            End If

            Dim nqCommand As New OdbcCommand()

            'prevent sql error when inserting descriptions w/ single quotes
            desc = cleanSql(desc)
            srcError = cleanSql(srcError)
            source = cleanSql(source)
            type = cleanSql(type)

            ssql = "INSERT INTO OneNetError (Type,Source,Description,SrcError,LogDate) VALUES (" & type & ",'" & source & "', '" & desc & "','" & srcError & "',GETDATE())"
            DbConn.ExecuteQuery(ssql, My.Settings.SettingValue("OneNetworkConn"))

            'determine whether to send individual e-mail messages
            If sendEmail Then
                Dim oEmail As New SmtpClient(My.Settings.SmtpServer)

                If type = 3 Then  'Type 3 errors have a different formatting for email  
                    'SEND ERROR EMAIL
                    oEmail.Send("OneNetwork@kikcorp.com", EmailDist, "One Network Integration Error" & EmailSubject, desc)
                Else
                    'SEND ERROR EMAIL
                    oEmail.Send("kik_apps@kikcorp.com", EmailDist, "OneNetwork Error at " & source & " - " & Now & " [" & Environment.MachineName & "]", desc)
                End If
            End If

        Catch ex As Exception
            'do nothing
            Debug.Print("error logging")
        End Try


    End Sub

    Public Shared Sub sendErrorSummaryEmail(ByVal startDate As Date)
        Dim rdrErrors As OdbcDataReader

        rdrErrors = DbConn.GetDataReader("SELECT * FROM OneNetError WHERE LogDate >= '" & startDate & "'", My.Settings.SettingValue("OneNetworkConn"), "sendErrorSummaryEmail")

        If rdrErrors.HasRows Then
            Dim errBody As String

            errBody = ""

            'loop through errors
            While rdrErrors.Read()
                'concatenate row record to email body
                errBody &= rdrErrors.Item("Source") & " - " & rdrErrors.Item("Description") & " - " & rdrErrors.Item("SrcError") & vbCrLf & vbCrLf
            End While

            Dim oEmail As New SmtpClient(My.Settings.SmtpServer)
            oEmail.Send("kik_apps@kikcorp.com", My.Settings.HelpDeskEmail, "PROJECT GOLD:TrackOneInterface Error Summary from " & startDate & " - " & Environment.MachineName, errBody)
            'oEmail.Send("kik_apps@kikcorp.com", My.Settings.HelpDeskEmail, "OneNetInterface Error Summary from " & startDate & " - " & Environment.MachineName, errBody)
        End If

        rdrErrors.Close()

    End Sub

    Public Shared Sub purge(Optional ByVal days As Long = 180)

        Dim ssql As String

        ssql = "DELETE FROM OneNetError WHERE LogDate <= DATEDIFF(d," & days & ",GETDATE())"
        DbConn.ExecuteQuery(ssql, My.Settings.SettingValue("OneNetworkConn"))

    End Sub

    Public Shared Function cleanSql(ByVal str As String) As String
        Return str.Replace("'", "''").Replace(",", " ")
    End Function


    Public Shared Function Payableslog(ByVal type As Integer, ByVal code As Integer, ByVal className As String, ByVal source As String, _
                                ByVal title As String, ByVal description As String, _
                                ByVal batchNumber As String, ByVal demandLocation As String, ByVal shipmentNumber As String) As Boolean
        Dim errMsg As String

        Dim ssql As String = "SELECT * FROM PayablesError WHERE 1=0"

        'set error flag
        HasError = True

        Static ldate As Date = Now

        Try
            Dim cnn As New Odbc.OdbcConnection(My.Settings.SettingValue("OneNetworkConn"))

            Dim da As New Odbc.OdbcDataAdapter(ssql, cnn)
            Dim ds As New DataSet
            Dim dr As DataRow
            Dim cmd As New Odbc.OdbcCommandBuilder(da)

            Dim rdrPayableErrorCode As Odbc.OdbcDataReader

            'see if this error code exists in our error code table
            rdrPayableErrorCode = DbConn.GetDataReader("SELECT TITLE FROM PAYABLEERRORCODES WHERE CODE = " & code, My.Settings.SettingValue("OneNetworkConn"), "Payableslog")

            If rdrPayableErrorCode IsNot Nothing Then

                If rdrPayableErrorCode.HasRows Then
                    title = rdrPayableErrorCode("TITLE")
                End If

            End If

            ErrLineCounter += 1

            da.Fill(ds)

            dr = ds.Tables(0).NewRow

            dr("ClassName") = className
            dr("Source") = source
            dr("title") = title
            dr("description") = description
            dr("type") = type
            dr("code") = code
            dr("seqno") = ErrLineCounter

            dr("DemandLocation") = demandLocation
            dr("ShipmentNumber") = shipmentNumber
            dr("BatchNumber") = batchNumber

            dr("ErrBatchDate") = ErrBatchDate
            dr("LogDate") = Now

            'add row and save changes
            ds.Tables(0).Rows.Add(dr)
            da.Update(ds)
            ds.AcceptChanges()

            'cleanup
            ds.Dispose()
            da.Dispose()

        Catch ex As Exception

            'ignore any log errors
        End Try

        errMsg = className & "." & source & vbCrLf
        errMsg = errMsg & title & vbCrLf
        errMsg = errMsg & description

        'Debug.Print(errMsg)

        Return False
    End Function

    Public Shared Function Paylog(ByVal type As Integer, ByVal code As Integer, ByVal className As String, ByVal source As String, _
                            ByVal title As String, ByVal description As String, _
                            ByVal batchNumber As String, ByVal demandLocation As String, ByVal shipmentNumber As String, _
                            ByVal retVal As RetProcType) As RetProcType

        Payableslog(type, code, className, source, title, description, batchNumber, demandLocation, shipmentNumber)

        Return retVal
    End Function

    Public Shared Sub InitError()
        HasError = False
        ErrLineCounter = 0
    End Sub

    Public Shared Sub sendPayableErrorEmail(ByVal startDate As Date)
        Dim rdrErrors As OdbcDataReader

        rdrErrors = DbConn.GetDataReader("SELECT * FROM PayablesError WHERE LogDate >= '" & startDate & "'", My.Settings.SettingValue("OneNetworkConn"), "sendPayableErrorEmail")

        If rdrErrors.HasRows Then
            Dim errBody As String

            errBody = ""

            'loop through errors
            While rdrErrors.Read()
                'concatenate row record to email body
                errBody &= rdrErrors.Item("Source") & " - " & rdrErrors.Item("Title") & " - " & rdrErrors.Item("Description") & " - Batch:" & rdrErrors.Item("BatchNumber") & " - Movement:" & rdrErrors.Item("ShipmentNumber") & vbCrLf & vbCrLf
            End While

            Dim oEmail As New SmtpClient(My.Settings.SmtpServer)
            oEmail.Send("kik_apps@kikcorp.com", My.Settings.HelpDeskEmail, "Payables Error Summary from " & startDate & " - " & Environment.MachineName, errBody)

        End If
        rdrErrors.Close()
    End Sub
End Class
