Imports System.Data.Odbc

Public Class DbConn
    '
    'Name: CreateConnection
    'Description: creates odbcconnection object
    'Return: OdbcConnection
    '
    'Parameters:
    '
    'connString - connecting string for database
    '
    Public Shared Function CreateConnection(ByVal connString As String) As OdbcConnection

        Dim retOdbcConnection As New OdbcConnection(connString)
        Return retOdbcConnection

    End Function

    '
    'Name: GetDataReader
    'Description: convenience function to get a data reader
    'Return: OdbcDataReader
    '
    'Parameters:
    '
    'ssql - sql string to execute
    'connString - db connection string to execute query in
    '
    Public Shared Function GetDataReader(ByVal ssql As String, ByVal connString As String, Optional ByVal Originator As String = "") As OdbcDataReader

        Dim dr As OdbcDataReader

        Try
            Dim conn As New OdbcConnection(connString)
            Dim cmd As New OdbcCommand(ssql, conn)

            cmd.CommandTimeout = 1800
            conn.Open()
            dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Catch ex As Exception
            dr = Nothing
            ErrObj.log(1, 0, "GetDataReader", "Problems Connecting or returning Query resaults. Process:" & Originator, ex, )
        End Try
        ' dr = Nothing

        Return dr

    End Function

    '
    'Name: GetDataReader
    'Description: convenience function to get a data reader
    'Return: OdbcDataReader
    '
    'Parameters:
    '
    'ssql - sql string to execute
    'conn - odbcconnection object
    '
    Public Shared Function GetDataReader(ByVal ssql As String, ByVal conn As OdbcConnection) As OdbcDataReader

        Dim dr As OdbcDataReader

        Try


            Dim cmd As New OdbcCommand(ssql, conn)
            cmd.CommandTimeout = 300
            If conn.State <> ConnectionState.Open Then
                conn.Open()
            End If
            dr = cmd.ExecuteReader()
        Catch ex As Exception
            dr = Nothing
        End Try

        Return dr

    End Function
    '
    'Name: GetDataSet
    'Description: convenience function to get a data set
    'Return: Integer - number of rows affected
    '
    'Parameters:
    '
    'ssql - sql string to execute
    'conn - db connection object
    'ds - dataset to assign results to
    '
    Public Shared Function GetDataSet(ByVal ssql As String, ByVal conn As OdbcConnection, ByRef ds As DataSet) As Integer

        Dim adapter As New OdbcDataAdapter(ssql, conn)

        Return adapter.Fill(ds)

    End Function
    '
    'Name: GetDataSet
    'Description: convenience function to get a data set
    'Return: Integer - number of rows affected
    '
    'Parameters:
    '
    'ssql - sql string to execute
    'connString - db connection object
    'ds - dataset to assign results to
    '
    Public Shared Function GetDataSet(ByVal ssql As String, ByVal connString As String, ByRef ds As DataSet) As Integer

        Dim recCount As Integer

        Using cnn As New OdbcConnection(connString)
            cnn.Open()

            recCount = GetDataSet(ssql, cnn, ds)
        End Using

        Return recCount

    End Function
    '
    'Name: ExecuteQuery
    'Description: execute scalar query against database
    'Return: Boolean
    '
    'Parameters:
    '
    'ssql - sql string to execute
    'connString - db connection string
    'recAffected - assign number of records affected
    '
    Public Shared Function ExecuteQuery(ByVal ssql As String, ByVal connString As String, Optional ByRef recAffected As Long = 0) As Boolean

        Dim retVal As Boolean = False

        Using conn As New OdbcConnection(connString)
            Try
                conn.Open()
                retVal = ExecuteQuery(ssql, conn, recAffected)
            Catch ex As Exception
                retVal = False
            End Try
            
        End Using

        Return retVal

    End Function

    '
    'Name: ExecuteQuery
    'Description: execute scalar query against database
    'Return: Boolean
    '
    'Parameters:
    '
    'ssql - sql string to execute
    'conn - db connection object
    'recAffected - assign number of records affected
    '
    Public Shared Function ExecuteQuery(ByVal ssql As String, ByRef conn As OdbcConnection, Optional ByRef recAffected As Long = 0, Optional ByRef txn As OdbcTransaction = Nothing) As Boolean

        Dim cmd As OdbcCommand

        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
            End If

            If txn Is Nothing Then
                cmd = New OdbcCommand(ssql, conn)
            Else
                cmd = New OdbcCommand(ssql, conn, txn)
            End If

            cmd.CommandText = ssql
            cmd.Connection = conn

            recAffected = cmd.ExecuteNonQuery()

            cmd.Dispose()

            If txn Is Nothing Then
                conn.Close()
            End If

        Catch ex As Exception
            ErrObj.log(1, 0, "ExecuteQuery", "sql: " & ssql, ex)
            Return False
        End Try

        Return True
    End Function

    '
    'Name: GetDateSqlServer
    'Description: execute getdate() function in remote sql server
    'Return: Object - date string from sql server.  system date if failed
    '
    'Parameters:
    '
    'strConn - connection string
    '
    Public Shared Function GetDateSqlServer(ByVal strConn As String) As Object

        Dim retVal As Object

        Using oConn As New OdbcConnection(strConn)
            Try
                oConn.Open()

                Dim cmd As New OdbcCommand("SELECT GETDATE()", oConn)
                retVal = cmd.ExecuteScalar()

                cmd.Dispose()
            Catch ex As Exception
                retVal = DateTime.Now
            End Try
            
        End Using
        
        Return retVal

    End Function
    '
    'Name: GetUpdateableDataAdapter
    'Description: return an updateable data adapater
    'Return: Boolean
    '
    'Parameters:
    '
    'ssql - query
    'conn - connection object
    'da - updateable data adapater
    '
    Public Shared Function GetUpdateableDataAdapter(ByVal ssql As String, ByRef conn As OdbcConnection, ByRef da As OdbcDataAdapter) As Boolean
        Try
            da = New OdbcDataAdapter(ssql, conn)
            Dim cmb As New OdbcCommandBuilder(da)
        Catch ex As Exception
            Return False
        End Try

        Return True
    End Function
    '
    'Name: GetUpdateableDataAdapter
    'Description: return an updateable data adapater
    'Return: Boolean
    '
    'Parameters:
    '
    'ssql - query
    'strConn - connection string
    'da - updateable data adapater
    '
    Public Shared Function GetUpdateableDataAdapter(ByVal ssql As String, ByRef strConn As String, ByRef da As OdbcDataAdapter) As Boolean
        Try
            Dim conn As New OdbcConnection(strConn)
            conn.Open()
            da = New OdbcDataAdapter(ssql, conn)
            'da.UpdateCommand.CommandTimeout = 3600
            da.SelectCommand.CommandTimeout = 6300
            Dim cmb As New OdbcCommandBuilder(da)
        Catch ex As Exception
            Return False
        End Try

        Return True
    End Function

    '
    'Name: GetUpdateableDataAdapter
    'Description: return an updateable data adapater (transaction enabled)
    'Return: Boolean
    '
    'Parameters:
    '
    'ssql - query
    'strConn - connection string
    'da - updateable data adapater
    'txn - odbc transaction
    '
    Public Shared Function GetUpdateableDataAdapter(ByVal ssql As String, ByRef conn As OdbcConnection, ByRef da As OdbcDataAdapter, ByRef txn As OdbcTransaction) As Boolean
        Dim cmd As OdbcCommand

        Try

            cmd = New OdbcCommand(ssql, conn, txn)
            cmd.CommandTimeout = 60
            da = New OdbcDataAdapter(cmd)

            Dim cmb As New OdbcCommandBuilder(da)

        Catch ex As Exception
            Return False
        End Try

        Return True
    End Function
    '
    'Name: ExecuteScalar
    'Description: execute scalar query and return 1 result
    'Return: Object
    '
    'Parameters:
    '
    'ssql - query
    'strConn - connection string
    '
    Public Shared Function ExecuteScalar(ByVal ssql As String, ByVal strConn As String) As Object

        Dim retVal As Object

        Try
            Using cnn As New OdbcConnection(strConn)
                cnn.Open()

                Dim cmd As New OdbcCommand(ssql, cnn)
                retVal = cmd.ExecuteScalar()
                cmd.Dispose()

            End Using
            
        Catch ex As Exception
            retVal = Nothing

        End Try

        Return retVal

    End Function
End Class
