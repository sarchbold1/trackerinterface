Option Explicit On

Imports System.Data.Odbc
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO
Imports OneNetworkSchema

'Added By Sarj_
Imports System.Data
Imports Oracle.DataAccess.Client
Imports Oracle.DataAccess.Types
Imports System.Text

Public Class OneShipments

    Public Event RecordCount(ByVal count As Long)
    Public Event Progress(ByVal i As Long)
    Public Event Status(ByVal msg As String)

    Private Shared oDbConn As New DbConn
    Private oMapping As OneNetworkSchema.OneMapping
    Private OneMasterBol As MasterBol

    Public Structure Order_Type
        Public Demand_Location As String
        Public Order_Number As String
        Public Document_Type As String
        Public FileName As String
        Public Order_type As String
        Public Last_Modified_Date As String
        Public Last_Modified_time As String
    End Structure

    Public Enum ProcessReturnType
        Success
        Fail
        Retry
        Skip
    End Enum

    Public Enum OrderReturnType
        Invalid
        Individual
        Master
        Child
    End Enum

    Public Sub New()
        'init some objects
        'oMapping = New OneNetworkSchema.OneMapping(My.Settings.OneNetworkConn, My.Settings.E1Conn, My.Settings.SettingValue("E1Database"), "User Id=kikapps;Password=custom;Data Source=KIKE1DB1")
        'MsgBox(My.Settings.SettingValue("OneNetworkConn"))
        '"User Id=kikapps;Password=custom;Data Source=KIKE1DB1"
        oMapping = New OneNetworkSchema.OneMapping(My.Settings.SettingValue("OneNetworkConn"), My.Settings.E1Conn, My.Settings.SettingValue("E1Database"), My.Settings.OraConn)
        OneMasterBol = New MasterBol(My.Settings.E1Conn, My.Settings.SettingValue("E1Database"))
    End Sub


   
    Function ProcessBatches_E1(ByRef Orders_Processed As String) As Boolean

        '*********************These variables are used in writing contents of the OneNetShipments_E1 view to the OneNetShipments_LOG table******************************
        Dim daLog As New OdbcDataAdapter
        Dim dsLog As New DataSet
        Dim LogConn As Boolean = False
        Dim RowsAdded As Boolean = False
        Dim LogDate As Date = Now

        'These variables are used in writing contents of the OneNetShipmentLine_E1 view to the OneNetShipmentLine_LOG table
        Dim daLineLog As New OdbcDataAdapter
        Dim dsLineLog As New DataSet
        '***************************************************************************************************************************************************************

        Dim sSql As String
        Dim rdrFps As OdbcDataReader
        Dim dsShipmentMap As New DataSet

        Dim createdShipmentList As ShipmentListMessage
        Dim updatedShipmentList As ShipmentListMessage
        Dim deletedShipmentList As ShipmentListMessage
        Dim archivedShipmentList As ShipmentListMessage
        Dim cancelledShipmentList As ShipmentListMessage
        Dim createdtoDraftShipmentList As ShipmentListMessage

        Const FILE_DATE_FORMAT As String = "yyyyMMddHHmmss"

        Try
            sSql = "SELECT * FROM OneNetShipments_E1_ALC where ORDER_NO = 1427898" ' 1430959
            ' 1401065 or Order_no = 1401056"
            'order_no = 1401066
            'Order_no in (1369905,1369906,1369907,1369908,1369909,1369910,1369911,1369912,1369913,1369914) "


            'get all the Shipments in the batch datestamp range

            rdrFps = DbConn.GetDataReader(sSql, My.Settings.SettingValue("OneNetworkConn"), "ProcessBatches_E1 - Outbound")

            'no data returned
            If rdrFps Is Nothing Then

                Return False
            End If

            'loop through all shipments in E1
            Dim lstNewShipments, lstUpdatedShipments, lstDeletedShipments, lstArchivedShipments, lstCancelledShipments, lstNewCreatetoDraftShipments As New ArrayList

            'Form OneNetwork outbound filenames for each type of action.  This was previously done at the end but we need the filenames ahead of time so we can write them to the outboundlog table for each order
            Dim outFileCreate As String = "kcp_" & Date.Now.ToString(FILE_DATE_FORMAT) & ".create.xml"
            Dim outFileUpdate As String = "kcp_" & Date.Now.ToString(FILE_DATE_FORMAT) & ".update.xml"
            Dim outFileDelete As String = "kcp_" & Date.Now.ToString(FILE_DATE_FORMAT) & ".deleted.xml"
            Dim outFileArchive As String = "kcp_" & Date.Now.ToString(FILE_DATE_FORMAT) & ".archived.xml"
            Dim outFileCancel As String = "kcp_" & Date.Now.ToString(FILE_DATE_FORMAT) & ".cancel.xml"
            Dim outFileCreatetoDraft As String = "kcp_" & DateAdd(DateInterval.Second, 2, Date.Now).ToString(FILE_DATE_FORMAT) & ".create.xml"
            Dim currentoutFile As String

            Dim i As Long
            If rdrFps.HasRows Then
                i = 0
                RaiseEvent RecordCount(100)
                '************************************************THIS SECTION STRICTLY FOR WRITING TO LOG FILES*********************************************
                'get data adapter for OneNetShipments_Log table 

                If DbConn.GetUpdateableDataAdapter("SELECT * FROM OneNetShipments_LOG WITH(NOLOCK) WHERE 1=0", My.Settings.SettingValue("OneNetworkConn"), daLog) Then
                    daLog.Fill(dsLog)
                    LogDate = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))

                    'get data adapter for OneNetShipmentLine_LOG table
                    If DbConn.GetUpdateableDataAdapter("SELECT * FROM OneNetShipmentLine_LOG WITH(NOLOCK) WHERE 1=0", My.Settings.SettingValue("OneNetworkConn"), daLineLog) Then
                        daLineLog.Fill(dsLineLog)
                        LogConn = True
                    Else
                        'If we can't open the OneNetShipmentLine_LOG dispose of the DataAdapter and DataSet we opened for the OneNetShipments_LOG table above
                        daLog.Dispose()
                        dsLog.Dispose()
                    End If
                End If

                '*********************************************************************************************************************************************
            End If

            'loop through all the shipment records
            Do While rdrFps.Read()

                Dim orderOk As Boolean

                orderOk = False

                'declare some temp vars and set it
                Dim DemandLocation, MfgingLocation, OrderNo, ShipmentNumber, Order_Type, ActionExec As String
                Dim Julian_Date_Modified, Julian_Time_Modified, ASN_PalletCount As Integer
                Dim HashValue As Long  'Hash value of the Sales Details data 
                Dim CreateToDraft As Boolean
                Dim SendCancel As Boolean

                'init vars
                DemandLocation = ""
                MfgingLocation = ""
                OrderNo = ""
                ShipmentNumber = ""
                Order_Type = ""
                ActionExec = ""
                Julian_Date_Modified = 0
                Julian_Time_Modified = 0
                HashValue = 0
                CreateToDraft = False
                currentoutFile = ""
                SendCancel = False
                ASN_PalletCount = 0

                Try
                    DemandLocation = rdrFps("TrafficDemand")
                    MfgingLocation = rdrFps("Mfging_Location")
                    OrderNo = rdrFps("Order_No")
                    Order_Type = rdrFps("Order_Type")
                    ShipmentNumber = rdrFps("TrafficShipmentNumber")
                    ActionExec = rdrFps("ActionExec")
                    Julian_Date_Modified = rdrFps("Julian_Date_Modified")
                    Julian_Time_Modified = rdrFps("Julian_Time_Modified")
                    HashValue = Val(rdrFps("HashValue") & "")
                    CreateToDraft = CBool(rdrFps("CreateToDraft") & "")
                    'SendCancel = CBool(rdrFps("CancelAppointment") & "")     CancelAppointment
                    ASN_PalletCount = Val(rdrFps("ASN_PalletCount") & "")

                    orderOk = True
                    'check if batch ID is missing, if so add detail to error log of OrderNo to make fixing easier
                    If rdrFps("SYEDBT").ToString.Trim = "" Then
                        ErrObj.log(1, 0, "ProcessBatches_E1", "Batch ID missing from F4201z1 for Order: " & OrderNo)
                    End If
                    'Set the name of the currentoutFile based on the action type
                    Select Case ActionExec
                        Case "C"    'Create
                            If CreateToDraft = True Then
                                currentoutFile = outFileCreatetoDraft
                            Else
                                currentoutFile = outFileCreate
                            End If
                            Orders_Processed = Orders_Processed & "" & rdrFps("SYEDBT") & ","
                        Case "U"    'Update
                            currentoutFile = outFileUpdate
                            Orders_Processed = Orders_Processed & "" & rdrFps("SYEDBT") & ","
                        Case "A"    'ARCHIVED, IS THE SAME AS UPDATE
                            currentoutFile = outFileArchive
                            Orders_Processed = Orders_Processed & "" & rdrFps("SYEDBT") & ","
                        Case "D"    'Deleted
                            currentoutFile = outFileDelete
                            Orders_Processed = Orders_Processed & "" & rdrFps("SYEDBT") & ","
                    End Select

                Catch ex As Exception
                    orderOk = False
                    ErrObj.log(1, 0, "ProcessBatches_E1", "Error w/ primary keys", ex)
                End Try

                'check to see if this is a OneNetwork enabled Location
                If orderOk And ActionExec <> "" Then

                    'create new Shipment object
                    Dim newShipment As New ShipmentListMessageShipment 'Shipment 'ALC
                    'ShipmentListMessageShipment

                    'map the record to the object
                    orderOk = oMapping.MapFields(rdrFps, newShipment, "Shipment", , My.Settings.InterfaceType)

                    'makes sure order is ok
                    If orderOk Then
                        'now get all the Line Items for this shipment
                        orderOk = GetShipmentLine_E1(newShipment, DemandLocation, MfgingLocation, OrderNo, ActionExec, dsLineLog, currentoutFile, LogDate)
                    End If


                    If orderOk Then

                        Select Case ActionExec
                            Case "C"
                                'CREATED
                                If CreateToDraft = True Then
                                    lstNewCreatetoDraftShipments.Add(newShipment)
                                Else
                                    lstNewShipments.Add(newShipment)
                                End If
                            Case "U"
                                'UPDATED
                                lstUpdatedShipments.Add(newShipment)
                                'IF THE SHIPPING PLANT OR DESTINATION CHANGES THEN The 'SendCancel' Flag was set and we must also send a cancel message
                                If SendCancel Then lstCancelledShipments.Add(newShipment)
                            Case "A"
                                'ARCHIVED, IS THE SAME AS UPDATE
                                newShipment.ActionName = "TMS.TrackShipmentViaInteg"
                                'newShipment.ActionName = "TrackShipmentViaInteg"
                                lstArchivedShipments.Add(newShipment)
                            Case "D"
                                'DELETED
                                lstDeletedShipments.Add(newShipment)
                        End Select

                    Else
                        'no shipment line so do not do anything
                        orderOk = False
                    End If
                Else

                    'this is not OneNetwork enabled, so just set order to Ok because we are not sending this to OneNetwork
                    orderOk = False
                End If

                'see if order was ok
                If orderOk Then

                    If Update_OutBoundLog_Table(DemandLocation, OrderNo, Order_Type, ActionExec, HashValue, currentoutFile, Julian_Date_Modified, Julian_Time_Modified, ASN_PalletCount) Then

                        '*****************************IF write to outbound log successfull then also write to the onenetshipments log********************************************
                        If LogConn Then
                            Dim drLog As DataRow

                            drLog = dsLog.Tables(0).NewRow

                            'Call function to copy data from all columns in rdrFps to the drLog datarow
                            If Copy_DataRow_Fields(drLog, , rdrFps) Then

                                'There are some additional field we want to write to as well
                                With drLog
                                    .Item("EntryDateTime") = LogDate
                                    .Item("FileName") = currentoutFile
                                End With

                                'add to dataset
                                dsLog.Tables(0).Rows.Add(drLog)
                                RowsAdded = True
                            End If
                        End If
                        '**************************************************************************************************************************************************************
                    Else
                        'failed to update outbound log
                        Return False
                    End If

                End If
                'increment
                i += 1

                RaiseEvent Progress(i)

            Loop

            '******************************************save changes to onenetshipments log table and dispose**************************************************
            If LogConn And RowsAdded Then
                daLog.Update(dsLog)
                dsLog.AcceptChanges()
                daLineLog.Update(dsLineLog)
                dsLineLog.AcceptChanges()
                daLog.Dispose()
                dsLog.Dispose()
                daLineLog.Dispose()
                dsLineLog.Dispose()
            End If
            '*************************************************************************************************************************************************

            'cleanup
            rdrFps.Close()

            'we're done, just max out progress bar
            RaiseEvent Progress(100)
            RaiseEvent Status("Processed " & i)

            Dim outFile As String

            'assign New Shipment Array to the array list (if any)

            If lstNewShipments.Count > 0 Then
                createdShipmentList = New ShipmentListMessage
                oMapping.MapFields(Nothing, createdShipmentList, "ShipmentListMessage", , My.Settings.InterfaceType)
                createdShipmentList.ActionName = "TMS.CreateShipmentViaInteg"
                'createdShipmentList.ActionName = "CreateShipmentViaInteg"
                createdShipmentList.Shipment = lstNewShipments.ToArray(GetType(ShipmentListMessageShipment)) 'ALC
                'createdShipmentList.Shipment = lstNewShipments.ToArray(GetType(Shipment))'OneNetwork
                'ShipmentListMessageShipment


                'write out the Created ShipmentList XML file
                'outFile = "kcp_" & Date.Now.ToString(FILE_DATE_FORMAT) & ".create.xml"
                outFile = outFileCreate
                If Not CreateShipmentXML(createdShipmentList, My.Settings.SettingValue("FTPShipLocalDir"), outFile) Then
                    Return False
                End If
            End If

            'assign New Create To Draft Shipment Array to the array list (if any)
            If lstNewCreatetoDraftShipments.Count > 0 Then
                createdtoDraftShipmentList = New ShipmentListMessage
                oMapping.MapFields(Nothing, createdtoDraftShipmentList, "ShipmentListMessage", , My.Settings.InterfaceType)
                createdtoDraftShipmentList.ActionName = "TMS.SaveToDraftViaInteg"
                'createdtoDraftShipmentList.ActionName = "SaveToDraftViaInteg"
                'createdtoDraftShipmentList.Shipment = lstNewCreatetoDraftShipments.ToArray(GetType(Shipment))'onenetwork
                createdtoDraftShipmentList.Shipment = lstNewCreatetoDraftShipments.ToArray(GetType(ShipmentListMessageShipment)) 'ALC

                'write out the Created ShipmentList XML file
                outFile = outFileCreatetoDraft
                If Not CreateShipmentXML(createdtoDraftShipmentList, My.Settings.SettingValue("FTPShipLocalDir"), outFile) Then
                    Return False
                End If
            End If

            'assign Updated Shipment Array to the array list
            If lstUpdatedShipments.Count > 0 Then
                updatedShipmentList = New ShipmentListMessage
                oMapping.MapFields(Nothing, updatedShipmentList, "ShipmentListMessage", , My.Settings.InterfaceType)
                updatedShipmentList.ActionName = "TMS.UpdateShipmentViaInteg"
                'updatedShipmentList.ActionName = "UpdateShipmentViaInteg"
                updatedShipmentList.Shipment = lstUpdatedShipments.ToArray(GetType(ShipmentListMessageShipment))

                'write out the Updated ShipmentList XML file
                'outFile = "kcp_" & Date.Now.ToString(FILE_DATE_FORMAT) & ".update.xml"
                outFile = outFileUpdate
                If Not CreateShipmentXML(updatedShipmentList, My.Settings.SettingValue("FTPShipLocalDir"), outFile) Then
                    Return False
                End If
            End If

            'assign Deleted Shipment Array to the array list
            If lstDeletedShipments.Count > 0 Then
                deletedShipmentList = New ShipmentListMessage
                oMapping.MapFields(Nothing, deletedShipmentList, "ShipmentListMessage", , My.Settings.InterfaceType)
                deletedShipmentList.ActionName = "TMS.DeleteShipmentViaInteg"
                'deletedShipmentList.ActionName = "DeleteShipmentViaInteg"
                deletedShipmentList.Shipment = lstDeletedShipments.ToArray(GetType(ShipmentListMessageShipment))

                'write out the Deleted ShipmentList XML file
                'outFile = "kcp_" & Date.Now.ToString(FILE_DATE_FORMAT) & ".deleted.xml"
                outFile = outFileDelete
                If Not CreateShipmentXML(deletedShipmentList, My.Settings.SettingValue("FTPShipLocalDir"), outFile) Then
                    Return False
                End If

            End If

            If lstArchivedShipments.Count > 0 Then
                archivedShipmentList = New ShipmentListMessage
                oMapping.MapFields(Nothing, archivedShipmentList, "ShipmentListMessage", , My.Settings.InterfaceType)
                archivedShipmentList.Shipment = lstArchivedShipments.ToArray(GetType(ShipmentListMessageShipment))
                archivedShipmentList.ActionName = "TMS.TrackShipmentViaInteg"
                'write out the archived ShipmentList XML file
                'outFile = "kcp_" & Date.Now.ToString(FILE_DATE_FORMAT) & ".archived.xml"
                outFile = outFileArchive
                If Not CreateShipmentXML(archivedShipmentList, My.Settings.SettingValue("FTPShipLocalDir"), outFile) Then
                    Return False
                End If

            End If

            If lstCancelledShipments.Count > 0 Then
                cancelledShipmentList = New ShipmentListMessage
                oMapping.MapFields(Nothing, cancelledShipmentList, "ShipmentListMessage", , My.Settings.InterfaceType)
                cancelledShipmentList.ActionName = "TMS.CancelShipmentViaInteg"
                'cancelledShipmentList.ActionName = "CancelShipmentViaInteg"
                cancelledShipmentList.Shipment = lstCancelledShipments.ToArray(GetType(ShipmentListMessageShipment))

                'write out the archived ShipmentList XML file
                'outFile = "kcp_" & Date.Now.ToString(FILE_DATE_FORMAT) & ".archived.xml"
                outFile = outFileCancel
                If Not CreateShipmentXML(cancelledShipmentList, My.Settings.SettingValue("FTPShipLocalDir"), outFile) Then
                    Return False
                End If

            End If

        Catch ex As Exception
            ErrObj.log(1, 0, "ProcessBatches_E1", "Error Processing Batches ", ex)
            Return False
        End Try

        Return True

    End Function

    Public Shared Function Convert_SiteRef(ByRef LocationID As String, ByVal Direction As String, Optional ByVal OrderNo As String = "9999999", Optional ByVal PadSize As Integer = 0, Optional ByVal ForceConversion As Boolean = False) As String
        Dim newLocationID As String = LocationID

        Try
            Select Case UCase(Direction)
                Case "MACOLA TO E1"
                    newLocationID = Trim(DbConn.ExecuteScalar("SELECT OneNetwork.dbo.convert_siteref('" & LocationID & "' , '" & Direction & "', " & PadSize.ToString & ")", My.Settings.SettingValue("OneNetworkConn")))
                Case "E1 TO MACOLA"
                    newLocationID = Trim(DbConn.ExecuteScalar("SELECT OneNetwork.dbo.convert_siteref('" & LocationID & "' , '" & Direction & "', " & PadSize.ToString & ")", My.Settings.SettingValue("OneNetworkConn")))
                Case "INBOUND"
                    newLocationID = Trim(DbConn.ExecuteScalar("SELECT OneNetwork.dbo.convert_siteref('" & LocationID & "' , 'MACOLA TO E1', " & PadSize.ToString & ")", My.Settings.SettingValue("OneNetworkConn")))
                Case "OUTBOUND"
                    If (My.Settings.ConvertSiteRef Or ForceConversion) And OrderNo < My.Settings.E1_Order_StartNo Then
                        newLocationID = Trim(DbConn.ExecuteScalar("SELECT OneNetwork.dbo.convert_siteref('" & LocationID & "' , 'E1 TO MACOLA', " & PadSize.ToString & ")", My.Settings.SettingValue("OneNetworkConn")))
                    End If
            End Select
        Catch ex As Exception
            ErrObj.log(1, 0, "Convert_SiteRef", "Error retrieving Site Ref lookup for Site:" & LocationID & " and Direction:" & Direction, ex)
        End Try

        If LocationID <> newLocationID Then
            '    ErrObj.log(1, 0, "Convert_SiteRef", "Just an FYI (Source Demand:" & LocationID & " New Demand:" & newLocationID & " Direction: " & Direction & " Order No: " & OrderNo & ")")
        End If
        Return newLocationID
    End Function
    'sbains - New function created for Updating OutboundLog table 
    'OutBoundLog Table - used to track E1 sales order info processed for Outbound to OneNetwork 
    'Function Update_OutBoundLog_Table - Will either Add or Edit entries for Outbound based on order # including Action Type, date processed, 
    'and a hash value that represents the state of all line items in order. 
    Public Shared Function Update_OutBoundLog_Table(ByVal Demand_Location As String, ByVal Order_No As String, ByVal Order_Type As String, Optional ByVal Action_Type As String = "", Optional ByVal HashValue As Long = 0, Optional ByVal OneNet_Filename As String = "", Optional ByRef LastModified_Julian_Date As Integer = 0, Optional ByRef LastModified_Julian_Time As Integer = 0, Optional ByRef ASN_PalletCount As Integer = 0) As Boolean
        Dim da As New Odbc.OdbcDataAdapter
        Dim ds As New DataSet
        Dim ssql As String
        Dim E1Database As String = My.Settings.SettingValue("E1Database")

        ssql = "SELECT * FROM OutBoundLog  WITH(NOLOCK) WHERE 1=0 " 'Order_Company = " & Demand_Location & " AND Order_No = " & Order_No & " AND Order_Type = " & Order_Type

        'get data adapter
        If Not DbConn.GetUpdateableDataAdapter(ssql, My.Settings.SettingValue("OneNetworkConn"), da) Then
            ErrObj.log(1, 0, "Update_OutBoundLog_Table", "could not update outbound log for Order #: " & Order_No & " and Filename: " & OneNet_Filename)
            Return False
        End If

        Try

            Dim enterDate As String
            Dim dr As DataRow

            enterDate = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))

            'open dataset 
            da.Fill(ds)
            'create new row
            dr = ds.Tables(0).NewRow

            'populate fields
            With dr
                .Item("Order_Company") = Demand_Location
                .Item("Order_No") = Order_No
                .Item("Order_type") = Order_Type
                .Item("Last_Modified_Date") = LastModified_Julian_Date
                .Item("Last_Modified_time") = LastModified_Julian_Time
                .Item("Action_Type") = Action_Type
                .Item("Direction") = "OUT"
                .Item("OneNet_Filename") = OneNet_Filename
                .Item("DateTime_Processed") = enterDate
                .Item("SalesDetail_Hash") = HashValue
                .Item("ASN_PalletCount") = ASN_PalletCount
            End With

            'add to dataset
            ds.Tables(0).Rows.Add(dr)
            'End If

            'save changes
            da.Update(ds)
            ds.AcceptChanges()


            'If it's a create shipment then Update Sales Header & Detail Shipment Number(URRF) with Order # as default
            If UCase(Action_Type) = "C" Then
                Try
                    ssql = "UPDATE " & E1Database & ".F4201 SET SHURRF = '" & Order_No & "' WHERE SHDOCO = " & Order_No & " AND SHDCTO = '" & Order_Type & "' AND SHURRF = ' ' "
                    If Not DbConn.ExecuteQuery(ssql, My.Settings.E1Conn) Then
                        ErrObj.log(1, 0, "Update_OutBoundLog_Table", "Error updating SHURRF for Order:" & Order_No.ToString)
                    End If

                    ssql = "UPDATE " & E1Database & ".F4211 SET SDURRF = '" & Order_No & "' WHERE SDDOCO = " & Order_No & " AND SDDCTO = '" & Order_Type & "' AND SDURRF = ' ' "
                    If Not DbConn.ExecuteQuery(ssql, My.Settings.E1Conn) Then
                        ErrObj.log(1, 0, "Update_OutBoundLog_Table", "Error updating SDURRF for Order:" & Order_No.ToString)
                    End If
                Catch ex As Exception
                    ErrObj.log(1, 0, "Update_OutBoundLog_Table", "Error updating SHURRF/SDURRF for Order:" & Order_No.ToString)
                End Try
            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "Update_OutBoundLog_Table", "could not update outbound log for Order #:" & Order_No & "-" & Demand_Location, ex)
            Return False
        End Try

        'cleanup 
        da.Dispose()
        ds.Dispose()

        Return True

    End Function
    '
    'Name: CreateShipmentXML
    'Description: Serializes the the oShipmentList object
    'Return: Boolean
    '
    'Parameters:
    '
    'XmlFileOut - full XML file output
    '
    Function CreateShipmentXML(ByVal oSerializable As Object, ByVal xmlFileDir As String, ByVal xmlFileOut As String) As Boolean

        Try

            Dim ns As XmlSerializerNamespaces = New XmlSerializerNamespaces()

            Dim serializer As New XmlSerializer(GetType(ShipmentListMessage))

            'fix to add prefix 'tms' to all elements w/ this namespace
            ns.Add("tms", "http://www.onenetwork.com/Transportation")

            'fix to add prefix 'sch' to all elements w/ this namespace
            ns.Add("sch", "http://www.onenetwork.com/Scheduling")

            'fix to add prefix 'scc' to all elements w/ this namespace
            ns.Add("scc", "http://www.onenetwork.com/SupplyChainCore")

            Dim sw As New StringWriterWithEncoding(Text.Encoding.UTF8)

            serializer.Serialize(sw, oSerializable, ns)

            'insert into queue to be processed later
            Agent.InsertOutboundFile(xmlFileDir, xmlFileOut, sw.ToString())

            'cleanup
            sw.Close()

        Catch ex As Exception
            ErrObj.log(1, 0, "CreateShipmentXml", "Error creating Xml file", ex, xmlFileOut)
            Throw New IOException("CreateShipmentXML", ex)
        End Try

        Return True

    End Function
    '
    'Name: GetShipmentLine
    'Description: get all line items for a particular Shipment
    'Return: Boolean
    '
    'Parameters:
    '
    'oShipment - Shipment Object to append line items to
    'DemandLocation - Demand location of shipment
    'MfgingLocation - Mfging location of shipment
    'OrderNo - Order #
    '
    'Function GetShipmentLine(ByRef oShipment As Shipment, ByVal demandLocation As String, ByVal mfgingLocation As String, ByVal orderNo As String, ByVal actionExec As String) As Boolean

    '    Dim dsMap As New DataSet
    '    Dim rdrOeordlin As OdbcDataReader
    '    Dim arrShipLine As New ArrayList

    '    Dim ssql As String

    '    If actionExec = "D" Then

    '        'if shipment to be deleted, there will be no shipment line, so just return true
    '        Return True
    '    End If

    '    'get line items()
    '    ssql = "SELECT * FROM OneNetShipmentLine  WHERE DEMANDLOCATION = '" & demandLocation & "' And ORDER_NO = " & orderNo
    '    rdrOeordlin = DbConn.GetDataReader(ssql, My.Settings.SettingValue("OneNetworkConn"), "GetShipmentLine")

    '    If rdrOeordlin Is Nothing Then
    '        rdrOeordlin.Close()
    '        Return False
    '    End If

    '    'we need to make sure we have line items, if not, this is an error
    '    If Not rdrOeordlin.HasRows Then
    '        rdrOeordlin.Close()
    '        ErrObj.log(1, 0, "GetShipmentLine", "No line items for " & orderNo & "-" & demandLocation, , orderNo)
    '        Return False
    '    End If

    '    'loop through all the Line items for this Order
    '    Do While rdrOeordlin.Read()

    '        Dim oShipmentLine As New ShipmentLine

    '        'do the mapping
    '        Dim hasError As Boolean = False
    '        If oMapping.MapFields(rdrOeordlin, oShipmentLine, "ShipmentLine", hasError) Then
    '            'add the line item to the shipment
    '            arrShipLine.Add(oShipmentLine)

    '            If hasError Then
    '                ErrObj.log(1, 0, "GetShipmentLine", "Errors encountered during mapping", , orderNo)
    '            End If
    '        End If

    '    Loop

    '    'assign the line items to shipment
    '    oShipment.ShipmentLine = arrShipLine.ToArray(GetType(ShipmentLine))

    '    'cleanup
    '    rdrOeordlin.Close()

    '    Return True

    'End Function

    'sbains - E1 version of GetShipmentLine Function
    'Name: GetShipmentLine
    'Description: get all line items for a particular Shipment
    'Return: Boolean
    '
    'Parameters:
    '
    'oShipment - Shipment Object to append line items to
    'DemandLocation - Demand location of shipment
    'MfgingLocation - Mfging location of shipment
    'OrderNo - Order #
    '
    Function GetShipmentLine_E1(ByRef oShipment As ShipmentListMessageShipment, ByVal demandLocation As String, ByVal mfgingLocation As String, ByVal orderNo As String, ByVal ActionExec As String, ByRef dsLineLog As DataSet, ByVal Currentoutfile As String, ByVal LogDate As Date) As Boolean

        Dim dsMap As New DataSet
        Dim rdrOeordlin As OdbcDataReader
        Dim arrShipLine As New ArrayList

        Dim ssql As String

        Try
            If ActionExec = "D" Then
                'if shipment to be deleted, there will be no shipment line, so just return true
                Return True
            End If

            'get line items()
            'ssql = "SELECT * FROM OneNetShipmentLine_E1 WHERE DEMANDLOCATION = '" & demandLocation & "' And ORDER_NO = " & orderNo & " ORDER BY SEQUENCE_NO"
            'ssql = "SELECT * FROM OneNetShipmentLine_E1 WHERE ORDER_NO = " & orderNo & " ORDER BY SEQUENCE_NO"
            ssql = "exec SP_OneNetShipmentLine_E1 '" & orderNo & "' "

            rdrOeordlin = DbConn.GetDataReader(ssql, My.Settings.SettingValue("OneNetworkConn"), "GetShipmentLine_E1")

            If rdrOeordlin Is Nothing Then
                rdrOeordlin.Close()
                Return False
            End If

            'we need to make sure we have line items, if not, this is an error
            If Not rdrOeordlin.HasRows Then
                rdrOeordlin.Close()
                ErrObj.log(1, 0, "GetShipmentLine_E1", "No line items for " & orderNo & "-" & demandLocation, , orderNo)
                Return False
            End If

            'loop through all the Line items for this Order
            Do While rdrOeordlin.Read()

                'ShipmentListMessageShipmentShipmentLine
                Dim oShipmentLine As New ShipmentListMessageShipmentShipmentLine 'ALC
                'Dim oShipmentLine As New ShipmentLine'OneNetwork

                'do the mapping
                Dim hasError As Boolean = False
                If oMapping.MapFields(rdrOeordlin, oShipmentLine, "ShipmentLine", hasError, My.Settings.InterfaceType) Then
                    'add the line item to the shipment
                    arrShipLine.Add(oShipmentLine)

                    If hasError Then
                        ErrObj.log(1, 0, "GetShipmentLine_E1", "Errors encountered during mapping", , orderNo)
                    End If

                    '*****************THIS SECTION IS FOR ADDING THE SHIPMENTLINE RECORDS TO THE ONENETSHIPMENTLINE_LOG *******************
                    Dim drLineLog As DataRow
                    drLineLog = dsLineLog.Tables(0).NewRow

                    'Call function to copy data from all columns in rdrFps to the drLog datarow
                    If Copy_DataRow_Fields(drLineLog, , rdrOeordlin) Then

                        'There are some additional field we want to write to as well
                        With drLineLog
                            .Item("EntryDateTime") = LogDate
                            .Item("FileName") = Currentoutfile
                            .Item("ActionExec") = ActionExec
                        End With

                        'add to dataset
                        dsLineLog.Tables(0).Rows.Add(drLineLog)
                    End If
                    '***********************************************************************************************************************


                End If

            Loop

            'assign the line items to shipment
            oShipment.ShipmentLine = arrShipLine.ToArray(GetType(ShipmentListMessageShipmentShipmentLine))
            'oShipment.ShipmentLine = arrShipLine.ToArray(GetType(ShipmentLine))'OneNEtwork

            'cleanup
            rdrOeordlin.Close()

        Catch ex As Exception
            ErrObj.log(1, 0, "GetShipmentLine_E1", "Error retrieving shipment line for order:" & orderNo, ex)
            Return False
        End Try

        Return True

    End Function


    '
    'Name: ProcessFGIIntegApptNotification
    'Description: process shipment appt notification
    'Return: Boolean
    '
    'Parameters:
    '
    'XmlSource - xml text to process
    '
    'Function ProcessApptNotification(ByVal ApptNotification As FGIIntegApptNotification, ByVal fileId As Integer, Optional ByRef refno As String = Nothing) As ProcessReturnType

    '    Dim cnnTracker As OdbcConnection
    '    Dim txn As OdbcTransaction

    '    Try
    '        cnnTracker = DbConn.CreateConnection(My.Settings.SettingValue("TrackerConn"))
    '        cnnTracker.Open()

    '    Catch ex As Exception
    '        ErrObj.log(1, 0, "ProcessApptNotification", "could not open connection", ex)
    '        Return ProcessReturnType.Fail
    '    End Try

    '    Dim ssql As String
    '    Dim apptShipNum As String
    '    Dim tmpShipmentNum As String = Nothing
    '    Dim tmpDemandLocation As String = Nothing

    '    'need to check for existence of shipment element
    '    If ApptNotification.Shipment Is Nothing Then
    '        ErrObj.log(1, 0, "ProcessApptNotification", "No shipment element in Appointment file id " & fileId)
    '        Return ProcessReturnType.Skip
    '    End If

    '    Dim currentShipment As Shipment

    '    'assign first shipment element
    '    currentShipment = ApptNotification.Shipment(0)

    '    'get the shipment number for this shipment
    '    apptShipNum = currentShipment.ShipmentNumber

    '    'need to do some checking of shipmentnumber
    '    If Not ParseShipmentNumber(apptShipNum, tmpShipmentNum, tmpDemandLocation) Then
    '        ErrObj.log(1, 0, "ProcessApptNotification", "Could not parse shipmentno", , currentShipment.ShipmentNumber)
    '        Return ProcessReturnType.Fail
    '    End If

    '    refno = Me.FormatReturnShipmentNumber(tmpShipmentNum, tmpDemandLocation)

    '    'do the merging of pickups based on appointment file for non-controlled
    '    Dim SubReturnValue As ProcessReturnType

    '    If Not MergeOrCancelShipment(ApptNotification, SubReturnValue) Then
    '        ErrObj.log(1, 0, "ProcessApptNotification", "Could not run MergeOrCancelShipment", , currentShipment.ShipmentNumber)

    '        'check for the sub return value for some extra processing
    '        If SubReturnValue = ProcessReturnType.Skip Then
    '            'skip because the function returned true (meaning not fatal), but we don't really want to process further
    '            Return ProcessReturnType.Skip
    '        Else
    '            'just default to fail, may not have been assigned
    '            Return ProcessReturnType.Fail
    '        End If
    '    End If

    '    'begin transaction portion
    '    txn = cnnTracker.BeginTransaction

    '    Try

    '        'fill up our data information
    '        Dim ds As New DataSet
    '        Dim dr As DataRow
    '        Dim da As OdbcDataAdapter = Nothing


    '        'search for the FpsShipmentmaster w/ that shipmentnumber
    '        ssql = "SELECT * FROM FPSShipmentMaster WHERE DemandLocation = '" & tmpDemandLocation & "' AND ShipmentNumber = " & tmpShipmentNum

    '        If Not DbConn.GetUpdateableDataAdapter(ssql, cnnTracker, da, txn) Then
    '            txn.Rollback()
    '            Return ProcessReturnType.Fail
    '        End If

    '        If da.Fill(ds) > 0 Then
    '            'get first row
    '            dr = ds.Tables(0).Rows(0)

    '            'map object to datarow
    '            If dr IsNot Nothing Then

    '                Dim writeChanges As Boolean = False

    '                'attempt to do the mapping
    '                If oMapping.MapObjectToDataRow(fileId, ApptNotification, dr, "ApptNotif", ApptNotification.SiteRsrcReservation.State) Then
    '                    writeChanges = True
    '                End If

    '                If writeChanges Then
    '                    'commit
    '                    da.Update(ds)
    '                    ds.AcceptChanges()
    '                End If
    '            End If
    '        Else

    '            'got nothing back, record may have been archived or it may have been merged w/ another order
    '            Dim rdrShipment As OdbcDataReader
    '            ssql = "SELECT * FROM TrafficInformationTable WHERE DemandLocation = '" & tmpDemandLocation & "' AND Order_no = " & tmpShipmentNum _
    '                    & " AND Order_no <> ShipmentNumber"
    '            rdrShipment = DbConn.GetDataReader(ssql, My.Settings.SettingValue("TrackerConn"), "ProcessApptNotification")
    '            If rdrShipment.HasRows Then
    '                'this is combined w/ another shipment this is ok
    '            Else
    '                rdrShipment.Close()
    '                ErrObj.log(1, 0, "ProcessApptNotification", "Could not find FPS may have been archived: " & tmpShipmentNum, , tmpShipmentNum)
    '            End If

    '        End If

    '        'now update the traffic records associated w/ this fps
    '        Dim tmpTrafficOrder, trafficShipNum As String

    '        'assign order no to tmp var
    '        trafficShipNum = currentShipment.ShipmentNumber
    '        tmpTrafficOrder = Nothing

    '        'parse order no
    '        If ParseShipmentNumber(trafficShipNum, tmpTrafficOrder, Nothing) Then
    '            Dim daTraffic As OdbcDataAdapter = Nothing
    '            Dim dsTraffic As New DataSet
    '            Dim drTraffic As DataRow

    '            ssql = "SELECT * FROM TrafficInformationTable WHERE DemandLocation = '" & tmpDemandLocation & "' AND Order_no = " & tmpTrafficOrder
    '            If Not DbConn.GetUpdateableDataAdapter(ssql, cnnTracker, daTraffic, txn) Then
    '                txn.Rollback()
    '                Return ProcessReturnType.Fail
    '            End If

    '            'populate our dataset
    '            If daTraffic.Fill(dsTraffic) > 0 Then
    '                drTraffic = dsTraffic.Tables(0).Rows(0)

    '                Select Case ApptNotification.SiteRsrcReservation.State
    '                    Case "Cancelled"
    '                        If Not oMapping.MapObjectToDataRow(fileId, currentShipment, drTraffic, "ApptNotif.Shipment", ApptNotification.SiteRsrcReservation.State) Then
    '                            txn.Rollback()
    '                            Return ProcessReturnType.Fail
    '                        End If
    '                    Case Else

    '                        Dim mapSubSet As String
    '                        mapSubSet = IIf(currentShipment.FreightControlledBySystem, "Delivered", "Pickup")

    '                        If Not oMapping.MapObjectToDataRow(fileId, currentShipment, drTraffic, "ApptNotif.Shipment", mapSubSet) Then
    '                            txn.Rollback()
    '                            Return ProcessReturnType.Fail
    '                        End If
    '                End Select

    '                'save changes
    '                daTraffic.Update(dsTraffic)
    '                dsTraffic.AcceptChanges()

    '                'do the ReasonCodes
    '                Dim oUdfs As New Udfs
    '                If currentShipment.Items IsNot Nothing Then
    '                    Dim oRc As Udfs.ReasonCode
    '                    oRc = oUdfs.ParseReasonCode(currentShipment.FreightControlledBySystem, currentShipment.Items(0))
    '                    If oRc IsNot Nothing Then
    '                        oUdfs.InsertReasonCode(tmpDemandLocation, tmpTrafficOrder, oRc.mfgloc, oRc.code, oRc.shipvia, currentShipment.LastModifiedUser)
    '                    End If
    '                End If
    '            Else

    '                ErrObj.log(1, 0, "ProcessApptNotification", "Could not find traffic info: " & trafficShipNum, , trafficShipNum)
    '            End If

    '        End If

    '    Catch ex As Exception
    '        txn.Rollback()
    '        ErrObj.log(1, 0, "ProcessApptNotification", "Error processing", ex)
    '        Return ProcessReturnType.Retry
    '    End Try

    '    'commit
    '    txn.Commit()

    '    'cleanup
    '    ApptNotification = Nothing

    '    Return ProcessReturnType.Success

    'End Function

    'sbains E1 Version of ProcessApptNotification Function
    Function ProcessApptNotification_E1(ByVal ApptNotification As FGIIntegApptNotification, ByVal fileId As Integer, ByRef Orders() As Agent.Order_Type, Optional ByRef refno As String = Nothing) As ProcessReturnType





        Dim cnnE1 As OdbcConnection
        Dim txn As OdbcTransaction
        Dim E1Database As String = My.Settings.SettingValue("E1Database")

        Try
            cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
            cnnE1.Open()

        Catch ex As Exception
            ErrObj.log(1, 0, "ProcessApptNotification", "could not open connection", ex)
            Return ProcessReturnType.Fail
        End Try

        Dim ssql As String
        Dim apptShipNum As String
        Dim tmpShipmentNum As String = Nothing
        Dim tmpDemandLocation As String = Nothing
        Dim tmpOrder(1) As Order_Type

        tmpOrder(1).Demand_Location = "" : tmpOrder(1).Order_Number = ""

        'need to check for existence of shipment element
        If ApptNotification.Shipment Is Nothing Then
            ErrObj.log(1, 0, "ProcessApptNotification", "No shipment element in Appointment file id " & fileId)
            Dispose_ODBC_Connection(cnnE1)
            Return ProcessReturnType.Skip
        End If

        Dim currentShipment As FGIIntegApptNotificationShipment

        'assign first shipment element
        currentShipment = ApptNotification.Shipment

        'get the shipment number for this shipment
        apptShipNum = currentShipment.ShipmentNumber
        Try
            'If Shipment is NOT Freight Controlled then it's a pickup so we need to make sure the carrier name has the words 'CPU' at the beginning
            If Not CBool(ApptNotification.Shipment.FreightControlledBySystem) Then
                If Trim(ApptNotification.Shipment.CurrentCarrierOrganizationName) <> "" Then
                    If InStr("CPU", ApptNotification.Shipment.CurrentCarrierOrganizationName) <= 0 Then ApptNotification.Shipment.CurrentCarrierOrganizationName = "CPU-" + ApptNotification.Shipment.CurrentCarrierOrganizationName
                End If
                If Trim(ApptNotification.Shipment.CurrentCarrierEnterpriseName) <> "" Then
                    If InStr("CPU", ApptNotification.Shipment.CurrentCarrierEnterpriseName) <= 0 Then ApptNotification.Shipment.CurrentCarrierEnterpriseName = "CPU-" + ApptNotification.Shipment.CurrentCarrierEnterpriseName
                End If
            End If
        Catch ex As Exception
            'Stop
        End Try

        'need to do some checking of shipmentnumber
        If Not ParseShipmentNumber(apptShipNum, tmpShipmentNum, tmpDemandLocation) Then
            ErrObj.log(1, 0, "ProcessApptNotification", "Could not parse shipmentno", , currentShipment.ShipmentNumber)
            Dispose_ODBC_Connection(cnnE1)
            Return ProcessReturnType.Fail
        End If

        refno = Me.FormatReturnShipmentNumber(tmpShipmentNum, tmpDemandLocation)

        'do the merging of pickups based on appointment file for non-controlled
        'Dim SubReturnValue As ProcessReturnType

        'If Not MergeOrCancelShipment_E1(ApptNotification, Orders, SubReturnValue) Then
        '    ErrObj.log(1, 0, "ProcessApptNotification", "Could not run MergeOrCancelShipment", , currentShipment.ShipmentNumber)

        '    'check for the sub return value for some extra processing
        '    If SubReturnValue = ProcessReturnType.Skip Then
        '        'skip because the function returned true (meaning not fatal), but we don't really want to process further
        '        Dispose_ODBC_Connection(cnnE1)
        '        Return ProcessReturnType.Skip
        '    Else
        '        'just default to fail, may not have been assigned
        '        Dispose_ODBC_Connection(cnnE1)
        '        Return ProcessReturnType.Fail
        '    End If
        'End If

        'begin transaction portion
        txn = cnnE1.BeginTransaction

        Try

            'fill up our data information
            Dim dsSales_Detail As New DataSet
            Dim dsSales_Header As New DataSet
            Dim drSales_Header As DataRow
            Dim daSales_Detail As OdbcDataAdapter = Nothing
            Dim daSales_Header As OdbcDataAdapter = Nothing
            Dim tmpTrafficOrder, trafficShipNum As String

            Dim mapSubSet As String
            mapSubSet = IIf(currentShipment.FreightControlledBySystem, "Delivered", "Pickup")

            'attempt to do the mapping
            'If oMapping.MapObjectToDataRow(fileId, ApptNotification, drSales_Detail, "ApptNotif", ApptNotification.SiteRsrcReservation.State) Then
            '       writeChanges = True
            'End If


            'assign order no to tmp var
            trafficShipNum = currentShipment.ShipmentNumber
            tmpTrafficOrder = Nothing

            'parse order no
            If ParseShipmentNumber(trafficShipNum, tmpTrafficOrder, Nothing) Then

                'Check Sales Order Header for matching orders
                'ssql = "SELECT * FROM " & E1Database & ".F4201 WHERE LTRIM(SHKCOO) = '" & tmpDemandLocation & "' AND SHDOCO = " & tmpTrafficOrder
                ssql = "SELECT * FROM " & E1Database & ".F4201 WHERE SHDOCO = " & tmpTrafficOrder
                If Not DbConn.GetUpdateableDataAdapter(ssql, cnnE1, daSales_Header, txn) Then
                    txn.Rollback()
                    Dispose_ODBC_Connection(cnnE1)
                    Return ProcessReturnType.Fail
                End If

                'If there are matching records are in Sales Order Header then also check Sales Order Detail for matching orders
                If daSales_Header.Fill(dsSales_Header) > 0 Then
                    'If Sales Order Header has matching records then grab the Sales Order Detail records as well
                    'ssql = "SELECT * FROM " & E1Database & ".F4211 WHERE LTRIM(SDKCOO) = '" & tmpDemandLocation & "' AND SDDOCO = " & tmpTrafficOrder
                    ssql = "SELECT * FROM " & E1Database & ".F4211 WHERE SDDOCO = " & tmpTrafficOrder
                    If Not DbConn.GetUpdateableDataAdapter(ssql, cnnE1, daSales_Detail, txn) Then
                        txn.Rollback()
                        Dispose_ODBC_DataAdapter(daSales_Header)
                        Dispose_ODBC_Connection(cnnE1)
                        Return ProcessReturnType.Fail
                    End If
                Else
                    'If no matches in sales header then check sales header history
                    'ssql = "SELECT * FROM " & E1Database & ".F42019 WHERE LTRIM(SHKCOO) = '" & tmpDemandLocation & "' AND SHDOCO = " & tmpTrafficOrder
                    ssql = "SELECT * FROM " & E1Database & ".F42019 WHERE SHDOCO = " & tmpTrafficOrder
                    If Not DbConn.GetUpdateableDataAdapter(ssql, cnnE1, daSales_Header, txn) Then
                        txn.Rollback()
                        Dispose_ODBC_Connection(cnnE1)
                        Return ProcessReturnType.Fail
                    Else
                        'If Sales Header history has matches then check Sales Detail history as well
                        If daSales_Header.Fill(dsSales_Header) > 0 Then
                            'ssql = "SELECT * FROM " & E1Database & ".F42119 WHERE LTRIM(SDKCOO) = '" & tmpDemandLocation & "' AND SDDOCO = " & tmpTrafficOrder
                            ssql = "SELECT * FROM " & E1Database & ".F42119 WHERE SDDOCO = " & tmpTrafficOrder
                            If Not DbConn.GetUpdateableDataAdapter(ssql, cnnE1, daSales_Detail, txn) Then
                                txn.Rollback()
                                Dispose_ODBC_DataAdapter(daSales_Header)
                                Dispose_ODBC_Connection(cnnE1)
                                Return ProcessReturnType.Fail
                            End If
                        End If
                    End If
                End If

                '******************************************POPULATE SALES HEADER DATASET******************************************************************************

                If daSales_Header IsNot Nothing Then

                    'There should only be one record - get the first record
                    drSales_Header = dsSales_Header.Tables(0).Rows(0)

                    If ApptNotification.SiteRsrcReservation.state <> Nothing Then
                        Select Case ApptNotification.SiteRsrcReservation.state.ToUpper
                            Case "CANCELLED"
                                If Not oMapping.MapObjectToDataRow(fileId, ApptNotification, drSales_Header, "ApptNotif.SalesHeader", ApptNotification.SiteRsrcReservation.state, My.Settings.InterfaceType) Then
                                    txn.Rollback()
                                    Dispose_ODBC_DataAdapter(daSales_Header)
                                    Dispose_ODBC_Connection(cnnE1)
                                    Return ProcessReturnType.Fail
                                End If
                            Case Else

                                If Not oMapping.MapObjectToDataRow(fileId, ApptNotification, drSales_Header, "ApptNotif.SalesHeader", mapSubSet, My.Settings.InterfaceType) Then
                                    txn.Rollback()
                                    Dispose_ODBC_DataAdapter(daSales_Header)
                                    Dispose_ODBC_Connection(cnnE1)
                                    Return ProcessReturnType.Fail
                                End If
                        End Select
                    Else
                        If Not oMapping.MapObjectToDataRow(fileId, ApptNotification, drSales_Header, "ApptNotif.SalesHeader", mapSubSet, My.Settings.InterfaceType) Then
                            txn.Rollback()
                            Dispose_ODBC_DataAdapter(daSales_Header)
                            Dispose_ODBC_Connection(cnnE1)
                            Return ProcessReturnType.Fail
                        End If
                    End If

                    daSales_Header.Update(dsSales_Header)
                    dsSales_Header.AcceptChanges()

                Else
                    ErrObj.log(1, 0, "ProcessApptNotification", "Could not find matches in Sales Order Header or history: " & trafficShipNum, , trafficShipNum)
                    Dispose_ODBC_Connection(cnnE1)
                    Return ProcessReturnType.Skip
                End If

                    '******************************************POPULATE SALES DETAIL DATASET******************************************************************************
                    If daSales_Detail.Fill(dsSales_Detail) > 0 Then
                        For Each drSales_Detail As DataRow In dsSales_Detail.Tables(0).Rows
                        If ApptNotification.SiteRsrcReservation.state <> Nothing Then
                            Select Case ApptNotification.SiteRsrcReservation.state.ToUpper
                                Case "CANCELLED"
                                    If Not oMapping.MapObjectToDataRow(fileId, ApptNotification, drSales_Detail, "ApptNotif.SalesDetail", ApptNotification.SiteRsrcReservation.state, My.Settings.InterfaceType) Then
                                        txn.Rollback()
                                        Dispose_ODBC_DataAdapter(daSales_Header)
                                        Dispose_ODBC_DataAdapter(daSales_Detail)
                                        Dispose_ODBC_Connection(cnnE1)
                                        Return ProcessReturnType.Fail
                                    End If
                                Case Else
                                    If Not oMapping.MapObjectToDataRow(fileId, ApptNotification, drSales_Detail, "ApptNotif.SalesDetail", mapSubSet, My.Settings.InterfaceType) Then
                                        txn.Rollback()
                                        Dispose_ODBC_DataAdapter(daSales_Header)
                                        Dispose_ODBC_DataAdapter(daSales_Detail)
                                        Dispose_ODBC_Connection(cnnE1)
                                        Return ProcessReturnType.Fail
                                    End If
                            End Select
                        Else
                            If Not oMapping.MapObjectToDataRow(fileId, ApptNotification, drSales_Detail, "ApptNotif.SalesDetail", mapSubSet, My.Settings.InterfaceType) Then
                                txn.Rollback()
                                Dispose_ODBC_DataAdapter(daSales_Header)
                                Dispose_ODBC_DataAdapter(daSales_Detail)
                                Dispose_ODBC_Connection(cnnE1)
                                Return ProcessReturnType.Fail
                            End If
                        End If

                        'save changes

                        daSales_Detail.Update(dsSales_Detail)
                        dsSales_Detail.AcceptChanges()

                        'Assign to tmpOrder so the order can be added to orders array
                        tmpOrder(1).Demand_Location = tmpDemandLocation
                        tmpOrder(1).Order_Number = tmpTrafficOrder
                    Next
                    Else
                        txn.Rollback()
                        ErrObj.log(1, 0, "ProcessApptNotification", "Could not find matches in Sales Order Detail or history: " & trafficShipNum, , trafficShipNum)
                        Dispose_ODBC_Connection(cnnE1)
                        Return ProcessReturnType.Skip
                    End If
                End If

            'do the ReasonCodes
            Dim oUdfs As New Udfs
            If currentShipment.ShipmentHeaderMDFs IsNot Nothing Then
                Dim oRc As Udfs.ReasonCode
                ' Dim reasoncode_index As Integer
                'Dim x_index As Integer

                'For x_index = 0 To currentShipment.Items.GetUpperBound(0)
                '    If currentShipment.Items(x_index).GetType.Name = "ShipmentHeaderMDFs2" Then
                '        reasoncode_index = x_index
                '        Exit For
                '    End If
                'Next x_index

                'oRc = oUdfs.ParseReasonCode_E1(currentShipment.FreightControlledBySystem, currentShipment.Items(reasoncode_index))
                oRc = oUdfs.ParseReasonCode_E1(currentShipment.FreightControlledBySystem, currentShipment.ShipmentHeaderMDFs)
                If oRc IsNot Nothing Then
                    oUdfs.InsertReasonCode(tmpDemandLocation, tmpTrafficOrder, oRc.mfgloc, oRc.code, oRc.shipvia, currentShipment.lastmodifieduser)
                End If
            End If

            'Clean Up
            Dispose_ODBC_DataAdapter(daSales_Header)
            Dispose_ODBC_DataAdapter(daSales_Detail)

        Catch ex As Exception
            txn.Rollback()
            ErrObj.log(1, 0, "ProcessApptNotification", "Error processing", ex)
            Dispose_ODBC_Connection(cnnE1)
            Return ProcessReturnType.Retry
        End Try

        'commit


        txn.Commit()


        'Add to orders array
        If tmpOrder(1).Order_Number <> "" Then Add_to_Orders_Array(Orders, tmpOrder)

        'cleanup
        txn.Dispose()

        cnnE1.Close()
        cnnE1.Dispose()
        ApptNotification = Nothing

        Return ProcessReturnType.Success

    End Function



    '
    'Name: ProcessTrackNotification
    'Description: process shipment tracking notification
    'Return: Boolean
    '
    'Parameters:
    '
    'XmlSource - xml text to process
    '
    'Function ProcessTrackNotification(ByVal FgTrack As FGIIntegTrackNotification, ByVal fileId As Integer, Optional ByRef refno As String = Nothing) As ProcessReturnType

    '    Dim sSql As String

    '    'db stuff
    '    Dim trackerConn As OdbcConnection
    '    Dim txn As OdbcTransaction = Nothing

    '    'we need to update the corresponding fps fields
    '    Dim daFps As OdbcDataAdapter = Nothing
    '    Dim dsFps As New DataSet
    '    Dim drFps As DataRow


    '    'create connection to tracker database
    '    trackerConn = DbConn.CreateConnection(My.Settings.SettingValue("TrackerConn"))
    '    trackerConn.Open()

    '    'create movement object we will be using to deserialize
    '    Dim oMovement As Movement
    '    oMovement = FgTrack.Movement

    '    Try

    '        Dim mapOk As Boolean

    '        'now we have to map the Shipment Elements
    '        Dim oShipment As Shipment = Nothing
    '        Dim trafficDemand As String = ""
    '        Dim trafficOrderNo As String = ""
    '        Dim trafficShipment As String = ""

    '        txn = trackerConn.BeginTransaction

    '        'loop through all shipments (TrafficInformation/TrafficHistory)
    '        For Each oShipment In FgTrack.ShipmentListMessage.Shipment

    '            If ParseShipmentNumber(oShipment.ShipmentNumber, trafficOrderNo, trafficDemand) Then
    '                Dim daTraffic As OdbcDataAdapter = Nothing
    '                Dim dsTraffic As New DataSet
    '                Dim drTraffic As DataRow

    '                mapOk = False

    '                'find me the trafficinformationtable record
    '                sSql = "SELECT * FROM TrafficInformationTable WHERE DemandLocation = '" & trafficDemand & "' AND Order_no =" & trafficOrderNo
    '                DbConn.GetUpdateableDataAdapter(sSql, trackerConn, daTraffic, txn)
    '                If daTraffic.Fill(dsTraffic) > 0 Then

    '                    'we should only really get back one record
    '                    drTraffic = dsTraffic.Tables(0).Rows(0)

    '                    trafficShipment = drTraffic("ShipmentNumber")
    '                    mapOk = oMapping.MapObjectToDataRow(fileId, oShipment, drTraffic, "TrackNotification.Traffic")

    '                Else

    '                    'could not find a trafficinfo record, now check to see if it's in traffichistorytable
    '                    sSql = "SELECT * FROM TrafficHistoryTable WHERE DemandLocation = '" & trafficDemand & "' AND Order_no = " & trafficOrderNo

    '                    DbConn.GetUpdateableDataAdapter(sSql, trackerConn, daTraffic, txn)
    '                    If daTraffic.Fill(dsTraffic) > 0 Then

    '                        'found it, now proceed to map
    '                        drTraffic = dsTraffic.Tables(0).Rows(0)

    '                        trafficShipment = drTraffic("ShipmentNumber")
    '                        mapOk = oMapping.MapObjectToDataRow(fileId, oShipment, drTraffic, "TrackNotification.Traffic")

    '                    Else
    '                        'could not find in history either, this is an error
    '                        ErrObj.log(1, 0, "ProcessTrackNotification", "could not find in history " & trafficShipment, , trafficShipment)
    '                        Return ProcessReturnType.Skip
    '                    End If

    '                End If

    '                'now map the Movement object
    '                If Not oMapping.MapObjectToDataRow(fileId, FgTrack.MovementEvt, drTraffic, "TrackNotification.MovementEvt", FgTrack.MovementEvt.EvtType) Then
    '                    ErrObj.log(1, 0, "ProcessTrackNotification", "Could not map MovementEvt to TrafficInfo", , trafficShipment)
    '                    txn.Rollback()
    '                    Return ProcessReturnType.Fail
    '                End If

    '                'save update
    '                daTraffic.Update(dsTraffic)
    '                dsTraffic.AcceptChanges()

    '                'cleanup
    '                daTraffic.Dispose()
    '                dsTraffic.Dispose()

    '                'do the ReasonCodes
    '                Dim oUdfs As New Udfs
    '                If oShipment.Items IsNot Nothing Then
    '                    Dim oRc As Udfs.ReasonCode
    '                    oRc = oUdfs.ParseReasonCode(oShipment.FreightControlledBySystem, oShipment.Items(0))
    '                    If oRc IsNot Nothing Then
    '                        oUdfs.InsertReasonCode(trafficDemand, trafficOrderNo, oRc.mfgloc, oRc.code, oRc.shipvia, oShipment.LastModifiedUser)
    '                    End If
    '                End If
    '            Else
    '                ErrObj.log(1, 0, "ProcessTrackNotification", "Error parsing shipment number " & oShipment.ShipmentNumber)
    '                'could not parse shipment number, error out
    '                txn.Rollback()

    '                Return ProcessReturnType.Skip
    '            End If

    '        Next

    '        'now we have to map the shipment information (FPSShipmentMaster)
    '        refno = Me.FormatReturnShipmentNumber(trafficShipment, trafficDemand)

    '        'get me fps record
    '        sSql = "SELECT (SELECT MIN(REQ_SHIP_DATE) FROM TrafficInformationTable WHERE DemandLocation = Fpsshipmentmaster.demandlocation AND Shipmentnumber = Fpsshipmentmaster.Shipmentnumber) REQ_SHIP_DATE, " _
    '                & " * FROM FpsShipmentMaster WHERE DemandLocation = '" & trafficDemand & "' AND ShipmentNumber = " & trafficShipment
    '        DbConn.GetUpdateableDataAdapter(sSql, trackerConn, daFps, txn)
    '        If daFps.Fill(dsFps) > 0 Then
    '            drFps = dsFps.Tables(0).Rows(0)

    '            'map the shipment object to the fps
    '            If Not oMapping.MapObjectToDataRow(fileId, oShipment, drFps, "TrackNotification.Fps") Then
    '                ErrObj.log(1, 0, "ProcessTrackNotification", "Error mapping TrackNotification.Fps", , trafficShipment)
    '                txn.Rollback()
    '                Return ProcessReturnType.Fail
    '            End If

    '            'we need to map tender information too
    '            If Not oMapping.MapObjectToDataRow(fileId, FgTrack.Tender, drFps, "TrackNotification.Tender") Then
    '                ErrObj.log(1, 0, "ProcessTrackNotification", "Error mapping TrackNotification.Tender", , trafficShipment)
    '                txn.Rollback()
    '                Return ProcessReturnType.Fail

    '            End If

    '            'save updates
    '            daFps.Update(dsFps)
    '            dsFps.AcceptChanges()

    '            'cleanup
    '            daFps.Dispose()
    '            dsFps.Dispose()

    '        End If

    '        'update the fpsshipmentmaster for this traffic record
    '        sSql = "UPDATE FpsShipmentMaster SET OneMovementNo = '" & oMovement.MovementNumber & "'" _
    '                & ", MaintenanceDateStamp = GETDATE(), MaintenanceTimeStamp = GETDATE()" _
    '                & " WHERE DemandLocation = '" & trafficDemand & "' AND ShipmentNumber = " & trafficShipment
    '        If Not DbConn.ExecuteQuery(sSql, trackerConn, , txn) Then
    '            ErrObj.log(1, 0, "ProcessTrackNotificaton", "Error udpating fps Maint fields", , trafficShipment)
    '            txn.Rollback()
    '            Return ProcessReturnType.Fail
    '        End If

    '        'commit changes
    '        txn.Commit()

    '    Catch ex As Exception
    '        txn.Rollback()
    '        ErrObj.log(1, 0, "ProcessTrackNotification", "General Error", ex, fileId)
    '        Return ProcessReturnType.Retry
    '    End Try

    '    'close connection
    '    trackerConn.Close()

    '    'cleanup
    '    FgTrack = Nothing

    '    Return ProcessReturnType.Success

    'End Function

    'sbains NEW E1 ProcessTrackNotification function to replace previous Tracker version of ProcessTrackNotification
    Function ProcessTrackNotification_E1(ByVal FgTrack As FGIIntegTrackNotification, ByVal fileId As Integer, ByRef Orders() As Agent.Order_Type, Optional ByRef refno As String = Nothing) As ProcessReturnType

        Dim sSql As String

        'db stuff
        Dim txn As OdbcTransaction = Nothing
        Dim cnnE1 As OdbcConnection
        Dim E1Database As String = My.Settings.SettingValue("E1Database")

        'create connection to E1 database
        cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
        cnnE1.Open()

        'create movement object we will be using to deserialize
        ' Dim oMovement As Movement 'OneNetwork
        Dim oMovement As FGIIntegTrackNotificationMovement 'ALC

        oMovement = FgTrack.Movement

        Try

            'now we have to map the Shipment Elements
            Dim oShipment As FGIIntegTrackNotificationShipmentListMessageShipment = Nothing
            Dim trafficDemand As String = ""
            Dim trafficOrderNo As String = ""
            Dim trafficShipment As String = ""
            Dim tmpOrders() As Order_Type
            Dim OrderCounter As Integer = 0
            Dim trafficMfgLoc As String = ""
            Dim tempMCU As String = ""

            tmpOrders = Nothing
            txn = cnnE1.BeginTransaction

            'loop through all shipments (TrafficInformation/TrafficHistory)
            For Each oShipment In FgTrack.ShipmentListMessage.Shipment

                If ParseShipmentNumber(oShipment.ShipmentNumber, trafficOrderNo, trafficDemand) Then
                    Dim daSales_Header As OdbcDataAdapter = Nothing
                    Dim daSales_Detail As OdbcDataAdapter = Nothing
                    Dim dsSales_Header As New DataSet
                    Dim dsSales_Detail As New DataSet
                    Dim drSales_Header As DataRow


                    '*************Find me the Sales Order Header Record************************
                    sSql = "SELECT * FROM " & E1Database & ".F4201 WHERE SHDOCO =" & trafficOrderNo
                    DbConn.GetUpdateableDataAdapter(sSql, cnnE1, daSales_Header, txn)
                    If daSales_Header.Fill(dsSales_Header) > 0 Then

                        'we should only really get back one Sales Header record
                        drSales_Header = dsSales_Header.Tables(0).Rows(0)

                        'Movement #
                        trafficShipment = Trim(drSales_Header("SHURRF") & "")
                        'trafficMfgLoc = Trim(drSales_Header("SHMCU") & "")

                        'Update data set from Shipment Elements
                        If Not oMapping.MapObjectToDataRow(fileId, oShipment, drSales_Header, "TrackNotification.SalesHeader.Traffic", , My.Settings.InterfaceType) Then
                            txn.Rollback()
                            ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Header with XML Shipment elements for Order: " & trafficOrderNo)
                            Dispose_ODBC_DataAdapter(daSales_Header)
                            Dispose_ODBC_Connection(cnnE1)
                            Return ProcessReturnType.Fail
                        End If

                        'Update data set from Various Elements
                        If Not oMapping.MapObjectToDataRow(fileId, FgTrack, drSales_Header, "TrackNotification.SalesHeader", , My.Settings.InterfaceType) Then
                            txn.Rollback()
                            ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Header with XML elements for Order: " & trafficOrderNo)
                            Dispose_ODBC_DataAdapter(daSales_Header)
                            Dispose_ODBC_Connection(cnnE1)
                            Return ProcessReturnType.Fail
                        End If


                        ''Update data set from Movement Elements
                        'If Not oMapping.MapObjectToDataRow(fileId, FgTrack.MovementEvt, drSales_Header, "TrackNotification.SalesHeader.MovementEvt", FgTrack.MovementEvt.EvtType) Then
                        '    txn.Rollback()
                        '    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Header with XML Movement elements for Order: " & trafficOrderNo)
                        '    Return ProcessReturnType.Fail
                        'End If

                        ''Update data set from Tender Elements
                        'If Not oMapping.MapObjectToDataRow(fileId, FgTrack.Tender, drSales_Header, "TrackNotification.SalesHeader.Tender") Then
                        '    txn.Rollback()
                        '    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Header with XML Tender elements for Order: " & trafficOrderNo)
                        '    Return ProcessReturnType.Fail
                        'End If

                        daSales_Header.Update(dsSales_Header)
                        dsSales_Header.AcceptChanges()

                        'Clean Up New
                        'daSales_Header.Dispose()
                        'dsSales_Header.Dispose()

                        '***********************************SALES ORDER DETAIL**************************************************
                        'Find the Sales Order Detail Record
                        sSql = "SELECT * FROM " & E1Database & ".F4211 WHERE SDDOCO =" & trafficOrderNo
                        DbConn.GetUpdateableDataAdapter(sSql, cnnE1, daSales_Detail, txn)

                        If daSales_Detail.Fill(dsSales_Detail) > 0 Then

                            'Store the order numbers in an array so we can add these to the main orders array after the commit command
                            OrderCounter = OrderCounter + 1
                            ReDim Preserve tmpOrders(OrderCounter)
                            tmpOrders(OrderCounter).Order_Number = trafficOrderNo
                            tmpOrders(OrderCounter).Demand_Location = trafficDemand

                            'assign to datarow we will be mapping
                            For Each drSales_Detail As DataRow In dsSales_Detail.Tables(0).Rows

                                tempMCU = Trim(drSales_Detail("SDMCU") & "")
                                If tempMCU <> "100" And tempMCU <> "200" Then trafficMfgLoc = tempMCU




                                'Update data set based on OneNetMap settings
                                If Not oMapping.MapObjectToDataRow(fileId, oShipment, drSales_Detail, "TrackNotification.SalesDetail.Traffic", , My.Settings.InterfaceType) Then
                                    txn.Rollback()
                                    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Detail with XML Shipment elements for Order: " & trafficOrderNo)
                                    Dispose_ODBC_DataAdapter(daSales_Detail)
                                    Dispose_ODBC_DataAdapter(daSales_Header)
                                    Dispose_ODBC_Connection(cnnE1)
                                    Return ProcessReturnType.Fail
                                End If

                                'Update data set from Various Elements
                                If Not oMapping.MapObjectToDataRow(fileId, FgTrack, drSales_Detail, "TrackNotification.SalesDetail", , My.Settings.InterfaceType) Then
                                    txn.Rollback()
                                    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Detail with XML elements for Order: " & trafficOrderNo)
                                    Dispose_ODBC_DataAdapter(daSales_Detail)
                                    Dispose_ODBC_DataAdapter(daSales_Header)
                                    Dispose_ODBC_Connection(cnnE1)
                                    Return ProcessReturnType.Fail
                                End If

                                ''Update data set from Movement Elements
                                'If Not oMapping.MapObjectToDataRow(fileId, FgTrack.MovementEvt, drSales_Detail, "TrackNotification.SalesDetail.MovementEvt", FgTrack.MovementEvt.EvtType) Then
                                '    txn.Rollback()
                                '    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Detail with XML Movement elements for Order: " & trafficOrderNo)
                                '    Return ProcessReturnType.Fail
                                'End If

                                ''Update data set from Tender Elements
                                'If Not oMapping.MapObjectToDataRow(fileId, FgTrack.Tender, drSales_Detail, "TrackNotification.SalesDetail.Tender") Then
                                '    txn.Rollback()
                                '    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Detail with XML Tender elements for Order: " & trafficOrderNo)
                                '    Return ProcessReturnType.Fail
                                'End If

                                daSales_Detail.Update(dsSales_Detail)
                                dsSales_Detail.AcceptChanges()

                                'Clean Up New
                                'daSales_Detail.Dispose()
                                'dsSales_Detail.Dispose()
                            Next
                        Else
                            'could not find in Sales Detail but found in Header, this is an error
                            ErrObj.log(1, 0, "ProcessTrackNotification", "could not find in Sales Detail but found in Sales Header " & trafficShipment, , trafficShipment)
                            Dispose_ODBC_DataAdapter(daSales_Header)
                            Dispose_ODBC_Connection(cnnE1)
                            Return ProcessReturnType.Skip
                        End If


                        'ORDER WAS NOT FOUND IN SALES HEADER SO WE MUST LOOK IN SALES HEADER HISTORICAL TABLE
                    Else

                        '***********Find me the HISTORICAL Sales Order Header Record
                        sSql = "SELECT * FROM " & E1Database & ".F42019 WHERE SHDOCO =" & trafficOrderNo
                        DbConn.GetUpdateableDataAdapter(sSql, cnnE1, daSales_Header, txn)
                        If daSales_Header.Fill(dsSales_Header) > 0 Then

                            'we should only really get back one Sales Header record
                            drSales_Header = dsSales_Header.Tables(0).Rows(0)

                            'Movement #
                            trafficShipment = drSales_Header("SHURRF")
                            'trafficMfgLoc = Trim(drSales_Header("SHMCU") & "")

                            'Update data set from Shipment Elements
                            If Not oMapping.MapObjectToDataRow(fileId, oShipment, drSales_Header, "TrackNotification.SalesHeader.Traffic", , My.Settings.InterfaceType) Then
                                txn.Rollback()
                                ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Header History with XML Shipment elements for Order: " & trafficOrderNo)
                                Dispose_ODBC_DataAdapter(daSales_Header)
                                Dispose_ODBC_Connection(cnnE1)
                                Return ProcessReturnType.Fail
                            End If

                            'Update data set from Various Elements
                            If Not oMapping.MapObjectToDataRow(fileId, FgTrack, drSales_Header, "TrackNotification.SalesHeader", , My.Settings.InterfaceType) Then
                                txn.Rollback()
                                ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Header History with XML elements for Order: " & trafficOrderNo)
                                Dispose_ODBC_DataAdapter(daSales_Header)
                                Dispose_ODBC_Connection(cnnE1)
                                Return ProcessReturnType.Fail
                            End If

                            ''Update data set from Movement Elements
                            'If Not oMapping.MapObjectToDataRow(fileId, FgTrack.MovementEvt, drSales_Header, "TrackNotification.SalesHeader.MovementEvt", FgTrack.MovementEvt.EvtType) Then
                            '    txn.Rollback()
                            '    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Header History with XML Movement elements for Order: " & trafficOrderNo)
                            '    Return ProcessReturnType.Fail
                            'End If

                            ''Update data set from Tender Elements
                            'If Not oMapping.MapObjectToDataRow(fileId, FgTrack.Tender, drSales_Header, "TrackNotification.SalesHeader.Tender") Then
                            '    txn.Rollback()
                            '    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Header History with XML Tender elements for Order: " & trafficOrderNo)
                            '    Return ProcessReturnType.Fail
                            'End If

                            daSales_Header.Update(dsSales_Header)
                            dsSales_Header.AcceptChanges()

                            'Clean Up New
                            'daSales_Header.Dispose()
                            'dsSales_Header.Dispose()

                            '**************Find the Sales Order Detail Record
                            sSql = "SELECT * FROM " & E1Database & ".F42119 WHERE SDDOCO =" & trafficOrderNo
                            DbConn.GetUpdateableDataAdapter(sSql, cnnE1, daSales_Detail, txn)

                            If daSales_Detail.Fill(dsSales_Detail) > 0 Then

                                'assign to datarow we will be mapping
                                For Each drSales_Detail As DataRow In dsSales_Detail.Tables(0).Rows

                                    tempMCU = Trim(drSales_Detail("SDMCU") & "")
                                    If tempMCU <> "100" And tempMCU <> "200" Then trafficMfgLoc = tempMCU

                                    'Update data set based on OneNetMap settings
                                    If Not oMapping.MapObjectToDataRow(fileId, oShipment, drSales_Detail, "TrackNotification.SalesDetail.Traffic", , My.Settings.InterfaceType) Then
                                        txn.Rollback()
                                        ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Detail History with XML Shipment elements for Order: " & trafficOrderNo)
                                        Dispose_ODBC_DataAdapter(daSales_Detail)
                                        Dispose_ODBC_DataAdapter(daSales_Header)
                                        Dispose_ODBC_Connection(cnnE1)
                                        Return ProcessReturnType.Fail
                                    End If

                                    'Update data set from Various Elements
                                    If Not oMapping.MapObjectToDataRow(fileId, FgTrack, drSales_Detail, "TrackNotification.SalesDetail", , My.Settings.InterfaceType) Then
                                        txn.Rollback()
                                        ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Detail History with XML elements for Order: " & trafficOrderNo)
                                        Dispose_ODBC_DataAdapter(daSales_Detail)
                                        Dispose_ODBC_DataAdapter(daSales_Header)
                                        Dispose_ODBC_Connection(cnnE1)
                                        Return ProcessReturnType.Fail
                                    End If

                                    ''Update data set from Movement Elements
                                    'If Not oMapping.MapObjectToDataRow(fileId, FgTrack.MovementEvt, drSales_Detail, "TrackNotification.SalesDetail.MovementEvt", FgTrack.MovementEvt.EvtType) Then
                                    '    txn.Rollback()
                                    '    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Detail History with XML Movement elements for Order: " & trafficOrderNo)
                                    '    Return ProcessReturnType.Fail
                                    'End If

                                    ''Update data set from Tender Elements
                                    'If Not oMapping.MapObjectToDataRow(fileId, FgTrack.Tender, drSales_Detail, "TrackNotification.SalesDetail.Tender") Then
                                    '    txn.Rollback()
                                    '    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Detail History with XML Tender elements for Order: " & trafficOrderNo)
                                    '    Return ProcessReturnType.Fail
                                    'End If

                                    daSales_Detail.Update(dsSales_Detail)
                                    dsSales_Detail.AcceptChanges()

                                    'Clean Up New 
                                    'daSales_Detail.Dispose()
                                    'dsSales_Detail.Dispose()
                                Next
                            Else
                                'could not find in Sales Detail but found in Header, this is an error
                                ErrObj.log(1, 0, "ProcessTrackNotification", "could not find in Sales Detail History but found in Sales Header History " & trafficOrderNo, , trafficShipment)
                                Dispose_ODBC_DataAdapter(daSales_Header)
                                Dispose_ODBC_Connection(cnnE1)
                                Return ProcessReturnType.Skip
                            End If

                        Else
                            'could not find in history either, this is an error
                            ErrObj.log(1, 0, "ProcessTrackNotification", "could not find in Sales Header history " & trafficOrderNo, , trafficShipment)

                            Dispose_ODBC_DataAdapter(daSales_Header)
                            Dispose_ODBC_Connection(cnnE1)
                            Return ProcessReturnType.Skip
                        End If

                    End If


                    'cleanup
                    Dispose_ODBC_DataAdapter(daSales_Header)
                    Dispose_ODBC_DataAdapter(daSales_Detail)
                    dsSales_Header.Dispose()
                    dsSales_Detail.Dispose()

                    'do the ReasonCodes
                    Dim oUdfs As New Udfs
                    Dim oRc As Udfs.ReasonCode

                    'OLD WAY
                    ''insert any reason code that can come in from the MovementEvt
                    'If oShipment.Items IsNot Nothing Then
                    '    oRc = oUdfs.ParseReasonCode_E1(oShipment.FreightControlledBySystem, oShipment.Items(0), FgTrack.MovementEvt)
                    '    If oRc IsNot Nothing Then
                    '        oUdfs.InsertReasonCode(trafficDemand, trafficOrderNo, oRc.mfgloc, oRc.code, oRc.shipvia, oShipment.LastModifiedUser)
                    '    End If
                    'End If

                    'insert reason code that may come in from the movement event
                    oRc = oUdfs.ParseReasonCode_E1(oShipment.FreightControlledBySystem, FgTrack.MovementEvt)
                    If oRc IsNot Nothing Then
                        oUdfs.InsertReasonCode(trafficDemand, trafficOrderNo, trafficMfgLoc, oRc.code, oRc.shipvia, oShipment.LastModifiedUser)
                    End If

                Else
                    ErrObj.log(1, 0, "ProcessTrackNotification", "Error parsing shipment number " & oShipment.ShipmentNumber)
                    'could not parse shipment number, error out
                    txn.Rollback()

                    Dispose_ODBC_Connection(cnnE1)
                    Return ProcessReturnType.Skip
                End If

            Next

            'now we have to map the shipment information (FPSShipmentMaster)
            'refno = Me.FormatReturnShipmentNumber(trafficShipment, trafficDemand)

            'commit changes

            txn.Commit()
            txn.Dispose()

            If tmpOrders IsNot Nothing Then
                If UBound(tmpOrders) > 0 Then Add_to_Orders_Array(Orders, tmpOrders)
            End If

        Catch ex As Exception
            txn.Rollback()
            ErrObj.log(1, 0, "ProcessTrackNotification", "General Error", ex, fileId)
            Dispose_ODBC_Connection(cnnE1)
            Return ProcessReturnType.Retry
        End Try

        'Clean Up
        Dispose_ODBC_Connection(cnnE1)
        FgTrack = Nothing

        Return ProcessReturnType.Success

    End Function

    Public Sub ProcessTrackNotif_ExpectedFreight(ByVal FgTrack As FGIIntegTrackNotification, ByVal fileId As Integer)
        ' Dim oMovement As Movement = FgTrack.Movement 'OneNetwork
        Dim oMovement As FGIIntegTrackNotificationMovement = FgTrack.Movement 'ALC
        Dim sSql As String = ""
        Dim daFreightCost As OdbcDataAdapter = Nothing
        Dim dsFreightCost As New DataSet
        Dim drFreightCost As DataRow

        Try
            'Proceed only if there is an amount in the TotalCostAmount element
            If Val(FgTrack.Tender.TotalCostAmount & "") > 0 Then
                sSql = "SELECT * FROM ExpectedFreight WHERE MovementNo = '" & oMovement.MovementNumber & "' "
                DbConn.GetUpdateableDataAdapter(sSql, My.Settings.SettingValue("OneNetworkConn"), daFreightCost)

                If daFreightCost IsNot Nothing Then
                    'set if we got a record back

                    'If record already exists then update values
                    If daFreightCost.Fill(dsFreightCost) > 0 Then

                        'get first row, we should only get back one anyway
                        drFreightCost = dsFreightCost.Tables(0).Rows(0)

                        'populate fields

                        drFreightCost.Item("ExpectedFreightCost") = FgTrack.Tender.TotalCostAmount

                    Else  'If record does not exist then add new row 
                        drFreightCost = dsFreightCost.Tables(0).NewRow

                        'populate fields
                        With drFreightCost
                            .Item("MovementNo") = oMovement.MovementNumber
                            .Item("ExpectedFreightCost") = FgTrack.Tender.TotalCostAmount
                        End With

                        'add to dataset
                        dsFreightCost.Tables(0).Rows.Add(drFreightCost)
                    End If

                    'save changes
                    daFreightCost.Update(dsFreightCost)
                    dsFreightCost.AcceptChanges()

                    'cleanup
                    dsFreightCost.Dispose()
                    daFreightCost.Dispose()
                Else
                    ErrObj.log(1, 0, "ProcessTrackNotif_ExpectedFreight", "Error accessing ExpectedFreight Table for Movement:" & oMovement.MovementNumber & " FileID:" & fileId.ToString)
                End If
            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "ProcessTrackNotif_ExpectedFreight", "Error writing to ExpectedFreight Table for Movement:" & oMovement.MovementNumber & " FileID:" & fileId.ToString, ex, fileId)
            dsFreightCost.Dispose()
            daFreightCost.Dispose()
        End Try

    End Sub

    '
    'Name: ProcessShipChargeNotif
    'Description: process payment information
    'Return: Boolean
    '
    'Parameters:
    '
    'xmlStr - xml text to process
    '
    'Function ProcessShipChargeNotif(ByVal ShipChargeNotif As FGIIntegPaymentNotification, ByVal fileId As Integer, Optional ByRef refno As String = Nothing) As ProcessReturnType
    '    Dim sSql As String

    '    Dim trackerConn As OdbcConnection
    '    Dim txnFps As OdbcTransaction

    '    Dim nowDate As Date

    '    nowDate = Now

    '    trackerConn = Nothing

    '    Try

    '        'open connection
    '        trackerConn = DbConn.CreateConnection(My.Settings.SettingValue("TrackerConn"))
    '        trackerConn.Open()

    '    Catch ex As Exception
    '        ErrObj.log(1, 0, "ProcessShipChargeNotif", "Could not open connection", ex)
    '        Return ProcessReturnType.Fail

    '    End Try

    '    Dim drDest As DataRow
    '    Dim ds As New DataSet
    '    Dim shipNum As String
    '    Dim tempShipmentNum As String
    '    Dim tempDemand As String

    '    shipNum = ShipChargeNotif.Movement.MovementNumber

    '    refno = shipNum

    '    'begin transaction
    '    txnFps = trackerConn.BeginTransaction()

    '    Try

    '        'find FPSShipmentMaster record
    '        Dim da As OdbcDataAdapter = Nothing
    '        sSql = "SELECT * FROM FpsShipmentMaster WHERE OneMovementNo = '" & shipNum & "'"
    '        DbConn.GetUpdateableDataAdapter(sSql, trackerConn, da, txnFps)

    '        'fill up dataset
    '        If da.Fill(ds) = 0 Then

    '            'look in history
    '            sSql = "SELECT * FROM FpsShipmentHistory WHERE OneMovementNo = '" & shipNum & "'"
    '            DbConn.GetUpdateableDataAdapter(sSql, trackerConn, da, txnFps)

    '            If da.Fill(ds) = 0 Then
    '                ErrObj.log(1, 0, "ProcessShipChargeNotif", "Could not find ShipmentNum " & shipNum, Nothing, shipNum)
    '                Return ProcessReturnType.Skip
    '            Else
    '                ErrObj.log(1, 0, "ProcessShipChargeNotif", "Already in history, cannot apply payment to " & shipNum, Nothing, shipNum)
    '                Return ProcessReturnType.Skip
    '            End If

    '        End If

    '        'get the first row retrieved, should only really get back one
    '        drDest = ds.Tables(0).Rows(0)

    '        'see if we get something back
    '        If drDest IsNot Nothing Then

    '            Dim hasShipCost As Boolean = False
    '            Dim hasTraffic As Boolean = False

    '            tempDemand = drDest("DemandLocation")
    '            tempShipmentNum = drDest("ShipmentNumber")

    '            refno = Me.FormatReturnShipmentNumber(tempShipmentNum, tempDemand)

    '            'we need to find out if we got a payment file back for this one already
    '            sSql = "SELECT * FROM ShipCostComponent WHERE DemandLocation = '" & tempDemand & "' AND ShipmentNumber = '" & tempShipmentNum & "'"
    '            Dim rdrShipCostComp As OdbcDataReader
    '            rdrShipCostComp = DbConn.GetDataReader(sSql, My.Settings.SettingValue("OneNetworkConn"), "ProcessShipChargeNotif")

    '            'see if we found a previous payment
    '            If rdrShipCostComp.HasRows Then
    '                ErrObj.log(1, 1, "ProcessShipChargeNotif", "Received payment file already for " & tempShipmentNum, , tempShipmentNum)
    '                hasShipCost = True
    '            Else
    '                hasShipCost = False
    '            End If

    '            'cleanup
    '            rdrShipCostComp.Close()

    '            'now do check in TrafficInformationTable
    '            sSql = "SELECT * FROM TrafficInformationTable WHERE DemandLocation = '" & tempDemand & "' AND ShipmentNumber = '" & tempShipmentNum & "'"
    '            Dim rdrTraffic As OdbcDataReader
    '            rdrTraffic = DbConn.GetDataReader(sSql, My.Settings.SettingValue("TrackerConn"), "ProcessShipChargeNotif")

    '            'see if we get a record back
    '            If rdrTraffic.HasRows Then
    '                ErrObj.log(1, 0, "ProcessShipChargeNotif", "Shipment still in TrafficInfo, cannot apply payment file for " & tempShipmentNum, , tempShipmentNum)
    '                hasTraffic = True
    '            Else
    '                hasTraffic = False
    '            End If

    '            'cleanup
    '            rdrTraffic.Close()

    '            If hasShipCost Or hasTraffic Then

    '                'we received a payment file already, we don't want to only map the first one we got

    '                'log error
    '                Return ProcessReturnType.Skip

    '            Else
    '                'map if this cost file has not come in yet
    '                If oMapping.MapObjectToDataRow(fileId, ShipChargeNotif, drDest, "ShipChargeNotif") Then

    '                    Dim dbDate As Date
    '                    Dim rdrCurrency As OdbcDataReader
    '                    Dim frLoc As String = Nothing

    '                    dbDate = DbConn.GetDateSqlServer(My.Settings.SettingValue("TrackerConn"))

    '                    'try to determine freight location using currency rule
    '                    sSql = "SELECT * FROM qryCurrencyCode WHERE HomeCurrencyCode = '" & drDest("ExpectedCurrency") & "'"
    '                    rdrCurrency = DbConn.GetDataReader(sSql, My.Settings.SettingValue("OneNetworkConn"), "ProcessShipChargeNotif")
    '                    If rdrCurrency.HasRows Then
    '                        frLoc = rdrCurrency("FreightLocation")
    '                    End If

    '                    rdrCurrency.Close()

    '                    drDest("MaintenanceDateStamp") = dbDate
    '                    drDest("MaintenanceTimeStamp") = dbDate
    '                    drDest("FreightLocation") = frLoc

    '                    'commit
    '                    da.Update(ds)
    '                    ds.AcceptChanges()

    '                    'cleanup
    '                    da.Dispose()
    '                Else
    '                    ErrObj.log(1, 0, "ProcessShipChargeNotif", "Could not map object ShipCharge to " & tempShipmentNum, , tempShipmentNum)
    '                End If

    '            End If

    '        Else
    '            txnFps.Rollback()
    '            ErrObj.log(1, 0, "ProcessShipChargeNotif", "Could not find fps record", , shipNum)
    '            Return ProcessReturnType.Fail
    '        End If

    '        'find out if we already processed this previously, if we did, we don't want to write to the cost component table again
    '        sSql = "SELECT * FROM ShipCostComponent WHERE FileId = " & fileId
    '        Dim rdrCostCompFileId As OdbcDataReader
    '        rdrCostCompFileId = DbConn.GetDataReader(sSql, My.Settings.SettingValue("OneNetworkConn"), "ProcessShipChargeNotif")

    '        'if we find one, we don't want to write to table, but we want to report it
    '        If rdrCostCompFileId.HasRows Then
    '            ErrObj.log(1, 0, "ProcessShipChargeNotif", "this payment file was already processed, skipping cost component insert")
    '            rdrCostCompFileId.Close()
    '        Else
    '            'cleanup
    '            rdrCostCompFileId.Close()

    '            'now we need to store the cost components in another table
    '            Dim daCostComp As OdbcDataAdapter = Nothing
    '            Dim dsCostComp As New DataSet
    '            Dim drCostComp As DataRow

    '            'open cost component table to write to
    '            sSql = "SELECT * FROM OneNetwork.dbo.ShipCostComponent WHERE 1 = 0"
    '            DbConn.GetUpdateableDataAdapter(sSql, trackerConn, daCostComp, txnFps)
    '            daCostComp.Fill(dsCostComp)

    '            'get the first CarrierCostDetail
    '            If ShipChargeNotif.CarrierCostDetail IsNot Nothing Then

    '                'make sure component is not empty
    '                If ShipChargeNotif.CarrierCostDetail(0).CostComponent IsNot Nothing Then
    '                    For Each oComponent As CostComponent In ShipChargeNotif.CarrierCostDetail(0).CostComponent

    '                        'create new row to dump data to
    '                        drCostComp = dsCostComp.Tables(0).NewRow

    '                        'assign the primary/parent keys
    '                        drCostComp("FileId") = fileId
    '                        drCostComp("DemandLocation") = tempDemand
    '                        drCostComp("ShipmentNumber") = tempShipmentNum
    '                        drCostComp("Entered") = nowDate

    '                        'map the component to table
    '                        If Not oMapping.MapObjectToDataRow(fileId, oComponent, drCostComp, "ShipCharge.CostComponent") Then
    '                            ErrObj.log(1, 0, "ProcessShipChargeNotif", "error mapping CostComponent", , tempShipmentNum)
    '                        End If

    '                        'add the row
    '                        dsCostComp.Tables(0).Rows.Add(drCostComp)

    '                    Next
    '                End If

    '                'commit changes
    '                daCostComp.Update(dsCostComp)
    '                dsCostComp.AcceptChanges()

    '            End If
    '        End If

    '        'FPSREQ section

    '        'get the BOL #
    '        Dim bolno As String
    '        sSql = "SELECT BOL_NO FROM TBLBOLINFORMATION WHERE DEMANDLOCATION = '" & tempDemand & "' AND ORDER_NO = " & tempShipmentNum
    '        bolno = DbConn.ExecuteScalar(sSql, My.Settings.SettingValue("TrackerConn"))

    '        If bolno Is Nothing Then
    '            'look for the bol_no in history
    '            sSql = "SELECT BOL_NO FROM TBLBOLINFORMATIONHISTORY WHERE DEMANDLOCATION = '" & tempDemand & "' AND ORDER_NO = " & tempShipmentNum
    '            bolno = DbConn.ExecuteScalar(sSql, My.Settings.SettingValue("TrackerConn"))

    '        End If

    '        If bolno IsNot Nothing Then
    '            sSql = "SELECT * FROM FPSREQ WHERE FPSREQBOLNO = " & bolno
    '            Dim daFpsreq As OdbcDataAdapter = Nothing
    '            Dim dsFpsreq As New DataSet
    '            Dim drFpsreq As DataRow

    '            DbConn.GetUpdateableDataAdapter(sSql, My.Settings.FpsConn, daFpsreq)

    '            If daFpsreq IsNot Nothing Then

    '                'set if we got a record back
    '                If daFpsreq.Fill(dsFpsreq) > 0 Then

    '                    'get first row, we should only get back one anyway
    '                    drFpsreq = dsFpsreq.Tables(0).Rows(0)

    '                    With drFpsreq
    '                        'assign some values

    '                        'need to round because the value is a string, and if not explicitly rounded, will be off a cent
    '                        .Item("FPSREQACTUALFREIGHT") = Math.Round(CDbl(ShipChargeNotif.Tender(0).TotalCostAmount), 2)
    '                        .Item("FPSREQFREIGHTLOCATI") = tempDemand
    '                        .Item("FPSREQCURRENCY") = ShipChargeNotif.Tender(0).TotalCostUOM
    '                        .Item("FPSREQACTUALCARRIER") = ShipChargeNotif.Contract(0).ContractName

    '                        .Item("FPSREQPROCESSFLAG") = "Y"
    '                        .Item("FPSREQPROCESSDATE") = Format(Now, "yyyyMMdd")
    '                        .Item("FPSREQPROCESSTIME") = Format(Now, "HHmmss")

    '                    End With

    '                    'save changes
    '                    daFpsreq.Update(dsFpsreq)
    '                    dsFpsreq.AcceptChanges()

    '                    'cleanup
    '                    daFpsreq.Dispose()
    '                    dsFpsreq.Dispose()

    '                End If
    '            Else
    '                ErrObj.log(1, 0, "ProcessShipChargeNotif", "Could not find FPSREQ bolno " & bolno, , bolno)
    '            End If
    '        Else
    '            ErrObj.log(1, 0, "ProcessShipChargeNotif", "Could not find bol #", , tempShipmentNum & "-" & tempDemand)

    '        End If

    '        'commit changes
    '        txnFps.Commit()

    '    Catch ex As Exception
    '        txnFps.Rollback()
    '        ErrObj.log(1, 0, "ProcessShipChargeNotif", "Error processing file", ex)
    '        Return ProcessReturnType.Retry
    '    End Try


    '    'cleanup
    '    ShipChargeNotif = Nothing

    '    Return ProcessReturnType.Success

    'End Function

    'sbains - E1 version of ProcessShipChargeNotif
    'Name: ProcessShipChargeNotif
    'Description: process payment information
    'Return: Boolean
    '
    'Parameters:
    '
    'xmlStr - xml text to process
    '
    Function ProcessShipChargeNotif_E1(ByVal ShipChargeNotif As FGIIntegPaymentNotification, ByVal fileId As Integer, ByRef Orders() As Agent.Order_Type, Optional ByRef refno As String = Nothing, Optional ByRef IgnoreDuplicateCharges As Boolean = False) As ProcessReturnType
        Dim OneNetworkConn As OdbcConnection
        Dim cnnE1 As OdbcConnection
        Dim txn As OdbcTransaction
        Dim E1Database As String = My.Settings.SettingValue("E1Database")
        Dim sSql As String
        Dim ErrMessage As String = ""
        Dim ReturnVal As ProcessReturnType = ProcessReturnType.Success

        Dim sqlDateTime As Date
        sqlDateTime = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))

        OneNetworkConn = Nothing
        cnnE1 = Nothing

        Dim MovementNo As String = ""
        Dim tempShipmentNum As String = ""
        Dim tempDemand As String = ""
        Dim tempMfgLoc As String = ""
        Dim MfgLoc As String = ""
        Dim CurrCode As String = ""
        Dim CarrierNum As Integer = 0
        Dim ProNumber As String = ""
        Dim Invoice_No As String = ""
        Dim BOL_No As String = ""
        Dim DocType As String = ""
        Dim ShipToNo As Integer
        Dim ShipFromCurr As String = ""
        Dim ShipToCurr As String = ""
        Dim Min_Status As Integer
        Dim BatchUserID As String = "TMS"
        Dim GrossAmount As Double = 0
        Dim AerosolOnlyFlag As Boolean = False

        MovementNo = ShipChargeNotif.Movement.MovementNumber
        refno = MovementNo
        '**************************************************Make Sure Movement No is NOT NULL ***********************************************************************************
        If Trim(MovementNo) = "" Then
            Write_AP_BatchError_Log(fileId, MovementNo, "Movement # is blank or missing", 1, 1)
            ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Error processing - File is missing the Movement# for FileID:" & fileId.ToString)
            ErrObj.Payableslog(1, 47, "OneShipments", "ProcessShipChargeNotif_E1", "Movement # is blank or missing", "Error processing - File is missing the Movement# for FileID:" & fileId.ToString, "", "", refno)
            Return ProcessReturnType.Fail
        End If
        '**************************************************************************************************************************************************************************

        '***************If Shipment is NOT Freight Controlled then it's a pickup so we need to make sure the carrier name has the words 'CPU' at the beginning*****************
        If Not (ShipChargeNotif.ShipmentListMessage.Shipment(0).FreightControlledBySystem) Then
            If Trim(ShipChargeNotif.Contract.ContractName) <> "" Then
                If InStr("CPU", ShipChargeNotif.Contract.ContractName) <= 0 Then ShipChargeNotif.Contract.ContractName = "CPU-" + ShipChargeNotif.Contract.ContractName
            End If
            If Trim(ShipChargeNotif.CarrierCostDetail.CostComponent(0).TenderCarrierOrgName) <> "" Then
                If InStr("CPU", ShipChargeNotif.CarrierCostDetail.CostComponent(0).TenderCarrierOrgName) <= 0 Then ShipChargeNotif.CarrierCostDetail.CostComponent(0).TenderCarrierOrgName = "CPU-" + ShipChargeNotif.CarrierCostDetail.CostComponent(0).TenderCarrierOrgName
            End If
        End If
        '**************************************************************************************************************************************************************************

        'Open OneNetwork Database Connection
        Try
            OneNetworkConn = DbConn.CreateConnection(My.Settings.SettingValue("OneNetworkConn"))
            OneNetworkConn.Open()
        Catch ex As Exception
            Write_AP_BatchError_Log(fileId, MovementNo, "Could not open OneNetwork connection.", 1, 1)
            ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Could not open OneNetwork connection. Error processing  FileID:" & fileId.ToString & " Movement#:" & MovementNo, ex)
            Return ProcessReturnType.Retry
        End Try

        ''**************************************************************WRITE ENTRY TO SHIPCOSTCOMPONENT TABLE ***************************************************
        'For now we're writing entries to the ShipCostComponent table first because some movements do not exist in E1 (old Macola orders) - if we try to do it afterwards it may skip it due to errors when writing to F0411Z1/F0911Z1
        'One the old Macola movements are all paid then we can move this to the end of this function
        ReturnVal = Write_ShipCostComponent_Entry(ShipChargeNotif, fileId, OneNetworkConn)
        If Not (ReturnVal = ProcessReturnType.Success) Then
            OneNetworkConn.Close()
            Return ReturnVal
        End If

        'If the ignore duplicatecharges flag is set for the payment file in OneNetfiles then do not do the check
        If IgnoreDuplicateCharges = False Then
            'Check if shipcost components have duplicate entries for this movement
            If HasDuplicateCharges(MovementNo, fileId, refno) Then Return ProcessReturnType.Skip
        End If
        ''**************************************************************************************************************************************************************************

        'Open connection
        Try
            cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
            cnnE1.ConnectionTimeout = 300
            cnnE1.Open()
        Catch ex As Exception
            Write_AP_BatchError_Log(fileId, MovementNo, "Error Opening E1 Database Connection.", 1, 1)
            ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Could not open connection. Error processing  FileID:" & fileId.ToString & " Movement#:" & MovementNo, ex)
            OneNetworkConn.Close() : OneNetworkConn.Dispose()
            Return ProcessReturnType.Retry
        End Try

        '******************************Find the MINIMUM STATUS of Order (plus other infor) in Sales detail for all orders in current movement(URRF) ***************************************************
        Dim rdr_SalesOrder As OdbcDataReader = Nothing
        Dim OrderFound As Boolean = True
        Dim CarrierName As String = ""

        Try
            sSql = "SELECT MIN(SDLTTR) as MIN_LTTR, SDKCOO, SDDOCO, SDDOC, SDDCTO, SDMCU, SDCRCD, SDCARS, SDCNID, SDDELN, SDSHPN, SDSHAN "
            sSql = sSql & "FROM " & E1Database & ".F4211 LEFT JOIN " & E1Database & ".F554202x ON SDDOCO = TMDOCO "
            sSql = sSql & "WHERE SDLTTR <> 980 AND SDLNTY = 'S' AND SDUORG > SDSOCN AND '" & MovementNo & "' = CASE WHEN SDURRF = '" & MovementNo & "' THEN SDURRF WHEN TMDOCO IS NULL THEN SDURRF ELSE TMURRF END "
            sSql = sSql & "GROUP BY SDKCOO, SDDOCO, SDDOC, SDDCTO, SDMCU, SDCRCD, SDCARS, SDCNID, SDDELN, SDSHPN, SDSHAN "
            rdr_SalesOrder = DbConn.GetDataReader(sSql, cnnE1)

            'If recordset is empty then look in Sales Order History
            If Not (rdr_SalesOrder Is Nothing) Then
                If rdr_SalesOrder.HasRows = False Then
                    rdr_SalesOrder.Close()

                    'look in history
                    sSql = "SELECT MIN(SDLTTR) as MIN_LTTR, SDKCOO, SDDOCO, SDDOC, SDDCTO, SDMCU, SDCRCD, SDCARS, SDCNID, SDDELN, SDSHPN, SDSHAN "
                    sSql = sSql & "FROM " & E1Database & ".F42119 LEFT JOIN " & E1Database & ".F554202x ON SDDOCO = TMDOCO "
                    sSql = sSql & "WHERE SDLTTR <> 980 AND SDLNTY = 'S' AND SDUORG > SDSOCN AND '" & MovementNo & "' = CASE WHEN SDURRF = '" & MovementNo & "' THEN SDURRF WHEN TMDOCO IS NULL THEN SDURRF ELSE TMURRF END "
                    sSql = sSql & "GROUP BY SDKCOO, SDDOCO, SDDOC, SDDCTO, SDMCU, SDCRCD, SDCARS, SDCNID, SDDELN, SDSHPN, SDSHAN "
                    rdr_SalesOrder = DbConn.GetDataReader(sSql, cnnE1)
                End If

                If rdr_SalesOrder Is Nothing Then
                    OrderFound = False
                ElseIf rdr_SalesOrder.HasRows = False Then
                    OrderFound = False
                End If


                'IF order is not found and shipment number < 900000 this means its a macola order so let's check OEHDRHST
                If OrderFound = False And ShipChargeNotif.ShipmentListMessage.Shipment IsNot Nothing Then
                    ParseShipmentNumber(ShipChargeNotif.ShipmentListMessage.Shipment(0).ShipmentNumber, tempShipmentNum, tempDemand)
                    If Val(tempShipmentNum) < 900000 Then
                        ErrObj.log(1, 0, "ProcessShipChargeNotif_E1-MacolaCode", "Could not find Sales Orders in Live or History with Movement #(URRF): " & MovementNo, Nothing, MovementNo)
                        'Min_Status = 560
                        'BOL_No = "999999999"
                        'BatchUserID = "MANONENET"
                        'ProNumber = Left(Trim(ShipChargeNotif.Movement.CarrierProNumber), 25)
                        'CarrierName = Trim(ShipChargeNotif.Movement.CurrentCarrierEnterpriseName)
                        'CarrierNum = Trim(DbConn.ExecuteScalar("SELECT Top 1 E1AN8 FROM CarrierMap where Carrier like '%" & Replace(CarrierName, "'", "") & "%' ", My.Settings.SettingValue("OneNetworkConn")))
                        'If CarrierNum = 1282 Then CarrierNum = 0 'Set Carrier to 0 if carrier is 1282 (OTHER)
                        'If InStr(ShipChargeNotif.ShipmentListMessage.Shipment(0).ShipFromAddress.Country, "US") > 0 Or ShipChargeNotif.ShipmentListMessage.Shipment(0).ShipFromAddress.Country = Country.US Then ShipFromCurr = "USD" Else ShipFromCurr = "CAD"
                        'If InStr(UCase(ShipChargeNotif.ShipmentListMessage.Shipment(0).ShipToAddress.Country), "US") > 0 Or ShipChargeNotif.ShipmentListMessage.Shipment(0).ShipToAddress.Country = Country.US Then ShipToCurr = "USD" Else ShipToCurr = "CAD"
                        'tempMfgLoc = Trim(DbConn.ExecuteScalar("SELECT top 1 dbo.convert_SiteRef(MFGING_LOCATION, 'MACOLA TO E1', 0) FROM sqldata_union.dbo.unionOEHDRHST where [NO] =" & tempShipmentNum, My.Settings.SettingValue("OneNetworkConn")))
                        'If tempMfgLoc <> "" Then OrderFound = True
                    End If
                ElseIf OrderFound = True Then
                    tempDemand = rdr_SalesOrder("SDKCOO")
                    tempMfgLoc = Trim(rdr_SalesOrder("SDMCU"))
                    CarrierNum = rdr_SalesOrder("SDCARS")
                    ProNumber = rdr_SalesOrder("SDCNID")
                    Invoice_No = CStr(rdr_SalesOrder("SDDOC"))
                    tempShipmentNum = rdr_SalesOrder("SDDOCO")

                    BOL_No = rdr_SalesOrder("SDSHPN").ToString
                    'If master BOL is blank then use BOL else add 'MBOL' prefix to Master BOL
                    If Val(BOL_No & "") = 0 Then BOL_No = rdr_SalesOrder("SDDELN").ToString Else BOL_No = "MBOL-" & BOL_No

                    DocType = UCase(rdr_SalesOrder("SDDCTO"))
                    Min_Status = rdr_SalesOrder("MIN_LTTR")
                    ShipToNo = rdr_SalesOrder("SDSHAN")
                    'CurrCode = rdr_SalesOrder("SDCRCD")
                    ShipFromCurr = Get_Default_Currency(tempMfgLoc)
                    ShipToCurr = Get_Default_Currency(ShipToNo.ToString)
                End If
                If OrderFound = False Then
                    Write_AP_BatchError_Log(fileId, MovementNo, "Could not find sales orders in Live or History associated with payment.", 1, 1)
                    ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Could not find Sales Orders in Live or History with Movement #(URRF): " & MovementNo, Nothing, MovementNo)
                    ErrObj.Payableslog(1, 48, "OneShipments", "ProcessShipChargeNotif_E1", "Could not find associated sales order(s) in E1", "Could not find Sales Orders in Live or History with Movement#: " & MovementNo, "", "", refno)
                    cnnE1.Close() : cnnE1.Dispose()
                    OneNetworkConn.Close() : OneNetworkConn.Dispose()
                    Return ProcessReturnType.Skip
                End If
            Else
                Write_AP_BatchError_Log(fileId, MovementNo, "Error retrieving recordset from Sales Detail (F4211).", 1, 1)
                ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Error retrieving recordset from Sales Detail (F4211) for Movement #(URRF): " & MovementNo, Nothing, MovementNo)
                ErrObj.Payableslog(1, 49, "OneShipments", "ProcessShipChargeNotif_E1", "Error accessing Sales Detail record", "Error retrieving recordset from Sales Detail for Movement#: " & MovementNo, "", "", refno)
                cnnE1.Close() : cnnE1.Dispose()
                OneNetworkConn.Close() : OneNetworkConn.Dispose()
                Return ProcessReturnType.Retry
            End If
            'see if we get something back

            'We've retrieved the value from Sales Order so we can close the reader 
            rdr_SalesOrder.Close()
        Catch ex As Exception
            Write_AP_BatchError_Log(fileId, MovementNo, "Error retrieving Sales Detail record for Movement#:" & MovementNo & " Error Message: " & ex.Message, 1, 1)
            ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Error retrieving Sales Detail record for Movement#:" & MovementNo, ex)
            ErrObj.Payableslog(1, 49, "OneShipments", "ProcessShipChargeNotif_E1", "Error accessing Sales Detail record", "Error retrieving recordset from Sales Detail for Movement#: " & MovementNo, "", "", refno)
            If Not (rdr_SalesOrder Is Nothing) Then
                If rdr_SalesOrder.IsClosed = False Then rdr_SalesOrder.Close()
            End If
            cnnE1.Close() : cnnE1.Dispose()
            OneNetworkConn.Close() : OneNetworkConn.Dispose()
            Return ProcessReturnType.Retry
        End Try
        '**********************************************************************************************************************************************************************

        '***************************************************DO INTEGRITY CHECKS*****************************************************************************************

        If ShipFromCurr = "USD" Or ShipToCurr = "USD" Then
            CurrCode = "USD"
            tempDemand = "00200"     'Added Rule as per discussion with Barry and Han on March 25 - if USD then demand = 200
        Else
            If ShipToNo = 1258 Then   'Exception added for Buffalo - Buffalo Set up as CAD(expected) but payment file will be in USD so must set Currcode to USD (Oct 4, 2010)
                CurrCode = "USD"
                tempDemand = "00200"
            Else
                CurrCode = "CAD"
                tempDemand = "00100"    'Added Rule as per discussion with Barry and Han on March 25 - if CAD then demand = 100
            End If
        End If

        'After Jan 1, 2011 for Transshipments we want to allocate freight cost to receiving plant
        'added Feb 10, 2011 the exception is Danville - all Daville aerosol freight must be allocated to 291 regardless of whether its Transshipment
        'Danville (1255) is already allocated to Company code 291 in the Business unit master so no mapping hard code is required just the transshipment execption
        If sqlDateTime >= "Jan 1, 2011 12:01am" And (DocType = "ST" Or DocType = "SJ") And tempMfgLoc <> 1255 Then
            tempMfgLoc = CStr(ShipToNo)
        End If

        'IF this shipment is Aerosol only products then assign charges to KIK Chicago 291 (Assign to Danville(1255) because there is no MCMCU = 291 in F0006)
        'Exception Added Mar 9, 2012 (as per Nadeem's email) - Do not assign KIK Private Fleet (29072) Aerosol for Oncal to 1255
        'If CarrierNum <> 29072 Then
        '    AerosolOnlyFlag = CBool(DbConn.ExecuteScalar("EXEC SP_Shipment_Aerosol_Only '" & MovementNo & "' ", My.Settings.SettingValue("OneNetworkConn")))
        '    If AerosolOnlyFlag Then tempMfgLoc = "1255" 'Danville
        'End If

        'If DocType = "ST" Then   'If its a TRANSHIP
        '   'If Get_Default_Currency(tempMfgLoc) = "USD" Or Get_Default_Currency(ShipToNo.ToString) = "USD" Then
        '   If CurrCode = "USD" Then
        '       'CurrCode = "USD"
        '       tempDemand = "00200"
        '   Else
        '       'CurrCode = "CAD"
        '       tempDemand = "00100"
        '   End If
        If Val(tempDemand) <> 100 And Val(tempDemand) <> 200 And DocType <> "ST" Then
            Write_AP_BatchError_Log(fileId, MovementNo, "Error processing payment - Order is not 'ST' type and had demand other that 100 or 200 for FileID:" & fileId.ToString & " Movement#:" & MovementNo, 1, 1)
            ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Error processing payment - Order is not 'ST' type and had demand other that 100 or 200 for FileID:" & fileId.ToString & " Movement#:" & MovementNo, , MovementNo)
            cnnE1.Close() : cnnE1.Dispose()
            OneNetworkConn.Close() : OneNetworkConn.Dispose()
            Return ProcessReturnType.Skip
        End If

        '----------IF CARRIER IS KIK PRIVATE FLEET & PRONUMBER = BLANK THEN SET PRONUMBER = MOVEMENT ID SO WE HAVE VALUE TO POPULATE INVOICE ON AP VOUCHER-----------------------
        Select Case CarrierNum
            Case 29072     'KIK PRIVATE FLEET 
                If Trim(ProNumber & "") = "" Then ProNumber = MovementNo
        End Select

        '---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        ErrMessage = ""
        If Trim(ProNumber) = "" Then
            ErrMessage = ErrMessage & "ProNumber is missing, "
            ErrObj.Payableslog(1, 50, "OneShipments", "ProcessShipChargeNotif_E1", "ProNumber is missing", "ProNumber is missing for Movement#: " & MovementNo, "", "", refno)
        End If

        If Val(BOL_No & "") = 0 And InStr(BOL_No, "MBOL") = 0 Then
            ErrMessage = ErrMessage & "BOL is missing, "
            ErrObj.Payableslog(1, 52, "OneShipments", "ProcessShipChargeNotif_E1", "Bill of Lading is missing", "Bill of Lading is missing for Movement#: " & MovementNo, "", "", refno)
        End If

        If CarrierNum <= 0 Then
            ErrMessage = ErrMessage & "Carrier is zero, "
            ErrObj.Payableslog(1, 51, "OneShipments", "ProcessShipChargeNotif_E1", "Carrier is not assigned", "Carrier is not assigned for Movement#: " & MovementNo, "", "", refno)
        End If

        'CHeck if Carrier in System matchs Carrier from XML.

        If CarrierNum <> CheckCarrier(ShipChargeNotif.Tender.CarrierOrgName, CurrCode) Then
            ErrMessage = ErrMessage & "Carrier does not match Salesorder, "
            ErrObj.Payableslog(1, 51, "OneShipments", "ProcessShipChargeNotif_E1", "Carrier does not match payfile", "Carrier in Payfile does not match Sales order for Movement#: " & MovementNo, "", "", refno)
        End If

        If Trim(ErrMessage) <> "" Then

            ErrMessage = ErrMessage.Substring(0, ErrMessage.Length - 2)
            Write_AP_BatchError_Log(fileId, MovementNo, "Cannot process payment file due to causes: " & ErrMessage, 1, 1)
            ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Cannot process payment file:" & fileId.ToString & " due to causes: " & ErrMessage, , MovementNo)
            cnnE1.Close() : cnnE1.Dispose()
            OneNetworkConn.Close() : OneNetworkConn.Dispose()
            Return ProcessReturnType.Skip
        End If

        'Check if the minimum status of the Sales Order Line Items is >= 560 (which is ship confirmed) 
        'We don't want to accept any carrier invoices for orders that have not shipped
        If Min_Status < 560 Then
            Write_AP_BatchError_Log(fileId, MovementNo, "At least one of the items in the Shipment has an order status " & Min_Status.ToString & ".  Order must be at least ship confirmed (status 560) to apply payment", 1, 1)
            ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "At least one of the items in the Movement(" & MovementNo & ") has an order status " & Min_Status.ToString & ".  Order must be at least ship confirmed (status 560) to apply payment", , MovementNo)
            ErrObj.Payableslog(1, 53, "OneShipments", "ProcessShipChargeNotif_E1", "Order has not been ship confirmed", "Order has not been ship confirmed for Movement#: " & MovementNo, "", "", refno)
            cnnE1.Close() : cnnE1.Dispose()
            OneNetworkConn.Close() : OneNetworkConn.Dispose()
            Return ProcessReturnType.Skip
        End If

        '**********************************************************************************************************************************************************************

        ''**************************We need to find out if we got a payment file back for this one already***********************************************
        'Try
        '    sSql = "SELECT * FROM ShipCostComponent WHERE MovementNo = '" & MovementNo & "'"
        '    Dim rdrShipCostComp As OdbcDataReader
        '    rdrShipCostComp = DbConn.GetDataReader(sSql, My.Settings.SettingValue("OneNetworkConn"))

        '    'see if we found a previous payment - if so then we don't need to process and exit
        '    If Not (rdrShipCostComp Is Nothing) Then
        '        If rdrShipCostComp.HasRows Then
        '            Write_AP_BatchError_Log(fileId, MovementNo, "Received payment file already for this Shipment", 1, 1)
        '            ErrObj.log(1, 1, "ProcessShipChargeNotif_E1", "Received payment file already for Movement #:" & MovementNo, , MovementNo)
        '            rdrShipCostComp.Close()
        '            cnnE1.Close() : cnnE1.Dispose()
        '            Return ProcessReturnType.Skip
        '        Else
        '            'Else no rows return so this is a new payment - just close recordset and continue
        '            If rdrShipCostComp.IsClosed = False Then rdrShipCostComp.Close()
        '        End If
        '    End If
        'Catch ex As Exception
        '    Write_AP_BatchError_Log(fileId, MovementNo, "Error retrieving ShipCostComponent record for Movement #:" & MovementNo, 1, 1)
        '    ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Error retrieving ShipCostComponent record for Movement #:" & MovementNo, ex)
        '    If Not (rdr_SalesOrder Is Nothing) Then
        '        If rdr_SalesOrder.IsClosed = False Then rdr_SalesOrder.Close()
        '    End If
        '    cnnE1.Close() : cnnE1.Dispose()
        '    Return ProcessReturnType.Fail
        'End Try

        ''**********************************************************************************************************************************************************************

        '**************************CHECK IF PAYMENT HAS ALREADY BEEN APPLIED TO THIS BOL NUMBER already ***********************************************
        'If 1 = 2 Then  'Delete Oct 7
        Try
            'Check if a voucher with current BOL has already been processed - meaning we don't want a payment applied twice to the same BOL
            'sSql = "select * from openquery(E1_PROD_ORA, 'SELECT * FROM " & E1Database & ".F0411  WHERE ROWNUM <= 1 AND  RPVR01 = ''" & BOL_No & "'' ') "
            sSql = "SELECT * FROM " & E1Database & ".F0411  WHERE ROWNUM <= 1 AND ((RPVINV = '" & ProNumber & "' AND RPPYE = " & CarrierNum.ToString & ") OR RPRMK = '" & MovementNo & "') "
            Dim rdrF0411 As OdbcDataReader
            rdrF0411 = DbConn.GetDataReader(sSql, My.Settings.E1Conn, "ProcessShipChargeNotif_E1")

            'if an entry exists then this is an error because it was already processed
            If Not (rdrF0411 Is Nothing) Then
                If rdrF0411.HasRows Then
                    Write_AP_BatchError_Log(fileId, MovementNo, "Payment file has already been processed for this Movment #:" & MovementNo & " Or Carrier/ProNumber:" & CarrierNum.ToString & "/" & ProNumber & " combination in the GL (F0411)", 1, 1)
                    ErrObj.log(1, 1, "ProcessShipChargeNotif_E1", "Entry already exists in the GL (F0411) for Movement #:" & MovementNo & " Or Carrier/ProNumber:" & CarrierNum.ToString & "/" & ProNumber & " combination.", , MovementNo)
                    ErrObj.Payableslog(1, 54, "OneShipments", "ProcessShipChargeNotif_E1", "Entry already exists in GL", "Entry already exists in GL for Movement #:" & MovementNo & " Or Carrier/ProNumber:" & CarrierNum.ToString & "/" & ProNumber & " combination.", "", "", refno)
                    rdrF0411.Close()
                    cnnE1.Close() : cnnE1.Dispose()
                    OneNetworkConn.Close() : OneNetworkConn.Dispose()
                    Return ProcessReturnType.Skip
                Else
                    'Else no rows return so this is a new payment - just close recordset and continue
                    If rdrF0411.IsClosed = False Then rdrF0411.Close()
                End If
            End If

            'Check if a voucher with current BOL has already been processed - meaning we don't want a payment applied twice to the same BOL
            'sSql = "select * from openquery(E1_PROD_ORA, 'SELECT * FROM " & E1Database & ".F0411Z1  WHERE ROWNUM <= 1 AND  VLVR01 = ''" & BOL_No & "'' ') "
            sSql = "SELECT * FROM " & E1Database & ".F0411Z1  WHERE ROWNUM <= 1 AND ((VLVINV = '" & ProNumber & "' AND VLPYE = " & CarrierNum.ToString & ") OR VLEDTN = '" & MovementNo & "')  "
            rdrF0411 = DbConn.GetDataReader(sSql, My.Settings.E1Conn, "ProcessShipChargeNotif_E1")

            'if an entry exists then this is an error because it was already processed
            If Not (rdrF0411 Is Nothing) Then
                If rdrF0411.HasRows Then
                    Write_AP_BatchError_Log(fileId, MovementNo, "Payment file has already been processed for this Movment #:" & MovementNo & " Or Carrier/ProNumber:" & CarrierNum.ToString & "/" & ProNumber & " combination in the GL (F0411Z1)", 1, 1)
                    ErrObj.log(1, 1, "ProcessShipChargeNotif_E1", "Entry already exists in the GL (F0411Z1) for Movement #:" & MovementNo & " Or Carrier/ProNumber:" & CarrierNum.ToString & "/" & ProNumber & " combination.", , MovementNo)
                    ErrObj.Payableslog(1, 55, "OneShipments", "ProcessShipChargeNotif_E1", "Entry already uploading for proofing", "Entry already uploading for proofing for Movement #:" & MovementNo & " Or Carrier/ProNumber:" & CarrierNum.ToString & "/" & ProNumber & " combination.", "", "", refno)
                    rdrF0411.Close()
                    cnnE1.Close() : cnnE1.Dispose()
                    OneNetworkConn.Close() : OneNetworkConn.Dispose()
                    Return ProcessReturnType.Skip
                Else
                    'Else no rows return so this is a new payment - just close recordset and continue
                    If rdrF0411.IsClosed = False Then rdrF0411.Close()
                End If
            End If

        Catch ex As Exception
            Write_AP_BatchError_Log(fileId, MovementNo, "Error accessing F0411/F0411Z1 table", 1, 1)
            ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Error accessing F0411/F0411Z1 table for Movenment: " & MovementNo & " FileID:" & fileId.ToString, ex)
            ErrObj.Payableslog(1, 56, "OneShipments", "ProcessShipChargeNotif_E1", "Error accessing F0411/F0411Z1 table", "Error accessing F0411/F0411Z1 table for Movement#: " & MovementNo, "", "", refno)
            If Not (rdr_SalesOrder Is Nothing) Then
                If rdr_SalesOrder.IsClosed = False Then rdr_SalesOrder.Close()
            End If
            cnnE1.Close() : cnnE1.Dispose()
            OneNetworkConn.Close() : OneNetworkConn.Dispose()
            Return ProcessReturnType.Retry
        End Try
        'End If     'Delete Oct 7
        '**********************************************************************************************************************************************************************


        '************************************************* INSERT FREIGHT PAYMENT DATA INTO F0911Z1 VOUCHER DISTRIBUTION **********************************************************************************
        'begin transaction
        Dim BatchNumber As Integer = 0
        txn = cnnE1.BeginTransaction()

        Try
            Dim daF0911Z1 As OdbcDataAdapter = Nothing
            Dim dsF0911Z1 As New DataSet
            Dim drF0911Z1 As DataRow
            Dim nowDate As Date = Now       'CDate("Oct 2, 2010")  oct 7
            Dim nowDateJulian As Integer
            Dim VNOBJ As String = ""
            Dim VNSUB As String = ""
            Dim VNCO As String = ""
            Dim CRRM As String = ""
            Dim ChargeName As String = ""

            nowDateJulian = oMapping.ConvertDateToJulian(nowDate)

            sSql = "SELECT * FROM " & E1Database & ".F0911Z1 WHERE 1=0"
            'DbConn.GetUpdateableDataAdapter(sSql, cnnE1, daF0911Z1, txn)

            If Not DbConn.GetUpdateableDataAdapter(sSql, cnnE1, daF0911Z1, txn) Then
                txn.Rollback()
                Write_AP_BatchError_Log(fileId, MovementNo, "Failed to get OdbcDataAdapter for table F0911Z1", 1, 1)
                ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Failed to get OdbcDataAdapter for table F0911Z1")
                ErrObj.Payableslog(1, 57, "OneShipments", "ProcessShipChargeNotif_E1", "Error accessing F0911Z1 table", "Error accessing F0911Z1 table for Movement#: " & MovementNo, "", "", refno)
                cnnE1.Close() : cnnE1.Dispose()
                OneNetworkConn.Close() : OneNetworkConn.Dispose()
                Return ProcessReturnType.Fail
            End If

            daF0911Z1.Fill(dsF0911Z1)

            'get the first CarrierCostDetail
            If ShipChargeNotif.CarrierCostDetail IsNot Nothing Then

                'make sure component is not empty
                If ShipChargeNotif.CarrierCostDetail.CostComponent IsNot Nothing Then

                    'Get the next EDI Batch Number from F0002
                    'BatchNumber = GetE1_NextNumber_AdvanceNumber("00", "NNN006", True)
                    If BatchNumber <= 0 Then BatchNumber = Get_AP_Batch_Number(tempDemand)

                    'If next EDI Batch Number = -1 then there was an error retrieving the next number or advancing the counter so we cannot process this payment
                    If BatchNumber <= 0 Then
                        txn.Rollback()
                        Write_AP_BatchError_Log(fileId, MovementNo, "Error retrieving/setting next Batch number in F0002 for Key:00 Index:N006", 1, 1)
                        ErrObj.log(1, 1, "ProcessShipChargeNotif_E1", "Error retrieving/setting next Batch number in F0002 for Key:00 Index:N006 for Movement# " & MovementNo, , MovementNo)
                        ErrObj.Payableslog(1, 58, "OneShipments", "ProcessShipChargeNotif_E1", "Error retrieving/setting next Batch number", "Error retrieving/setting next Batch number for Movement#: " & MovementNo, "", "", refno)
                        cnnE1.Close() : cnnE1.Dispose()
                        OneNetworkConn.Close() : OneNetworkConn.Dispose()
                        Return ProcessReturnType.Skip
                    Else
                        'For Each oComponent As CostComponent In ShipChargeNotif.CarrierCostDetail.CostComponent
                        For Each oComponent As FGIIntegPaymentNotificationCarrierCostDetailCostComponent In ShipChargeNotif.CarrierCostDetail.CostComponent

                            'GET CHARGE NAME FOR COST
                            If Trim(oComponent.BaseRateContractName & "") = "" Then
                                If Trim(oComponent.AdditionalChargeAccName & "") = "" Then
                                    If Trim(oComponent.AccessorialRateAccessorialName & "") = "" Then
                                        If UCase(Trim(oComponent.CostMethod & "")) = "PER STOP" Then
                                            ChargeName = oComponent.CostMethod
                                        Else
                                            ChargeName = ""
                                        End If
                                    Else
                                        ChargeName = oComponent.AccessorialRateAccessorialName
                                    End If
                                Else
                                    ChargeName = oComponent.AdditionalChargeAccName
                                End If
                            Else
                                ChargeName = "BaseRateCharge"
                            End If

                            'Check if currency in Payment file matches the expected currency - if not then generate error and rollback and exit
                            If Not (oComponent.UnitRateCurrency Is Nothing) And Trim(oComponent.UnitRateCurrency) <> "" And UCase(Trim(oComponent.UnitRateCurrency)) <> CurrCode Then
                                txn.Rollback()
                                Write_AP_BatchError_Log(fileId, MovementNo, "Currency in Payment file (" & oComponent.UnitRateCurrency & ") does not match expected currency (" & CurrCode & ") for Movenment: " & MovementNo & " FileID:" & fileId.ToString, 1, 1)
                                ErrObj.log(1, 1, "ProcessShipChargeNotif_E1", "Currency in Payment file (" & oComponent.UnitRateCurrency & ") does not match expected currency (" & CurrCode & ") for Movenment: " & MovementNo & " FileID:" & fileId.ToString, , MovementNo)
                                ErrObj.Payableslog(1, 59, "OneShipments", "ProcessShipChargeNotif_E1", "Payment file currency does not match expected", "Payment file currency does not match expected for Movement#: " & MovementNo, "", "", refno)
                                cnnE1.Close() : cnnE1.Dispose()
                                OneNetworkConn.Close() : OneNetworkConn.Dispose()
                                Return ProcessReturnType.Skip
                            End If

                            'if chargename is blank then do not process this cost component
                            If ChargeName = "" Then
                                ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "ChargeName is empty for FileID:" & fileId.ToString & " Shipment#:" & MovementNo & " CostComponent:" & oComponent.CostComponentNo.ToString)
                                'ErrObj.Payableslog(1, 60, "OneShipments", "ProcessShipChargeNotif_E1", "Non-Critical Error: Chargename is empty", "Chargename is empty for Movement #(URRF): " & MovementNo & " CostComponent:" & oComponent.CostComponentNo.ToString, "", "", refno)
                            ElseIf ChargeName <> "BaseRateCharge" And oComponent.NetCost > 5000 Then
                                txn.Rollback()
                                Write_AP_BatchError_Log(fileId, MovementNo, "Accessorial Charge (" & ChargeName & ") in the amount $" & oComponent.NetCost.ToString & " exceeds $5000 for Movenment: " & MovementNo & " FileID:" & fileId.ToString, 1, 1)
                                ErrObj.log(1, 1, "ProcessShipChargeNotif_E1", "Accessorial Charge (" & ChargeName & ") in the amount $" & oComponent.NetCost.ToString & " exceeds $5000 for Movenment: " & MovementNo & " FileID:" & fileId.ToString, , MovementNo)
                                ErrObj.Payableslog(1, 65, "OneShipments", "ProcessShipChargeNotif_E1", "Accessorial Charge Exceeds $5000", "Accessorial Charge (" & ChargeName & ") exceeds $5000 for Movement#: " & MovementNo, "", "", refno)
                                cnnE1.Close() : cnnE1.Dispose()
                                OneNetworkConn.Close() : OneNetworkConn.Dispose()
                                Return ProcessReturnType.Skip
                            Else
                                'Do Lookup for chargename and get GL Codes
                                Charge_Code_Lookup(VNOBJ, VNSUB, MovementNo, ChargeName, DocType, CurrCode)

                                'IF codes are blank then skip and do not create record
                                If VNOBJ <> "" And VNSUB <> "" Then

                                    'create new row to dump data to
                                    drF0911Z1 = dsF0911Z1.Tables(0).NewRow
                                    If Apply_DataRow_Defaults(drF0911Z1) Then
                                        'assign values to fields
                                        drF0911Z1("VNEDUS") = BatchUserID                                'User ID                          'PK
                                        drF0911Z1("VNEDTN") = Left(Trim(MovementNo), 22)                     'Transaction Number               'PK 
                                        drF0911Z1("VNEDLN") = (oComponent.CostComponentNo + 1) * 1000     'Line Number                      'PK
                                        drF0911Z1("VNEDDT") = nowDateJulian                               'EDI - Tranmission Date
                                        drF0911Z1("VNEDER") = "B"                                         'Send/Receive Indicator
                                        drF0911Z1("VNEDTC") = "A"                                         'Transaction Action
                                        drF0911Z1("VNEDTR") = "V"                                         'Transaction Type
                                        drF0911Z1("VNEDBT") = BatchNumber                                 'Batch Number                     'PK
                                        drF0911Z1("VNDGJ") = nowDateJulian                                'G/L Date
                                        drF0911Z1("VNAM") = "2"                                           'Account Mode
                                        drF0911Z1("VNLT") = "AA"                                          'Ledger Type
                                        drF0911Z1("VNCRCD") = CurrCode                                    'Currency Code
                                        drF0911Z1("VNOBJ") = VNOBJ                                        'Object Account
                                        drF0911Z1("VNSUB") = VNSUB                                        'Subsidiary

                                        If (tempDemand = "00100" And UCase(CurrCode) <> "CAD") Or (tempDemand = "00200" And UCase(CurrCode) <> "USD") Then
                                            CRRM = "F"
                                        Else
                                            CRRM = "D"
                                        End If
                                        drF0911Z1("VNCRRM") = CRRM                                  'Mode(F)
                                        If CRRM = "D" Then drF0911Z1("VNAA") = Val(oComponent.NetCost) * 100 Else drF0911Z1("VNAA") = 0 'Amount
                                        If CRRM = "F" Then drF0911Z1("VNACR") = Val(oComponent.NetCost) * 100 Else drF0911Z1("VNACR") = 0 'Currency Amount
                                        GrossAmount = GrossAmount + (Val(oComponent.NetCost) * 100)

                                        drF0911Z1("VNEXA") = Left(Trim(oComponent.Description), 30) 'Explanation - Name Alpha
                                        drF0911Z1("VNEXR") = Left(Trim(MovementNo), 30)   'Left(Trim(Invoice_No), 30)             'Voucher Number ?????????? - Maybe from next numbers table F0002 under system code 04 (accounts payable) under Voucher Number (index 1)
                                        drF0911Z1("VNAN8") = CarrierNum                             'Address Number
                                        drF0911Z1("VNIVD") = nowDateJulian                          'Voucher date
                                        drF0911Z1("VNUSER") = BatchUserID                          'User ID
                                        drF0911Z1("VNPID") = "INTERFACE"                            'Program ID

                                        '****************************** CHANGES AS PER DEANS INSTRUCTIONS FEB 17, 2010 ******************************************* 

                                        MfgLoc = GetE1_ShipCharge_MCU(tempMfgLoc, VNOBJ)
                                        drF0911Z1("VNMCU") = MfgLoc     'Business Unit

                                        'Pad VNCO with zero on the left so total characters = 5.  i.e. 00110
                                        If InStr(MfgLoc, "0000") = 0 Then VNCO = MfgLoc.PadLeft(5, "0") Else VNCO = CStr(Left(MfgLoc, MfgLoc.Length - 4)).PadLeft(5, "0")
                                        drF0911Z1("VNCO") = VNCO              'Changed based on Dean's email Mar 30, 2010  tempDemand                                      'Company

                                        drF0911Z1("VNEDSP") = "0"                                   'EDI - Successfully processed
                                        drF0911Z1("VNICUT") = "V"                                   'Batch Type
                                        drF0911Z1("VNANI") = MfgLoc & "." & VNOBJ & "." & VNSUB     'Account Number
                                        drF0911Z1("VNVINV") = Left(Trim(ProNumber.ToString), 25)              'Invoice Number 

                                        'add the row
                                        dsF0911Z1.Tables(0).Rows.Add(drF0911Z1)
                                    Else
                                        'Could Not Apply Row Defaults
                                        txn.Rollback()
                                        Write_AP_BatchError_Log(fileId, MovementNo, "Error Applying Row Defaults. Cannot write to F0911Z1 ", 1, 1)
                                        ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Error Applying Row Defaults. Cannot write to F0911Z1 tables for FileID:" & fileId.ToString & " Shipment#:" & MovementNo)
                                        ErrObj.Payableslog(1, 61, "OneShipments", "ProcessShipChargeNotif_E1", "Error applying row defaults to F0911Z1/F0411Z1", "Error applying row defaults to F0911Z1 for Movement#: " & MovementNo, "", "", refno)
                                        cnnE1.Close() : cnnE1.Dispose()
                                        OneNetworkConn.Close() : OneNetworkConn.Dispose()
                                        Return ProcessReturnType.Retry
                                    End If
                                Else
                                    'did not get a gl acct back so rollback and do not process
                                    txn.Rollback()
                                    Write_AP_BatchError_Log(fileId, MovementNo, "Could not find G/L for: " & ChargeName & " " & VNOBJ & " " & VNSUB & " and Currency:" & CurrCode & ". Cannot write to F0911Z1", 1, 1)
                                    ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Error processing file - Could not find G/L for: " & ChargeName & " " & VNOBJ & " " & VNSUB & " and Currency:" & CurrCode & ". Cannot write to F0911Z1 tables for FileID:" & fileId.ToString & " Shipment#:" & MovementNo)
                                    ErrObj.Payableslog(1, 62, "OneShipments", "ProcessShipChargeNotif_E1", "Could not find GL Code", "Could not find GL code for: " & ChargeName & " " & VNOBJ & " " & VNSUB & " and Currency:" & CurrCode & " Movement#: " & MovementNo, "", "", refno)
                                    cnnE1.Close() : cnnE1.Dispose()
                                    OneNetworkConn.Close() : OneNetworkConn.Dispose()
                                    Return ProcessReturnType.Skip
                                End If
                            End If
                        Next
                    End If
                End If
            End If

            'commit changes
            daF0911Z1.Update(dsF0911Z1)
            dsF0911Z1.AcceptChanges()

            'commit changes
            txn.Commit()
        Catch ex As Exception
            txn.Rollback()
            Write_AP_BatchError_Log(fileId, MovementNo, "Error processing file - Cannot write to F0911Z1", 1, 1)
            ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Error processing file - Cannot write to F0911Z1 tables for FileID:" & fileId.ToString & " Movement#:" & MovementNo, ex)
            ErrObj.Payableslog(1, 63, "OneShipments", "ProcessShipChargeNotif_E1", "Error writing to F0911Z1/F0411Z1 table", "Error writing to F0911Z1 table for Movement#: " & MovementNo, "", "", refno)
            cnnE1.Close() : cnnE1.Dispose()
            OneNetworkConn.Close() : OneNetworkConn.Dispose()
            Return ProcessReturnType.Skip
        End Try
        '**********************************************************************************************************************************************************************

        '************************************************* INSERT FREIGHT PAYMENT DATA INTO F0411Z1 VOUCHER HEADER **********************************************************************************
        'begin transaction
        txn = cnnE1.BeginTransaction()

        Try
            Dim daF0411Z1 As OdbcDataAdapter = Nothing
            Dim dsF0411Z1 As New DataSet
            Dim drF0411Z1 As DataRow
            Dim nowDate As Date = Now   ' CDate("Oct 2, 2010") oct 7
            Dim nowDateJulian As Integer
            Dim InvoiceDateJulian As Integer
            Dim InvoiceDate As Date
            Dim VLOBJ As String = ""
            Dim VLSUB As String = ""
            Dim CRRM As String = ""
            Dim TXA1 As String = ""
            Dim EXR1 As String = ""
            Dim AA_Charge As Double = 0

            nowDateJulian = oMapping.ConvertDateToJulian(nowDate)
            InvoiceDateJulian = nowDateJulian           'Set current date as default invoice date

            'Get the Date Entered value for FileID from OneNetFiles table - want to use the date the file was received as the invoice date.
            InvoiceDate = DbConn.ExecuteScalar("SELECT entered FROM OneNetFiles WHERE ID =" & CStr(fileId), My.Settings.SettingValue("OneNetworkConn"))
            If IsDate(InvoiceDate) Then InvoiceDateJulian = oMapping.ConvertDateToJulian(InvoiceDate)

            sSql = "SELECT * FROM " & E1Database & ".F0411Z1 WHERE 1=0"
            'DbConn.GetUpdateableDataAdapter(sSql, cnnE1, daF0411Z1, txn)

            If Not DbConn.GetUpdateableDataAdapter(sSql, cnnE1, daF0411Z1, txn) Then
                txn.Rollback()
                Write_AP_BatchError_Log(fileId, MovementNo, "Failed to get OdbcDataAdapter for table F0411Z1", 1, 1)
                ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Failed to get OdbcDataAdapter for table F0411Z1")
                ErrObj.Payableslog(1, 56, "OneShipments", "ProcessShipChargeNotif_E1", "Error accessing F0411/F0411Z1 table", "Error accessing F0411Z1 table for Movement#: " & MovementNo, "", "", refno)
                cnnE1.Close() : cnnE1.Dispose()
                OneNetworkConn.Close() : OneNetworkConn.Dispose()
                Return ProcessReturnType.Fail
            End If

            daF0411Z1.Fill(dsF0411Z1)


            If daF0411Z1 IsNot Nothing Then
                drF0411Z1 = dsF0411Z1.Tables(0).NewRow              'create new row to dump data to
                If Apply_DataRow_Defaults(drF0411Z1) Then           'Sets string types to " " and numeric type to zero
                    'assign values to fields
                    drF0411Z1("VLEDUS") = BatchUserID              'User ID
                    drF0411Z1("VLEDTN") = Left(Trim(MovementNo), 22)                   'Transaction Number
                    drF0411Z1("VLEDLN") = 1000                      'Line Number
                    drF0411Z1("VLEDER") = "B"                       'Send/Receive Indicator
                    drF0411Z1("VLEDTC") = "A"                       'Transaction Action
                    drF0411Z1("VLEDTR") = "V"                       'Transaction Type
                    drF0411Z1("VLEDBT") = BatchNumber               'Batch Number
                    drF0411Z1("VLEDDH") = 1                         'Discount Handling
                    drF0411Z1("VLAN8") = CarrierNum                 'Address Number
                    drF0411Z1("VLDIVJ") = InvoiceDateJulian         'Invoice Date
                    drF0411Z1("VLDGJ") = nowDateJulian              'G/L Date
                    drF0411Z1("VLCO") = tempDemand                  'Company
                    drF0411Z1("VLBALJ") = "Y"                       'Balanced - J.E.s
                    drF0411Z1("VLPST") = "A"                        'Pay Status

                    Get_FreightChargeAA(AA_Charge, MovementNo, BatchNumber)

                    If (tempDemand = "00100" And UCase(CurrCode) <> "CAD") Or (tempDemand = "00200" And UCase(CurrCode) <> "USD") Then
                        CRRM = "F"
                    Else
                        CRRM = "D"
                    End If

                    If CRRM = "D" Then drF0411Z1("VLATXA") = AA_Charge Else drF0411Z1("VLATXA") = 0 'Taxable Amount

                    If CurrCode = "USD" Then   'Overriede tax codes if it's USD because its a US-Canada, Canada-US, US-US shipment
                        drF0411Z1("VLTXA1") = "EXEMPT"                      'Tax Rate/Area
                        drF0411Z1("VLEXR1") = "E"                      'Tax Expl Code
                    Else
                        Carrier_TaxRateCode_Lookup(TXA1, EXR1, CarrierNum)
                        drF0411Z1("VLTXA1") = TXA1                      'Tax Rate/Area
                        drF0411Z1("VLEXR1") = EXR1                      'Tax Expl Code
                    End If



                    drF0411Z1("VLCRRM") = CRRM                      'Mode(F)
                    drF0411Z1("VLCRCD") = CurrCode                  'Currency Code
                    If CRRM = "F" Then drF0411Z1("VLCTXA") = AA_Charge Else drF0411Z1("VLCTXA") = 0 'Foreign Taxable Amount
                    drF0411Z1("VLVR01") = Trim(BOL_No)     'Reference
                    drF0411Z1("VLTORG") = BatchUserID              'Transaction Originator
                    drF0411Z1("VLUSER") = BatchUserID              'User ID

                    '****************************** CHANGES AS PER DEANS INSTRUCTIONS FEB 17, 2010 *******************************************
                    'drF0411Z1("VLMCU") = MfgLoc                                    'Business Unit
                    drF0411Z1("VLEDSP") = "0"                                       'EDI - Successfully processed
                    drF0411Z1("VLICUT") = "V"                                       'Batch Type
                    drF0411Z1("VLVINV") = Left(Trim(ProNumber.ToString), 25)                  'Invoice Number 
                    drF0411Z1("VLPYE") = CarrierNum                                 'Payee Address Number
                    drF0411Z1("VLKCO") = tempDemand                                 'Document Company
                    drF0411Z1("VLDSVJ") = nowDateJulian                             'Date - Service/Tax
                    drF0411Z1("VLRMK") = Left(Trim(MovementNo), 30)                    'REMARK FIELD - place movement # here

                    drF0411Z1("VLAG") = GrossAmount

                    'add the row
                    dsF0411Z1.Tables(0).Rows.Add(drF0411Z1)
                End If
            End If

            'commit changes
            daF0411Z1.Update(dsF0411Z1)
            dsF0411Z1.AcceptChanges()

            'commit changes
            txn.Commit()
        Catch ex As Exception
            txn.Rollback()
            Write_AP_BatchError_Log(fileId, MovementNo, "Error processing file - Cannot write to F0411Z1 tables", 1, 1)
            ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Error processing file - Cannot write to F0411Z1 tables for FileID:" & fileId.ToString & " Movement#:" & MovementNo, ex)
            ErrObj.Payableslog(1, 63, "OneShipments", "ProcessShipChargeNotif_E1", "Error writing to F0911Z1/F0411Z1 table", "Error writing to F0411Z1 table for Movement#: " & MovementNo, "", "", refno)
            cnnE1.Close() : cnnE1.Dispose()
            OneNetworkConn.Close() : OneNetworkConn.Dispose()
            Return ProcessReturnType.Skip
        End Try
        '**********************************************************************************************************************************************************************


        ''*************************************************************UPDATE SHIPCOSTCOMPONET & SHIPCOSTHEADER WITH BatchNumber ***************************************************

        Try
            sSql = "UPDATE ShipCostComponent SET BatchNo = " & BatchNumber.ToString & " WHERE MovementNo = '" & MovementNo & "' "
            If Not DbConn.ExecuteQuery(sSql, My.Settings.SettingValue("OneNetworkConn")) Then
                ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Error updating ShipCostComponent with BatchNo for Movement #:" & MovementNo & " (FileId:" & fileId.ToString & ")")
            End If

            sSql = "UPDATE ShipCostHeader SET BatchNo = " & BatchNumber.ToString & " WHERE MovementNo = '" & MovementNo & "' "
            If Not DbConn.ExecuteQuery(sSql, My.Settings.SettingValue("OneNetworkConn")) Then
                ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Error updating ShipCostHeader with BatchNo for Movement #:" & MovementNo & " (FileId:" & fileId.ToString & ")")
            End If
        Catch ex As Exception
            'Not critical error so do not exit out of function
            Write_AP_BatchError_Log(fileId, MovementNo, "Error updating ShipCostComponent/Header with BatchNo for Movement #:" & MovementNo & " (FileId:" & fileId.ToString & ")", 1, 1)
            ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Error updating ShipCostComponent/Header with BatchNo for Movement #:" & MovementNo & " (FileId:" & fileId.ToString & ")", ex)
            ErrObj.Payableslog(1, 64, "OneShipments", "ProcessShipChargeNotif_E1", "Error updating ShipCostComponent/Header with BatchNo", "Error updating ShipCostComponent/Header with BatchNo for Movement#: " & MovementNo, "", "", refno)
        End Try
        ''**************************************************************************************************************************************************************************

        'cleanup
        ShipChargeNotif = Nothing
        cnnE1.Close() : cnnE1.Dispose()
        OneNetworkConn.Close() : OneNetworkConn.Dispose()

        Return ProcessReturnType.Success

    End Function

    Public Function Get_OneNetFile_DateEntered(ByVal fileId As Integer, Optional ByRef OneNetworkConn As OdbcConnection = Nothing) As Date
        Dim ConnState As System.Data.ConnectionState = ConnectionState.Closed

        If ConnState = ConnectionState.Closed Then
            Try
                OneNetworkConn = DbConn.CreateConnection(My.Settings.SettingValue("OneNetworkConn"))
                OneNetworkConn.Open()
            Catch ex As Exception
                ErrObj.log(1, 0, "Get_OneNetFile_DateEntered", "Could not open OneNetwork connection. Error processing retrieve Entry Date from FileID:" & fileId.ToString, ex)
                If ConnState = ConnectionState.Closed Then OneNetworkConn.Close()
            End Try
        End If

    End Function

    Public Function Write_ShipCostComponent_Entry(ByVal ShipChargeNotif As FGIIntegPaymentNotification, ByVal fileId As Integer, Optional ByRef OneNetworkConn As OdbcConnection = Nothing, Optional ByVal MovementNo As String = "") As ProcessReturnType
        Dim txn As OdbcTransaction
        Dim sSql As String = ""
        'Dim OneNetworkConn As OdbcConnection
        Dim Shipfrom As String = ""
        Dim CustomerNo As String = ""
        Dim BOL As String = ""
        Dim ConnState As System.Data.ConnectionState = ConnectionState.Closed

        'Save state of current connection so we know whether or not to close it on exit 
        If Not (OneNetworkConn Is Nothing) Then ConnState = OneNetworkConn.State
        If MovementNo = "" Then MovementNo = ShipChargeNotif.Movement.MovementNumber

        'Open OneNetwork Database Connection
        If ConnState = ConnectionState.Closed Then
            Try
                OneNetworkConn = DbConn.CreateConnection(My.Settings.SettingValue("OneNetworkConn"))
                OneNetworkConn.Open()
            Catch ex As Exception
                Write_AP_BatchError_Log(fileId, MovementNo, "Could not open OneNetwork connection.", 1, 1)
                ErrObj.log(1, 0, "Write_ShipCostComponent_Entry", "Could not open OneNetwork connection. Error processing  FileID:" & fileId.ToString & " Movement#:" & MovementNo, ex)
                If ConnState = ConnectionState.Closed Then OneNetworkConn.Close()
                Return ProcessReturnType.Fail
            End Try
        End If

        'begin transaction
        txn = OneNetworkConn.BeginTransaction()

        Try
            'find out if we already processed this previously, if we did, we don't want to write to the cost component table again
            sSql = "SELECT * FROM ShipCostComponent WHERE FileId = " & fileId & " OR MovementNo = '" & MovementNo & "' "
            Dim rdrCostCompFileId As OdbcDataReader
            rdrCostCompFileId = DbConn.GetDataReader(sSql, My.Settings.SettingValue("OneNetworkConn"))

            'find out if we already processed this previously, if we did, we don't want to write to the cost component table again
            sSql = "SELECT * FROM ShipCostHeader WHERE FileId = " & fileId & " OR MovementNo = '" & MovementNo & "' "
            Dim rdrCostHeader As OdbcDataReader
            rdrCostHeader = DbConn.GetDataReader(sSql, My.Settings.SettingValue("OneNetworkConn"))

            'if we find one, we don't want to write to table, but we want to report it
            If rdrCostCompFileId.HasRows Or rdrCostHeader.HasRows Then
                'Return ProcessReturnType.Success
                ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Skipping cost component/header insert, this payment file was already processed for FileID:" & fileId.ToString & " Movement #:" & MovementNo)
                txn.Rollback()
                rdrCostCompFileId.Close()
                rdrCostHeader.Close()
                If ConnState = ConnectionState.Closed Then OneNetworkConn.Close()
                Return ProcessReturnType.Success
            Else
                'cleanup
                rdrCostCompFileId.Close()
                rdrCostHeader.Close()

                Dim nowDate As Date = Now


                '**********************************************WRITE TO SHIP COST HEADER TABLE******************************************************************************
                'now we need to store the cost components in a table
                Dim daCostHead As OdbcDataAdapter = Nothing
                Dim dsCostHead As New DataSet
                Dim drCostHead As DataRow
                Dim BOLNo As String = ""
                Dim ShipmentNo As String = ""


                'get the first CarrierCostHeader Info
                If ShipChargeNotif.ShipmentListMessage IsNot Nothing Then

                    'make sure component is not empty
                    If ShipChargeNotif.ShipmentListMessage.Shipment IsNot Nothing Then
                        For Each oShipment As FGIIntegPaymentNotificationShipmentListMessageShipment In ShipChargeNotif.ShipmentListMessage.Shipment

                            If oShipment.ShipmentNumber Is Nothing Then
                                ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Skipping cost component/header insert due to missing/blank Shipment Number. (FileID:" & fileId.ToString & " Movement #:" & MovementNo & ")")
                                txn.Rollback()
                                If ConnState = ConnectionState.Closed Then OneNetworkConn.Close()
                                Return ProcessReturnType.Skip

                            Else
                                ShipmentNo = Trim(oShipment.ShipmentNumber.ToString)

                                'check if we've processed a payment for this BOL - if not then continue else generate error and exit
                                sSql = "SELECT * FROM OneNetwork.dbo.ShipCostHeader WHERE ShipmentNo = '" & ShipmentNo & "' "
                                DbConn.GetUpdateableDataAdapter(sSql, OneNetworkConn, daCostHead, txn)
                                daCostHead.Fill(dsCostHead)

                                If dsCostHead.Tables(0).Rows.Count > 0 Then
                                    'Return ProcessReturnType.Success
                                    ErrObj.log(1, 0, "ProcessShipChargeNotif_E1", "Skipping cost component/header insert, this payment file was already processed for Shipment:" & ShipmentNo & " (FileID:" & fileId.ToString & " Movement #:" & MovementNo & ")")
                                    txn.Rollback()
                                    daCostHead.Dispose()
                                    If ConnState = ConnectionState.Closed Then OneNetworkConn.Close()
                                    Return ProcessReturnType.Skip
                                Else

                                    'create new row to dump data to
                                    drCostHead = dsCostHead.Tables(0).NewRow

                                    'assign the primary/parent keys
                                    drCostHead("FileId") = fileId
                                    drCostHead("Entered") = nowDate
                                    drCostHead("MovementNo") = MovementNo
                                    drCostHead("ShipmentNo") = ShipmentNo
                                    drCostHead("ProNumber") = Trim(ShipChargeNotif.Movement.CarrierProNumber)
                                    drCostHead("ShipFrom") = Trim(oShipment.ShipFromSiteName)
                                    drCostHead("Customer") = Trim(oShipment.ShipToOrganizationName)
                                    drCostHead("BOL") = Val(BOLNo)

                                    'add the row
                                    dsCostHead.Tables(0).Rows.Add(drCostHead)

                                    'Concatenate all BOLs and assign to variable to write later to ShipCostComponent table
                                    If BOL = "" Then
                                        BOL = BOLNo
                                    Else
                                        BOL = BOL & ", " & BOLNo
                                    End If

                                    'Concatenate all ShipFroms and assign to variable to write later to ShipCostComponent table
                                    If Shipfrom = "" Then
                                        Shipfrom = Trim(oShipment.ShipFromSiteName)
                                    ElseIf Shipfrom <> Trim(oShipment.ShipFromSiteName) Then
                                        Shipfrom = Shipfrom & ", " & Trim(oShipment.ShipFromSiteName)
                                    End If

                                    'Concatenate all Customers and assign to variable to write later to ShipCostComponent table
                                    If CustomerNo = "" Then
                                        CustomerNo = Trim(oShipment.ShipToOrganizationName)
                                    ElseIf CustomerNo <> Trim(oShipment.ShipToOrganizationName) Then
                                        CustomerNo = CustomerNo & ", " & Trim(oShipment.ShipToOrganizationName)
                                    End If
                                End If
                                daCostHead.Update(dsCostHead)
                                dsCostHead.AcceptChanges()
                                dsCostHead.Clear()
                                daCostHead.Dispose()
                                dsCostHead.Dispose()
                            End If
                        Next
                    End If
                End If

                '***********************************************************************************************************************************************************************************

                '**********************************************WRITE TO SHIP COST COMPONENT TABLE******************************************************************************
                'now we need to store the cost components in a table
                Dim daCostComp As OdbcDataAdapter = Nothing
                Dim dsCostComp As New DataSet
                Dim drCostComp As DataRow

                'open cost component table to write to
                sSql = "SELECT * FROM OneNetwork.dbo.ShipCostComponent WHERE 1 = 0"
                DbConn.GetUpdateableDataAdapter(sSql, OneNetworkConn, daCostComp, txn)
                daCostComp.Fill(dsCostComp)

                'get the first CarrierCostDetail
                If ShipChargeNotif.CarrierCostDetail IsNot Nothing Then

                    'make sure component is not empty
                    If ShipChargeNotif.CarrierCostDetail.CostComponent IsNot Nothing Then
                        'For Each oComponent As CostComponent In ShipChargeNotif.CarrierCostDetail.CostComponent
                        For Each oComponent As FGIIntegPaymentNotificationCarrierCostDetailCostComponent In ShipChargeNotif.CarrierCostDetail.CostComponent
                            'create new row to dump data to
                            drCostComp = dsCostComp.Tables(0).NewRow

                            'assign the primary/parent keys
                            drCostComp("FileId") = fileId
                            drCostComp("DemandLocation") = ""
                            drCostComp("ShipmentNumber") = ""
                            drCostComp("Entered") = nowDate
                            drCostComp("MovementNo") = MovementNo
                            drCostComp("ProNumber") = ShipChargeNotif.Movement.CarrierProNumber
                            drCostComp("ShipFrom") = Shipfrom
                            drCostComp("CustomerNo") = CustomerNo
                            drCostComp("BOL") = BOL

                            'map the component to table
                            If Not oMapping.MapObjectToDataRow(fileId, oComponent, drCostComp, "ShipCharge.CostComponent", , My.Settings.InterfaceType) Then
                                Write_AP_BatchError_Log(fileId, MovementNo, "Error mapping CostComponent for FileID:" & fileId.ToString & " Movement #:" & MovementNo, 0, 1)
                                ErrObj.log(1, 0, "Write_ShipCostComponent_Entry", "error mapping CostComponent for FileID:" & fileId.ToString & " Movement #:" & MovementNo, , MovementNo)
                            End If

                            'add the row
                            dsCostComp.Tables(0).Rows.Add(drCostComp)

                        Next
                    End If

                    'commit changes
                    daCostComp.Update(dsCostComp)
                    dsCostComp.AcceptChanges()

                End If
                '***********************************************************************************************************************************************************************************
            End If

            'Commit the transaction
            txn.Commit()
        Catch ex As Exception
            txn.Rollback()
            Write_AP_BatchError_Log(fileId, MovementNo, "Error processing file - Cannot write to ShipCostComponent table", 1, 1)
            ErrObj.log(1, 0, "Write_ShipCostComponent_Entry", "Error processing file - Cannot write to ShipCostComponent table for FileID:" & fileId.ToString & " Movement #:" & MovementNo, ex)
            If ConnState = ConnectionState.Closed Then OneNetworkConn.Close()
            Return ProcessReturnType.Skip
        End Try

        Return ProcessReturnType.Success
    End Function

    Function HasDuplicateCharges(ByVal MovementNo As String, ByVal fileId As Integer, ByVal refno As String) As Boolean

        Dim ssql As String
        Dim rdr As OdbcDataReader
        Dim retVal As Boolean = False

        'query will find duplicate line charges in a shipment (excluding the base rate, only additional and accessorial)
        ssql = "SELECT MovementNo,COALESCE(ADDITIONALCHARGEACCNAME,ACCESSORIALRATEACCESSORIALNAME) CHARGENAME " _
            & " FROM SHIPCOSTCOMPONENT " _
            & " WHERE(COALESCE(ADDITIONALCHARGEACCNAME, ACCESSORIALRATEACCESSORIALNAME) Is Not NULL) " _
            & " AND MovementNo = '" & MovementNo & "'" _
            & " GROUP BY MovementNo,COALESCE(ADDITIONALCHARGEACCNAME,ACCESSORIALRATEACCESSORIALNAME)" _
            & " HAVING COUNT(*) > 1"

        rdr = DbConn.GetDataReader(ssql, My.Settings.SettingValue("OneNetworkConn"))

        Try
            If rdr.HasRows Then
                retVal = True

                'loop through the charges so we can report on it
                Dim dupCharges As String = ""

                While rdr.Read
                    If dupCharges = "" Then
                        dupCharges = rdr("chargename")
                    Else
                        dupCharges = dupCharges & vbCrLf & rdr("chargename")
                    End If
                End While

                ErrObj.Payableslog(1, 44, "OneShipments", "HasDuplicateCharges", "Error with duplicate charges", "'" & dupCharges & "' duplicate charges for Movement#: " & MovementNo, "", "", refno)
                ErrObj.log(1, 0, "HasDuplicateCharges", "'" & dupCharges & "' duplicate charges for Movement #(URRF): " & MovementNo & " FileID:" & fileId.ToString)

            End If

            rdr.Close()
        Catch ex As Exception
            'error executing the query, we'll have to let this one through because it's only semi-critical to do this check
            'log an error indicate as such
            ErrObj.Payableslog(1, 44, "OneShipments", "HasDuplicateCharges", "Unable to check for duplicate charges", "Please double check Movement#: " & MovementNo, "", "", refno)
            ErrObj.log(1, 0, "HasDuplicateCharges", "Unable to check for duplicate charges for Movement #(URRF): " & MovementNo & " FileID:" & fileId.ToString, ex)

        End Try

        Return retVal

    End Function

    Function Get_Default_Currency(ByVal LocationCode As String) As String
        Dim sSql As String
        Dim ReturnVal As String = "USD"   'Set default to USD

        Try
            sSql = "SELECT AICRCD FROM " & My.Settings.SettingValue("E1Database") & ".F03012 WHERE ROWNUM <= 1 AND AIAN8 = " & Val(LocationCode).ToString
            ReturnVal = UCase(Trim(DbConn.ExecuteScalar(sSql, My.Settings.E1Conn) & ""))

            If Trim(ReturnVal) = "" Then ReturnVal = "USD"

        Catch ex As Exception
            ErrObj.log(1, 0, "Get_Default_Currency", "Error retrieving Default Currency for Location:" & LocationCode, ex)
            Return ReturnVal
        End Try
        Return ReturnVal
    End Function

    Public Sub Set_Defualt_Movement()
        Dim sSql As String

        Try
            'UPDATE F4201 Movement # (SHURRF) AND SET AS DEFAULT TO Order #(SHDOCO)
            sSql = "UPDATE " & My.Settings.SettingValue("E1Database") & ".F4201 SET SHURRF = SHDOCO WHERE SHURRF = ' ' "
            sSql = sSql & "AND SHDOCO IN (SELECT distinct(SDDOCO) FROM " & My.Settings.SettingValue("E1Database") & ".F4211 WHERE SDURRF = ' ' AND SDNXTR > 526 AND SDNXTR <= 620 AND SDLTTR <> 980)"
            If Not (DbConn.ExecuteQuery(sSql, My.Settings.E1Conn)) Then Exit Sub

        Catch ex As Exception
            ErrObj.log(1, 0, "Set_Defualt_Movement", "Error Setting Default Movement to Order #.", ex)
            Exit Sub
        End Try
    End Sub

    Public Function Apply_DataRow_Defaults(ByRef rs As DataRow) As Boolean

        Try
            For Each dc As DataColumn In rs.Table.Columns
                Select Case Type.GetTypeCode(dc.DataType)
                    Case TypeCode.Decimal, TypeCode.Double, TypeCode.Int16, TypeCode.Int32, TypeCode.Int64, TypeCode.Single
                        rs(dc) = 0
                    Case TypeCode.String, TypeCode.Char
                        rs(dc) = " "
                    Case Else
                        ErrObj.log(1, 0, "Apply_DataRow_Defaults", "Unhandled data type for column:" & dc.ColumnName)
                End Select
            Next dc
        Catch ex As Exception
            ErrObj.log(1, 0, "Apply_DataRow_Defaults", "Error writing defaults to data row", ex)
            Return False
        End Try

        Return True
    End Function

    'This function copies the values in each column of a DataRow or Datareader to a destination datarow.
    'IMPORTANT - this function assumes the destination as columns names that exactly match the source column names
    Public Function Copy_DataRow_Fields(ByRef rsTo As DataRow, Optional ByRef rsFrom As DataRow = Nothing, Optional ByRef rdFrom As OdbcDataReader = Nothing) As Boolean
        Dim ColName As String = ""

        Try

            If rsFrom IsNot Nothing Then
                '*************************************THIS CODE IS FOR DATAROW AS SOURCE************************************
                'Cycle through columns in datarow
                For Each dc As DataColumn In rsFrom.Table.Columns
                    'Store column name in local variable for error handler and also to check if column with same name exists in destination datarow
                    ColName = dc.ColumnName

                    'Check to see if destination row has matching column name
                    If rsTo.Table.Columns(ColName).Equals(System.DBNull.Value) Then
                        'Assign source datarow column value to destination datarow column
                        rsTo(ColName) = rsFrom(dc)
                    End If
                Next dc
                '************************************************************************************************************
            ElseIf rdFrom IsNot Nothing Then
                '*************************************THIS CODE IS FOR DATAROW AS SOURCE************************************
                Dim ColNum As Integer

                'Cycle through column names in datareader
                For ColNum = 0 To rdFrom.FieldCount - 1
                    'Store column name in local variable for error handler and also to check if column with same name exists in destination datarow
                    ColName = rdFrom.GetName(ColNum)

                    'Check to see if destination row has matching column name
                    If rsTo.Table.Columns.Contains(ColName) Then
                        'Assign source datarow column value to destination datarow column
                        rsTo(ColName) = rdFrom.Item(ColNum)
                    End If
                Next ColNum
                '************************************************************************************************************
            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "Copy_DataRow_Fields", "Error copying data from column:" & ColName & " in table:" & rsFrom.Table.TableName & " to destination table:" & rsTo.Table.TableName, ex)
            Return False
        End Try

        Return True
    End Function

    Sub Get_FreightChargeAA(ByRef ChargeAA As Double, ByVal Shipnum As String, ByVal BatchNumber As String)
        Dim sSql As String
        Dim cnnE1 As OdbcConnection
        Dim E1Database As String = My.Settings.SettingValue("E1Database")

        'Open connection
        Try
            cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
            cnnE1.Open()
        Catch ex As Exception
            ErrObj.log(1, 0, "Get_FreightChargeAA", "Could not open E1 connection", ex)
            Exit Sub
        End Try


        Dim drF0911Z1 As OdbcDataReader
        Try
            'Lookup up Carrier Tax Rate and Tax Expl Code
            sSql = "SELECT SUM(VNAA) as ChargeAA FROM " & E1Database & ".F0911Z1 WHERE VNEDUS = 'ONENETWORK' AND VNEDTN = '" & Shipnum & "' AND VNEDBT = '" & BatchNumber & "' "
            drF0911Z1 = DbConn.GetDataReader(sSql, cnnE1)

            'see if we get any records back
            If drF0911Z1.HasRows Then
                'should on have one record so lets get it
                'drF0911Z1.Read()

                ChargeAA = Val(drF0911Z1("ChargeAA") & "")

                drF0911Z1.Close()
                drF0911Z1 = Nothing
            Else
                ErrObj.log(1, 0, "Get_FreightChargeAA", "There are NO Voucher Distribution amounts(VNAA) in F0911Z1 for the Voucher Header (F0411Z1) record with shipment#(EDTN):" & Shipnum)
            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "Get_FreightChargeAA", "Error retrieving Charge amount(VNAA) for shipment#(EDTN):" & Shipnum, ex)
            cnnE1.Close() : cnnE1.Dispose()
            Exit Sub
        End Try
        cnnE1.Close() : cnnE1.Dispose()
        Return
    End Sub

    Sub Carrier_TaxRateCode_Lookup(ByRef TXA1 As String, ByRef EXR1 As String, ByVal AN8 As Integer)
        Dim sSql As String
        Dim cnnE1 As OdbcConnection
        Dim E1Database As String = My.Settings.SettingValue("E1Database")

        'Set default value
        TXA1 = "" : EXR1 = ""

        'Open connection
        Try
            cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
            cnnE1.Open()
        Catch ex As Exception
            ErrObj.log(1, 0, "Carrier_TaxRateCode_Lookup", "Could not open E1 connection", ex)
            Exit Sub
        End Try


        Try
            'Lookup up Carrier Tax Rate and Tax Expl Code
            Dim drF0401 As OdbcDataReader
            sSql = "SELECT A6TXA2, A6EXR2 FROM " & E1Database & ".F0401 WHERE A6AN8 = " & AN8.ToString
            drF0401 = DbConn.GetDataReader(sSql, cnnE1)

            'see if we get any records back
            If drF0401.HasRows Then
                'should on have one record so lets get it
                drF0401.Read()

                TXA1 = drF0401("A6TXA2")
                EXR1 = drF0401("A6EXR2")

                drF0401.Close()
                drF0401 = Nothing
            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "Carrier_TaxRateCode_Lookup", "Error retrieving tax rate/tax expl code for carrier#:" & AN8.ToString, ex)
            Exit Sub
        End Try

    End Sub


    Sub Charge_Code_Lookup(ByRef OBJ As String, ByRef SUBAcct As String, ByVal ShipNum As String, ByVal ChargeName As String, ByVal DocType As String, ByVal ExpectedCurrency As String)
        Dim sSql As String
        Dim cnnOneNet As OdbcConnection

        OBJ = ""
        SUBAcct = ""

        'Open connection
        Try
            cnnOneNet = DbConn.CreateConnection(My.Settings.SettingValue("OneNetworkConn"))
            cnnOneNet.Open()
        Catch ex As Exception
            ErrObj.log(1, 0, "Charge_Code_Lookup", "Could not open OneNetwork connection", ex)
            Exit Sub
        End Try

        Try
            'Use ChargeName to look up DistId in Shippingcharges -> Use DistId to lookup Acct, SubAcct in Distribution table
            Dim drShipCharge As OdbcDataReader

            sSql = "SELECT Acct, SubAcct FROM ShippingCharges INNER JOIN Distribution ON Distribution.[ID] = SHIPPINGCHARGES.DistId WHERE ShippingCharges.Chargename = '" & ChargeName & "' AND Distribution.DocType = '" & DocType.Trim & "' AND Distribution." & ExpectedCurrency & " = 1"
            drShipCharge = DbConn.GetDataReader(sSql, cnnOneNet)

            'see if we get any records back
            If Not (drShipCharge Is Nothing) Then
                If drShipCharge.HasRows Then
                    'should on have one record so lets get it
                    drShipCharge.Read()

                    OBJ = drShipCharge("Acct")
                    SUBAcct = drShipCharge("SubAcct")

                    drShipCharge.Close()
                    drShipCharge = Nothing
                End If
            End If

        Catch ex As Exception
            ErrObj.log(1, 0, "Charge_Code_Lookup", "Error retrieving Charge Codes for Order:" & ShipNum & " CostComponent#:" & ChargeName & " Currency:" & ExpectedCurrency, ex)
            cnnOneNet.Close() : cnnOneNet.Dispose()
            Exit Sub
        End Try

    End Sub


    Public Function Get_AP_Batch_Number(ByVal DemandLocation As String) As Integer
        Dim BatchNumber As Object
        Dim BatchDate As Date
        Dim SSQL As String = ""

        BatchNumber = 0
        'Check to see if a BatchNumber already exists for today for this particular location
        Try

            BatchDate = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))
            BatchDate = CDate(BatchDate.ToShortDateString())
            SSQL = "SELECT Max(BatchNo) as BatchNo FROM BatchNo_Log WHERE BatchType = 'AP' AND DemandLocation = '" & DemandLocation & "' AND BatchDate = '" & BatchDate.ToString("MM/dd/yyyy") & "'"
            BatchNumber = DbConn.ExecuteScalar(SSQL, My.Settings.SettingValue("OneNetworkConn"))
            If BatchNumber.Equals(System.DBNull.Value) Then BatchNumber = 0
        Catch ex As Exception
            ErrObj.log(1, 0, "Get_AP_Batch_Number", "Error Retrieveing AP Batch Number from BatchNo_Log", ex)
            Return 0
        End Try

        'If Batchnumber exists already in BatchNo_Log (above step) then Check the F0411Z1 table to see if any entries with the retrieved batch number exist.  
        'If not then entries with above BatchNumber have been posted already and we need a new batch no
        Try
            If BatchNumber > 0 Then
                'if a batchnumber is no returned then set the current BatchNumber = 0 so this will trigger the next step to retrieve and advance the next number
                SSQL = "select * from openquery(E1_PROD_ORA, 'SELECT VLEDBT FROM " & My.Settings.SettingValue("E1Database") & ".F0411Z1  WHERE ROWNUM <= 1 AND VLEDSP = 1 AND VLEDBT = ''" & BatchNumber.ToString & "'' ') "
                If Val(DbConn.ExecuteScalar(SSQL, My.Settings.E1Conn)) = Val(BatchNumber) Then BatchNumber = 0
                'If Val(DbConn.ExecuteScalar(SSQL, My.Settings.E1Conn) & "") = 0 Then BatchNumber = 0
            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "Get_AP_Batch_Number", "Error Checking existence of  AP Batch Number (VLEDBT) in F0411Z1", ex)
            Return 0
        End Try

        'if BatchNumber <= 0 then the Batch_Log table does not already have a number we can use today for this demandlocation
        'Now get next number for AP Batch and then write this new Batch number to BatchNo_Log so this number can be used for other Payments processed today for this demand locations
        If BatchNumber <= 0 Then
            Try
                'Get batch number from next numbers table in oracle and advance counter
                BatchNumber = GetE1_NextNumber_AdvanceNumber("00", "NNN006", True)
            Catch ex As Exception
                ErrObj.log(1, 0, "Get_AP_Batch_Number", "Error getting/advancing AP next number", ex)
                Return 0
            End Try

            Try
                Dim da As Odbc.OdbcDataAdapter = Nothing

                'Write this new batch number to BatchNo_Log table so all payments for this run will have the same batch number
                If DbConn.GetUpdateableDataAdapter("SELECT * FROM BatchNo_Log WITH(NOLOCK) WHERE 1=0 ", My.Settings.SettingValue("OneNetworkConn"), da) Then
                    Dim ds As New DataSet
                    Dim dr As DataRow

                    'open dataset 
                    da.Fill(ds)

                    'If dataset is nothing then there's a problem and we can't write this number to the BatchNo_Log table
                    If da Is Nothing Then
                        Return 0
                    End If

                    'create new row
                    dr = ds.Tables(0).NewRow

                    'Get current date from SQL server
                    BatchDate = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))
                    BatchDate = CDate(BatchDate.ToShortDateString())

                    'populate fields
                    With dr
                        .Item("BatchType") = "AP"
                        .Item("DemandLocation") = DemandLocation
                        .Item("BatchDate") = BatchDate
                        .Item("BatchNo") = BatchNumber
                    End With

                    'add to dataset
                    ds.Tables(0).Rows.Add(dr)

                    'save changes
                    da.Update(ds)
                    ds.AcceptChanges()

                    ds.Dispose()
                    da.Dispose()
                End If
            Catch ex As Exception
                ErrObj.log(1, 0, "Get_AP_Batch_Number", "Error Writing AP next number to BatchNo_Log", ex)
                Return 0
            End Try
        End If
        Return BatchNumber
    End Function

    'Writes Error Log to AP_BatchError_Log table when an error is encountered processing payment files
    Private Sub Write_AP_BatchError_Log(ByVal OneNetFileID As Integer, ByVal MovementNo As String, ByVal ErrorMessage As String, ByVal CriticalError As Byte, Optional ByVal ShowOnReport As Byte = 0)
        Dim BatchDate As Date

        Try
            Dim da As Odbc.OdbcDataAdapter = Nothing

            'Write this new batch number to BatchNo_Log table so all payments today will have the same batch number
            If DbConn.GetUpdateableDataAdapter("SELECT * FROM AP_BatchError_Log WITH(NOLOCK) WHERE 1=0 ", My.Settings.SettingValue("OneNetworkConn"), da) Then
                Dim ds As New DataSet
                Dim dr As DataRow

                BatchDate = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))

                'open dataset 
                da.Fill(ds)

                'If dataset is nothing then there's a problem and we can't write this number to the BatchNo_Log table
                If da IsNot Nothing Then

                    'create new row
                    dr = ds.Tables(0).NewRow

                    'populate fields
                    With dr
                        .Item("DateProcessed") = BatchDate
                        .Item("OneNet_FileID") = OneNetFileID
                        .Item("Movement") = MovementNo
                        .Item("Message") = ErrorMessage
                        .Item("Critical") = CriticalError
                        .Item("ShowOnReport") = ShowOnReport
                    End With

                    'add to dataset
                    ds.Tables(0).Rows.Add(dr)

                    'save changes
                    da.Update(ds)
                    ds.AcceptChanges()
                End If

                ds.Dispose()
                da.Dispose()
            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "Write_AP_BatchError_Log", "Error Writing AP_BatchError_Log For Movement:" & MovementNo & " FileID: " & OneNetFileID.ToString & " Message: " & ErrorMessage, ex)
        End Try

    End Sub


    Function GetE1_NextNumber_AdvanceNumber(ByVal Key As String, ByVal Index As String, ByRef AdvanceCounter As Boolean) As Integer
        Dim cnnE1 As OdbcConnection = Nothing
        Dim txn As OdbcTransaction
        Dim E1Database_CTL As String = My.Settings.SettingValue("E1Database_CTL")
        Dim ssql As String
        Dim NextNum As Integer = -1

        Dim da_NextNumber As OdbcDataAdapter = Nothing

        'Open connection
        Try
            cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
            cnnE1.Open()
        Catch ex As Exception
            ErrObj.log(1, 0, "GetE1_NextNumber_AdvanceNumber", "Could not open connection", ex)
            Return -1
        End Try

        txn = cnnE1.BeginTransaction

        Try
            ssql = "Select * from " & E1Database_CTL & ".F0002 WHERE NNSY = '00'"
            DbConn.GetUpdateableDataAdapter(ssql, cnnE1, da_NextNumber, txn)

            If da_NextNumber IsNot Nothing Then
                Dim ds_NextNumber As New DataSet
                Dim dr_NextNumber As DataRow

                'Only continue if we got a record back
                If da_NextNumber.Fill(ds_NextNumber) > 0 Then

                    'get first row, we should only get back one anyway
                    dr_NextNumber = ds_NextNumber.Tables(0).Rows(0)

                    'get current value of next number
                    NextNum = dr_NextNumber.Item(Index)

                    'IF advance counter flag is set the increment counter and update 
                    If AdvanceCounter Then
                        'Increment Next Number value
                        dr_NextNumber.Item(Index) = dr_NextNumber.Item(Index) + 1

                        'save changes
                        da_NextNumber.Update(ds_NextNumber)
                        ds_NextNumber.AcceptChanges()
                    End If

                    txn.Commit()
                    'cleanup
                    da_NextNumber.Dispose()
                    ds_NextNumber.Dispose()

                End If
            Else
                ErrObj.log(1, 0, "GetE1_NextNumber_AdvanceNumber", "Next number does not exist for Key:" & Key & " AND Index:" & Index, , Key & "-" & Index)
            End If
        Catch ex As Exception
            txn.Rollback()
            If NextNum > -1 Then
                ErrObj.log(1, 0, "GetE1_NextNumber_AdvanceNumber", "Could not increment next number for Key:" & Key & " AND Index:" & Index, ex)
            Else
                ErrObj.log(1, 0, "GetE1_NextNumber_AdvanceNumber", "Could not retrieve info from next numbers table (F0002) ", ex)
            End If
            cnnE1.Close() : cnnE1.Dispose()
            Return NextNum
        End Try

        cnnE1.Close()
        cnnE1.Dispose()

        Return NextNum
    End Function


    Function GetE1_MastBOL_NextNumber(ByVal MovementNo As String) As Integer
        Dim cnnE1 As OdbcConnection = Nothing
        Dim txn As OdbcTransaction
        Dim E1Database_CTL As String = My.Settings.SettingValue("E1Database_CTL")
        Dim ssql As String
        Dim NextNum As Integer = -1

        Dim da_NextNumber As OdbcDataAdapter = Nothing

        'Open connection
        Try
            cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
            cnnE1.Open()
        Catch ex As Exception
            ErrObj.log(1, 0, "GetE1_NextNumber_AdvanceNumber", "Could not open connection", ex)
            Return -1
        End Try

        txn = cnnE1.BeginTransaction

        Try
            ssql = "Select Max( from " & E1Database_CTL & ".F5547003 WHERE NNSY = '00'"
            DbConn.GetUpdateableDataAdapter(ssql, cnnE1, da_NextNumber, txn)

            'If da_NextNumber IsNot Nothing Then
            '    Dim ds_NextNumber As New DataSet
            '    Dim dr_NextNumber As DataRow

            '    'Only continue if we got a record back
            '    If da_NextNumber.Fill(ds_NextNumber) > 0 Then

            '        'get first row, we should only get back one anyway
            '        dr_NextNumber = ds_NextNumber.Tables(0).Rows(0)

            '        'get current value of next number
            '        NextNum = dr_NextNumber.Item(Index)

            '        'IF advance counter flag is set the increment counter and update 
            '        If AdvanceCounter Then
            '            'Increment Next Number value
            '            dr_NextNumber.Item(Index) = dr_NextNumber.Item(Index) + 1

            '            'save changes
            '            da_NextNumber.Update(ds_NextNumber)
            '            ds_NextNumber.AcceptChanges()
            '        End If

            '        txn.Commit()
            '        'cleanup
            '        da_NextNumber.Dispose()
            '        ds_NextNumber.Dispose()

            '    End If
            'Else
            '    ErrObj.log(1, 0, "GetE1_NextNumber_AdvanceNumber", "Next number does not exist for Key:" & Key & " AND Index:" & Index, , Key & "-" & Index)
            'End If
        Catch ex As Exception
            txn.Rollback()
            If NextNum > -1 Then
                ErrObj.log(1, 0, "GetE1_NextNumber_AdvanceNumber", "Could not increment next number for MovementNo:" & MovementNo, ex)
            Else
                ErrObj.log(1, 0, "GetE1_NextNumber_AdvanceNumber", "Could not retrieve info from next numbers table (F5547003) ", ex)
            End If
            cnnE1.Close() : cnnE1.Dispose()
            Return NextNum
        End Try

        cnnE1.Close()
        cnnE1.Dispose()

        Return NextNum
    End Function

    'Looks up the MCU code in F0006 to determine its a custom plant - if the CO field is '00291' then its a custom plant and all custom charges are sent to '2900001' so that becomes the new MCU
    'Also if the OBJ (object account) > = 4000 then the MCU is a 7 digit number (trailing zeroes) - so 200 would be 2000000
    '           - However if the MCU is 291 and OBJ >= 4000 then MCU becomes 2900001
    'IF the OBJ < 4000 then MCU remains as is - so 200 remains 200

    Function GetE1_ShipCharge_MCU(ByRef tempMfgLoc As String, ByVal VNOBJ As String) As String
        Dim E1Database As String = My.Settings.SettingValue("E1Database")
        Dim ssql As String = ""
        Dim F0006_MCCO As String = ""
        Dim MfgLoc As String = ""

        MfgLoc = Val(tempMfgLoc).ToString
        Try
            'Look up MCCO value in F0006 based on MCMCU field
            ssql = "SELECT MCCO FROM " & E1Database & ".F0006 WHERE TRIM(MCMCU) = '" & MfgLoc & "' "
            F0006_MCCO = Trim(DbConn.ExecuteScalar(ssql, My.Settings.E1Conn))

            If F0006_MCCO = "" Then
                'If MCCO is blank then generate Error since location code should be set up in F0006
                ErrObj.log(1, 0, "GetE1_ShipCharge_MCU", "No record exists in F0006 for MCU:" & MfgLoc)
            ElseIf Val(F0006_MCCO) = 291 Then
                'Else if MCCO is 291 then the new Manufac Location becomes 290 or 2900001 determined by the OBJ
                If Val(VNOBJ) >= 4000 Then
                    MfgLoc = "2900001"
                Else
                    MfgLoc = "290"
                End If
            Else
                'Else Manufac Location is not 291 and is not blank so it does not change
                'Now we need to check if traling zeroes need to be added based on OBJ
                If Val(VNOBJ) >= 4000 Then    'for 200 add trailing zeroes to make it 2000000 for example
                    MfgLoc = CStr(Val(F0006_MCCO)) & "0000"
                    'MfgLoc = tempMfgLoc & "0000"
                End If
            End If

        Catch ex As Exception
            ErrObj.log(1, 0, "GetE1_ShipCharge_MCU", "Error retrieving MCCO from F0006 for MCU:" & MfgLoc, ex)
        End Try

        Return MfgLoc
    End Function

    '
    'Name: ProcessModShipNotif
    'Description: process shipment modification information
    'Return: ProcessReturnType
    '
    'Parameters:
    '
    'xmlStr - xml text to process
    '
    'Function ProcessModShipNotif(ByVal ModShipNot As ShipmentListMessage, ByVal fileId As Integer, Optional ByRef refno As String = Nothing) As ProcessReturnType
    '    Dim ssql As String
    '    Dim cnnTracker As OdbcConnection
    '    Dim oUdfs As New Udfs

    '    Try

    '        cnnTracker = DbConn.CreateConnection(My.Settings.SettingValue("TrackerConn"))
    '        cnnTracker.Open()

    '    Catch ex As Exception
    '        ErrObj.log(1, 0, "ProcessModShipNotification", "Could not deserializer xml", ex)
    '        Return ProcessReturnType.Fail
    '    End Try

    '    Dim tmpShipmentNum As String
    '    Dim orderNo As String = Nothing
    '    Dim demand As String = Nothing

    '    Dim txn As OdbcTransaction

    '    'loop through the shipment
    '    For Each oShipment As Shipment In ModShipNot.Shipment

    '        Dim trackerShipmentNumber As String = Nothing

    '        'assign temporary shipment number
    '        tmpShipmentNum = oShipment.ShipmentNumber

    '        '**** TRAFFICINFORMATIONTABLE *****
    '        Dim daTraffic As OdbcDataAdapter = Nothing
    '        Dim dsTraffic As New DataSet
    '        Dim drTraffic As DataRow

    '        'parse shipment number
    '        If Not ParseShipmentNumber(tmpShipmentNum, orderNo, demand) Then
    '            Return ProcessReturnType.Fail
    '        End If

    '        'begin my transaction
    '        txn = cnnTracker.BeginTransaction

    '        refno = Me.FormatReturnShipmentNumber(orderNo, demand)

    '        'get me my traffic information table
    '        ssql = "SELECT * FROM TrafficInformationTable WHERE DemandLocation = '" & demand & "' AND Order_no = " & orderNo

    '        If Not DbConn.GetUpdateableDataAdapter(ssql, cnnTracker, daTraffic, txn) Then
    '            txn.Rollback()
    '            ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to get OdbcDataAdapter")
    '            Return ProcessReturnType.Fail
    '        End If

    '        'see if we got any records back
    '        If daTraffic.Fill(dsTraffic) > 0 Then
    '            'assign to datarow we will be mapping
    '            drTraffic = dsTraffic.Tables(0).Rows(0)

    '            trackerShipmentNumber = drTraffic("ShipmentNumber")

    '            'map object to datarow
    '            If Not oMapping.MapObjectToDataRow(fileId, oShipment, drTraffic, "ModShipNotif.Traffic") Then
    '                txn.Rollback()
    '                ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map " & tmpShipmentNum)
    '                Return ProcessReturnType.Fail

    '            Else
    '                'commit our changes
    '                daTraffic.Update(dsTraffic)
    '                dsTraffic.AcceptChanges()

    '            End If

    '        Else
    '            ErrObj.log(2, 0, "ProcessModShipNot", "Could not find traffic info " & tmpShipmentNum)
    '        End If


    '        '**** FPSSHIPMENTMASTER ****
    '        If trackerShipmentNumber IsNot Nothing Then
    '            ssql = "SELECT * FROM FpsShipmentMaster WHERE DemandLocation = '" & demand & "' AND ShipmentNumber = " & trackerShipmentNumber
    '            Dim daFps As OdbcDataAdapter = Nothing
    '            Dim dsFps As New DataSet
    '            Dim drFps As DataRow

    '            If Not DbConn.GetUpdateableDataAdapter(ssql, cnnTracker, daFps, txn) Then
    '                txn.Rollback()
    '                Return ProcessReturnType.Fail
    '            End If

    '            'fill out dataset
    '            If daFps.Fill(dsFps) > 0 Then

    '                'fill our datarow
    '                drFps = dsFps.Tables(0).Rows(0)

    '                'map object to datarow
    '                If Not oMapping.MapObjectToDataRow(fileId, oShipment, drFps, "ModShipNotif.Fps") Then
    '                    txn.Rollback()
    '                    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map FPS " & trackerShipmentNumber)
    '                    Return ProcessReturnType.Fail
    '                Else
    '                    daFps.Update(dsFps)
    '                    dsFps.AcceptChanges()
    '                End If

    '            Else
    '                ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to fill FPS dataset " & trackerShipmentNumber)
    '            End If

    '            'cleanup
    '            daFps.Dispose()
    '            dsFps.Dispose()

    '        End If

    '        'do the ReasonCodes
    '        If oShipment.Items IsNot Nothing Then
    '            Dim oRc As Udfs.ReasonCode
    '            oRc = oUdfs.ParseReasonCode(oShipment.FreightControlledBySystem, oShipment.Items(0))
    '            If oRc IsNot Nothing Then
    '                oUdfs.InsertReasonCode(demand, orderNo, oRc.mfgloc, oRc.code, oRc.shipvia, oShipment.LastModifiedUser)
    '            End If
    '        End If

    '        'cleanup
    '        daTraffic.Dispose()
    '        dsTraffic.Dispose()

    '        'commit changes
    '        txn.Commit()

    '        'we only want the first one, so get out after, in the future we may need to turn it off
    '        Exit For

    '    Next

    '    Return ProcessReturnType.Success

    'End Function


    'sbains E1 version of ProcessModShipNotif function
    'Function ProcessModShipNotif_E1_History_DONOTUSE(ByVal ModShipNot As ShipmentListMessage, ByVal fileId As Integer, ByRef Orders() As Agent.Order_Type, Optional ByRef refno As String = Nothing) As ProcessReturnType

    '    Dim ssql As String
    '    Dim cnnE1 As OdbcConnection
    '    Dim oUdfs As New Udfs
    '    Dim E1Database As String = My.Settings.SettingValue("E1Database")

    '    Try
    '        cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
    '        cnnE1.Open()
    '    Catch ex As Exception
    '        ErrObj.log(1, 0, "ProcessModShipNotification", "Could not deserializer xml", ex)
    '        Return ProcessReturnType.Fail
    '    End Try

    '    Dim tmpShipmentNum As String
    '    Dim orderNo As String = Nothing
    '    Dim demand As String = Nothing
    '    Dim tmpOrder(1) As Order_Type

    '    Dim txn As OdbcTransaction

    '    'loop through the shipment
    '    For Each oShipment As ShipmentListMessageShipment In ModShipNot.Shipment

    '        'assign temporary shipment number
    '        tmpShipmentNum = oShipment.ShipmentNumber

    '        'parse shipment number
    '        If Not ParseShipmentNumber(tmpShipmentNum, orderNo, demand) Then
    '            Dispose_ODBC_Connection(cnnE1)
    '            Return ProcessReturnType.Fail
    '        End If

    '        'begin my transaction
    '        txn = cnnE1.BeginTransaction

    '        refno = Me.FormatReturnShipmentNumber(orderNo, demand)

    '        '**** SALES HEADER TABLE VARIABLES*****
    '        Dim daSalesHeader As OdbcDataAdapter = Nothing
    '        Dim dsSalesHeader As New DataSet
    '        Dim drSalesHeader As DataRow
    '        '**** SALES DETAIL TABLE VARIABLES*****
    '        Dim daSalesDetail As OdbcDataAdapter = Nothing
    '        Dim dsSalesDetail As New DataSet

    '        'GET SALES HEADER RECORDS 
    '        ssql = "SELECT * FROM " & E1Database & ".F4201 WHERE SHDOCO =" & orderNo
    '        If Not DbConn.GetUpdateableDataAdapter(ssql, cnnE1, daSalesHeader, txn) Then
    '            txn.Rollback()
    '            ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to get Sales Header OdbcDataAdapter")
    '            Dispose_ODBC_Connection(cnnE1)
    '            Return ProcessReturnType.Fail
    '        End If

    '        ''If there is a matching record in Sales Order Header then also check Sales Order Detail for matching orders
    '        If daSalesHeader.Fill(dsSalesHeader) > 0 Then

    '            'GET SALES DETAIL RECORDS 
    '            ssql = "SELECT * FROM " & E1Database & ".F4211 WHERE SDDOCO =" & orderNo

    '            If Not DbConn.GetUpdateableDataAdapter(ssql, cnnE1, daSalesDetail, txn) Then
    '                txn.Rollback()
    '                ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to get Sales Detail OdbcDataAdapter")
    '                Dispose_ODBC_DataAdapter(daSalesHeader)
    '                Dispose_ODBC_Connection(cnnE1)
    '                Return ProcessReturnType.Fail
    '            End If

    '        Else
    '            'GET SALES HEADER HISTORY: If no matches in sales header then check sales header history
    '            ssql = "SELECT * FROM " & E1Database & ".F42019 WHERE SHDOCO =" & orderNo
    '            If Not DbConn.GetUpdateableDataAdapter(ssql, cnnE1, daSalesHeader, txn) Then
    '                txn.Rollback()
    '                ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to get Sales Header History OdbcDataAdapter")
    '                Dispose_ODBC_Connection(cnnE1)
    '                Return ProcessReturnType.Fail
    '            End If

    '            If daSalesHeader.Fill(dsSalesHeader) > 0 Then
    '                'GET SALES DETAIL HISTORY : If Sales Header History has a match then get sales detail history as well 
    '                ssql = "SELECT * FROM " & E1Database & ".F42119 WHERE SDDOCO =" & orderNo

    '                If Not DbConn.GetUpdateableDataAdapter(ssql, cnnE1, daSalesDetail, txn) Then
    '                    txn.Rollback()
    '                    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to get Sales Detail OdbcDataAdapter")
    '                    Dispose_ODBC_DataAdapter(daSalesHeader)
    '                    Dispose_ODBC_Connection(cnnE1)
    '                    Return ProcessReturnType.Fail
    '                End If
    '            End If
    '        End If

    '        '******************************************POPULATE SALES HEADER DATASET******************************************************************************
    '        If daSalesHeader IsNot Nothing Then
    '            'assign to datarow we will be mapping
    '            drSalesHeader = dsSalesHeader.Tables(0).Rows(0)

    '            'map object to datarow
    '            If Not oMapping.MapObjectToDataRow(fileId, oShipment, drSalesHeader, "ModShipNotif.SalesHeader", , My.Settings.InterfaceType) Then
    '                txn.Rollback()
    '                ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Header " & tmpShipmentNum)
    '                Dispose_ODBC_Connection(cnnE1)
    '                Return ProcessReturnType.Fail

    '            Else
    '                'commit our changes
    '                daSalesHeader.Update(dsSalesHeader)
    '                dsSalesHeader.AcceptChanges()

    '            End If
    '        Else
    '            ErrObj.log(2, 0, "ProcessModShipNot", "Could not find Sales Header info in Current or History " & tmpShipmentNum)
    '            Dispose_ODBC_Connection(cnnE1)
    '            Return ProcessReturnType.Skip
    '        End If


    '        '******************************************POPULATE SALES DETAIL DATASET******************************************************************************
    '        'see if we got any records back
    '        If daSalesDetail.Fill(dsSalesDetail) > 0 Then
    '            'assign to datarow we will be mapping
    '            For Each drSalesDetail As DataRow In dsSalesDetail.Tables(0).Rows

    '                'map object to datarow
    '                If Not oMapping.MapObjectToDataRow(fileId, oShipment, drSalesDetail, "ModShipNotif.SalesDetail", , My.Settings.InterfaceType) Then
    '                    txn.Rollback()
    '                    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Detail " & tmpShipmentNum)
    '                    Dispose_ODBC_DataAdapter(daSalesHeader)
    '                    Dispose_ODBC_DataAdapter(daSalesDetail)
    '                    Dispose_ODBC_Connection(cnnE1)
    '                    Return ProcessReturnType.Fail

    '                Else
    '                    'commit our changes
    '                    daSalesDetail.Update(dsSalesDetail)
    '                    dsSalesDetail.AcceptChanges()

    '                End If
    '            Next
    '        Else
    '            ErrObj.log(2, 0, "ProcessModShipNot", "Could not find Sales Detail info in Current or History " & tmpShipmentNum)
    '            Dispose_ODBC_DataAdapter(daSalesHeader)
    '            Dispose_ODBC_Connection(cnnE1)
    '            Return ProcessReturnType.Skip
    '        End If


    '        'do the ReasonCodes
    '        If oShipment.Items IsNot Nothing Then
    '            Dim oRc As Udfs.ReasonCode
    '            oRc = oUdfs.ParseReasonCode(oShipment.FreightControlledBySystem, oShipment.Items(0))
    '            If oRc IsNot Nothing Then
    '                oUdfs.InsertReasonCode(demand, orderNo, oRc.mfgloc, oRc.code, oRc.shipvia, oShipment.LastModifiedUser)
    '            End If
    '        End If

    '        'cleanup
    '        Dispose_ODBC_DataAdapter(daSalesHeader)
    '        Dispose_ODBC_DataAdapter(daSalesDetail)

    '        'commit changes
    '        txn.Commit()

    '        'Add orders to order array so we can modify the checksum value
    '        tmpOrder(1).Demand_Location = demand
    '        tmpOrder(1).Order_Number = orderNo
    '        Add_to_Orders_Array(Orders, tmpOrder)

    '        'clean up
    '        txn.Dispose()

    '        'we only want the first one, so get out after, in the future we may need to turn it off
    '        Exit For

    '    Next

    '    'Clean Up
    '    Dispose_ODBC_Connection(cnnE1)
    '    Return ProcessReturnType.Success

    'End Function


    'sbains E1 version of ProcessModShipNotif function
    Function ProcessModShipNotif_E1(ByVal ModShipNot As FGIIntegModshipnot, ByVal fileId As Integer, ByRef Orders() As Agent.Order_Type, Optional ByRef refno As String = Nothing) As ProcessReturnType


        Dim ssql As String
        Dim cnnE1 As OdbcConnection
        Dim oUdfs As New Udfs
        Dim E1Database As String = My.Settings.SettingValue("E1Database")

        Try
            cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
            cnnE1.Open()
        Catch ex As Exception
            ErrObj.log(1, 0, "ProcessModShipNotification", "Could not deserialize xml", ex)
            Return ProcessReturnType.Fail

        End Try

        Dim tmpShipmentNum As String
        Dim orderNo As String = Nothing
        Dim demand As String = Nothing
        Dim tmpOrder(1) As Order_Type

        Dim txn As OdbcTransaction

        'loop through the shipment
        ' For Each oShipment As ShipmentListMessageShipment In ModShipNot.Shipment
        Dim oshipment As FGIIntegModshipnotShipmentListMessageShipment = ModShipNot.ShipmentListMessage.Shipment

        'assign temporary shipment number
        tmpShipmentNum = oshipment.ShipmentNumber

        'parse shipment number
        If Not ParseShipmentNumber(tmpShipmentNum, orderNo, demand) Then
            Dispose_ODBC_Connection(cnnE1)
            Return ProcessReturnType.Fail
        End If

        'Insert/Update ShipWithGroupRef value for this order in the F554202x table
        Process_ShipWithGroupRef(orderNo, Trim(oshipment.ShipWithGroupRef & ""), fileId, cnnE1, E1Database)

        'begin my transaction
        txn = cnnE1.BeginTransaction

        refno = Me.FormatReturnShipmentNumber(orderNo, demand)

        '**** SALES HEADER TABLE VARIABLES*****
        Dim daSalesHeader As OdbcDataAdapter = Nothing
        Dim dsSalesHeader As New DataSet
        Dim drSalesHeader As DataRow

        'GET SALES HEADER RECORDS 
        ssql = "SELECT * FROM " & E1Database & ".F4201 WHERE SHDOCO =" & orderNo
        'ssql = "SELECT * FROM " & E1Database & ".F4201 WHERE LTRIM(SHKCOO) = '" & demand & "' AND SHDOCO =" & orderNo

        If Not DbConn.GetUpdateableDataAdapter(ssql, cnnE1, daSalesHeader, txn) Then
            txn.Rollback()
            ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to get Sales Header OdbcDataAdapter")
            Dispose_ODBC_Connection(cnnE1)
            Return ProcessReturnType.Fail
        End If

        'see if we got any records back
        If daSalesHeader.Fill(dsSalesHeader) > 0 Then
            'assign to datarow we will be mapping
            drSalesHeader = dsSalesHeader.Tables(0).Rows(0)

            'map object to datarow
            If Not oMapping.MapObjectToDataRow(fileId, oshipment, drSalesHeader, "ModShipNotif.SalesHeader", , My.Settings.InterfaceType) Then
                txn.Rollback()
                ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Header " & tmpShipmentNum)
                Dispose_ODBC_Connection(cnnE1)
                Return ProcessReturnType.Fail

            Else
                'commit our changes

                daSalesHeader.Update(dsSalesHeader)
                dsSalesHeader.AcceptChanges()

            End If

        Else
            ErrObj.log(2, 0, "ProcessModShipNot", "Could not find Sales Header info " & tmpShipmentNum)
            Dispose_ODBC_Connection(cnnE1)
            Return ProcessReturnType.Skip
        End If


        '**** SALES DETAIL TABLE VARIABLES*****
        Dim daSalesDetail As OdbcDataAdapter = Nothing
        Dim dsSalesDetail As New DataSet
        'Dim drSalesDetail As DataRow

        'GET SALES DETAIL RECORDS 
        ssql = "SELECT * FROM " & E1Database & ".F4211 WHERE SDDOCO =" & orderNo

        If Not DbConn.GetUpdateableDataAdapter(ssql, cnnE1, daSalesDetail, txn) Then
            txn.Rollback()
            ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to get Sales Detail OdbcDataAdapter")
            Dispose_ODBC_DataAdapter(daSalesHeader)
            Dispose_ODBC_Connection(cnnE1)
            Return ProcessReturnType.Fail
        End If

        'see if we got any records back
        If daSalesDetail.Fill(dsSalesDetail) > 0 Then
            'assign to datarow we will be mapping
            For Each drSalesDetail As DataRow In dsSalesDetail.Tables(0).Rows

                'map object to datarow
                If Not oMapping.MapObjectToDataRow(fileId, oshipment, drSalesDetail, "ModShipNotif.SalesDetail", , My.Settings.InterfaceType) Then
                    txn.Rollback()
                    ErrObj.log(1, 0, "ProcessModShipNotif", "Failed to map Sales Detail " & tmpShipmentNum)
                    Dispose_ODBC_DataAdapter(daSalesHeader)
                    Dispose_ODBC_DataAdapter(daSalesDetail)
                    Dispose_ODBC_Connection(cnnE1)
                    Return ProcessReturnType.Fail

                Else
                    'commit our changes


                    daSalesDetail.Update(dsSalesDetail)
                    dsSalesDetail.AcceptChanges()

                End If
            Next
        Else
            ErrObj.log(2, 0, "ProcessModShipNot", "Could not find Sales Detail info " & tmpShipmentNum)
            Dispose_ODBC_DataAdapter(daSalesHeader)
            Dispose_ODBC_Connection(cnnE1)
            Return ProcessReturnType.Skip
        End If


        'do the ReasonCodes
        If oshipment.ShipmentHeaderMDFs IsNot Nothing Then
            Dim oRc As Udfs.ReasonCode
            Dim reasoncode_index As Integer = 0
            Dim x_index As Integer = 0

            'For x_index = 0 To oshipment.ShipmentHeaderMDFs.GetUpperBound(0)
            '    If oshipment.Items(x_index).GetType.Name = "ShipmentHeaderMDFs2" Then
            '        reasoncode_index = x_index
            '        Exit For
            '    End If
            ' Next x_index

            oRc = oUdfs.ParseReasonCode_E1(oshipment.FreightControlledBySystem, oshipment.ShipmentHeaderMDFs)
            If oRc IsNot Nothing Then
                oUdfs.InsertReasonCode(demand, orderNo, oRc.mfgloc, oRc.code, oRc.shipvia, oshipment.LastModifiedUser)
            End If
        End If

        'cleanup
        Dispose_ODBC_DataAdapter(daSalesHeader)
        Dispose_ODBC_DataAdapter(daSalesDetail)

        'commit changes

        txn.Commit()

        'Add orders to order array so we can modify the checksum value
        tmpOrder(1).Demand_Location = demand
        tmpOrder(1).Order_Number = orderNo
        Add_to_Orders_Array(Orders, tmpOrder)

        'clean up
        txn.Dispose()

        'we only want the first one, so get out after, in the future we may need to turn it off
        'Exit For

        ' Next

        'Clean Up
        Dispose_ODBC_Connection(cnnE1)
        Return ProcessReturnType.Success

    End Function


    '
    'Name: ParseShipmentNumber
    'Description: parse shipment number as they come back from OneNetwork
    'Return: String array
    '
    'Parameters:
    '
    'shipmentNumber - shipment # string
    '
    Public Shared Function ParseShipmentNumber(ByVal shipmentNumber As String, ByRef orderNo As String, ByRef demand As String) As Boolean

        Dim retShipmentNum() As String
        Dim arrShipNum() As String
        Const CONSOLIDATED_PREFIX As String = "M"
        Const CONSOLIDATED_DELIM As String = "-"
        Dim Temp_OrderNo As String = ""

        'init some vars
        retShipmentNum = Nothing

        'check for integrity stuff
        'Try
        'too short, we need at least 1 character
        If shipmentNumber.Length < 1 Then
            Return False
        End If

        'check for an 'm' prefix as it can happending during a consolidated/unconsolidated shipment
        If shipmentNumber(0).ToString().Equals(CONSOLIDATED_PREFIX) Then

            'split the string with dash as delimiter, this should give us 3 elements back
            '1st - "M"
            '2nd - Order #
            '3rd - Demand Location
            arrShipNum = shipmentNumber.Split(CONSOLIDATED_DELIM)

            'check to see if we get 3 elements back, the 2nd element should be the shipmentnumber
            If arrShipNum.Length = 3 Then
                'do some more integ checking, make sure elements 1 & 3 are numeric
                If IsNumeric(arrShipNum(1)) And IsNumeric(arrShipNum(2)) Then
                    retShipmentNum = arrShipNum
                Else
                    retShipmentNum = Nothing
                End If

            Else
                'error, we should get 3 elements back
                retShipmentNum = Nothing
            End If

        Else
            'no 'M' prefix, then the format should just Order # - Demand Location
            arrShipNum = shipmentNumber.Split(CONSOLIDATED_DELIM)

            'check if we got 2 elements back
            If arrShipNum.Length = 2 Then
                'check for valid numeric values
                If IsNumeric(arrShipNum(0)) And IsNumeric(arrShipNum(1)) Then
                    'assign return value var w/ temp parsed var value
                    retShipmentNum = arrShipNum
                Else
                    'both numbers not numeric, set ret value var to nothing
                    retShipmentNum = Nothing
                End If
            Else
                'error, length of array not 2, return nothing
                retShipmentNum = Nothing
            End If
        End If

        'check if we got an array back
        If retShipmentNum Is Nothing Then
            Return False
        Else
            If retShipmentNum.Length = 2 Then
                'get first array element, which should be the order #
                orderNo = retShipmentNum(0)
                demand = retShipmentNum(1)
                'Call function to convert the demand code if necessary
                demand = Convert_SiteRef(demand, "INBOUND", orderNo.ToString, 5, False)

                'If in Migration testing mode
                'If UCase(My.Settings.InterfaceType) = "E1 TO ONENETWORK" Then
                '    'sbains Remap Macola Ordernum to E1 Ordernum - TEMPORARY until swith is made to E1
                '    'Temp_OrderNo = Trim(DbConn.ExecuteScalar("SELECT E1_OrderNum FROM E1_OrderNum_Lookup WHERE Macola_OrderNum = '" & orderNo & "' ", My.Settings.SettingValue("OneNetworkConn")))
                '    'If Temp_OrderNo <> "" And Not (Temp_OrderNo Is Nothing) Then orderNo = Temp_OrderNo

                '    'sbains Remap demand loction - This is only temporary until Onenetwork has the updated demand location codes
                '    If demand = "15" Then
                '        demand = "00100"
                '    ElseIf demand = "55" Then
                '        demand = "00200"
                '    End If
                'End If
            ElseIf retShipmentNum.Length = 3 Then
                orderNo = retShipmentNum(1)
                demand = retShipmentNum(2)
                'Call function to convert the demand code if necessary
                demand = Convert_SiteRef(demand, "INBOUND", orderNo.ToString, 5, False)

                'If in Migration testing mode
                'If UCase(My.Settings.InterfaceType) = "E1 TO ONENETWORK" Then
                '    'sbains Remap Macola Ordernum to E1 Ordernum - TEMPORARY until swith is made to E1
                '    'Temp_OrderNo = Trim(DbConn.ExecuteScalar("SELECT E1_OrderNum FROM E1_OrderNum_Lookup WHERE Macola_OrderNum = '" & orderNo & "' ", My.Settings.SettingValue("OneNetworkConn")))
                '    'If Temp_OrderNo <> "" And Not (Temp_OrderNo Is Nothing) Then orderNo = Temp_OrderNo
                '    'sbains Remap demand loction - This is only temporary until Onenetwork has the updated demand location codes
                '    If demand = "15" Then
                '        demand = "00100"
                '    ElseIf demand = "55" Then
                '        demand = "00200"
                '    End If
                'End If
            Else
                'catch all, this should not happen
                ErrObj.log(1, 0, "ParseShipmentNumber", "Invalid ShipNum array length", , retShipmentNum.ToString())
                Return False

            End If

        End If
        'Catch ex As Exception
        '    Stop
        'End Try

        'return value var
        Return True

    End Function
    '
    'Name: ProcessConsolidationNotification
    'Description: process combine shipment xml from OneNetwork
    'Return: boolean
    '
    'Parameters:
    '
    'xmlFile - xml file from OneNetwork
    '
    'Function ProcessConsolidationNotification(ByVal FGCombine As FGIIntegConsolidationNotification, Optional ByRef refNo As String = Nothing) As ProcessReturnType

    '    Dim retResult As Boolean

    '    'determine course of action
    '    Select Case FGCombine.ActionCode
    '        Case "Add", "Remove"
    '            refNo = FGCombine.CurrentMovementShipmentInfo.MasterShipmentNumber

    '            'call combine shipment function
    '            retResult = MergeShipment(FGCombine.ActionCode, FGCombine.PreviousMovementShipmentInfo, FGCombine.CurrentMovementShipmentInfo)

    '    End Select

    '    'cleanup
    '    FGCombine = Nothing

    '    If retResult Then
    '        Return ProcessReturnType.Success
    '    Else
    '        Return ProcessReturnType.Fail
    '    End If

    'End Function

    'sbains  This is the E1 version of ProcessConsolidationNotification function
    Function ProcessConsolidationNotification_E1(ByVal FGCombine As FGIIntegConsolidationNotification, ByRef Orders() As Agent.Order_Type, Optional ByRef refNo As String = Nothing) As ProcessReturnType

        Dim retResult As Boolean

        'determine course of action'
        If FGCombine.ActionCode = "Delete" Then FGCombine.ActionCode = "Remove"
        Select Case FGCombine.ActionCode
            Case "Add", "Remove"
                refNo = FGCombine.CurrentMovementShipmentInfo.MasterShipmentNumber

                'call combine shipment function

                retResult = MergeShipment_E1(FGCombine.ActionCode, Nothing, FGCombine.CurrentMovementShipmentInfo, Orders)


        End Select

        'cleanup
        FGCombine = Nothing

        If retResult Then
            Return ProcessReturnType.Success
        Else
            Return ProcessReturnType.Fail
        End If

    End Function

    'Function RemoveShipment(ByRef prevMovementInfo As MovementShipmentInfo, ByRef curMovementInfo As MovementShipmentInfo) As Boolean 'OneNetwork
    'Function RemoveShipment(ByRef prevMovementInfo As FGIIntegConsolidationNotificationCurrentMovementShipmentInfo, ByRef curMovementInfo As FGIIntegConsolidationNotificationCurrentMovementShipmentInfo) As Boolean 'ALC

    '    Dim masterShipmentNumber As String = Nothing
    '    Dim masterDemand As String = Nothing

    '    Dim demandloc As String = ""
    '    Dim ssql As String

    '    Dim cnnTracker As OdbcConnection

    '    Dim deleteAll As Boolean = False
    '    Dim prevShipNumList, curShipNumList As New ArrayList

    '    If prevMovementInfo.ShipmentInfo Is Nothing Then
    '        ErrObj.log(1, 0, "RemoveShipment", "Movement info passed is Nothing")
    '        Return False
    '    Else
    '        'build list of old ship #
    '        For Each shipInfo As ShipmentInfo In prevMovementInfo.ShipmentInfo
    '            prevShipNumList.Add(shipInfo.ShipmentNumber)
    '        Next
    '    End If

    '    'check to see if the resulting movement info is empty
    '    If curMovementInfo.ShipmentInfo Is Nothing Then
    '        'it is empty, this means they have completely dismantled this combined shipment
    '        deleteAll = True
    '    Else
    '        'build list of new ship #'s that will stay combined (this will exclude the one's being removed)
    '        For Each shipInfo As ShipmentInfo In curMovementInfo.ShipmentInfo
    '            curShipNumList.Add(shipInfo.ShipmentNumber)
    '        Next
    '    End If

    '    'see if were able to parse a valid shipmentnumber
    '    If Not ParseShipmentNumber(prevMovementInfo.MasterShipmentNumber, masterShipmentNumber, masterDemand) Then
    '        'nope...fail
    '        ErrObj.log(1, 0, "RemoveShipment", "Could not parse ShipNo", , prevMovementInfo.MasterShipmentNumber)
    '        Return False
    '    End If

    '    'open db connection
    '    cnnTracker = DbConn.CreateConnection(My.Settings.SettingValue("TrackerConn"))
    '    cnnTracker.Open()

    '    'begin transaction
    '    Dim txn As OdbcTransaction
    '    txn = cnnTracker.BeginTransaction

    '    'PART 1 - get me the master shipment first
    '    ssql = "SELECT * FROM FpsShipmentmaster WHERE DemandLocation = " & masterDemand & " AND ShipmentNumber = " & masterShipmentNumber
    '    Dim daMaster As OdbcDataAdapter
    '    Dim cmd As OdbcCommand
    '    Dim cmbMaster As OdbcCommandBuilder
    '    Dim dsMaster As New DataSet

    '    Try
    '        cmd = New OdbcCommand(ssql, cnnTracker, txn)
    '        daMaster = New OdbcDataAdapter(cmd)
    '        cmbMaster = New OdbcCommandBuilder(daMaster)
    '        If daMaster.Fill(dsMaster) = 0 Then
    '            ErrObj.log(1, 0, "RemoveShipment", "Could not find Master Shipment. ShipNo " & masterShipmentNumber & ",Demand " & masterDemand)
    '            Return False

    '        End If
    '    Catch ex As Exception
    '        'could not find master fpsshipmentmaster
    '        ErrObj.log(1, 0, "RemoveShipment", "Error getting dataset for Master Shipment. ShipNo " & masterShipmentNumber & ",Demand " & masterDemand, ex)
    '        Return False
    '    End Try

    '    'PART 2 - build a list of all shipments to remove
    '    Dim removeOrders As New ArrayList
    '    Dim childShipment As String = Nothing
    '    Dim childOrderNo As String = Nothing
    '    Dim childDemand As String = Nothing

    '    childDemand = ""

    '    Dim i As Integer

    '    'look for the number
    '    For i = 0 To prevShipNumList.ToArray().Length - 1
    '        Dim indexLoc As Integer

    '        'see if we can find the prev ship num in the cur ship num
    '        childShipment = prevShipNumList.ToArray().GetValue(i)

    '        If Not ParseShipmentNumber(childShipment, childOrderNo, childDemand) Then
    '            ErrObj.log(1, 0, "Removeshipment", "error parsing child shipnum", , childOrderNo)
    '            Return False
    '        End If

    '        'make sure this order is not equal to master ship number
    '        If childOrderNo <> masterShipmentNumber Then
    '            'check deleteall flag, if raised, all orders need to be unconsolidated
    '            If deleteAll Then
    '                removeOrders.Add(childOrderNo)
    '            Else
    '                'find order # in array
    '                indexLoc = System.Array.IndexOf(curShipNumList.ToArray(), childShipment)
    '                If indexLoc = -1 Then
    '                    removeOrders.Add(childOrderNo)
    '                End If
    '            End If
    '        End If

    '    Next

    '    'PART 3 - now get all orders compiled in TrafficInformationTable
    '    Dim daChild As OdbcDataAdapter
    '    Dim dsChild As New DataSet
    '    Dim strOrderList As String = ""

    '    Try

    '        'build list of Orders that will be removed from shipment
    '        strOrderList = Join(removeOrders.ToArray(), ",")

    '        If strOrderList Is Nothing Then
    '            'nothing to process
    '            ErrObj.log(1, 0, "RemoveShipment", "No child ship to process", , masterShipmentNumber)
    '            Return False
    '        End If

    '        ssql = "SELECT * FROM TrafficInformationTable WHERE DemandLocation = " & childDemand & _
    '            " AND ShipmentNumber = " & masterShipmentNumber & _
    '            " AND ORDER_NO in (" & strOrderList & ")"

    '        Dim cmdChild As OdbcCommand = New OdbcCommand(ssql, cnnTracker, txn)
    '        daChild = New OdbcDataAdapter(cmdChild)
    '        Dim cmbChild As New OdbcCommandBuilder(daChild)

    '        If Not daChild.Fill(dsChild) = removeOrders.Count Then
    '            'orders to remove not equal to orders in system
    '            txn.Rollback()
    '            ErrObj.log(1, 0, "RemoveShipment", "Not all orders to be removed can be found in Tracker for orderNo ", , masterShipmentNumber)
    '            Return False
    '        End If

    '    Catch ex As Exception
    '        txn.Rollback()
    '        ErrObj.log(1, 0, "RemoveShipment", "Error removing orders " & strOrderList, ex)
    '        Return False

    '    End Try

    '    'PART 4 - now loop through all the shipments to remove
    '    For Each drChild As DataRow In dsChild.Tables(0).Rows

    '        'process each shipment to remove

    '        'update the master shipment record this child was a part of, recalc values accordingly
    '        'the -1 operand indicates we are subtracting the child values from the original master shipment values
    '        If Not RecalcShipment(dsMaster, drChild, -1) Then
    '            txn.Rollback()
    '            ErrObj.log(1, 0, "RemoveShipment", "Error with RecalcShipment call. ShipNo " & masterShipmentNumber & ",Demand " & masterDemand)
    '            Return False
    '        End If

    '        'make the child it's own master by changing the shipment number to it's own order #
    '        drChild.Item("ShipmentNumber") = drChild("ORDER_NO")

    '        '-- TBD - NEED TO CREATE NEW SHIPMENTMASTER RECORD ---

    '        'update changes to child dataset (which should now be it's own master)
    '        Try
    '            daChild.Update(dsChild)
    '            dsChild.AcceptChanges()

    '        Catch ex As Exception
    '            txn.Rollback()
    '            ErrObj.log(1, 0, "RemoveShipment", "Error updating child shipment. Order # " & drChild("ORDER_NO") & ", Demand " & childDemand, ex)
    '            Return False

    '        End Try

    '    Next

    '    'PART 5 - now update the master shipment
    '    Try
    '        daMaster.Update(dsMaster)
    '        dsMaster.AcceptChanges()

    '    Catch ex As Exception
    '        txn.Rollback()
    '        ErrObj.log(1, 0, "RemoveShipment", "Could not update master dataset. ShipNo " & prevMovementInfo.MasterShipmentNumber, ex)
    '        Return False
    '    End Try

    '    'commit transaction when no error
    '    txn.Commit()

    '    Return True

    'End Function
    ''FGIIntegConsolidationNotificationCurrentMovementShipmentInfo
    ' Function CombineShipment(ByRef prevMovementInfo As MovementShipmentInfo, ByRef curMovementInfo As MovementShipmentInfo) As Boolean 'OneNetwork
    'Function CombineShipment(ByRef prevMovementInfo As FGIIntegConsolidationNotificationCurrentMovementShipmentInfo, ByRef curMovementInfo As FGIIntegConsolidationNotificationCurrentMovementShipmentInfo) As Boolean 'ALC

    '    Dim masterShipmentNumber As String

    '    Dim OneNetworkOrderList As New ArrayList
    '    Dim demandLoc As String = ""

    '    Dim cnnTracker As OdbcConnection

    '    '***************** PART 1 - BUILD LIST OF ORDERS *****************

    '    For Each shipInfo As ShipmentInfo In curMovementInfo.ShipmentInfo

    '        Dim orderNo As String = ""

    '        'split string delimited by dash
    '        If Not ParseShipmentNumber(shipInfo.ShipmentNumber, orderNo, demandLoc) Then
    '            Return False
    '        End If

    '        'append first element, which is the order # to the array
    '        OneNetworkOrderList.Add(orderNo)

    '    Next

    '    'check to see if we added orders to shipment
    '    If OneNetworkOrderList.ToArray.Length = 0 Then
    '        'no shipments to combine, get out
    '        Return False
    '    End If

    '    '***************** PART 2 - DATABASE UPDATES *****************

    '    cnnTracker = DbConn.CreateConnection(My.Settings.SettingValue("TrackerConn"))
    '    cnnTracker.Open()

    '    'get the master shipmentnumber
    '    masterShipmentNumber = curMovementInfo.MasterShipmentNumber

    '    Dim masterOrderNo As String = Nothing
    '    Dim masterDemand As String = Nothing

    '    'need to parse masterShipmentNumber which will be in the format Order#-Demand (eg. 757831-55)
    '    If Not ParseShipmentNumber(masterShipmentNumber, masterOrderNo, masterDemand) Then
    '        'parsing error, get out
    '        Return False
    '    End If

    '    'now build a string of Order #'s that we will be using for the sql query
    '    Dim csvOrders As String
    '    csvOrders = Join(OneNetworkOrderList.ToArray(), ",")

    '    'look for all Tracker shipment that will be combined
    '    Dim ssql As String
    '    Dim dsTraffic As New DataSet

    '    'get all orders in OneNetwork file that will be consolidated. Fill result into dataset.
    '    ssql = "SELECT * FROM TrafficInformationTable WHERE Order_no in (" & csvOrders & ") AND DemandLocation = " & demandLoc
    '    Dim trafficCount As Integer
    '    trafficCount = DbConn.GetDataSet(ssql, cnnTracker, dsTraffic)

    '    'make sure the the orders in the OneNetwork file can be found in Tracker
    '    'if OneNetwork sends us 3 orders that need to be consolidated, we need to find those 3 orders in Tracker
    '    If trafficCount = OneNetworkOrderList.Count Then

    '        'begin my transaction
    '        Dim trackerTxn As OdbcTransaction
    '        trackerTxn = cnnTracker.BeginTransaction(IsolationLevel.ReadCommitted)

    '        Dim dsFpsMaster As New DataSet
    '        Dim daFpsMaster As OdbcDataAdapter
    '        Dim cmd As OdbcCommand

    '        'get master's fpsshipmentmaster dataset so we can do stuff w/ it when come time to combine stuff
    '        ssql = "SELECT * FROM FpsShipmentMaster WHERE DemandLocation = " & masterDemand & " AND ShipmentNumber = " & masterOrderNo
    '        cmd = New OdbcCommand(ssql, cnnTracker, trackerTxn)
    '        daFpsMaster = New OdbcDataAdapter(cmd)
    '        Dim cmb As New OdbcCommandBuilder(daFpsMaster)

    '        Try
    '            'check to see if we found the master fpsshipmentmaster record
    '            If Not daFpsMaster.Fill(dsFpsMaster) > 0 Then
    '                'couldn't find master fpsshipmentmaster record, not good, rollback
    '                trackerTxn.Rollback()
    '                ErrObj.log(1, 0, "CombineShipment", "Could not find fpsmaster. ShipNo " & masterShipmentNumber)
    '                Return False
    '            End If

    '        Catch ex As Exception
    '            trackerTxn.Rollback()
    '            ErrObj.log(1, 0, "Error executing sql", ssql, ex, masterShipmentNumber)
    '            Return False

    '        End Try

    '        Dim removeChildFps As New ArrayList

    '        'loop through all the trafficinformationtable rows
    '        For Each drTraffic As DataRow In dsTraffic.Tables(0).Rows

    '            Dim childShipmentNumber As String
    '            Dim childDemandLoc As String

    '            childShipmentNumber = drTraffic("ShipmentNumber")
    '            childDemandLoc = drTraffic("DemandLocation")

    '            'change only TrafficInfo records that are not the same as the masterShipmentNumber
    '            If childShipmentNumber = masterOrderNo Then
    '                'this is the master shipment, don't do anything w/ this
    '            Else

    '                'check to see if shipment combineable
    '                Dim retOrderType As OrderReturnType
    '                retOrderType = IsCombineable(drTraffic)

    '                'if either a master or got invalid back, do a rollback
    '                If retOrderType = OrderReturnType.Master Or retOrderType = OrderReturnType.Invalid Then
    '                    'rollback, order not combineable
    '                    trackerTxn.Rollback()
    '                    ErrObj.log(1, 0, "CombineShipment", "This Order Cannot be combined. ShipNo " & childShipmentNumber & "-" & childDemandLoc)
    '                    Return False
    '                End If

    '                'passed our acid test, then proceed to combine w/ master

    '                'integrate child fpsshipmentmaster w/ master's fpsshipmentmaster
    '                If RecalcShipment(dsFpsMaster, drTraffic) Then

    '                    Try

    '                        'build list of children to remove later
    '                        removeChildFps.Add(childShipmentNumber)

    '                        'update child traffic with new shipmentnumber of master
    '                        ssql = "UPDATE TrafficInformationTable SET  ShipmentNumber = " & masterOrderNo & _
    '                                " WHERE DemandLocation = " & masterDemand & _
    '                                " AND ShipmentNumber = " & childShipmentNumber

    '                        Dim cmdTraffic As OdbcCommand
    '                        cmdTraffic = New OdbcCommand(ssql, cnnTracker, trackerTxn)

    '                        'now execute both queries in one shot
    '                        cmdTraffic.ExecuteNonQuery()

    '                        cmdTraffic.Dispose()

    '                    Catch ex As Exception
    '                        trackerTxn.Rollback()
    '                        ErrObj.log(1, 0, "CombineShipment", "Could not delete FpsShipmentMaster", , childShipmentNumber)
    '                        Return False

    '                    End Try

    '                Else
    '                    'couldn't integrate, rollback
    '                    trackerTxn.Rollback()
    '                    ErrObj.log(1, 0, "CombineShipment", "Failed to recalc vaues for ShipNo " & masterShipmentNumber)
    '                    Return False
    '                End If

    '            End If

    '        Next

    '        'cleanup child shipments that were combined w/ another shipment
    '        Try
    '            If removeChildFps.ToArray().Length = 0 Then
    '                trackerTxn.Rollback()
    '                ErrObj.log(1, 0, "CombineShipment", "No children to remove", , masterShipmentNumber)
    '                Return False
    '            End If

    '            Dim strChildFps As String = Join(removeChildFps.ToArray(), ",")

    '            'now we have to delete the fpsshipmentmaster of this traffic order, because it is part of another shipment now
    '            ssql = "DELETE FROM FpsShipmentMaster WHERE DemandLocation = " & masterDemand & _
    '                    " AND ShipmentNumber IN (" & strChildFps & ")"
    '            Dim cmdChildFpsRemove As New OdbcCommand(ssql, cnnTracker, trackerTxn)

    '            Dim recDeleted As Integer

    '            recDeleted = cmdChildFpsRemove.ExecuteNonQuery()

    '            'make sure we deleted the same number of records as the list of array shipments we compiled
    '            'otherwise, it is an error
    '            If recDeleted <> removeChildFps.ToArray.Length Then
    '                trackerTxn.Rollback()
    '                ErrObj.log(1, 0, "CombineShipment", "records affected " & recDeleted & " <> child list " & removeChildFps.ToArray.Length)
    '                Return False
    '            End If

    '        Catch ex As Exception
    '            trackerTxn.Rollback()
    '            ErrObj.log(1, 0, "CombineShipment", "Error cleaning up child fps records")
    '            Return False

    '        End Try

    '        Try
    '            'update via dataadapter, and accept changes
    '            daFpsMaster.Update(dsFpsMaster)
    '            dsFpsMaster.AcceptChanges()

    '        Catch ex As Exception
    '            trackerTxn.Rollback()
    '            ErrObj.log(1, 0, "CombineShipment", "Failed to update final step. ShipNo " & masterShipmentNumber, ex)
    '            Return False

    '        End Try

    '        'everything is ok, commit changes
    '        trackerTxn.Commit()

    '    Else
    '        'number of rows returned not equal to number of orders parsed in file, this is an error
    '        ErrObj.log(1, 0, "CombineShipment", "File Integrity Error. OneNetwork Order count is " & OneNetworkOrderList.Count _
    '                & ", Tracker only found " & dsTraffic.Tables(0).Rows.Count)
    '        Return False
    '    End If

    '    'exit
    '    Return True

    'End Function

    Private Function IsCombineable(ByRef drTraffic As DataRow) As OrderReturnType
        Dim sSql As String

        Dim cnnTracker As OdbcConnection

        'open db connection
        cnnTracker = DbConn.CreateConnection(My.Settings.SettingValue("TrackerConn"))
        If cnnTracker Is Nothing Then
            Return OrderReturnType.Invalid
        End If

        'open connection
        cnnTracker.Open()

        'do some integ checking
        Dim curShipmentNumber = drTraffic("ShipmentNumber")
        Dim curDemand = drTraffic("DemandLocation")

        'check to see if this order is a master shipment by looking at the fpsshipmentmaster
        Dim drFps As OdbcDataReader
        sSql = "SELECT * FROM FpsShipmentMaster where DemandLocation = " & curDemand & " AND ShipmentNumber = " & curShipmentNumber
        drFps = DbConn.GetDataReader(sSql, cnnTracker)

        'see if we get an fpsshipmentmaster record back
        If drFps Is Nothing Then
            'integ error, no fpsshipmentmaster found for this order
            Return OrderReturnType.Invalid

        ElseIf Not drFps.HasRows Then
            drFps.Close()

            'integ error, no fpsshipmentmaster found for this order
            Return OrderReturnType.Invalid
        End If

        'read first record
        drFps.Read()

        Dim orderNo, shipmentNumber As String
        Dim totalOrder As Integer

        Try
            shipmentNumber = drTraffic("ShipmentNumber")
            orderNo = drTraffic("Order_no")
            totalOrder = drFps("TotalNumberOrders")

            drFps.Close()
        Catch ex As Exception
            Return OrderReturnType.Invalid
        End Try


        If orderNo = shipmentNumber And totalOrder = 1 Then
            'this is a loner
            Return OrderReturnType.Individual

        ElseIf orderNo = shipmentNumber And totalOrder > 1 Then
            'this is a master order, we can't mess around with this one.
            Return OrderReturnType.Master

        ElseIf orderNo <> shipmentNumber And totalOrder > 1 Then
            'this is a child order, ok to mess around with
            Return OrderReturnType.Child
        Else
            ErrObj.log(1, 0, "IsCombineable", "Check Exception " & orderNo & ", ShipNo " & shipmentNumber & ",TotOrder " & totalOrder)
            Return OrderReturnType.Invalid
        End If

        'shouldn't really get here, but just in case
        Return OrderReturnType.Invalid

    End Function

    Private Function RecalcShipment(ByRef masterShipment As DataSet, ByRef childTraffic As DataRow, Optional ByVal operand As Integer = 1) As Boolean

        Dim drMaster As DataRow
        Dim objUomConv As New UomConversion
        Dim retVal As Double

        Dim childTotalWeight, childTotalVolume As Double
        Dim childWeightUom, childVolumeUom, shipmentWeightUom, shipmentVolumeUom As String

        drMaster = masterShipment.Tables(0).Rows(0)

        'now update the master shipment
        With drMaster

            'assign to temp vars
            childTotalWeight = childTraffic("TOTAL_WEIGHT")
            childTotalVolume = childTraffic("TotalVolume")
            childWeightUom = childTraffic("WeightUOM")
            childVolumeUom = childTraffic("VolumeUOM")
            shipmentWeightUom = .Item("ShipmentWeightUOM")
            shipmentVolumeUom = .Item("ShipmentVolumeUOM")

            'INCREMENT WEIGHT
            If childWeightUom = shipmentWeightUom Then
                retVal = childTraffic("TOTAL_WEIGHT")
            Else
                'attempt to convert uom
                If Not objUomConv.ConvertUOM(childWeightUom, shipmentWeightUom, CDbl(childTotalWeight), retVal) Then
                    'conversion failed, leave at 0
                    retVal = 0
                End If
            End If
            .Item("ShipmentWeight") += retVal * operand

            'INCREMENT VOLUME
            If childVolumeUom = shipmentVolumeUom Then
                retVal = childTraffic("TotalVolume")
            Else
                'attempt to convert uom
                If Not objUomConv.ConvertUOM(childVolumeUom, shipmentVolumeUom, CDbl(childTotalVolume), retVal) Then
                    'conversion failed, leave at 0
                    retVal = 0
                End If
            End If
            .Item("ShipmentVolume") += retVal * operand

            'INCREMENT PALLETS
            .Item("ShipmentPallets") += childTraffic("TotalPallets") * operand

            'INCREMENT ORDERS
            .Item("TotalNumberOrders") += 1 * operand

            'UPDATE AUDIT FIELDS
            Dim sqlDateTime As DateTime
            sqlDateTime = DbConn.GetDateSqlServer(My.Settings.SettingValue("TrackerConn"))

            .Item("MaintenanceDateStamp") = sqlDateTime
            .Item("MaintenanceTimeStamp") = sqlDateTime
            .Item("MaintenanceUserID") = My.Settings.MaintUserId

        End With

        'cleanup
        objUomConv = Nothing

        Return True
    End Function

    'Function MergeShipment(ByVal actionCode As String, ByVal prevMove As MovementShipmentInfo, ByVal curMove As MovementShipmentInfo) As Boolean

    '    Dim ssql As String
    '    Dim cnntracker As OdbcConnection

    '    Dim newMasterNum As String = ""

    '    Dim txn As OdbcTransaction

    '    cnntracker = DbConn.CreateConnection(My.Settings.SettingValue("TrackerConn"))
    '    cnntracker.Open()

    '    Dim prevOrderList As New ArrayList
    '    Dim newOrderList As New ArrayList

    '    Dim tmpShipNum, tmpMasterDemand As String
    '    Dim masterMovement As String = Nothing

    '    'init
    '    tmpShipNum = Nothing
    '    tmpMasterDemand = Nothing

    '    'record all old
    '    If prevMove.ShipmentInfo IsNot Nothing Then
    '        For Each prevShip As ShipmentInfo In prevMove.ShipmentInfo

    '            'parse shipmentnumber
    '            If ParseShipmentNumber(prevShip.ShipmentNumber, tmpShipNum, tmpMasterDemand) Then
    '                prevOrderList.Add(tmpShipNum)
    '            Else
    '                ErrObj.log(1, 0, "MergeShipment", "error parsing shipnum " & prevShip.ShipmentNumber)
    '                Return False
    '            End If
    '        Next
    '    End If


    '    'record all new
    '    If curMove.ShipmentInfo IsNot Nothing Then
    '        masterMovement = curMove.MasterShipmentNumber
    '        For Each curShip As ShipmentInfo In curMove.ShipmentInfo

    '            'parse the shipmentnumber
    '            If Not ParseShipmentNumber(curShip.ShipmentNumber, tmpShipNum, tmpMasterDemand) Then
    '                ErrObj.log(1, 0, "MergeShipment", "error parsing shipnum " & curShip.ShipmentNumber)
    '                Return False
    '            End If

    '            'if first one, then record as new master ship number
    '            If newMasterNum = "" Then
    '                newMasterNum = tmpShipNum
    '            End If

    '            newOrderList.Add(tmpShipNum)
    '        Next
    '    End If

    '    Dim i As Integer
    '    Dim oldArr As Object() = prevOrderList.ToArray
    '    Dim newArr As Object() = newOrderList.ToArray

    '    'begin transaction
    '    txn = cnntracker.BeginTransaction

    '    'this is the old shipment configuration
    '    If oldArr.Length > 0 Then
    '        'now delete any fpsshipmentmaster for all these orders
    '        ssql = "DELETE FROM FpsShipmentMaster WHERE DemandLocation = '" & tmpMasterDemand & "' AND ShipmentNumber in (" & Join(oldArr, ",") & ")"
    '        If Not DbConn.ExecuteQuery(ssql, cnntracker, , txn) Then
    '            txn.Rollback()
    '            Return False
    '        End If

    '        'reset all shipment number back for these respective orders
    '        ssql = "UPDATE TrafficInformationTable SET ShipmentNumber = Order_no" _
    '                        & " WHERE DemandLocation = '" & tmpMasterDemand & "' AND Order_No IN (" & Join(oldArr, ",") & ")"
    '        If Not DbConn.ExecuteQuery(ssql, cnntracker, , txn) Then
    '            txn.Rollback()
    '            Return False
    '        End If

    '        're-create fps records for the one's that were removed because they wouldn't have any now
    '        For i = 0 To oldArr.Length - 1

    '            'get trafficinfo data
    '            ssql = "SELECT * FROM TrafficInformationTable WHERE DemandLocation = '" & tmpMasterDemand & "' AND Order_No = " & oldArr.GetValue(i)
    '            Dim da As OdbcDataAdapter = Nothing
    '            If Not DbConn.GetUpdateableDataAdapter(ssql, cnntracker, da, txn) Then
    '                txn.Rollback()
    '                Return False
    '            End If

    '            Dim dsTraffic As New DataSet
    '            If da.Fill(dsTraffic) > 0 Then
    '                'create new shipment, don't create for master shipment number (if exists)

    '                If oldArr.GetValue(i) <> newMasterNum Then
    '                    If Not CreateShipment(oldArr.GetValue(i), dsTraffic, cnntracker, txn) Then
    '                        txn.Rollback()
    '                        Return False
    '                    End If
    '                End If
    '            Else
    '                'could not find order, may have been deleted from tracker
    '            End If

    '        Next
    '    End If

    '    'this will be populated if the new shipment configuration is available
    '    If newArr.Length > 0 Then

    '        'delete all FPS for the new consolidated shipments, because we are going to end up w/ only one Master FPS record
    '        ssql = "DELETE FROM FpsShipmentMaster WHERE DemandLocation = '" & tmpMasterDemand & "' AND ShipmentNumber in (" & Join(newArr, ",") & ")"
    '        If Not DbConn.ExecuteQuery(ssql, cnntracker, , txn) Then
    '            txn.Rollback()
    '            Return False
    '        End If

    '        'now update the one's that are part of the NEW shipment to the new master shipment number
    '        ssql = "UPDATE TrafficInformationTable SET ShipmentNumber = " & newMasterNum _
    '                    & " WHERE DemandLocation = '" & tmpMasterDemand & "' AND Order_No in (" & Join(newArr, ",") & ")"
    '        If Not DbConn.ExecuteQuery(ssql, cnntracker, , txn) Then
    '            txn.Rollback()
    '            Return False
    '        End If

    '        'look for the master shipment number
    '        ssql = "SELECT * FROM TrafficInformationTable WHERE DemandLocation = '" & tmpMasterDemand & "' AND Order_No = " & newMasterNum
    '        Dim dsMaster As New DataSet
    '        Dim daMaster As OdbcDataAdapter = Nothing

    '        If DbConn.GetUpdateableDataAdapter(ssql, cnntracker, daMaster, txn) Then

    '            'check to see if we got nothing back
    '            If daMaster.Fill(dsMaster) <= 0 Then
    '                ErrObj.log(1, 0, "OneShipments", "Could not find order", , newMasterNum)
    '                txn.Rollback()
    '                Return False
    '            End If

    '        Else
    '            txn.Rollback()
    '            Return False
    '        End If

    '        'create the master shipment record
    '        If Not CreateShipment(masterMovement, dsMaster, cnntracker, txn) Then
    '            txn.Rollback()
    '            Return False
    '        End If

    '        'get all traffic records that will be consolidated
    '        ssql = "SELECT * FROM TrafficInformationTable WHERE DemandLocation = '" & tmpMasterDemand & "' AND Order_No IN (" & Join(newArr, ",") & ")"
    '        Dim daNewShipment As OdbcDataAdapter = Nothing
    '        Dim dsNewShipment As New DataSet

    '        If DbConn.GetUpdateableDataAdapter(ssql, cnntracker, daNewShipment, txn) Then
    '            daNewShipment.Fill(dsNewShipment)
    '        Else
    '            txn.Rollback()
    '            Return False
    '        End If


    '        'get newly created fps record
    '        ssql = "SELECT * FROM FpsShipmentMaster WHERE DemandLocation = '" & tmpMasterDemand & "' AND ShipmentNumber = " & newMasterNum
    '        Dim daFpsMaster As OdbcDataAdapter = Nothing
    '        Dim dsFpsMaster As New DataSet

    '        If DbConn.GetUpdateableDataAdapter(ssql, cnntracker, daFpsMaster, txn) Then
    '            daFpsMaster.Fill(dsFpsMaster)
    '        Else
    '            txn.Rollback()
    '            Return False
    '        End If

    '        For Each dr As DataRow In dsNewShipment.Tables(0).Rows

    '            'do the recalc if not the master, because we already did this when we created the shipment record for it
    '            If Not dr("Order_No") = newMasterNum Then
    '                RecalcShipment(dsFpsMaster, dr, 1)
    '            End If

    '        Next

    '        daFpsMaster.Update(dsFpsMaster)
    '        dsFpsMaster.AcceptChanges()
    '    End If

    '    'commit
    '    txn.Commit()

    '    Return True

    'End Function

    'sbains NEW E1 MergeShipment function to replace previous Tracker version of MergeShipment
    'FGIIntegConsolidationNotificationCurrentMovementShipmentInfo
    'Function MergeShipment_E1(ByVal actionCode As String, ByVal prevMove As MovementShipmentInfo, ByVal curMove As MovementShipmentInfo, ByRef Orders() As Agent.Order_Type) As Boolean 'OneNetwork


    Function MergeShipment_E1(ByVal actionCode As String, ByVal prevMove As FGIIntegConsolidationNotificationCurrentMovementShipmentInfo, ByVal curMove As FGIIntegConsolidationNotificationCurrentMovementShipmentInfo, ByRef Orders() As Agent.Order_Type) As Boolean 'ALC
        Dim ssql As String
        Dim cnnE1 As OdbcConnection
        Dim E1Database As String = My.Settings.SettingValue("E1Database")
        cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
        cnnE1.Open()


        Dim newMasterNum As String = ""
        Dim txn As OdbcTransaction

        Dim prevOrderList As New ArrayList
        Dim newOrderList As New ArrayList
        Dim prev_masterMovement As String = Nothing

        Dim tmpShipNum, tmpMasterDemand As String
        Dim masterMovement As String = Nothing
        Dim tmpMasterMovement As String = Nothing

        'init
        tmpShipNum = Nothing
        tmpMasterDemand = Nothing

        Try
            
            'record all new
            If curMove.ShipmentInfo IsNot Nothing Then
                masterMovement = curMove.MasterShipmentNumber
                'FGIIntegConsolidationNotificationCurrentMovementShipmentInfoShipmentInfo
                ' For Each curShip As ShipmentInfo In curMove.ShipmentInfo'OneNetwork
                For Each curShip As FGIIntegConsolidationNotificationCurrentMovementShipmentInfoShipmentInfo In curMove.ShipmentInfo

                    'parse the shipmentnumber
                    If Not ParseShipmentNumber(curShip.ShipmentNumber, tmpShipNum, tmpMasterDemand) Then
                        ErrObj.log(1, 0, "MergeShipment", "error parsing shipnum " & curShip.ShipmentNumber)
                        Dispose_ODBC_Connection(cnnE1)
                        Return False
                    End If

                    'if first one, then record as new master ship number
                    If newMasterNum = "" Then
                        newMasterNum = tmpShipNum
                    End If

                    newOrderList.Add(tmpShipNum)
                Next
            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "MergeShipment_E1", "Error adding/removing shipment", ex, tmpMasterMovement)
            cnnE1.Close()
            cnnE1.Dispose()
            Return False
        End Try

        'Dim oldArr As Object() = prevOrderList.ToArray
        Dim newArr As Object() = newOrderList.ToArray
        'Dim E1_ship_no As Integer = -1       DELETE THIS IF XREF TABLE IS NOT USED 
        'Dim ShipNoObj As Object    DELETE THIS IF XREF TABLE IS NOT USED 


        'begin transaction
        txn = cnnE1.BeginTransaction
        Try

            
            'this will be populated if the new shipment configuration is available
            If newArr.Length > 0 Then
                tmpMasterMovement = masterMovement
                '***************************** DELETE THIS SEGMENT OF CODE IF URRF IS USED WITHOUT XREF TABLE ********************************
                

                ssql = "UPDATE " & E1Database & ".F4201 SET SHURRF = '" & masterMovement & "' WHERE SHDOCO in (" & Join(newArr, ",") & ")"
                If Not DbConn.ExecuteQuery(ssql, cnnE1, , txn) Then
                    txn.Rollback()
                    Dispose_ODBC_Connection(cnnE1)
                    Return False
                End If

                ssql = "UPDATE " & E1Database & ".F4211 SET SDURRF = '" & masterMovement & "' WHERE SDDOCO in (" & Join(newArr, ",") & ")"
                If Not DbConn.ExecuteQuery(ssql, cnnE1, , txn) Then
                    txn.Rollback()
                    Dispose_ODBC_Connection(cnnE1)
                    Return False
                End If

                'Call Marks Class to create MasterBOL


            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "MergeShipment_E1", "Error adding/removing shipment", ex, tmpMasterMovement)
            txn.Rollback()
            txn.Dispose()
            cnnE1.Close()
            cnnE1.Dispose()
            Return False
        End Try

        'commit
        txn.Commit()


        '******************************************MASTER BOL ADDITION - FEB 26, 2010*******************************************************
        Try
            'If oldArr.Length > 0 Then
            '    If OneMasterBol.HasMultipleOrders(prev_masterMovement) < 2 Then
            '        'Call Marks Class to Delete MasterBOL
            '        OneMasterBol.DeleteMovementMasterBol(prev_masterMovement)
            '    End If
            'End If

            If newArr.Length > 1 Then
                'masterMovement
                Dim MasterBOLno As Long = 0

                If OneMasterBol.GetMovementMasterBol(masterMovement) = 0 Then

                    MasterBOLno = OneMasterBol.GetNextMasterBol
                    If (OneMasterBol.InsertMasterBol(masterMovement, MasterBOLno)) <> 1 Then
                        ErrObj.log(1, 0, "MergeShipment_E1", "Error INSERTING MASTERBOL in to F5547003 for Movement#:" & masterMovement)
                    End If
                End If

            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "MergeShipment_E1", "Error Inserting/Delete Master BOL", ex, tmpMasterMovement)
            txn.Rollback()
            txn.Dispose()
            cnnE1.Close()
            cnnE1.Dispose()
            Return False
        End Try

        '*************************************************************************************************************************************

        '**********************************************Add Orders to Orders Array******************************************************
        Try
            Dim x As Integer
            'Copy orders in newArr to the tmpOrders_New array before we pass it to procedure
            If newArr.Length > 0 Then
                Dim tmpOrders_New() As Order_Type
                tmpOrders_New = Nothing
                For x = 1 To newArr.Length
                    ReDim Preserve tmpOrders_New(x)
                    tmpOrders_New(x).Demand_Location = tmpMasterDemand
                    tmpOrders_New(x).Order_Number = newArr(x - 1)

                Next
                'Add these orders we just updated to the Orders array
                Add_to_Orders_Array(Orders, tmpOrders_New)
            End If

            'Copy orders in oldArr to the tmpOrders_Old array before we pass it to procedure
            'If oldArr.Length > 0 Then
            '    Dim tmpOrders_Old() As Order_Type
            '    tmpOrders_Old = Nothing
            '    For x = 1 To oldArr.Length
            '        ReDim Preserve tmpOrders_Old(x)
            '        tmpOrders_Old(x).Demand_Location = tmpMasterDemand
            '        tmpOrders_Old(x).Order_Number = oldArr(x - 1)
            '    Next
            '    'Add these orders we just updated to the Orders array
            '    Add_to_Orders_Array(Orders, tmpOrders_Old)
            'End If
        Catch ex As Exception
            ErrObj.log(1, 0, "MergeShipment_E1", "Error adding/removing shipment", ex, tmpMasterMovement)
            txn.Dispose()
            cnnE1.Close()
            cnnE1.Dispose()
            Return False
        End Try
        '*********************************************************************************************************************************

        'Clean Up
        txn.Dispose()
        cnnE1.Close()
        cnnE1.Dispose()

        Return True

    End Function


    '
    'Name: MergeOrCancelShipment
    'Description: Merge or Cancel shipment post appointment for controlled or non-controlled
    'Return: N/A
    '
    'Parameters:
    '
    'apptNotif - appointment notification serialized object to process
    '
    'Function MergeOrCancelShipment(ByVal apptNotif As FGIIntegApptNotification, Optional ByRef SubReturnValue As ProcessReturnType = ProcessReturnType.Success) As Boolean

    '    Dim ssql As String
    '    Dim cnnTracker As OdbcConnection
    '    Dim txn As OdbcTransaction

    '    Dim oneMovementNo As String = Nothing

    '    Dim currentShipment As Shipment

    '    'open db connection
    '    Try
    '        cnnTracker = DbConn.CreateConnection(My.Settings.SettingValue("TrackerConn"))
    '        cnnTracker.Open()
    '    Catch ex As Exception
    '        ErrObj.log(1, 0, "MergeOrCancelShipment", "Could not open connection")
    '        Return False
    '    End Try

    '    'check for existence of shipment element
    '    If apptNotif.Shipment Is Nothing Then
    '        ErrObj.log(1, 0, "MergeOrCancelShipment", "Shipment element is missing")
    '        Return False
    '    End If

    '    currentShipment = apptNotif.Shipment(0)

    '    'found a record w/ the same movement #, we will have to drop the fpsshipmentmaster for this order
    '    Dim tmpShipment As String = Nothing
    '    Dim tmpDemand As String = Nothing

    '    If Not ParseShipmentNumber(currentShipment.ShipmentNumber, tmpShipment, tmpDemand) Then
    '        Return False
    '    End If

    '    'get traffic info record
    '    ssql = "SELECT * FROM TrafficInformationTable WHERE DemandLocation = '" & tmpDemand & "' AND Order_no = " & tmpShipment
    '    Dim dsTraffic As New DataSet
    '    Dim drTraffic As DataRow
    '    If DbConn.GetDataSet(ssql, cnnTracker, dsTraffic) > 0 Then
    '        drTraffic = dsTraffic.Tables(0).Rows(0)
    '    Else
    '        ssql = "SELECT * FROM TrafficHistoryTable WHERE DemandLocation = '" & tmpDemand & "' AND Order_no = " & tmpShipment
    '        If DbConn.GetDataSet(ssql, cnnTracker, dsTraffic) > 0 Then
    '            'this is already in history, just let it go through and log an error
    '            ErrObj.log(1, 0, "MergeOrCancelShipemnt", "Shipment is already in history " & currentShipment.ShipmentNumber, , currentShipment.ShipmentNumber)
    '            Return True
    '        Else
    '            ErrObj.log(1, 0, "MergeOrCancelShipment", "Could not find trafficinfo in live or history - " & currentShipment.ShipmentNumber, , currentShipment.ShipmentNumber)
    '            SubReturnValue = ProcessReturnType.Skip  'set this to skip because this order is probably deleted
    '            Return False
    '        End If

    '    End If

    '    Dim daMaster As OdbcDataAdapter = Nothing
    '    Dim dsMaster As New DataSet


    '    'check to see if this is a pickup order first
    '    If currentShipment.FreightControlledBySystem Then
    '        ssql = "SELECT * FROM FpsShipmentMaster WHERE DemandLocation = '" & tmpDemand & "' AND ShipmentNumber = " & drTraffic("ShipmentNumber")
    '        Dim rdrFpsMaster As OdbcDataReader
    '        rdrFpsMaster = DbConn.GetDataReader(ssql, cnnTracker)

    '        If rdrFpsMaster.HasRows Then
    '            If Not rdrFpsMaster("OneMovementNo") Is System.DBNull.Value Then
    '                oneMovementNo = rdrFpsMaster("OneMovementNo")
    '            Else

    '                'need to close existing reader, or it will cause an issue
    '                rdrFpsMaster.Close()

    '                'movement # is null for this order, let's go look for the MasterShipmentNumber provided in file
    '                ssql = "SELECT * FROM FpsShipmentMaster WHERE DemandLocation = '" & tmpDemand & "' AND OneMovementNo = '" & currentShipment.MasterShipmentNumber & "'"
    '                rdrFpsMaster = DbConn.GetDataReader(ssql, cnnTracker)

    '                If rdrFpsMaster IsNot Nothing Then
    '                    oneMovementNo = currentShipment.MasterShipmentNumber

    '                ElseIf rdrFpsMaster.HasRows Then
    '                    oneMovementNo = currentShipment.MasterShipmentNumber

    '                Else
    '                    'ok...we can't find the shipment, looks like we'll have to assign the movement # we got back to this shipment
    '                    oneMovementNo = currentShipment.MasterShipmentNumber

    '                    Dim recAffected As Integer
    '                    ssql = "UPDATE FpsShipmentMaster SET OneMovementNo = '" & oneMovementNo & "' WHERE DemandLocation = '" & tmpDemand & "' AND Shipmentnumber = " & drTraffic("ShipmentNumber")
    '                    If Not DbConn.ExecuteQuery(ssql, cnnTracker, recAffected) Then
    '                        ErrObj.log(1, 0, "MergeOrCancelShipment", "could not update fps " & drTraffic("ShipmentNumber"), , drTraffic("ShipmentNumber"))
    '                        Return False

    '                    ElseIf recAffected = 0 Then
    '                        ErrObj.log(1, 0, "MergeOrCancelShipment", "No records updated for movement # " & oneMovementNo, , oneMovementNo)
    '                        Return False

    '                    End If

    '                End If

    '            End If
    '        Else
    '            'rows not found
    '            ErrObj.log(1, 0, "MergeOrCancelShipment", "could not find fps " & drTraffic("ShipmentNumber"), drTraffic("ShipmentNumber"))

    '        End If

    '        'cleanup
    '        rdrFpsMaster.Close()
    '        rdrFpsMaster = Nothing

    '    Else
    '        oneMovementNo = apptNotif.SiteRsrcReservation.ReservationId

    '    End If

    '    'look for the movement #/reservation id if exist in system
    '    ssql = "SELECT * FROM FpsShipmentMaster WHERE OneMovementNo = '" & oneMovementNo & "'"

    '    'BEGIN TRANSACTION
    '    txn = cnnTracker.BeginTransaction

    '    'now see if we can find the master
    '    If Not DbConn.GetUpdateableDataAdapter(ssql, cnnTracker, daMaster, txn) Then
    '        'error executing statement
    '        Return False
    '    End If

    '    'master shipmentnumber found

    '    'fill our master dataset
    '    If daMaster.Fill(dsMaster) > 0 And apptNotif.SiteRsrcReservation.State <> "Cancelled" Then

    '        Dim drMaster As DataRow
    '        drMaster = dsMaster.Tables(0).Rows(0)

    '        If tmpShipment = drMaster("ShipmentNumber") Then
    '            'this is the master order, don't do anything
    '            txn.Commit()
    '            Return True
    '        End If

    '        If drTraffic("ShipmentNumber") = drMaster("ShipmentNumber") Then
    '            'already combined w/ this shipment, don't do anything
    '            txn.Commit()
    '            Return True
    '        End If

    '        'recalc master shipment
    '        If Not RecalcShipment(dsMaster, drTraffic, 1) Then
    '            txn.Rollback()
    '            ErrObj.log(1, 0, "MergeOrCancelShipment", "Error deleting fpsmaster for child", , tmpShipment)
    '            Return False
    '        Else
    '            daMaster.Update(dsMaster)
    '            dsMaster.AcceptChanges()
    '        End If

    '        'we need to update the shipmentnumber of the child order to match the master
    '        ssql = "UPDATE TrafficInformationTable SET ShipmentNumber = " & drMaster("ShipmentNumber") _
    '                & " WHERE DemandLocation = '" & tmpDemand & "' AND Order_no = " & tmpShipment
    '        If Not DbConn.ExecuteQuery(ssql, cnnTracker, , txn) Then
    '            txn.Rollback()
    '            ErrObj.log(1, 0, "MergeOrCancelShipment", "Error udpating shipno for child", , tmpShipment)
    '        End If

    '        'blow away the child's old fpsshipmentmaster because it is now part of a master shipment
    '        ssql = "DELETE FROM FpsShipmentMaster WHERE DemandLocation = '" & tmpDemand & "' AND ShipmentNumber = " & tmpShipment
    '        If Not DbConn.ExecuteQuery(ssql, cnnTracker, , txn) Then
    '            txn.Rollback()
    '            ErrObj.log(1, 0, "MergeOrCancelShipment", "Error deleting fpsmaster for child", , tmpShipment)
    '        End If

    '    Else
    '        'no master shipmentnumber found, see if can find an fps for this
    '        ssql = "SELECT * FROM FpsShipmentMaster WHERE DemandLocation = '" & tmpDemand & "' AND ShipmentNumber = " & tmpShipment
    '        Dim rdrFps As OdbcDataReader
    '        rdrFps = DbConn.GetDataReader(ssql, My.Settings.SettingValue("TrackerConn"))

    '        'check to see if we got an fps master back
    '        If Not rdrFps.HasRows Then

    '            'couldn't find an fps record, means this was probably unmerged from another shipment, get me the traffic info so i can find
    '            'out what shipment this was combined with previously.  the shipnum field in the traffic should hold the master shipment number
    '            ssql = "SELECT * FROM FpsShipmentMaster WHERE DemandLocation = '" & tmpDemand & "' AND ShipmentNumber = " & drTraffic("ShipmentNumber")
    '            If DbConn.GetUpdateableDataAdapter(ssql, cnnTracker, daMaster, txn) Then

    '                If daMaster.Fill(dsMaster) > 0 Then
    '                    If Not RecalcShipment(dsMaster, drTraffic, -1) Then
    '                        txn.Rollback()
    '                        ErrObj.log(1, 0, "MergeOrCancelShipment", "Could not recalc shipment", , tmpShipment)
    '                        Return False
    '                    Else
    '                        'accept changes
    '                        daMaster.Update(dsMaster)
    '                        dsMaster.AcceptChanges()
    '                    End If

    '                    'now create a new fpsshipmentmaster for this order
    '                    If Not CreateShipment(oneMovementNo, dsTraffic, cnnTracker, txn) Then
    '                        ErrObj.log(1, 0, "MergeOrCancelShipment", "Could not create shipment", , tmpShipment)
    '                        txn.Rollback()
    '                        Return False
    '                    End If

    '                    'now update the shipmentnumber of the unmerged order to be it's own shipmentnumber
    '                    ssql = "UPDATE TrafficInformationTable SET ShipmentNumber = Order_no WHERE DemandLocation = '" & tmpDemand & "' AND Order_no = " & drTraffic("Order_no")
    '                    If Not DbConn.ExecuteQuery(ssql, cnnTracker, , txn) Then
    '                        txn.Rollback()
    '                        ErrObj.log(1, 0, "MergeOrCancelShipment", "Could not update TrafficInfo for child order", , drTraffic("ShipmentNumber"))
    '                        Return False
    '                    End If

    '                Else
    '                    'could not find it's parent either, this is fatal
    '                    txn.Rollback()
    '                    ErrObj.log(1, 0, "MergeOrCancelShipment", "Could not find parent shipment", , drTraffic("ShipmentNumber"))
    '                    Return False
    '                End If

    '            End If

    '        Else

    '            'we found it, now just assign the value of the movement #/reservation id
    '            ssql = "UPDATE FpsShipmentMaster SET OneMovementNo = '" & oneMovementNo & "' " _
    '                & " WHERE DemandLocation = '" & tmpDemand & "'" _
    '                & " AND ShipmentNumber = " & tmpShipment

    '            If Not DbConn.ExecuteQuery(ssql, cnnTracker, , txn) Then
    '                txn.Rollback()
    '                Return False
    '            End If

    '        End If

    '        rdrFps.Close()
    '    End If

    '    'commit transactions
    '    txn.Commit()

    '    Return True

    'End Function


    'sbains - E1 version of MergeOrCancelShipment function
    Function MergeOrCancelShipment_E1(ByVal apptNotif As FGIIntegApptNotification, ByRef orders() As Agent.Order_Type, Optional ByRef SubReturnValue As ProcessReturnType = ProcessReturnType.Success) As Boolean

        Dim ssql As String
        Dim cnnE1 As OdbcConnection
        Dim E1Database As String = My.Settings.SettingValue("E1Database")
        Dim txn As OdbcTransaction
        Dim oneMovementNo As String = Nothing
        Dim currentShipment As FGIIntegApptNotificationShipment
        Dim tmp_Orders(1) As Order_Type

        'open db connection
        Try
            cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
            cnnE1.Open()
        Catch ex As Exception
            ErrObj.log(1, 0, "MergeOrCancelShipment", "Could not open connection")
            Return False
        End Try

        'check for existence of shipment element
        If apptNotif.Shipment Is Nothing Then
            ErrObj.log(1, 0, "MergeOrCancelShipment", "Shipment element is missing")
            Dispose_ODBC_Connection(cnnE1)
            Return False
        End If

        currentShipment = apptNotif.Shipment

        'found a record w/ the same movement #, we will have to drop the fpsshipmentmaster for this order
        Dim tmpShipment As String = Nothing
        Dim tmpDemand As String = Nothing

        If Not ParseShipmentNumber(currentShipment.ShipmentNumber, tmpShipment, tmpDemand) Then
            Dispose_ODBC_Connection(cnnE1)
            Return False
        End If

        'get Sales Order Header Movement # for current order
        Dim Sales_Header_MovementNo As String
        'ssql = "SELECT SHURRF FROM " & E1Database & ".F4201 WHERE LTRIM(SHKCOO) = '" & tmpDemand & "' AND SHDOCO = " & tmpShipment
        'Sales_Header_MovementNo = Trim(DbConn.ExecuteScalar(ssql, My.Settings.E1Conn))


        'If Sales_Header_MovementNo Is Nothing Or Sales_Header_MovementNo = "" Then            'If no records in Sales Header then check the history.
        '    ssql = "SELECT SHURRF FROM " & E1Database & ".F42019 WHERE LTRIM(SHKCOO) = '" & tmpDemand & "' AND SHDOCO = " & tmpShipment
        '    Sales_Header_MovementNo = Trim(DbConn.ExecuteScalar(ssql, My.Settings.E1Conn))
        '    If Sales_Header_MovementNo IsNot Nothing And Sales_Header_MovementNo <> "" Then
        '        'this is already in history, just let it go through and log an error
        '        ErrObj.log(1, 0, "MergeOrCancelShipemnt", "Order is already in history " & tmpShipment, , tmpShipment)
        '        Dispose_ODBC_Connection(cnnE1)
        '        Return True
        '    Else
        '        ErrObj.log(1, 0, "MergeOrCancelShipment", "Could not find Order in live or history - " & tmpShipment, , tmpShipment)
        '        SubReturnValue = ProcessReturnType.Skip  'set this to skip because this order is probably deleted
        '        Dispose_ODBC_Connection(cnnE1)
        '        Return False
        '    End If

        'End If


        Dim drSalesHeader As OdbcDataReader
        'ssql = "SELECT SHURRF, SHASN FROM " & E1Database & ".F4201 WHERE LTRIM(SHKCOO) = '" & tmpDemand & "' AND SHDOCO = " & tmpShipment
        ssql = "SELECT SHURRF, SHASN FROM " & E1Database & ".F4201 WHERE SHDOCO = " & tmpShipment
        drSalesHeader = DbConn.GetDataReader(ssql, My.Settings.E1Conn)

        'If the recordset has rows then retrieve the movement number (if there is one)
        If drSalesHeader.HasRows Then

            'get the first row - should only have one record
            drSalesHeader.Read()
            Sales_Header_MovementNo = Trim(drSalesHeader.Item("SHURRF") & "")

            'If this order has no movement # and is not a pickup then something is wrong.  Only pickups will have no movement in which case the reservation id will be used
            If Sales_Header_MovementNo = "" And UCase(Trim(drSalesHeader.Item("SHASN") & "")) <> "PICKUP" Then
                ErrObj.log(1, 0, "MergeOrCancelShipemnt", "Order has no Movement #(SHURRF) and is not a PICKUP: " & tmpShipment, , tmpShipment)
                drSalesHeader.Close()
                Dispose_ODBC_Connection(cnnE1)
                Return True
            End If
        Else
            'Else lets check history for the order number
            drSalesHeader.Close()
            'ssql = "SELECT SHURRF, SHASN FROM " & E1Database & ".F42019 WHERE LTRIM(SHKCOO) = '" & tmpDemand & "' AND SHDOCO = " & tmpShipment
            ssql = "SELECT SHURRF, SHASN FROM " & E1Database & ".F42019 WHERE SHDOCO = " & tmpShipment
            drSalesHeader = DbConn.GetDataReader(ssql, My.Settings.E1Conn)

            If drSalesHeader.HasRows Then
                'this is already in history, just let it go through and log an error
                ErrObj.log(1, 0, "MergeOrCancelShipemnt", "Order is already in history " & tmpShipment, , tmpShipment)
                drSalesHeader.Close()
                Dispose_ODBC_Connection(cnnE1)
                Return True
            Else
                ErrObj.log(1, 0, "MergeOrCancelShipment", "Could not find Order in live or history - " & tmpShipment, , tmpShipment)
                SubReturnValue = ProcessReturnType.Skip  'set this to skip because this order is probably deleted
                Dispose_ODBC_Connection(cnnE1)
                Return False
            End If
        End If
        drSalesHeader.Close()
        'At this point the order either has a sales header movement # or its a PICKUP in which case will use the reservation id as the movement

        'check to see if this is a pickup order first
        If currentShipment.FreightControlledBySystem Then

            If Sales_Header_MovementNo IsNot Nothing Then   'If Sales Header Movement # exists then make it the active Movement #
                oneMovementNo = Sales_Header_MovementNo
            Else
                'If the Sales Header Movement # is Null then use the Appointment file's movement #
                oneMovementNo = currentShipment.MasterShipmentNumber

            End If

        Else  'ELSE use Reservation ID as Movenment #
            oneMovementNo = apptNotif.SiteRsrcReservation.reservationID
        End If


        If UCase(Trim(oneMovementNo)) <> UCase(Trim(Sales_Header_MovementNo)) Then    'Only proceed if active Movement # is different from Original Movement # for Sales Header

            'BEGIN TRANSACTION
            txn = cnnE1.BeginTransaction

            Dim recAffected As Long = 0
            'Update the Sales Header Movement # with new active Movenment # 
            ssql = "UPDATE " & E1Database & ".F4201 SET SHURRF = '" & oneMovementNo & "' WHERE SHDOCO = " & tmpShipment
            If Not DbConn.ExecuteQuery(ssql, cnnE1, recAffected, txn) Then
                ErrObj.log(1, 0, "MergeOrCancelShipment", "could not update Sales Header with new Movement # " & oneMovementNo & " for Order " & tmpShipment, , tmpShipment)
                Dispose_ODBC_Connection(cnnE1)
                Return False
            End If

            If recAffected = 0 Then
                ErrObj.log(1, 0, "MergeOrCancelShipment", "No records updated in Sales Header with new Movement # " & oneMovementNo & " for Order " & tmpShipment, , tmpShipment)
                Dispose_ODBC_Connection(cnnE1)
                Return False

            ElseIf recAffected > 0 Then
                'If Sales Header update succeeded then update Sales Detail Movement # with new active Movement #
                recAffected = 0
                ssql = "UPDATE " & E1Database & ".F4211 SET SDURRF = '" & oneMovementNo & "' WHERE SDDOCO = " & tmpShipment
                If Not DbConn.ExecuteQuery(ssql, cnnE1, recAffected, txn) Then
                    ErrObj.log(1, 0, "MergeOrCancelShipment", "could not update Sales Detail with new Movement # " & oneMovementNo & " for Order " & tmpShipment, , tmpShipment)
                    Dispose_ODBC_Connection(cnnE1)
                    Return False
                End If
                If recAffected = 0 Then
                    ErrObj.log(1, 0, "MergeOrCancelShipment", "No records updated in Sales Detail with new Movement # " & oneMovementNo & " for Order " & tmpShipment, , tmpShipment)
                    Dispose_ODBC_Connection(cnnE1)
                    Return False
                End If

            End If

            'commit transactions
            txn.Commit()

            'Add the order the orders array to so we can store the checksum value in the outbound log table.
            tmp_Orders(1).Demand_Location = tmpDemand
            tmp_Orders(1).Order_Number = tmpShipment
            Add_to_Orders_Array(orders, tmp_Orders)

            'Clean Up
            txn.Dispose()

            cnnE1.Close()
            cnnE1.Dispose()
        End If

        Return True

    End Function


    Function CreateShipment(ByVal movementNo As String, ByVal dsTraffic As DataSet, ByVal cnn As OdbcConnection, Optional ByVal txn As OdbcTransaction = Nothing) As Boolean

        Dim ssql As String
        Dim da As OdbcDataAdapter = Nothing
        Dim drNew, drTraffic As DataRow
        Dim ds As New DataSet
        Dim dbDate As Date

        ssql = "SELECT * FROM FpsShipmentMaster WITH(NOLOCK) WHERE 1=0"

        DbConn.GetUpdateableDataAdapter(ssql, cnn, da, txn)

        If da Is Nothing Then
            Return False
        End If

        da.Fill(ds)

        Try

            'retrieve traffic information row
            drTraffic = dsTraffic.Tables(0).Rows(0)
            drNew = ds.Tables(0).NewRow

            With drNew
                .Item("OneMovementNo") = movementNo
                .Item("ShipmentNumber") = drTraffic.Item("ORDER_NO")
                .Item("DEMANDLOCATION") = drTraffic.Item("DEMANDLOCATION")
                .Item("ShippingLocation") = drTraffic.Item("MFGING_LOCATION")
                .Item("City") = drTraffic.Item("ShipToCity")
                .Item("StateProvince") = drTraffic.Item("ShipToState_Province")
                .Item("ZipPostal") = drTraffic.Item("ShipToZip_PostalCode")

                .Item("ShipVia") = drTraffic.Item("SHIP_VIA_CODE")
                .Item("ShipmentStatus") = "O"

                .Item("ShipmentWeight") = drTraffic.Item("TOTAL_WEIGHT")
                .Item("ShipmentWeightUOM") = drTraffic.Item("WeightUOM")
                .Item("ShipmentVolume") = drTraffic.Item("TotalVolume")
                .Item("ShipmentVolumeUOM") = drTraffic.Item("VolumeUOM")
                .Item("ShipmentPallets") = drTraffic.Item("TotalPallets")

                dbDate = DbConn.GetDateSqlServer(My.Settings.SettingValue("TrackerConn"))

                .Item("TotalNumberOrders") = 1
                .Item("CreationDateStamp") = dbDate
                .Item("CreationTimeStamp") = dbDate
                .Item("MaintenanceDateStamp") = dbDate
                .Item("MaintenanceTimeStamp") = dbDate
                .Item("MaintenanceUserID") = "OneNet CreateShipment"
            End With
        Catch ex As Exception
            ErrObj.log(1, 0, "CreateShipment", "Error creating shipment", ex, movementNo)
            Return False
        End Try

        ds.Tables(0).Rows.Add(drNew)
        da.Update(ds)
        ds.AcceptChanges()

        Return True

    End Function

    'Procedure to add orders to the Orders() array
    'The Orders() array contains a list of orders that were modified as a result of processing the incoming onenetwork files.  
    'If order is modified by an incoming file the checksum value changes which will trigger an UPDATE outbound file creation.
    'So we need to keep track of which orders we changed so we can update their checksum value in the outbound log.
    Sub Add_to_Orders_Array(ByRef orders() As Agent.Order_Type, ByVal Order_No() As Order_Type)
        Dim Array_Ubound As Integer
        Dim Orders_Ubound As Integer
        Dim x As Integer
        Dim duplicate As Boolean = False
        Dim y As Integer

        Try
            Orders_Ubound = UBound(Order_No)

            'Cycle through the new order numbers and add them to the orders array
            For x = 1 To Orders_Ubound
                If orders Is Nothing Then
                    Array_Ubound = -1
                Else
                    Array_Ubound = UBound(orders)
                End If

                'if there's no orders in array then this is the first element
                If Array_Ubound <= 0 Then
                    Array_Ubound = 1
                    ReDim orders(Array_Ubound)
                    orders(Array_Ubound).Demand_Location = Order_No(x).Demand_Location
                    orders(Array_Ubound).Order_Number = Order_No(x).Order_Number

                Else
                    'Otherwise cycle through orders array to see if the order being added already exists
                    For y = 1 To Array_Ubound
                        If Order_No(x).Order_Number = orders(y).Order_Number Then duplicate = True
                    Next y

                    'If the duplicate flage is false then the order doesn't already exist
                    If duplicate = False Then
                        Array_Ubound = Array_Ubound + 1
                        ReDim Preserve orders(Array_Ubound)
                        orders(Array_Ubound).Demand_Location = Order_No(x).Demand_Location
                        orders(Array_Ubound).Order_Number = Order_No(x).Order_Number
                    End If
                End If
            Next x

        Catch ex As Exception
            ErrObj.log(1, 0, "Add_to_Orders_Array", "Error adding order(" & Order_No(x).Order_Number & ") to orders array.", ex, Order_No(x).Order_Number)
        End Try

    End Sub

    Function FormatReturnShipmentNumber(ByVal shipnum As String, ByVal demand As String) As String
        Return shipnum & "-" & demand
    End Function

    Public Class StringWriterWithEncoding
        Inherits StringWriter
        Dim myencoding As Text.Encoding

        Public Sub New(ByVal encoding As Text.Encoding)
            Me.myencoding = encoding
        End Sub

        Public Overrides ReadOnly Property Encoding() As Text.Encoding
            Get
                Return Me.myencoding
            End Get
        End Property

    End Class

    'sbains Dispose of ODBC Connection
    Public Sub Dispose_ODBC_Connection(ByRef ODBC_Conn As OdbcConnection)
        If Not ODBC_Conn Is Nothing Then
            If ODBC_Conn.State = ConnectionState.Open Then
                ODBC_Conn.Close()
                ODBC_Conn.Dispose()
            End If
        End If
    End Sub

    'sbains Dispose of ODBC DataAdapter
    Public Sub Dispose_ODBC_DataAdapter(ByRef ODBC_Adapt As OdbcDataAdapter)
        If Not ODBC_Adapt Is Nothing Then
            If Not ODBC_Adapt.SelectCommand Is Nothing Then ODBC_Adapt.SelectCommand.Dispose()
            If Not ODBC_Adapt.InsertCommand Is Nothing Then ODBC_Adapt.InsertCommand.Dispose()
            If Not ODBC_Adapt.UpdateCommand Is Nothing Then ODBC_Adapt.UpdateCommand.Dispose()
            If Not ODBC_Adapt.DeleteCommand Is Nothing Then ODBC_Adapt.DeleteCommand.Dispose()
            ODBC_Adapt.Dispose()
        End If
    End Sub

    'Sub WriteOrderText(ByVal doco As String, ByVal doctype As String, ByVal kco As String, ByVal msg As String, Optional ByVal Username As String = "")
    '    Using cnn As New System.Data.Odbc.OdbcConnection("dsn=E1NPD;UID=KIKAPPS;PWD=CUSTOM")

    '        Dim key As String = String.Format("{0}|{1}|{2}", doco, doctype, kco)

    '        Dim ssql As String = "SELECT * FROM " & My.Settings.SettingValue("E1Database") & ".F00165 where GDOBNM = 'GT4201A' AND GDTXKY = :KEY"

    '        cnn.Open()
    '        Using cmd As New System.Data.Odbc.OdbcCommand(ssql, cnn)

    '            cmd.Parameters.Add("KEY", Odbc.OdbcType.VarChar).Value = key
    '            Dim rdr As System.Data.Odbc.OdbcDataReader
    '            rdr = cmd.ExecuteReader

    '            If rdr.HasRows Then

    '                'this is to read the rtf text (sample)
    '                Dim instring As String = System.Text.Encoding.Unicode.GetString(rdr("GDTXFT"))

    '                'this is to write it back out
    '                Dim encode As New System.Text.UnicodeEncoding
    '                Dim outstr() As Byte = encode.GetBytes(msg)

    '                'open connection
    '                Using orcnn As New OracleConnection("user id=kikapps;password=custom;data source=E1NPD")
    '                    Dim issql As String

    '                    'update the text w/ our new message
    '                    issql = "UPDATE PRODDTA.F00165 SET GDTXFT = :NEWTEXT WHERE GDOBNM = 'GT4201A' AND GDTXKY = :KEY AND GDUSER = ''SBAINS'' "

    '                    orcnn.Open()
    '                    Using orcmd As New OracleCommand(issql, orcnn)
    '                        orcmd.Parameters.Add("NEWTEXT", OracleDbType.Blob).Value = outstr
    '                        orcmd.Parameters.Add("KEY", OracleDbType.NVarchar2).Value = key
    '                        orcmd.ExecuteNonQuery()
    '                    End Using

    '                End Using

    '            End If
    '        End Using
    '    End Using
    'End Sub

    'Converts Oracle Blob Data to string  (E1 Sales Order Notes are stored as Blob data type in Oracle)
    'Public Function WriteBlobData(ByVal Notes As String, ByVal Order_No As String, ByVal Order_Type As String, ByVal KCOO As String, Optional ByVal TestCase As String = "") As Boolean
    '    Dim retval As Boolean = False
    '    Dim ssql As String = ""
    '    'Dim conn As OracleConnection = New OracleConnection("User Id=kikapps;Password=custom;Data Source=KIKE1DB1")
    '    Dim cmd As New OracleCommand
    '    Dim returnstring As String = ""
    '    cmd.Connection = New OracleConnection("User Id=kikapps;Password=custom;Data Source=")
    '    cmd.Connection.Open()

    '    'Add a comment to end of notes to let the user know these notes are used by OneNetwork and will be overwritten
    '    Notes = Notes & ControlChars.CrLf & "(These notes are overwritten by OneNetwork)"

    '    Try
    '        Select Case UCase(TestCase)
    '            Case "READONLY"
    '                cmd.CommandText = "SELECT GDTXKY FROM " & My.Settings.SettingValue("E1Database") & ".F00165 WHERE GDOBNM = 'GT4201A' AND GDTXKY = '" & Order_No & "|" & Order_Type & "|" & KCOO & "' AND GDGTITNM = 'ONENETWORK' "
    '                Dim Notes_Str As String = System.Text.Encoding.Unicode.GetString(cmd.ExecuteScalar())

    '            Case Else
    '                cmd.CommandText = "SELECT Max(GDMOSEQN) FROM " & My.Settings.SettingValue("E1Database") & ".F00165 WHERE GDOBNM = 'GT4201A' AND GDTXKY = '" & Order_No & "|" & Order_Type & "|" & KCOO & "' AND GDGTITNM = 'ONENETWORK'"
    '                Dim MaxOneNetLineNum As Object = cmd.ExecuteScalar()

    '                'If MaxOneNetLineNum has no value then there is no existing with the label 'ONENETWORK' so we have to get the next line number and insert a new record
    '                If MaxOneNetLineNum.Equals(System.DBNull.Value) Then
    '                    cmd.CommandText = "SELECT Max(GDMOSEQN) FROM " & My.Settings.SettingValue("E1Database") & ".F00165 WHERE GDOBNM = 'GT4201A' AND GDTXKY = '" & Order_No & "|" & Order_Type & "|" & KCOO & "' "
    '                    MaxOneNetLineNum = cmd.ExecuteScalar()
    '                    If MaxOneNetLineNum.Equals(System.DBNull.Value) Then MaxOneNetLineNum = 1 Else MaxOneNetLineNum = MaxOneNetLineNum + 1
    '                    Dim blob As Byte() = System.Text.Encoding.Unicode.GetBytes(Notes)
    '                    Dim param As OracleParameter = cmd.Parameters.Add("blobtodb", OracleDbType.Blob)
    '                    param.Value = blob
    '                    param.Direction = ParameterDirection.Input

    '                    cmd.CommandText = "INSERT INTO " & My.Settings.SettingValue("E1Database") & ".F00165 (GDOBNM,GDTXKY,GDMOSEQN,GDGTMOTYPE,GDLNGP,GDUSER,GDUPMJ,GDTDAY,GDGTITNM,GDQUNAM,GDGTFILENM,GDGTFUTS1,GDGTFUTS2,GDGTFUTS3,GDGTFUTS4,GDGTFUTM1,GDGTFUTM2,GDTXFT) VALUES ('GT4201A','" & Order_No & "|" & Order_Type & "|" & KCOO & "'," & MaxOneNetLineNum.ToString & ",0,' ','ONENETWORK',KIKDATETOJUL(CURRENT_DATE),to_char(Current_Date, 'HHmmss'),'ONENETWORK',' ',' ',' ',' ',' ',' ',0,0,:blobtodb)"
    '                    cmd.ExecuteNonQuery()
    '                Else
    '                    'Otherwise if MaxOneNetLineNum has a value then we just need to update the record with line number  = MaxOneNetLineNum


    '                    Dim blob As Byte() = System.Text.Encoding.Unicode.GetBytes(Notes)
    '                    Dim param As OracleParameter = cmd.Parameters.Add("blobtodb", OracleDbType.Blob)
    '                    param.Value = blob
    '                    param.Direction = ParameterDirection.Input

    '                    cmd.CommandText = "UPDATE " & My.Settings.SettingValue("E1Database") & ".F00165 SET GDTXFT = :blobtodb WHERE GDOBNM = 'GT4201A' AND GDGTITNM = 'ONENETWORK' AND GDTXKY =  '" & Order_No & "|" & Order_Type & "|" & KCOO & "' AND GDMOSEQN = " & MaxOneNetLineNum.ToString
    '                    cmd.ExecuteNonQuery()

    '                End If

    '                'Notes = Notes & ControlChars.CrLf & "(These notes are overwritten by OneNetwork)"
    '                'Dim blob As Byte() = System.Text.Encoding.Unicode.GetBytes(Notes)
    '                'Dim param As OracleParameter = cmd.Parameters.Add("blobtodb", OracleDbType.Blob)
    '                'param.Value = blob
    '                'param.Direction = ParameterDirection.Input

    '                'cmd.CommandText = "UPDATE " & My.Settings.SettingValue("E1Database") & ".F00165 SET GDGTITNM = 'ONENETWORK', GDTXFT = :blobtodb WHERE GDOBNM = 'GT4201A' AND GDTXKY = '17936|SO|00200' AND GDMOSEQN = 2 "
    '                'cmd.ExecuteNonQuery()
    '        End Select

    '        cmd.Connection.Close()

    '        cmd.Connection.Dispose()
    '        cmd.Dispose()
    '        cmd = Nothing
    '        retval = True
    '        'Byte[] byteBLOBData = new Byte[0];
    '        'byteBLOBData = (Byte[])(row["DOCLIST"]);
    '        'Dim s As String = System.Text.Encoding.UTF8.GetString(byteBLOBData)

    '    Catch ex As Exception
    '        retval = False
    '        cmd.Connection.Close()

    '        cmd.Connection.Dispose()
    '        cmd.Dispose()
    '        cmd = Nothing
    '    End Try

    '    Return retval

    'End Function

    Public Sub Update_UnpaidOrders_Table()
        Dim sSql As String = ""
        Dim rdrUnpaid As OdbcDataReader = Nothing
        Dim RefNo As String = ""

        rdrUnpaid = DbConn.GetDataReader("Select * from unpaidorders where refno not in (select refno from onenetfiles where entered >= 'feb 1, 2010' and [filename] like '%pay%')", My.Settings.SettingValue("OneNetworkConn"))
        Try
            If rdrUnpaid IsNot Nothing Then
                'loop through all the shipment records
                Do While rdrUnpaid.Read()
                    RefNo = Trim(DbConn.ExecuteScalar("SELECT Top 1 RefNo FROM OneNetfiles where entered >= 'Feb 20, 2010' and filename like '%pay%' and content like '%ShipmentNumber>" & rdrUnpaid("OrderNo").ToString & "%' ", My.Settings.SettingValue("OneNetworkConn")))
                    DbConn.ExecuteQuery("UPDATE UnpaidOrders SET REFNO = '" & RefNo & "' WHERE OrderNo = " & rdrUnpaid("OrderNo").ToString, My.Settings.SettingValue("OneNetworkConn"))
                Loop
                rdrUnpaid.Close()
            End If
        Catch ex As Exception
            rdrUnpaid.Close()
        End Try
    End Sub

    Sub Update_AppointmentDates()
        Dim sSql As String
        Dim rdrOneNetReport As OdbcDataReader = Nothing
        Dim dsShipmentMap As New DataSet
        Dim cnnE1 As OdbcConnection
        Dim E1Database As String = My.Settings.SettingValue("E1Database")
        Dim txn As OdbcTransaction = Nothing

        'METHOD 1 - USE THIS METHOD FIRST AND THEN METHOD 2
        'THIS METHOD MATCHES ONENET REPORT TO E1 TABLE ON MOVEMENT = URRF - DOES NOT WORK FOR E1 ORDERS THAT ARE MISSING URRF DATA    
        'sSql = "select top 100 percent cast(kikmaster.dbo.formatdatetime(NewTime,  'HHMMSS 24') as integer) as OneNetTime, kikmaster.dbo.bigdatetojulian(NewTime) as OneNetDate, * from OneNetShipmentReport "
        'sSql = sSql & "inner join openquery(E1_PROD_ORA, 'Select MIN(SDLNID), SDURRF, SDDOCO, SDPDDJ, SDPDTT FROM " & E1Database & ".F4211 WHERE SDLTTR <> 980 AND SDLNTY = ''S'' GROUP BY SDURRF, SDDOCO, SDPDDJ, SDPDTT ') F4211  "
        'sSql = sSql & "ON F4211.SDURRF COLLATE DATABASE_DEFAULT = movement COLLATE DATABASE_DEFAULT "
        'sSql = sSql & "WHERE (kikmaster.dbo.bigdatetojulian(NewTime) <> cast(SDPDDJ as numeric) or cast(kikmaster.dbo.formatdatetime(NewTime,  'HHMMSS 24') as integer)  <> cast(SDPDTT as numeric) ) "
        'sSql = sSql & "ORDER BY OneNetDate, OneNetTime "

        'METHOD 2
        'THIS METHOD MATCHES ONENET REPORT TO E1 TABLE ON ORDER # (movement stripped of extra info) = DOCO - DOES NOT WORK FOR COMBINED SHIPMENTS    
        sSql = "select top 100 percent *, cast(kikmaster.dbo.formatdatetime(NewTime,  'HHMMSS 24') as integer) as OneNetTime, kikmaster.dbo.bigdatetojulian(NewTime) as OneNetDate from OneNetShipmentReport "
        sSql = sSql & "INNER join openquery(E1_PROD_ORA, 'Select MIN(SDLNID), SDURRF, SDDOCO, SDPDDJ, SDPDTT FROM " & E1Database & ".F4211 WHERE SDLTTR <> 980 AND SDLNTY = ''S'' GROUP BY SDURRF, SDDOCO, SDPDDJ, SDPDTT ') F4211  "
        sSql = sSql & "ON F4211.SDDOCO COLLATE DATABASE_DEFAULT = replace(replace(replace(replace(replace(movement, 'M-', ''), '-55', ''), '-00200', ''), '-15', ''), '-00100', '') COLLATE DATABASE_DEFAULT "
        sSql = sSql & "WHERE (kikmaster.dbo.bigdatetojulian(NewTime) <> cast(SDPDDJ as numeric) or cast(kikmaster.dbo.formatdatetime(NewTime,  'HHMMSS 24') as integer)  <> cast(SDPDTT as numeric) ) "
        sSql = sSql & "ORDER BY OneNetDate, OneNetTime "


        'NO MATCHES FOR CONSOLIDATED SHIPMENTS (RUN IN QUERY ANALYZER) - This returns all records in OneNetShipmentReport table that do not match up with any records in F4201/F4211 for consolidate shipments where either the dates are different or the movement does not exist in F4211/F4201
        'sSql = "select * from openquery(E1_PROD_ORA, 'Select SHURRF, SHDOCO, SHPDDJ, SHPDTT from " & E1Database & ".F4201') SalesHeader "
        'sSql = sSql & "RIGHT Join "
        'sSql = sSql & "(select top 100 percent cast(kikmaster.dbo.formatdatetime(NewTime,  'HHMMSS 24') as integer) as OneNetTime, kikmaster.dbo.bigdatetojulian(NewTime) as OneNetDate, * from OneNetShipmentReport "
        'sSql = sSql & "LEFT join openquery(E1_PROD_ORA, 'SELECT MIN(SDLNID), SDURRF, SDDOCO, SDPDDJ, SDPDTT FROM (Select * FROM " & E1Database & ".F4211 UNION Select * FROM " & E1Database & ".F42119) F4211 WHERE SDLTTR <> 980 AND SDLNTY = ''S'' GROUP BY SDURRF, SDDOCO, SDPDDJ, SDPDTT ') F4211   "
        'sSql = sSql & "ON F4211.SDDOCO COLLATE DATABASE_DEFAULT = replace(replace(replace(replace(replace(movement, 'M-', ''), '-55', ''), '-00200', ''), '-15', ''), '-00100', '') COLLATE DATABASE_DEFAULT "
        'sSql = sSql & "WHERE SDDOCO IS NULL) StripMovement_Match "
        'sSql = sSql & "ON StripMovement_Match.Movement COLLATE DATABASE_DEFAULT = SalesHeader.SHURRF COLLATE DATABASE_DEFAULT "
        'sSql = sSql & "WHERE (OneNetTime <> salesheader.SHPDTT Or OneNetDate <> salesheader.SHPDDJ) OR salesheader.SHURRF IS NULL"

        Try
            'get all the Shipments in the batch datestamp range
            rdrOneNetReport = DbConn.GetDataReader(sSql, My.Settings.SettingValue("OneNetworkConn"))

            'no data returned
            If rdrOneNetReport Is Nothing Then
                rdrOneNetReport.Close()
                Exit Sub
            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "Update_AppointmentDates", "Error opening OneNetShipmentReport table", ex)
        End Try


        'open db connection
        Try
            cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
            cnnE1.Open()
        Catch ex As Exception
            ErrObj.log(1, 0, "Update_AppointmentDates", "Could not open connection")
            rdrOneNetReport.Close()
            Exit Sub
        End Try

        Try
            'loop through all the shipment records
            Do While rdrOneNetReport.Read()


                'declare some temp vars and set it
                Dim orderNo, ApptTime, ApptDate, recaffected2 As Integer
                Dim Movement As String

                'init vars
                orderNo = 0
                ApptTime = 0
                ApptDate = 0
                Movement = ""
                recaffected2 = 0
                orderNo = rdrOneNetReport("SDDOCO")
                ApptTime = rdrOneNetReport("OneNetTime")
                ApptDate = rdrOneNetReport("OneNetDate")
                Movement = rdrOneNetReport("Movement")


                txn = cnnE1.BeginTransaction

                Dim recAffected As Long = 0
                'Update the Sales Header Movement # with new active Movenment # 
                sSql = "UPDATE " & E1Database & ".F4201 SET SHPDTT = " & ApptTime.ToString & ",  SHPDDJ = " & ApptDate.ToString & " WHERE SHDOCO = " & orderNo.ToString
                If Not DbConn.ExecuteQuery(sSql, cnnE1, recAffected, txn) Then
                    ErrObj.log(1, 0, "Update_AppointmentDates", "could not update Sales Header with new Appt Time:" & ApptTime.ToString & " for Order: " & orderNo.ToString, , orderNo.ToString)
                    Dispose_ODBC_Connection(cnnE1)
                    Exit Sub
                Else
                    'UPDATE THE URRF with OneNet Movement IF the current SHURRF = SHDOCO (which is the default and means it was never updated)
                    sSql = "UPDATE " & E1Database & ".F4201 SET SHURRF = '" & Movement & "' WHERE trim(SHURRF) = cast(SHDOCO as varchar(10)) AND SHDOCO = " & orderNo.ToString
                    If Not DbConn.ExecuteQuery(sSql, cnnE1, recaffected2, txn) Then
                        '  Stop
                    ElseIf recaffected2 > 0 Then
                        ' Stop
                    End If
                End If

                If recAffected = 0 Then
                    ErrObj.log(1, 0, "Update_AppointmentDates", "No records updated in Sales Header with new Appt Time:" & ApptTime.ToString & " for Order: " & orderNo.ToString, , orderNo.ToString)
                    Dispose_ODBC_Connection(cnnE1)
                    Exit Sub

                ElseIf recAffected > 0 Then
                    'If Sales Header update succeeded then update Sales Detail Movement # with new active Movement #
                    recAffected = 0
                    sSql = "UPDATE " & E1Database & ".F4211 SET SDPDTT = " & ApptTime.ToString & ",  SDPDDJ = " & ApptDate.ToString & " WHERE SDDOCO = " & orderNo.ToString
                    If Not DbConn.ExecuteQuery(sSql, cnnE1, recAffected, txn) Then
                        ErrObj.log(1, 0, "Update_AppointmentDates", "could not update Sales Detail with new Appt Time:" & ApptTime.ToString & " for Order: " & orderNo.ToString, , orderNo.ToString)
                        Dispose_ODBC_Connection(cnnE1)
                        Exit Sub
                    Else
                        'UPDATE THE URRF with OneNet Movement IF the current SDURRF = SDDOCO (which is the default and means it was never updated)
                        sSql = "UPDATE " & E1Database & ".F4211 SET SDURRF = '" & Movement & "' WHERE trim(SDURRF) = cast(SDDOCO as varchar(10)) AND SDDOCO = " & orderNo.ToString
                        If Not DbConn.ExecuteQuery(sSql, cnnE1, recaffected2, txn) Then
                            '  Stop
                        ElseIf recaffected2 > 0 Then
                            ' Stop
                        End If
                    End If
                    If recAffected = 0 Then
                        ErrObj.log(1, 0, "Update_AppointmentDates", "No records updated in Sales Detail with new Appt Time:" & ApptTime.ToString & " for Order: " & orderNo.ToString, , orderNo.ToString)
                        Dispose_ODBC_Connection(cnnE1)
                        Exit Sub
                    End If

                End If

                'commit transactions
                txn.Commit()


            Loop

            'cleanup
            rdrOneNetReport.Close()
            txn.Dispose()
            cnnE1.Close()
            cnnE1.Dispose()

        Catch ex As Exception
            ErrObj.log(1, 0, "Update_AppointmentDates", "Error writing to F4211 or F4201 or reading from OneNetShipmentReport table", ex)
        End Try
    End Sub

    Sub Assign_MasterBOL()
        Dim sSql As String
        Dim rdrConsolMovements As OdbcDataReader = Nothing
        Dim E1Database As String = My.Settings.SettingValue("E1Database")

        'SELECT * MOVEMENTS #'s THAT ARE DUPLICATES (Consolidated shipments) THAT DO NOT ALREADY EXIST IN F5547003 (which means they do not have a Master BOL)
        sSql = "SELECT SHURRF FROM "
        sSql = sSql & "     (SELECT Distinct(SHURRF) as SHURRF FROM "
        sSql = sSql & "         (select SHURRF, count(*) from " & E1Database & ".F4201 WHERE SHURRF <> ' '  GROUP BY SHURRF HAVING COUNT(*) > 1) Dupes_URRF "
        sSql = sSql & "     INNER JOIN "
        sSql = sSql & "         (SELECT SDURRF FROM " & E1Database & ".F4211 WHERE SDLTTR <> 980 AND SDLNTY = 'S' AND SDLTTR >= 526 AND SDNXTR < 980 ) INNER_F4211 "
        'sSql = sSql & "         (SELECT SDURRF FROM " & E1Database & ".F4211 WHERE SDLTTR <> 980 AND SDLNTY = 'S' AND SDLTTR >= 526 AND SDLTTR < 560 AND SDNXTR <= 560 ) INNER_F4211 "
        sSql = sSql & "     ON Dupes_URRF.SHURRF = INNER_F4211.SDURRF) Dupes "
        sSql = sSql & "WHERE SHURRF NOT IN (Select MBURRF FROM " & E1Database & ".F5547003) "

        Try
            'get all the Shipments in the batch datestamp range
            rdrConsolMovements = DbConn.GetDataReader(sSql, My.Settings.E1Conn)

            'no data returned
            If rdrConsolMovements Is Nothing Then
                Exit Sub
            ElseIf Not (rdrConsolMovements.HasRows) Then
                'If rdrConsolMovements Is Nothing Then
                rdrConsolMovements.Close()
                Exit Sub
            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "Assign_MasterBOL", "Error running query in E1 Oracle:" & sSql, ex)
            Exit Sub
        End Try

        Try
            'loop through all the shipment records
            Do While rdrConsolMovements.Read()

                'declare some temp vars and set it
                Dim Movement As String = ""
                Dim MasterBOLno As String = ""

                Movement = Trim(rdrConsolMovements("SHURRF"))

                MasterBOLno = OneMasterBol.GetNextMasterBol
                If (OneMasterBol.InsertMasterBol(Movement, MasterBOLno)) <> 1 Then
                    ErrObj.log(1, 0, "Assign_MasterBOL", "Error INSERTING MASTERBOL in to F5547003 for Movement#:" & Movement & " MasterBOL:" & MasterBOLno)
                End If

            Loop

            'cleanup
            rdrConsolMovements.Close()

        Catch ex As Exception
            ErrObj.log(1, 0, "Assign_MasterBOL", "Error reading recordset or retrieving next MasterBOL number", ex)
        End Try
    End Sub

    Sub Delete_MasterBOL()
        Dim sSql As String
        Dim rdrConsolMovements As OdbcDataReader = Nothing
        Dim E1Database As String = My.Settings.SettingValue("E1Database")

        'SELECT * MOVEMENTS NO FROM F5547003 WHERE THE MOVEMENT NO IS NO LONGER ALLOCATED TO MORE THAT ONE SALES ORDER (which means it's no longer consolidated and no longer require Master BOL)
        sSql = "SELECT MBURRF, MBSHPN FROM " & E1Database & ".F5547003 "
        sSql = sSql & "WHERE MBURRF NOT IN  "
        sSql = sSql & "     (SELECT Distinct(SHURRF) as SHURRF FROM "
        sSql = sSql & "         (select SHURRF, count(*) from (SELECT SHURRF FROM " & E1Database & ".F4201 UNION ALL SELECT SHURRF FROM " & E1Database & ".F42019) Header WHERE SHURRF <> ' '  GROUP BY SHURRF HAVING COUNT(*) <= 1) Dupes_URRF "
        sSql = sSql & "     INNER JOIN  "
        sSql = sSql & "         (SELECT SDURRF FROM (SELECT SDURRF, SDLTTR, SDNXTR, SDLNTY FROM " & E1Database & ".F4211 UNION ALL SELECT SDURRF, SDLTTR, SDNXTR, SDLNTY FROM " & E1Database & ".F42119) Detail WHERE SDLTTR <> 980 AND SDLNTY = 'S' ) INNER_F4211 "
        sSql = sSql & "     ON Dupes_URRF.SHURRF = INNER_F4211.SDURRF) "


        Try
            'get all the Shipments in the batch datestamp range
            rdrConsolMovements = DbConn.GetDataReader(sSql, My.Settings.E1Conn)

            'no data returned
            If rdrConsolMovements Is Nothing Then
                Exit Sub
            ElseIf Not (rdrConsolMovements.HasRows) Then
                rdrConsolMovements.Close()
                Exit Sub
            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "Delete_MasterBOL", "Error running query in E1 Oracle:" & sSql, ex)
            Exit Sub
        End Try

        Try
            'loop through all the shipment records
            Do While rdrConsolMovements.Read()

                'declare some temp vars and set it
                Dim Movement As String = ""
                Dim MasterBOL As Integer = 0

                Movement = Trim(rdrConsolMovements("MBURRF"))
                MasterBOL = rdrConsolMovements("MBSHPN")

                If (OneMasterBol.DeleteMovementMasterBol(Movement)) <> 1 Then
                    ErrObj.log(1, 0, "Delete_MasterBOL", "Error DELETING MASTERBOL from F5547003 for Movement#:" & Movement)
                End If

            Loop

            'cleanup
            rdrConsolMovements.Close()

        Catch ex As Exception
            ErrObj.log(1, 0, "Delete_MasterBOL", "Error Reading/Deleting MasterBOL entry in F5547003", ex)
            rdrConsolMovements.Close()
        End Try
    End Sub

    Public Sub Process_ShipWithGroupRef(ByVal OrderNumber As Integer, ByVal ShipWithGroupRef As String, ByVal FileId As Integer, ByRef cnnE1 As OdbcConnection, ByVal E1Database As String)
        Dim sSql As String = ""
        Dim da_TMS_XREF As OdbcDataAdapter = Nothing
        Dim ds_TMS_XREF As New DataSet
        Dim dr_TMS_XREF As DataRow
        Dim nowDateJulian As Integer
        Dim nowTimeJulian As Integer
        nowDateJulian = oMapping.ConvertDateToJulian(Now)
        nowTimeJulian = Hour(Now) * 10000 + Minute(Now) * 100 + Second(Now)

        Try
            sSql = "SELECT * FROM " & E1Database & ".F554202x WHERE TMDOCO = " & OrderNumber.ToString
            DbConn.GetUpdateableDataAdapter(sSql, cnnE1, da_TMS_XREF)

            If UCase(Left(ShipWithGroupRef, 2)) <> "G-" And UCase(Left(ShipWithGroupRef, 2)) <> "" And ShipWithGroupRef <> "0" Then ShipWithGroupRef = "G-" & ShipWithGroupRef

            If da_TMS_XREF IsNot Nothing Then
                'set if we got a record back

                'If record already exists then update values
                If da_TMS_XREF.Fill(ds_TMS_XREF) > 0 Then

                    'get first row, we should only get back one anyway
                    dr_TMS_XREF = ds_TMS_XREF.Tables(0).Rows(0)

                    'populate fields
                    dr_TMS_XREF.Item("TMUSRRSV1") = ShipWithGroupRef
                    dr_TMS_XREF.Item("TMUPMJ") = nowDateJulian
                    dr_TMS_XREF.Item("TMTDAY") = nowTimeJulian

                    'save changes
                    da_TMS_XREF.Update(ds_TMS_XREF)
                    ds_TMS_XREF.AcceptChanges()

                Else  'If record does not exist then add new row 
                    dr_TMS_XREF = ds_TMS_XREF.Tables(0).NewRow

                    'populate fields
                    dr_TMS_XREF.Item("TMDOCO") = Val(OrderNumber & "")
                    dr_TMS_XREF.Item("TMURRF") = " "
                    dr_TMS_XREF.Item("TMUSRRSV1") = ShipWithGroupRef
                    dr_TMS_XREF.Item("TMUPMJ") = nowDateJulian
                    dr_TMS_XREF.Item("TMTDAY") = nowTimeJulian

                    'add to dataset
                    ds_TMS_XREF.Tables(0).Rows.Add(dr_TMS_XREF)

                    'save changes
                    da_TMS_XREF.Update(ds_TMS_XREF)
                    ds_TMS_XREF.AcceptChanges()
                End If


                'cleanup
                ds_TMS_XREF.Dispose()
                da_TMS_XREF.Dispose()
            Else
                ErrObj.log(1, 0, "Process_ShipWithGroupRef", "Error accessing F554202x (TMS Cross Reference By Sales Order) for Order:" & OrderNumber.ToString & " FileID:" & FileId.ToString)
            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "Process_ShipWithGroupRef", "Error writing to F554202x (TMS Cross Reference By Sales Order) for Order:" & OrderNumber.ToString & " FileID:" & FileId.ToString, ex, FileId)
            ds_TMS_XREF.Dispose()
            da_TMS_XREF.Dispose()
        End Try

    End Sub

    Public Function Update_MasterBOL_Xrefs() As Boolean
        Dim cnnE1 As OdbcConnection
        Dim cnnE2 As OdbcConnection = DbConn.CreateConnection(My.Settings.E1Conn)
        Dim E1Database As String = My.Settings.SettingValue("E1Database")
        Dim sSql As String = ""
        Dim nowDateJulian As Integer
        Dim nowTimeJulian As Integer
        nowDateJulian = oMapping.ConvertDateToJulian(Now)
        nowTimeJulian = Hour(Now) * 10000 + Minute(Now) * 100 + Second(Now)

        Try
            cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
            cnnE1.Open()
        Catch ex As Exception
            ErrObj.log(1, 2, "Update_MasterBOL_Xrefs", "Could not open connection " & My.Settings.E1Conn, ex)
            Return False
        End Try

        '------------------------------INSERT XREF RECORD INTO F554202x for ORDERS THAT DO NOT ALREADY EXIST IN F554202x-------------------------------
        Try
            Dim recInserted As Integer

            sSql = "INSERT INTO " & E1Database & ".F554202x (TMDOCO, TMURRF, TMUSRRSV1, TMUPMJ, TMTDAY, TMWEID) "
            sSql = sSql & "SELECT SDDOCO, SDURRF, ' ', KIKDATETOJUL(CURRENT_DATE),to_char(Current_Date, 'HH24MISS'), ' ' from " & E1Database & ".F4211 "
            sSql = sSql & "WHERE trim(SDURRF) <> ' ' and SDLTTR <> 980 and SDLNTY = 'S' AND SDDOCO NOT IN (SELECT TMDOCO FROM " & E1Database & ".F554202x) group by SDDOCO, SDURRF "

            Using cmd As New OdbcCommand(sSql, cnnE1)
                recInserted = cmd.ExecuteNonQuery()
                cmd.Dispose()
            End Using
        Catch ex As Exception
            ErrObj.log(1, 0, "Update_MasterBOL_Xrefs", "Error inserting into F554202x (TMS Cross Reference By Sales Order)" & vbCrLf & sSql & vbCrLf, ex)
            cnnE1.Close()
            Return False
        End Try

        '------------------------------UPDATE TMURRF XREF VALUE IN F554202x (with F4201.SHURRF) for ORDERS THAT ALREADY HAVE EXISTING RECORD IN F554202x-------------------------------
        Dim rdr_Sales As OdbcDataReader = Nothing

        Try
            'Select only records where the existing TMURRF in F554202x does not match sales order F4201.SHURRF - only these orders need to be updated with current urrf
            'BUT...DO NOT select sales orders that already have a ShipWithGroupRef as Movement (first two characters in SHURRF <> 'G-') becuase we do not want to overwrite the existing TMURRF with a GroupWithRef value
            sSql = "SELECT SDDOCO, SDURRF, KIKDATETOJUL(CURRENT_DATE) CurrJulianDate, to_char(Current_Date, 'HH24MISS') CurrTime "
            sSql = sSql & "FROM (Select * FROM " & E1Database & ".F4211 F4211 WHERE F4211.SDLNID = (SELECT CAST(SUBSTR(MIN(SDLTTR || '-' || to_char(SDLNID, '099999')), 5) as integer) SDLNID FROM " & E1Database & ".F4211 F4211_INNER WHERE F4211_INNER.SDDOCO = F4211.SDDOCO AND F4211_INNER.SDKCOO = F4211.SDKCOO AND F4211_INNER.SDDCTO = F4211.SDDCTO AND F4211_INNER.SDLNTY = 'S' AND F4211_INNER.SDMCU NOT IN (100, 200))) F4211"
            sSql = sSql & "INNER JOIN " & E1Database & ".F554202x ON SDDOCO = TMDOCO WHERE SDURRF <> nvl(TMURRF, ' ') AND SUBSTR(SDURRF, 1,2) <> 'G-'"
            rdr_Sales = DbConn.GetDataReader(sSql, cnnE1)

            If rdr_Sales IsNot Nothing Then
                'loop through all the sales orders
                Do While rdr_Sales.Read()
                    'Update each sales order record in F554202x - Set TMURRF = SHURRF, TMUPMJ = Current Julian Date, TMTDAY = Current Time
                    DbConn.ExecuteQuery("UPDATE " & E1Database & ".F554202x SET TMURRF = '" & Trim(rdr_Sales("SDURRF") & "") & "', TMUPMJ = " & rdr_Sales("CurrJulianDate").ToString & ", TMTDAY = " & rdr_Sales("CurrTime").ToString & "  WHERE TMDOCO = " & rdr_Sales("SDDOCO").ToString, cnnE2)
                Loop
                rdr_Sales.Close()
            End If

        Catch ex As Exception
            ErrObj.log(1, 0, "Update_MasterBOL_Xrefs", "Error Updating TMURRF in F554202x (TMS Cross Reference By Sales Order)" & vbCrLf & sSql & vbCrLf, ex)
            If Not (rdr_Sales Is Nothing) Then
                If rdr_Sales.IsClosed = False Then rdr_Sales.Close()
            End If
            cnnE1.Close() : cnnE1.Dispose()
            Return False
        End Try

        '------------------------------Determine which value in F554202x (Movement or ShipWithGroupRef) should be used for determining Master BOL and used to update URRF in F4201/F4211 as the new Shipment Number
        Dim rdr_TMX_XREF As OdbcDataReader = Nothing

        Try
            'Select all records from TMS Xref table (F554202x) that meet the criteria for updating the Sales Orders (F4201/F4211 URRF) with a either the Movement # or ShipWithGroupRef from F554202x
            'Also within this query is an error message if both a consolidated Movement # and ShipWithGroupRef exist for an order - If an error exists then do not update Sales Order and send email alert
            sSql = "SELECT URRF_Updates.* "
            sSql = sSql & ", CASE WHEN URRF_COUNT > 1 AND GROUPREF_COUNT >=1 AND DateTimeUpdated > PrevErrorDateTime THEN"
            sSql = sSql & "             'ORDER ' || to_char(TMDOCO) || ': Must unconsolidate Movement ''' || to_char(trim(TMURRF)) || ''' before Group Ref ''' || to_char(trim(TMUSRRSV1)) || ''' can become effective.'  "
            sSql = sSql & "       WHEN URRF_COUNT > 1 AND GROUPREF_COUNT >=1 THEN 'PREVIOUSLY ERRORED' "    'ADDED TO FLAG PREVIOUSLY ERRORED ORDER SO WE CAN IGNORE IT BUT NOT SEND EMAIL
            sSql = sSql & "       ELSE 'OK' "
            sSql = sSql & "  END ErrorMessage "
            sSql = sSql & "FROM ( "
            sSql = sSql & "      Select TMDOCO, SDURRF, TMURRF "
            sSql = sSql & "      , CASE WHEN SUBSTR(TMUSRRSV1, 1, 2) = 'G-' THEN TMUSRRSV1 ELSE TMURRF END URRF_NEW "
            'sSql = sSql & "      , (Select count(*) from " & E1Database & ".F554202x F554202x_inner where F554202x_inner.TMURRF = F554202x.TMURRF AND F554202x.TMURRF <> ' ') URRF_Count "
            sSql = sSql & "      , (Select count(*) from " & E1Database & ".F554202x F554202x_inner where F554202x_inner.TMURRF = F554202x.TMURRF AND SUBSTR(TMURRF, 1, 2) = 'M-') URRF_Count "
            sSql = sSql & "      , TMUSRRSV1 "
            sSql = sSql & "      , (Select count(*) from " & E1Database & ".F554202x F554202x_inner where F554202x_inner.TMUSRRSV1 = F554202x.TMUSRRSV1 AND F554202x.TMUSRRSV1 <> '0' AND F554202x.TMUSRRSV1 <> ' ') GroupRef_Count "
            sSql = sSql & "      , substr(to_char(TMUPMJ) || to_char(TMTDAY ), 1, 10) DateTimeUpdated "
            sSql = sSql & "      , to_char(KIKDATETOJUL(CURRENT_DATE)) || to_char(Current_Date, 'HH24MI') CurrErrorDateTime "
            sSql = sSql & "      , TMWEID PrevErrorDateTime "
            sSql = sSql & "      from (Select SDDOCO, SDURRF from " & E1Database & ".F4211 WHERE TRIM(SDURRF) <> ' ' group by SDDOCO, SDURRF) SALES inner join " & E1Database & ".F554202x on SDDOCO = TMDOCO "
            sSql = sSql & "      WHERE SDURRF <> CASE WHEN SUBSTR(TMUSRRSV1, 1, 2) = 'G-' THEN TMUSRRSV1 ELSE TMURRF END "
            sSql = sSql & "      ) URRF_Updates "
            sSql = sSql & "WHERE trim(SDURRF) <> trim(URRF_NEW) "

            rdr_TMX_XREF = DbConn.GetDataReader(sSql, cnnE1)

            If rdr_TMX_XREF IsNot Nothing Then
                'loop through all the TMS XREF records
                Do While rdr_TMX_XREF.Read()

                    'If ErrorMessage = 'OK' then proceed to update the Sales Order (F4201/F4211 URRF) with the new URRF
                    If Trim(rdr_TMX_XREF("ERRORMESSAGE") & "") = "OK" Then

                        'If URRF_NEW is not blank then update sales order tables
                        If Trim(rdr_TMX_XREF("URRF_NEW") & "") <> "" Then
                            DbConn.ExecuteQuery("UPDATE " & E1Database & ".F4201 SET SHURRF = '" & Trim(rdr_TMX_XREF("URRF_NEW") & "") & "'  WHERE SHDOCO = " & rdr_TMX_XREF("TMDOCO").ToString, cnnE2)
                            DbConn.ExecuteQuery("UPDATE " & E1Database & ".F4211 SET SDURRF = '" & Trim(rdr_TMX_XREF("URRF_NEW") & "") & "'  WHERE SDDOCO = " & rdr_TMX_XREF("TMDOCO").ToString, cnnE2)
                        Else 'URRF_NEW is blank then there's something wrong - write to Error Log
                            ErrObj.log(1, 0, "Update_MasterBOL_Xrefs", "Cannot assign a blank value to URRF field in sales for Order " & rdr_TMX_XREF("TMDOCO").ToString)
                        End If

                    ElseIf Trim(rdr_TMX_XREF("ERRORMESSAGE") & "") <> "PREVIOUSLY ERRORED" Then 'Else if there's an error do not update Sales Order, Update Error Date Time in TMS XREF (F554202x.TMWEID), and also create error log entry to be later emailed

                        'Update the TMS XREF Error DateTime (F554202x.TMWEID) field with the datetime of current error
                        DbConn.ExecuteQuery("UPDATE " & E1Database & ".F554202x SET TMWEID = '" & Trim(rdr_TMX_XREF("CurrErrorDateTime") & "") & "' WHERE TMDOCO = " & rdr_TMX_XREF("TMDOCO").ToString, cnnE2)

                        'Write ErrorLog of Type 3 so this will get sent out to email
                        ErrObj.log(3, 0, "Update_MasterBOL_Xrefs", "You must unconsolidate the Movement before you can consolidate using the 'Ship With Group Ref' feature for order:" & rdr_TMX_XREF("TMDOCO").ToString, , , True, My.Settings.SettingValue("OneNetworkEmailDist"), "Consolidation Error for Order:" & rdr_TMX_XREF("TMDOCO").ToString)
                    End If
                Loop
                rdr_TMX_XREF.Close()
            End If

        Catch ex As Exception
            ErrObj.log(1, 0, "Update_MasterBOL_Xrefs", "Error Updating TMURRF in F554202x (TMS Cross Reference By Sales Order)", ex)
            If Not (rdr_TMX_XREF Is Nothing) Then
                If rdr_TMX_XREF.IsClosed = False Then rdr_TMX_XREF.Close()
            End If
            cnnE1.Close() : cnnE1.Dispose()
            Return False
        End Try

        cnnE1.Close() : cnnE1.Dispose()
        Return True

    End Function


    Public Sub populate_reasoncodes(ByVal EnteredDate As DateTime, ByVal filetype As String, Optional ByVal ApptNotification As FGIIntegApptNotification = Nothing, Optional ByVal FgTrack As FGIIntegTrackNotification = Nothing, Optional ByVal ModShipNot As FGIIntegModshipnotShipmentListMessage = Nothing)
        Dim mcurrentShipment As FGIIntegModshipnotShipmentListMessageShipment = Nothing
        Dim tcurrentShipment As FGIIntegTrackNotificationShipmentListMessageShipment = Nothing
        Dim acurrentShipment As FGIIntegApptNotificationShipment = Nothing
        Dim tmpShipmentNum As String = ""
        Dim tmpDemandLocation As String = ""
        Dim tmpTrafficOrder As String = ""
        Dim tmpMfgLoc As String = ""
        Dim oRc As Udfs.ReasonCode
        Dim oUdfs As New Udfs

        Select Case UCase(filetype)
            Case "MOD"
                mcurrentShipment = ModShipNot.Shipment
                tmpShipmentNum = mcurrentShipment.ShipmentNumber
                ParseShipmentNumber(tmpShipmentNum, tmpTrafficOrder, tmpDemandLocation)

                If mcurrentShipment.ShipmentHeaderMDFs IsNot Nothing Then
                    oRc = oUdfs.ParseReasonCode_E1(mcurrentShipment.FreightControlledBySystem, mcurrentShipment.ShipmentHeaderMDFs)
                    If oRc IsNot Nothing Then
                        oUdfs.InsertReasonCode_Rerun(tmpDemandLocation, tmpTrafficOrder, oRc.mfgloc, oRc.code, oRc.shipvia, mcurrentShipment.LastModifiedUser, EnteredDate)
                    End If
                End If

            Case "TRACK"
                For Each tcurrentShipment In FgTrack.ShipmentListMessage.Shipment
                    ParseShipmentNumber(tcurrentShipment.ShipmentNumber, tmpTrafficOrder, tmpDemandLocation)

                    'insert reason code that may come in from the movement event
                    oRc = oUdfs.ParseReasonCode_E1(tcurrentShipment.FreightControlledBySystem, FgTrack.MovementEvt)
                    If oRc IsNot Nothing Then
                        If populate_reasoncodes_orderinfo(tmpDemandLocation, tmpTrafficOrder, tcurrentShipment.ShipmentNumber, tmpMfgLoc) Then
                            oUdfs.InsertReasonCode_Rerun(tmpDemandLocation, tmpTrafficOrder, tmpMfgLoc, oRc.code, oRc.shipvia, tcurrentShipment.LastModifiedUser, EnteredDate)
                        End If
                    End If
                Next
            Case "APP"
                acurrentShipment = ApptNotification.Shipment
                tmpShipmentNum = acurrentShipment.ShipmentNumber
                ParseShipmentNumber(tmpShipmentNum, tmpTrafficOrder, tmpDemandLocation)

                If acurrentShipment.ShipmentHeaderMDFs IsNot Nothing Then
                    oRc = oUdfs.ParseReasonCode_E1(acurrentShipment.FreightControlledBySystem, acurrentShipment.ShipmentHeaderMDFs)
                    If oRc IsNot Nothing Then
                        oUdfs.InsertReasonCode_Rerun(tmpDemandLocation, tmpTrafficOrder, oRc.mfgloc, oRc.code, oRc.shipvia, acurrentShipment.LastModifiedUser, EnteredDate)
                    End If
                End If
        End Select
    End Sub

    Public Function populate_reasoncodes_orderinfo(ByVal Demand As String, ByVal Order_no As String, ByRef MovementNo As String, ByRef MfgLoc As String) As Boolean
        Dim daOrder As OdbcDataReader = Nothing
        Dim ssql As String
        Dim E1Database As String = My.Settings.SettingValue("E1Database")

        ssql = "SELECT TOP 1 * FROM (SELECT * FROM " & E1Database & ".F4211 WHERE SDMCU NOT IN (200, 100) AND SDDOCO =" & Order_no & " UNION SELECT * FROM " & E1Database & ".F42019 WHERE SDMCU NOT IN (200, 100) AND SDDOCO =" & Order_no & " ) F4201 "
        daOrder = DbConn.GetDataReader(ssql, My.Settings.E1Conn)
        If daOrder.HasRows Then
            MovementNo = Trim(daOrder("SDURRF") & "")
            MfgLoc = Trim(daOrder("SDMCU") & "")
            Return True
        ElseIf Val(Order_no) < 900000 Then
            ssql = "SELECT * FROM TrafficInformationTable WHERE DemandLocation = '" & Demand & "' AND Order_no =" & Order_no
            daOrder = DbConn.GetDataReader(ssql, My.Settings.SettingValue("TrackerConn"))
            If daOrder.HasRows Then
                MovementNo = daOrder("ShipmentNumber")
                MfgLoc = daOrder("MFGING_LOCATION")
                Return True
            Else
                'could not find a trafficinfo record, now check to see if it's in traffichistorytable
                ssql = "SELECT * FROM TrafficHistoryTable WHERE DemandLocation = '" & Demand & "' AND Order_no = " & Order_no
                daOrder = DbConn.GetDataReader(ssql, My.Settings.SettingValue("TrackerConn"))
                If daOrder.HasRows Then
                    MovementNo = daOrder("ShipmentNumber")
                    MfgLoc = daOrder("MFGING_LOCATION")
                    Return True
                Else
                    Return False
                End If
            End If
        End If
        Return False
    End Function


    'sarchbbold - process to look for shipconfirmed records 1 day old and create an update file to update delivered to oneNet.
    'Name: ProcessShipConfirmed_E1
    'Description: process to look for shipconfirmed records 1 day old and create an update file to update delivered to oneNet.
    'Return: Boolean
    '
    'Parameters:
    '
    'BatchDateStamp - date of shipment batch to process
    '
    Function ProcessShipConfirmed_E1() As Boolean

        '*********************These variables are used in writing contents of the OneNetShipments_E1 view to the OneNetShipments_LOG table******************************
        Dim daLog As New OdbcDataAdapter
        Dim dsLog As New DataSet
        Dim LogConn As Boolean = False
        Dim RowsAdded As Boolean = False
        Dim LogDate As Date = Now
        Dim PrevOrderNo As String = ""

        'These variables are used in writing contents of the OneNetShipmentLine_E1 view to the OneNetShipmentLine_LOG table
        Dim daLineLog As New OdbcDataAdapter
        Dim dsLineLog As New DataSet
        '***************************************************************************************************************************************************************

        Dim sSql As String
        Dim rdrFps As OdbcDataReader
        Dim rdrOrderNo As OdbcDataReader
        Dim dsShipmentMap As New DataSet
        Dim OrderNoList As String = ""
        Dim CarrierID As String = ""
        Dim MovementNo As String = ""
        Dim archivedShipmentList As ShipmentListMessage


        Const FILE_DATE_FORMAT As String = "yyyyMMddHHmmss"

        Try
            CarrierID = GetCarrierID() 'Get the accepted Carrier ID's from the system.
            sSql = ""
            Dim NextTime As Date = Now.Date
            NextTime = NextTime.AddDays(-5)
            'Select orders that are ShipConfirmed and match the list.
            sSql = "Select Distinct SDDOCO,SDAN8,SDCARS from E1DataMining.dbo.F42119 "
            sSql = sSql + "Left Join OneNetwork.dbo.DeliveredTracker DT "
            sSql = sSql + "ON OrderID = SDDOCO "
            sSql = sSql + "where OrderID is null and SDNXTR = '999' and SDLTTR = '620' and SDADDJ >= '" & ConvertDateToJulian(Now.Date) - 6 & "' and SDADDJ <= '" & ConvertDateToJulian(Now.Date) - 3 & "' and SDCARS in (" & CarrierID & ")"


            rdrOrderNo = DbConn.GetDataReader(sSql, My.Settings.SettingValue("OneNetworkConn"), "ProcessShipConfirmed_E1 - Outbound")



            If rdrOrderNo Is Nothing Then
                Return False
            Else

                Do While rdrOrderNo.Read()
                    If GetAutoMarkExctp(rdrOrderNo("SDAN8").ToString.Trim, rdrOrderNo("SDCARS").ToString.Trim) = False Then
                        OrderNoList = OrderNoList & "''" & rdrOrderNo("SDDOCO") & "'',"
                    End If
                Loop
                If OrderNoList.Length > 0 Then ' remove last ","
                    OrderNoList = OrderNoList.Substring(0, OrderNoList.Length - 1)
                Else
                    Return False
                End If

                sSql = ""

                'reset NextTime
                NextTime = Now.Date
                NextTime = NextTime.AddDays(-2)

                sSql = "SP_OneNetShipmentsDelivered_E1 '" & OrderNoList & "', " & ConvertDateToJulian(NextTime)
                'get all the Shipments in the batch datestamp range

                rdrFps = DbConn.GetDataReader(sSql, My.Settings.SettingValue("OneNetworkConn"), "ProcessShipConfirmed_E1 - Outbound")

                'no data returned
                If rdrFps Is Nothing Then
                    Return False
                End If
            End If
            rdrOrderNo.Close()
            'loop through all shipments in E1
            Dim lstNewShipments, lstUpdatedShipments, lstDeletedShipments, lstArchivedShipments, lstCancelledShipments, lstNewCreatetoDraftShipments As New ArrayList

            'Form OneNetwork outbound filenames for each type of action.  This was previously done at the end but we need the filenames ahead of time so we can write them to the outboundlog table for each order

            Dim outFileArchive As String = "kcp_" & Date.Now.ToString(FILE_DATE_FORMAT) & ".archived.xml"
            Dim currentoutFile As String

            Dim i As Long
            If rdrFps.HasRows Then
                i = 0
                RaiseEvent RecordCount(100)
                '************************************************THIS SECTION STRICTLY FOR WRITING TO LOG FILES*********************************************
                'get data adapter for OneNetShipments_Log table 

                If DbConn.GetUpdateableDataAdapter("SELECT * FROM OneNetShipments_LOG WITH(NOLOCK) WHERE 1=0", My.Settings.SettingValue("OneNetworkConn"), daLog) Then
                    daLog.Fill(dsLog)
                    LogDate = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))

                    'get data adapter for OneNetShipmentLine_LOG table
                    If DbConn.GetUpdateableDataAdapter("SELECT * FROM OneNetShipmentLine_LOG WITH(NOLOCK) WHERE 1=0", My.Settings.SettingValue("OneNetworkConn"), daLineLog) Then
                        daLineLog.Fill(dsLineLog)
                        LogConn = True
                    Else
                        'If we can't open the OneNetShipmentLine_LOG dispose of the DataAdapter and DataSet we opened for the OneNetShipments_LOG table above
                        daLog.Dispose()
                        dsLog.Dispose()
                    End If
                End If

                '*********************************************************************************************************************************************
            End If


            'loop through all the shipment records
            Do While rdrFps.Read()
                MovementNo = ""
                Dim orderOk As Boolean
                Dim Processed As Boolean = False
                If PrevOrderNo.Trim <> rdrFps("Order_no").ToString.Trim Then
                    PrevOrderNo = rdrFps("Order_no").ToString.Trim
                    orderOk = False
                End If


                If Processed = False Then 'only do this if the order has not been processed.

                    'declare some temp vars and set it
                    Dim DemandLocation, MfgingLocation, OrderNo, ShipmentNumber, Order_Type, ActionExec As String
                    Dim Julian_Date_Modified, Julian_Time_Modified, ASN_PalletCount As Integer
                    Dim HashValue As Long  'Hash value of the Sales Details data 
                    Dim CreateToDraft As Boolean
                    Dim SendCancel As Boolean


                    'init vars
                    DemandLocation = ""
                    MfgingLocation = ""
                    OrderNo = ""
                    ShipmentNumber = ""
                    Order_Type = ""
                    ActionExec = ""
                    Julian_Date_Modified = 0
                    Julian_Time_Modified = 0
                    HashValue = 0
                    CreateToDraft = False
                    currentoutFile = ""
                    SendCancel = False
                    ASN_PalletCount = 0

                    Try
                        DemandLocation = rdrFps("TrafficDemand")
                        MfgingLocation = rdrFps("Mfging_Location")
                        OrderNo = rdrFps("Order_No")
                        Order_Type = rdrFps("Order_Type")
                        ShipmentNumber = rdrFps("TrafficShipmentNumber")
                        ActionExec = Trim(rdrFps("ActionExec") & "")
                        Julian_Date_Modified = rdrFps("Julian_Date_Modified")
                        Julian_Time_Modified = rdrFps("Julian_Time_Modified")
                        HashValue = Val(rdrFps("HashValue") & "")
                        CreateToDraft = CBool(rdrFps("CreateToDraft") & "")
                        ASN_PalletCount = Val(rdrFps("ASN_PalletCount") & "")

                        orderOk = True

                        'Set the name of the currentoutFile based on the action type
                        currentoutFile = outFileArchive

                    Catch ex As Exception
                        orderOk = False
                        ErrObj.log(1, 0, "ProcessShipConfirmed_E1", "Error w/ primary keys", ex)
                    End Try

                    'check to see if this is a OneNetwork enabled Location
                    If orderOk Then

                        'create new Shipment object
                        Dim newShipment As New ShipmentListMessageShipment

                        'map the record to the object
                        orderOk = oMapping.MapFields(rdrFps, newShipment, "Shipment", , My.Settings.InterfaceType, "AMD") 'AMD = Auto Mark Complete

                        'makes sure order is ok
                        If orderOk Then
                            'now get all the Line Items for this shipment
                            orderOk = GetShipmentLine_E1(newShipment, DemandLocation, MfgingLocation, OrderNo, ActionExec, dsLineLog, currentoutFile, LogDate)
                        End If


                        If orderOk Then
                            'UPDATED
                            'lstUpdatedShipments.Add(newShipment)
                            newShipment.ActionName = "TMS.TrackShipmentViaInteg"
                            'newShipment.ActionName = "TrackShipmentViaInteg"
                            lstArchivedShipments.Add(newShipment)
                        Else
                            'no shipment line so do not do anything
                            orderOk = False
                        End If
                    Else
                        'this is not OneNetwork enabled, so just set order to Ok because we are not sending this to OneNetwork
                        orderOk = False
                    End If

                    'see if order was ok
                    If orderOk Then

                        If Update_OutBoundLog_Table(DemandLocation, OrderNo, Order_Type, ActionExec, HashValue, currentoutFile, Julian_Date_Modified, Julian_Time_Modified, ASN_PalletCount) Then

                            '*****************************IF write to outbound log successfull then also write to the onenetshipments log********************************************
                            If LogConn Then
                                Dim drLog As DataRow

                                drLog = dsLog.Tables(0).NewRow

                                'Call function to copy data from all columns in rdrFps to the drLog datarow
                                If Copy_DataRow_Fields(drLog, , rdrFps) Then

                                    'There are some additional field we want to write to as well
                                    With drLog
                                        .Item("EntryDateTime") = LogDate
                                        .Item("FileName") = currentoutFile
                                    End With

                                    'add to dataset
                                    dsLog.Tables(0).Rows.Add(drLog)
                                    RowsAdded = True
                                End If
                            End If
                            '**************************************************************************************************************************************************************
                        Else
                            'failed to update outbound log
                            Return False
                        End If

                        'increment
                        i += 1
                        '######################## Update [DeliveredTracker] with most recent entry ####################
                        Dim daLastTime As OdbcDataAdapter = Nothing
                        Dim dsLastTime As New DataSet
                        Dim drLastTime As DataRow

                        'open  table to write to
                        sSql = "SELECT * FROM DeliveredTracker WHERE 1 = 0"
                        DbConn.GetUpdateableDataAdapter(sSql, My.Settings.SettingValue("OneNetworkConn"), daLastTime)
                        daLastTime.Fill(dsLastTime)

                        'create new row to dump data to
                        drLastTime = dsLastTime.Tables(0).NewRow
                        Dim CurTime As String

                        CurTime = Date.Now.ToString("HH:mm:ss").Replace(":", "")

                        drLastTime("ORDERID") = OrderNo
                        drLastTime("TimeMarked") = CurTime
                        drLastTime("DateMarked") = ConvertDateToJulian(Date.Now.Date)
                        drLastTime("MovementNo") = rdrFps("TrafficShipmentNumber")
                        drLastTime("FileName") = currentoutFile

                        'add the row
                        dsLastTime.Tables(0).Rows.Add(drLastTime)

                        'commit changes
                        daLastTime.Update(dsLastTime)
                        dsLastTime.AcceptChanges()
                        '######################## END ####################


                        RaiseEvent Progress(i)
                    End If
                End If


            Loop

            '******************************************save changes to onenetshipments log table and dispose**************************************************
            If LogConn And RowsAdded Then
                daLog.Update(dsLog)
                dsLog.AcceptChanges()
                daLineLog.Update(dsLineLog)
                dsLineLog.AcceptChanges()
                daLog.Dispose()
                dsLog.Dispose()
                daLineLog.Dispose()
                dsLineLog.Dispose()
            End If
            '*************************************************************************************************************************************************

            'cleanup
            rdrFps.Close()

            'we're done, just max out progress bar
            RaiseEvent Progress(100)
            RaiseEvent Status("Processed " & i)

            Dim outFile As String


            ' everything should be archive now
            If lstArchivedShipments.Count > 0 Then
                archivedShipmentList = New ShipmentListMessage
                oMapping.MapFields(Nothing, archivedShipmentList, "ShipmentListMessage", , My.Settings.InterfaceType)

                archivedShipmentList.Shipment = lstArchivedShipments.ToArray(GetType(ShipmentListMessageShipment))

                'write out the archived ShipmentList XML file
                'outFile = "kcp_" & Date.Now.ToString(FILE_DATE_FORMAT) & ".archived.xml"
                outFile = outFileArchive
                If Not CreateShipmentXML(archivedShipmentList, My.Settings.SettingValue("FTPShipLocalDir"), outFile) Then
                    Return False
                End If

            End If
        Catch ex As Exception
            ErrObj.log(1, 0, "ProcessShipConfirmed_E1", "Error Processing Batches ", ex)
            Return False
        End Try


        Return True

    End Function

    Public Sub ProcessShipChargeNotif_ProNum(ByVal FgPay As FGIIntegPaymentNotification, ByVal fileId As Integer, ByRef Orders() As Agent.Order_Type, Optional ByRef refno As String = Nothing)
        ' Dim oMovement As Movement = FgTrack.Movement 'OneNetwork
        Dim Movementno As String = ""
        Try

            Movementno = FgPay.Movement.MovementNumber.ToString.Trim
            Dim CurrentPro As String = ""
            Dim NewPro As String = FgPay.Movement.CarrierProNumber.ToString.Trim
            Dim sSql As String = ""
            Dim E1Database As String = My.Settings.SettingValue("E1Database")
            Dim cnnE1 As OdbcConnection
            Dim txn As OdbcTransaction
            Dim Doco As String

            Try
                cnnE1 = DbConn.CreateConnection(My.Settings.E1Conn)
                cnnE1.ConnectionTimeout = 300
                cnnE1.Open()
            Catch ex As Exception
                ErrObj.log(1, 0, "ProcessShipChargeNotif_ProNum", "Could not open connection. Error processing  FileID:" & fileId.ToString & " Movement#:" & Movementno, ex)
                Exit Sub
            End Try

            Dim rdr_SalesOrder As OdbcDataReader = Nothing





            'Get check the order to see if the SHCNID exists and if not grab the DOCO to do the updates
            sSql = "SELECT SHCNID, SHDOCO "
            sSql = sSql & "FROM " & E1Database & ".F42019 LEFT JOIN " & E1Database & ".F554202x ON SHDOCO = TMDOCO "
            sSql = sSql & "WHERE '" & Movementno & "' = CASE WHEN SHURRF = '" & Movementno & "' THEN SHURRF WHEN TMDOCO IS NULL THEN SHURRF ELSE TMURRF END "
            rdr_SalesOrder = DbConn.GetDataReader(sSql, cnnE1)

            'rdr_SalesOrder.Read()
            txn = cnnE1.BeginTransaction
            'CHange this to not care if it exists, we will use what ever is in payment file.
            While rdr_SalesOrder IsNot Nothing And rdr_SalesOrder.HasRows = True And rdr_SalesOrder.Read = True

                Doco = rdr_SalesOrder("SHdoco").ToString.Trim
                'Now we do the updates
                Dim daSales_Header As OdbcDataAdapter = Nothing
                Dim daSales_Detail As OdbcDataAdapter = Nothing
                Dim dsSales_Header As New DataSet
                Dim dsSales_Detail As New DataSet
                Dim drSales_Header As DataRow

                sSql = "SELECT * FROM " & E1Database & ".F42019 WHERE SHDOCO =" & Doco
                DbConn.GetUpdateableDataAdapter(sSql, cnnE1, daSales_Header, txn)
                If daSales_Header.Fill(dsSales_Header) > 0 Then

                    'we should only really get back one Sales Header record
                    drSales_Header = dsSales_Header.Tables(0).Rows(0)
                    drSales_Header("SHCNID") = NewPro

                    daSales_Header.Update(dsSales_Header)
                    dsSales_Header.AcceptChanges()
                End If
                sSql = "SELECT * FROM " & E1Database & ".F42119 WHERE SDDOCO =" & Doco
                DbConn.GetUpdateableDataAdapter(sSql, cnnE1, daSales_Detail, txn)

                If daSales_Detail.Fill(dsSales_Detail) > 0 Then
                    For Each drSales_Detail As DataRow In dsSales_Detail.Tables(0).Rows
                        drSales_Detail("SDCNID") = NewPro
                        daSales_Detail.Update(dsSales_Detail)
                        dsSales_Detail.AcceptChanges()
                    Next
                End If
                'rdr_SalesOrder.Read()
            End While
            txn.Commit()
            txn.Dispose()

        Catch ex As Exception
            ErrObj.log(1, 0, "ProcessShipChargeNotif_ProNum", "Error writing to ExpectedFreight Table for Movement:" & Movementno & " FileID:" & fileId.ToString, ex, fileId)

        End Try

    End Sub

    Public Function ConvertDateToJulian(ByVal InDate As Object) As Integer
        Dim BigDate As Date

        If InDate Is Nothing Or InDate Is System.DBNull.Value Then Return 0
        If IsDate(InDate) = False Then Return 0
        BigDate = CDate(InDate)
        If BigDate > CDate("2899-12-31") Then BigDate = CDate("2899-12-31")

        Return (DateDiff(DateInterval.Year, CDate("1900-01-01"), BigDate) * 1000) + DateDiff(DateInterval.Day, CDate(Format(Year(BigDate), "0000") + "-01-01"), BigDate) + 1
    End Function
    Public Function GetCarrierID()
        Dim SSql As String
        Dim CarrierID As String = ""
        Dim rdrCarrier As OdbcDataReader

        SSql = "Select CarrierID from AutoDeliverCarrierList"
        rdrCarrier = DbConn.GetDataReader(SSql, My.Settings.SettingValue("OneNetworkConn"), "ProcessShipConfirmed_E1 - Outbound")
        Do While rdrCarrier.Read()
            CarrierID = CarrierID & "'" & rdrCarrier("CarrierID") & "',"
        Loop
        If CarrierID.Length > 0 Then ' remove last ","
            CarrierID = CarrierID.Substring(0, CarrierID.Length - 1)
        End If
        rdrCarrier.Close()
        Return CarrierID
    End Function
    Public Function GetAutoMarkExctp(ByVal Carrier, ByVal ABAN8)
        Dim SSql As String
        Using cnn As New OdbcConnection(My.Settings.SettingValue("OneNetworkConn"))
            Dim dr As OdbcDataReader
            SSql = "Select ID from AutoMarkExcept where ABAN8 = ? and Carrier = ?"

            Dim cmd As New OdbcCommand(SSql, cnn)
            cmd.Parameters.Add("@ABAN8", OdbcType.VarChar).Value = Trim(ABAN8).Replace("'", "''")
            cmd.Parameters.Add("@CARRIER", OdbcType.VarChar).Value = Trim(Carrier).Replace("'", "")

            cnn.Open()
            dr = cmd.ExecuteReader

            If dr IsNot Nothing Then
                If dr.HasRows Then
                    cnn.Close()
                    dr.Close()
                    Return True
                Else
                    cnn.Close()
                    dr.Close()
                    Return False
                End If
            Else
                cnn.Close()
                dr.Close()
                Return False
            End If

        End Using
    End Function
    Function Update_Z_BatchID(ByVal Orders_Processed As String)

        Dim daOrder As OdbcDataReader = Nothing
        Dim ssql As String
        Dim E1Database As String = My.Settings.SettingValue("E1Database")

        ssql = "Update " & E1Database & ".F4201z1 Set SYEDSP ='Y' where SYEDBT in (" & Orders_Processed & ")"
        If Not DbConn.ExecuteQuery(ssql, My.Settings.E1Conn) Then
            ErrObj.log(1, 1, "OneShipments.Update_Z_BatchID", "Update query failed for SYEDBT: " & Orders_Processed, , True)
        End If
        Return True
    End Function

    Function CheckCarrier(ByVal Carrier As String, ByVal Country As String) As Integer
        Dim SSql As String
        Dim carrierID As Integer
        Using cnn As New OdbcConnection(My.Settings.SettingValue("OneNetworkConn"))
            Dim dr As OdbcDataReader
            SSql = "Select E1AN8 from CarrierMap where Country = ? and Carrier = ?"

            Dim cmd As New OdbcCommand(SSql, cnn)
            cmd.Parameters.Add("@Country", OdbcType.VarChar).Value = Trim(Country).Replace("'", "''")
            cmd.Parameters.Add("@CARRIER", OdbcType.VarChar).Value = Trim(Carrier).Replace("'", "")

            cnn.Open()
            dr = cmd.ExecuteReader

            If dr IsNot Nothing Then
                If dr.HasRows Then

                    carrierID = dr.Item("E1AN8")
                    cnn.Close()
                    dr.Close()
                     Return carrierID
                Else
                    cnn.Close()
                    dr.Close()
                    Return 0
                End If
            Else
                cnn.Close()
                dr.Close()
                Return 0
            End If

        End Using



    End Function
End Class

