Imports System.Data.Odbc

Public Class UomConversion
    Private convTable As DataSet

    Public Sub New()
        'grab the UOM Conversion table from Macola
        convTable = New DataSet

        'get dataset of uom conversion
        DbConn.GetDataSet("SELECT * FROM UOMCONV", My.Settings.XkikappsConn, convTable)
    End Sub
    '
    'Name: ConvertUOM
    'Description: goes after Macola Conversion table to find uom conversion table
    'Return: Boolean
    '
    'Parameters:
    '
    'fromUOM - uom to convert
    'toUOM - uom destination
    'value - numeric value to convert
    'retVal - numeric result after conversion
    '
    Public Function ConvertUOM(ByVal fromUOM As String, ByVal toUOM As String, ByVal value As Double, ByRef retVal As Double) As Boolean
        'first look to see if the fromUOM -> toUOM can be found
        Dim dr() As DataRow
        dr = convTable.Tables(0).Select("FROM_UOM = '" & fromUOM & "' AND TO_UOM = '" & toUOM & "'")

        'check to see if we get anything back
        If dr.Length = 0 Then
            ErrObj.log(1, 0, "ConvertUom", "Can't find UOM Conversion " & fromUOM & " -> " & toUOM)
            Return False
        Else

            Dim operation As String
            Dim factor As Double

            'got something back, now all we need is the first row
            operation = dr(0).Item("OPERATION")
            factor = dr(0).Item("FACTOR")

            Select Case operation
                Case "*"
                    retVal = value * factor
                Case "/"
                    retVal = value / factor
            End Select

        End If

        Return True

    End Function

End Class
