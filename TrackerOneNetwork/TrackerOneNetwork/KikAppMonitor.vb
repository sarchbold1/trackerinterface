Public Class KikAppMonitor

    Private Const APPID As Integer = 32

    Public Enum Status
        START
        FINISH
    End Enum

    Public Shared Sub Register(ByVal stat As Status)

        Dim ssql As String

        If stat = Status.START Then
            Archive(APPID)

            ssql = "INSERT INTO Process (ID,RUNDATE,RUNTIME,RUN,ARCHIVE,EXCEEDED,NOTIFIED) VALUES (" & APPID & ",GETDATE(),GETDATE(),1,0,0,0)"
        Else
            ssql = "INSERT INTO Process (ID,RUNDATE,RUNTIME,RUN,ARCHIVE,EXCEEDED,NOTIFIED) VALUES (" & APPID & ",GETDATE(),GETDATE(),0,0,0,0)"
        End If

        DbConn.ExecuteQuery(ssql, My.Settings.KikAppMonConn)

    End Sub

    Public Shared Sub Archive(ByVal appid As Integer)
        Dim ssql As String
        ssql = "UPDATE Process SET ARCHIVE = 1 WHERE ID = " & appid & " AND ARCHIVE = 0"
        DbConn.ExecuteQuery(ssql, My.Settings.KikAppMonConn)
    End Sub


End Class
