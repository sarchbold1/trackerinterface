Imports System.IO
Imports System.Data.Odbc



Public Class Agent
    Private Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

    Public Structure Order_Type
        Public Demand_Location As String
        Public Order_Number As String
        Public Document_Type As String
        Public FileName As String
    End Structure
    Private WithEvents oShipment As OneShipments

    Public Event Progress(ByVal i As Long)
    Public Event Status(ByVal msg As String)

    Private Sub oShipment_Progress(ByVal i As Long) Handles oShipment.Progress
        RaiseEvent Progress(i)
    End Sub

    Private Sub oShipment_Status(ByVal msg As String) Handles oShipment.Status
        RaiseEvent Status(msg)
    End Sub

    Public Sub New()
        oShipment = New OneShipments
    End Sub

    Public Function StartTransfers() As Boolean

        Dim sqlDateTime As Date
        Dim procStep As String = Nothing

        Try

            'get db server date time
            sqlDateTime = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))

            '***** PART 1 - Process Outbound Files *****
            procStep = "SelectBatches"

            oShipment.SelectBatches()
            'oShipment.ProcessBatches(sqlDateTime)
            'New April 30th - process auto Mark Delivered.
            oShipment.ProcessShipConfirmed_E1()
            RaiseEvent Status("Sending files")

            procStep = "ProcessOutboundQueue"
            ProcessOutboundQueue()

            RaiseEvent Status("Receiving files")

            'download files from OneNetwork            
            procStep = "ReceiveFiles"
            ReceiveFiles()

            RaiseEvent Status("Finished sending")

            '***** PART 2 - Process Inbound Files *****

            InsertInboundFile("*.xml", My.Settings.SettingValue("FTPInboundDir"))

            procStep = "ProcessInboundQueue"
            ProcessInboundQueue()

            'send out error summary
            procStep = "sendErrorSummary"
            ErrObj.sendErrorSummaryEmail(sqlDateTime)

        Catch ex As Exception
            ErrObj.log(1, 1, "Agent.StartTransfers", "Error Attempting the Transfers. Current Proc " & procStep, ex, , True)
            Return False
        End Try

        RaiseEvent Status("Done")

        'cleanup
        oShipment = Nothing
        Return True

    End Function

    Public Function StartTransfers_E1() As Boolean

        Dim sqlDateTime As Date
        Dim procStep As String = Nothing
        Dim Batchno As Integer = 0
        

        Try
            'This is only run to synchronize E1 appointment date/time with table OneNetShipmentReport (which is OneNet sched data)
            'THIS SHOULD BE DISABLED EXCEPT IN THE RARE OCCASION A SYNC IN NEEDED
            'oShipment.Update_AppointmentDates()
            'oShipment.Update_UnpaidOrders_Table()
            '-------------------------------------


            'get db server date time
            sqlDateTime = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))

            'Update Orders with Blank Movement(URRF) that have cleared checkpoint
            'oShipment.Set_Defualt_Movement()

            'ErrObj.log(3, 0, "Pallet UOM Conversion", "Error Encountered During Pallet UOM Coversion for Order:1062875", , , True, My.Settings.SettingValue("OneNetworkEmailDist"), "Pallet UOM Conversion Error")
            'Dim dt As String
            'Dim dt2 As DateTime
            '***** PART 1 - Process Outbound Files *****


            procStep = "SelectBatches"

            oShipment.ProcessBatches_E1()

            procStep = "SelectBatches - ShipConfirmed"
            'Process shipconfirmed

            oShipment.ProcessShipConfirmed_E1()

            RaiseEvent Status("Sending files")

            ProcessOutboundQueue()

            RaiseEvent Status("Finished sending")

            RaiseEvent Status("Receiving files")
            ''download files from OneNetwork           
            procStep = "ReceiveFiles"

            ReceiveFiles()

            RaiseEvent Status("Finished receiving")


            '***** PART 2 - Process Inbound Files *****
            RaiseEvent Status("Processing Inbound Files")
            'Insert XML files

            InsertInboundFile("*.xml", My.Settings.SettingValue("FTPInboundDir"))

            'Insert CSV Routing Sequence files

            InsertInboundFile("*.csv", My.Settings.SettingValue("FTPInboundDir"))

            procStep = "ProcessInboundQueue"
            'ProcessInboundQueue_E1_TEST(False, True)


            ProcessInboundQueue_E1()

            'Only proceed to Delete/Assign if XREFS (upon which the Master BOLs are determined) have been updated 

            If oShipment.Update_MasterBOL_Xrefs Then
                'Delete MasterBOL for UNConsolidated orders that shared the same Movement/Reservation ID that still exist in F5547003
                oShipment.Delete_MasterBOL()

                'Create a MasterBOL for Consolidated orders that share the same Movement/Reservation ID that don't already exist in F5547003
                oShipment.Assign_MasterBOL()
            End If

            'send out error summary
            procStep = "sendErrorSummary"
            ErrObj.sendErrorSummaryEmail(sqlDateTime)

        Catch ex As Exception
            ErrObj.log(1, 1, "Agent.StartTransfers", "Error Attempting the Transfers. Current Proc " & procStep, ex, , True)
        End Try
        RaiseEvent Status("Done")

        'cleanup
        oShipment = Nothing
        Return True

    End Function

    Sub FtpStatusHandler(ByVal msg As String)
        RaiseEvent Status(msg)
    End Sub

    Sub FtpProgressHandler(ByVal i As Long)
        RaiseEvent Progress(i)
    End Sub

    Function ReceiveFiles() As Boolean

        Dim oFtp As New FtpClient

        AddHandler oFtp.Status, AddressOf FtpStatusHandler
        AddHandler oFtp.Progress, AddressOf FtpProgressHandler

        Try
            'create connection
            If oFtp.Connect(My.Settings.SettingValue("OneNetWorkFTP"), My.Settings.SettingValue("FTPUsername"), My.Settings.SettingValue("FTPPassword")) Then
                'Receive - Inbound Files from OneNetwork
                oFtp.ReceiveFiles(My.Settings.SettingValue("FTPInboundDir"), My.Settings.FTPInboundRemoteMask, My.Settings.SettingValue("FTPRemoteReceiveDir"))

                'Receive - Inbound CSV files from OneNetwork
                oFtp.ReceiveFiles(My.Settings.SettingValue("FTPInboundDir"), "*.csv", My.Settings.SettingValue("FTPRemoteReceiveDir"))

                oFtp.Disconnect()
            Else
                ErrObj.log(1, 0, "Agent.ReceiveFiles", "could not connect to ftp " & My.Settings.SettingValue("OneNetWorkFTP"))
            End If

        Catch ex As Exception
            Throw New Exception("Can't connect to " & My.Settings.SettingValue("OneNetWorkFTP"), ex)
        End Try

        'cleanup
        RemoveHandler oFtp.Status, AddressOf FtpStatusHandler
        RemoveHandler oFtp.Progress, AddressOf FtpProgressHandler

        oFtp = Nothing

        Return True

    End Function

    Function ProcessOutboundQueue() As Boolean

        Dim ssql As String
        Dim ds As New DataSet

        Dim cnn As OdbcConnection = Nothing
        Dim da As OdbcDataAdapter = Nothing

        Dim i As Integer = 0

        Dim oFtp As New FtpClient

        'connect to ftp
        If Not oFtp.Connect(My.Settings.SettingValue("OneNetWorkFTP"), My.Settings.SettingValue("FTPUsername"), My.Settings.SettingValue("FTPPassword")) Then
            Return False
        End If

        Try
            cnn = DbConn.CreateConnection(My.Settings.SettingValue("OneNetworkConn"))
            cnn.ConnectionTimeout = 300
            cnn.Open()

            ssql = "SELECT * FROM OneNetFiles WHERE direction = 'out' AND Status = 'P'"

            DbConn.GetUpdateableDataAdapter(ssql, cnn, da)

            If da.Fill(ds) > 0 Then

                For Each dr As DataRow In ds.Tables(0).Rows

                    Dim fname As String
                    Dim content As String

                    fname = CheckDir(My.Settings.SettingValue("FTPShipLocalDir"))
                    If fname <> "" Then
                        fname = fname & dr("filename")
                        content = dr("content")

                        Try
                            'write to file
                            Dim writer As New StreamWriter(fname)
                            writer.Write(content)

                            'close file so we can send it up
                            writer.Close()

                            'send file to remote ftp site
                            If oFtp.SendFile(fname, My.Settings.SettingValue("FTPShipRemoteDir")) Then
                                dr("processed") = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))
                                dr("status") = "C"

                                da.Update(ds)
                                ds.AcceptChanges()
                            Else
                                ErrObj.log(1, 0, "Agent.ProcessOutboundQueue", "Outbound FTP transfer failed for filename: " & fname)
                            End If

                        Catch ex As Exception
                            ErrObj.log(1, 0, "Agent.ProcessOutboundQueue", "could not send/serialize file", ex)
                        End Try

                    End If

                    i += 1
                    RaiseEvent Progress(CInt(i / ds.Tables(0).Rows.Count))
                Next

            End If

        Catch ex As Exception
            ErrObj.log(1, 0, "Agent.ProcessOutboundQueue", "Error retrieving record set from table 'OneNetFiles'", ex)
        End Try

        'cleanup
        oFtp.Disconnect()
        da.Dispose()
        ds.Dispose()

        cnn.Close()
        cnn.Dispose()
        oFtp = Nothing

    End Function


    '
    'Function: ProcessInboundQueue
    'Description: process incoming files stored in database in order of timestamp on files
    '
    Function ProcessInboundQueue() As Boolean

        Dim ssql As String
        Dim ds As New DataSet
        Dim cnn As OdbcConnection
        Dim i As Long

        Dim refNo As String = Nothing

        Try
            cnn = DbConn.CreateConnection(My.Settings.SettingValue("OneNetworkConn"))
            cnn.Open()
        Catch ex As Exception
            ErrObj.log(1, 2, "ProcessQueue", "Could not open connection " & My.Settings.SettingValue("OneNetworkConn"), ex)
            Return False
        End Try

        ssql = "SELECT * FROM OneNetProcessFiles WHERE direction = 'in'"
        DbConn.GetDataSet(ssql, cnn, ds)

        RaiseEvent Status("Processing Incoming files")

        'loop through files to process
        For i = 0 To ds.Tables(0).Rows.Count - 1

            Dim dr As DataRow

            Dim fname As String
            Dim fileId As Long
            Dim strContents As String

            Dim retProc As OneShipments.ProcessReturnType = OneShipments.ProcessReturnType.Fail

            'assign row
            dr = ds.Tables(0).Rows(i)

            'save some temp vars
            fileId = dr("id")
            fname = dr("filename")
            strContents = dr("content")

            Try

                'create object for xml file
                Dim oFile As New OneFile(fname, strContents)

                'determine environment
                If oFile.Environment.Equals("MACOLA") Then
                    refNo = oFile.RefNo
                    'process file based on type
                    Select Case oFile.xmlFiletype
                        Case OneFile.OneFileType.Consolidated
                            retProc = oShipment.ProcessConsolidationNotification(oFile.xmlObject, refNo)

                        Case OneFile.OneFileType.Appointment
                            retProc = oShipment.ProcessApptNotification(oFile.xmlObject, fileId, refNo)

                        Case OneFile.OneFileType.Tracking
                            retProc = oShipment.ProcessTrackNotification(oFile.xmlObject, fileId, refNo)

                        Case OneFile.OneFileType.Payment
                            retProc = oShipment.ProcessShipChargeNotif(oFile.xmlObject, fileId, refNo)

                        Case OneFile.OneFileType.Modified
                            retProc = oShipment.ProcessModShipNotif(oFile.xmlObject, fileId, refNo)

                        Case Else
                            'if we don't know the file, just ignore it
                            ErrObj.log(1, 3, "ProcessQueue", "Unknown file received " & fname)
                            retProc = OneShipments.ProcessReturnType.Success

                    End Select
                Else
                    'unhandled, skip
                    refNo = oFile.RefNo
                    retProc = OneShipments.ProcessReturnType.Skip
                End If



            Catch ex As Exception

                ErrObj.log(1, 0, "Agent.ProcessInboundQueue", "Could not process fileid " & fileId, ex)

            End Try

            'see the result of processing
            If retProc = OneShipments.ProcessReturnType.Success Then

                'Set status to Complete
                ssql = "UPDATE OneNetFiles SET STATUS = 'C', PROCESSED = GETDATE(), REFNO = '" & refNo & "' WHERE ID = " & fileId
                If Not DbConn.ExecuteQuery(ssql, My.Settings.SettingValue("OneNetworkConn")) Then
                    ErrObj.log(1, 0, "ProcessQueue", "Could not update file id " & fileId)
                    Return False
                End If
            ElseIf retProc = OneShipments.ProcessReturnType.Skip Then

                'Set status to Skip
                ssql = "UPDATE OneNetFiles SET STATUS = 'S', PROCESSED = GETDATE(),REFNO = '" & refNo & "' WHERE ID = " & fileId
                If Not DbConn.ExecuteQuery(ssql, My.Settings.SettingValue("OneNetworkConn")) Then
                    ErrObj.log(1, 0, "ProcessQueue", "Could not update file id " & fileId)
                    Return False
                End If


            Else
                'need to reprocess this again later or fix it
                ErrObj.log(1, 0, "ProcessQueue", "Could not process file " & fname)
                Return False

            End If

            RaiseEvent Progress(i)

        Next

        'just put bar to 100%
        RaiseEvent Progress(100)

        'cleanup
        cnn.Close()

        Return True

    End Function

    '
    'Function: ProcessInboundQueue
    'Description: process incoming files stored in database in order of timestamp on files
    '
    Function ProcessInboundQueue_E1() As Boolean

        Dim ssql As String
        Dim ds As New DataSet
        Dim cnn As OdbcConnection
        Dim i As Long
        Dim MasterOrders() As Order_Type = Nothing
        Dim MasterOrderCount As Integer = 0
        Dim IgnoreDuplicateCharges As Boolean = False
        Dim RoutingSequence As Boolean = False
        Dim CurrentDateTime As Date = Now
        Dim E1Database As String = My.Settings.SettingValue("E1Database")

        Dim refNo As String = Nothing

        Try
            cnn = DbConn.CreateConnection(My.Settings.SettingValue("OneNetworkConn"))
            cnn.ConnectionTimeout = 300
            cnn.Open()
        Catch ex As Exception
            ErrObj.log(1, 2, "Agent.ProcessInboundQueue_E1", "Could not open connection " & My.Settings.SettingValue("OneNetworkConn"), ex)
            Return False
        End Try

        Try
           
            ssql = "select * from onenetfiles where direction = 'in' and status = 'P'  ORDER BY Fileseq"
           
            DbConn.GetDataSet(ssql, cnn, ds)

        Catch ex As Exception
            ErrObj.log(1, 2, "Agent.ProcessInboundQueue_E1", "Could not retrieve recordset from table OneNetFiles", ex)
            Return False
        End Try


        RaiseEvent Status("Processing Incoming files")
        Using writer As StreamWriter = New StreamWriter("C:/Temp/payTracking.txt", True)
            writer.WriteLine("  ")
            writer.WriteLine("  ")
            writer.WriteLine("-----START ROUND------ ")

            'loop through files to process
            For i = 0 To ds.Tables(0).Rows.Count - 1

                Dim dr As DataRow
                Dim Orders() As Order_Type = Nothing
                Dim fname As String
                Dim fileId As Long
                Dim strContents As String

                Dim retProc As OneShipments.ProcessReturnType = OneShipments.ProcessReturnType.Fail

                'assign row
                dr = ds.Tables(0).Rows(i)

                'save some temp vars
                fileId = dr("id")
                fname = dr("filename")
                strContents = dr("content")
                IgnoreDuplicateCharges = CBool(dr("IgnoreDuplicateCharges"))
                RoutingSequence = CBool(dr("RoutingSequence"))

                If UCase(fname).Contains(".CSV") = True Then   'IF TRUE THEN THIS IS A ROUTE SEQUENCE CSV FILE
                    Dim cnnE1 As OdbcConnection = Nothing
                    Dim CSV_Rows() As String
                    Dim Row_Index, Col_Index As Integer
                    Dim Fields(), ColHeadings() As String
                    Dim SalesHeaderUpdate As String = ""
                    Dim SalesDetailUpdate As String = ""
                    Dim MovementNo As String = ""
                    Dim ShipNums As String = ""
                    Dim ErrProcessing As Boolean = False

                    Try
                        If Trim(strContents) <> "" Then
                            CSV_Rows = strContents.Split(Environment.NewLine.ToCharArray, System.StringSplitOptions.RemoveEmptyEntries)
                            ColHeadings = CSV_Rows(0).Split(",")

                            For Row_Index = 1 To CSV_Rows.GetUpperBound(0)
                                Dim ShipmentNumber As String = ""
                                Dim DeliverySequenceNo As String = ""
                                Fields = CSV_Rows(Row_Index).Split(",")

                                For Col_Index = 0 To ColHeadings.GetUpperBound(0)
                                    Select Case Trim(UCase(ColHeadings(Col_Index)))
                                        Case "MOVEMENTNUMBER"
                                            MovementNo = Trim(Fields(Col_Index))
                                        Case "SHIPMENTNUMBER"
                                            ShipmentNumber = Fields(Col_Index).Split("-").GetValue(0)
                                        Case "DELIVERYSEQUENCENO"
                                            DeliverySequenceNo = Trim(Fields(Col_Index))
                                    End Select
                                Next

                                If ShipmentNumber.Length > 0 And DeliverySequenceNo.Length > 0 Then
                                    If ShipNums.Length > 0 Then ShipNums = ShipNums & ", " & ShipmentNumber Else ShipNums = ShipmentNumber
                                    SalesHeaderUpdate = SalesHeaderUpdate & " WHEN SHDOCO = " & ShipmentNumber & " THEN '" & DeliverySequenceNo & "' "
                                    SalesDetailUpdate = SalesDetailUpdate & " WHEN SDDOCO = " & ShipmentNumber & " THEN '" & DeliverySequenceNo & "' "
                                End If
                            Next

                            'Build query string to update all orders at the same time so it's either all or nothing so we don't get a partial update 
                            If SalesHeaderUpdate.Length > 0 And SalesDetailUpdate.Length > 0 Then
                                SalesHeaderUpdate = "UPDATE " & E1Database & ".F4201 SET SHIR05 = CASE " & SalesHeaderUpdate & " ELSE ' ' END WHERE SHDOCO IN (" & ShipNums & ")"
                                SalesDetailUpdate = "UPDATE " & E1Database & ".F4211 SET SDIR05 = CASE " & SalesDetailUpdate & " ELSE ' ' END WHERE SDDOCO IN (" & ShipNums & ")"
                            End If


                            'Update E1 Sales Header with new routing sequence for applicable orders
                            ssql = SalesHeaderUpdate
                            If Not DbConn.ExecuteQuery(ssql, My.Settings.E1Conn) Then
                                ErrProcessing = True
                                ErrObj.log(1, 0, "Agent.ProcessInboundQueue_E1", "Error updating SHIR05 for Orders:" & ShipNums & ", File ID:" & fileId)

                            Else 'IF Header update succesful then Update E1 Sales Detail with new routing sequence for applicable orders
                                ssql = SalesDetailUpdate
                                If Not DbConn.ExecuteQuery(ssql, My.Settings.E1Conn) Then
                                    ErrProcessing = True
                                    ErrObj.log(1, 0, "Agent.ProcessInboundQueue_E1", "Error updating SDIR05 for Orders:" & ShipNums & ", File ID:" & fileId)
                                End If
                            End If

                            If Not ErrProcessing Then  'DO NOT SET STATUS TO COMPLETE IF WE ENCOUNTERED AN ERROR PROCESSING
                                ssql = "UPDATE OneNetFiles SET STATUS = 'C', PROCESSED = GETDATE(), REFNO = '" & MovementNo & "' WHERE ID = " & fileId
                                If Not DbConn.ExecuteQuery(ssql, My.Settings.SettingValue("OneNetworkConn")) Then
                                    ErrObj.log(1, 0, "Agent.ProcessInboundQueue_E1", "Could not update CSV file id " & fileId)
                                End If
                            End If
                        End If

                    Catch ex As Exception
                        ErrObj.log(1, 0, "Agent.ProcessInboundQueue_E1", "Could not process CSV fileid " & fileId, ex)
                    End Try

                Else   'ELSE THIS IS A REGULAR XML INTEGRATION MESSAGE
                    Try

                        'create object for xml file
                        Dim oFile As New OneFile(fname, strContents)

                        'determine environment
                        If IsNothing(oFile.Environment) Or (oFile.Environment & "") = "E1" Then
                            refNo = oFile.RefNo
                            'process file based on type
                            Select Case oFile.xmlFiletype
                                Case OneFile.OneFileType.Consolidated
                                    writer.WriteLine("START| Consolidated | " & Now.TimeOfDay.ToString)
                                    retProc = oShipment.ProcessConsolidationNotification_E1(oFile.xmlObject, Orders, refNo)
                                    writer.WriteLine("END| Consolidated | " & Now.TimeOfDay.ToString)
                                Case OneFile.OneFileType.Appointment
                                    writer.WriteLine("START| Appointment | " & Now.TimeOfDay.ToString)
                                    retProc = oShipment.ProcessApptNotification_E1(oFile.xmlObject, fileId, Orders, refNo)
                                    writer.WriteLine("END| Appointment | " & Now.TimeOfDay.ToString)
                                Case OneFile.OneFileType.Tracking
                                    writer.WriteLine("START| Tracking | " & Now.TimeOfDay.ToString)
                                    retProc = oShipment.ProcessTrackNotification_E1(oFile.xmlObject, fileId, Orders, refNo)
                                    oShipment.ProcessTrackNotif_ExpectedFreight(oFile.xmlObject, fileId)
                                    writer.WriteLine("END| Tracking | " & Now.TimeOfDay.ToString)
                                Case OneFile.OneFileType.Payment

                                    writer.WriteLine("START| ProcessShipChargeNotif_E1 | " & Now.TimeOfDay.ToString)
                                    retProc = oShipment.ProcessShipChargeNotif_E1(oFile.xmlObject, fileId, Orders, refNo, IgnoreDuplicateCharges)
                                    writer.WriteLine("END| ProcessShipChargeNotif_E1 | " & Now.TimeOfDay.ToString)

                                    'retProc = OneShipments.ProcessReturnType.Fail
                                    'retProc = oShipment.Write_ShipCostComponent_Entry(oFile.xmlObject, fileId)     

                                Case OneFile.OneFileType.Modified
                                    writer.WriteLine("START| Modified | " & Now.TimeOfDay.ToString)
                                    retProc = oShipment.ProcessModShipNotif_E1(oFile.xmlObject, fileId, Orders, refNo)
                                    writer.WriteLine("END| Modified | " & Now.TimeOfDay.ToString)
                                Case Else
                                    'if we don't know the file, just ignore it
                                    ErrObj.log(1, 3, "ProcessQueue", "Unknown file received " & fname)
                                    retProc = OneShipments.ProcessReturnType.Success

                            End Select
                        Else
                            'unhandled, skip

                            refNo = oFile.RefNo
                            retProc = OneShipments.ProcessReturnType.Skip
                        End If



                    Catch ex As Exception
                        ErrObj.log(1, 0, "Agent.ProcessInboundQueue", "Could not process fileid " & fileId, ex)
                    End Try

                    'see the result of processing
                    If retProc = OneShipments.ProcessReturnType.Success Then
                        '-----------------IF ORDERS ARRAY HAS VALUES THEN COPY ARRAY VALUES TO MASTER LIST SO WE CAN LATER UPDATE OUTBOUND_LOG HASH VALUES-------------------------------------
                        If Not (Orders Is Nothing) Then
                            'Update_OutBoundLog_CheckSum(Orders, fname)

                            '------------------------------------COPY ORDERS TO MASTER ORDER ARRAY SO WE UPDATE HASH VALUE ALL AT ONCE-------------------------------------------------
                            Dim x As Integer
                            For x = 1 To UBound(Orders)
                                MasterOrderCount = MasterOrderCount + 1
                                If MasterOrderCount = 1 Then ReDim MasterOrders(MasterOrderCount) Else ReDim Preserve MasterOrders(MasterOrderCount)
                                MasterOrders(MasterOrderCount) = Orders(x)
                                MasterOrders(MasterOrderCount).FileName = fname
                            Next x
                            '----------------------------------------------------------------------------------------------------------------------------------------------------------
                        End If

                        'Set status to Complete
                        ssql = "UPDATE OneNetFiles SET STATUS = 'C', PROCESSED = GETDATE(), REFNO = '" & refNo & "' WHERE ID = " & fileId
                        If Not DbConn.ExecuteQuery(ssql, My.Settings.SettingValue("OneNetworkConn")) Then
                            ErrObj.log(1, 0, "Agent.ProcessInboundQueue_E1", "Could not update file id " & fileId)
                            'Return False
                        End If
                    ElseIf retProc = OneShipments.ProcessReturnType.Skip Then

                        'Set status to Skip
                        ssql = "UPDATE OneNetFiles SET STATUS = 'S', PROCESSED = GETDATE(),REFNO = '" & refNo & "' WHERE ID = " & fileId
                        If Not DbConn.ExecuteQuery(ssql, My.Settings.SettingValue("OneNetworkConn")) Then
                            ErrObj.log(1, 0, "Agent.ProcessInboundQueue_E1", "Could not update file id " & fileId)
                            'Return False
                        End If


                    Else
                        'need to reprocess this again later or fix it
                        If InStr(fname, "pay") = 0 Then
                            ErrObj.log(1, 0, "Agent.ProcessInboundQueue_E1", "Could not process file " & fname)
                            'Return False
                        End If

                    End If

                End If

                RaiseEvent Progress(i)

            Next
            writer.WriteLine("START| Update_OutBoundLog_Hash | " & Now.TimeOfDay.ToString)
            Update_OutBoundLog_Hash(MasterOrders)
            writer.WriteLine("END| Update_OutBoundLog_Hash | " & Now.TimeOfDay.ToString)
            writer.WriteLine("-----END ROUND------ ")
        End Using
        'just put bar to 100%
        RaiseEvent Progress(100)

        'cleanup
        ds.Dispose()
        cnn.Close()
        cnn.Dispose()
        Erase MasterOrders

        Return True

    End Function

    Function ProcessInboundQueue_E1_TEST(ByVal ProcessReasonCodes As Boolean, ByVal ProcessTrackFreightCost As Boolean) As Boolean

        Dim ssql As String
        Dim ds As New DataSet
        Dim cnn As OdbcConnection
        Dim i As Long
        Dim EnteredDate As DateTime = Now

        Dim refNo As String = Nothing

        Try
            cnn = DbConn.CreateConnection(My.Settings.SettingValue("OneNetworkConn"))
            cnn.Open()
        Catch ex As Exception
            ErrObj.log(1, 2, "ProcessInboundQueue_E1_TEST", "Could not open connection " & My.Settings.SettingValue("OneNetworkConn"), ex)
            Return False
        End Try

        'ssql = "SELECT * FROM dbo.OneNetProcessFiles" 'WHERE direction = 'in'"

        ssql = "select * from onenetfiles where direction = 'in' and status = 'P' "
        DbConn.GetDataSet(ssql, cnn, ds)

        RaiseEvent Status("Processing Incoming files")

        'loop through files to process
        For i = 0 To ds.Tables(0).Rows.Count - 1

            Dim dr As DataRow
            Dim Orders() As Order_Type = Nothing
            Dim fname As String
            Dim fileId As Long
            Dim strContents As String

            Dim retProc As OneShipments.ProcessReturnType = OneShipments.ProcessReturnType.Fail

            'assign row
            dr = ds.Tables(0).Rows(i)

            'save some temp vars
            fileId = dr("id")
            fname = dr("filename")
            strContents = dr("content")
            EnteredDate = dr("entered")

            Try

                'create object for xml file
                Dim oFile As New OneFile(fname, strContents)

                'determine environment
                If oFile.Environment.Equals("E1") Then
                    refNo = oFile.RefNo
                    'process file based on type
                    Select Case oFile.xmlFiletype
                        Case OneFile.OneFileType.Consolidated
                            'retProc = oShipment.ProcessConsolidationNotification_E1(oFile.xmlObject, Orders, refNo)

                        Case OneFile.OneFileType.Appointment
                            'retProc = oShipment.ProcessApptNotification_E1(oFile.xmlObject, fileId, Orders, refNo)    'sbains changed to E1 version
                            If ProcessReasonCodes Then oShipment.populate_reasoncodes(EnteredDate, "APP", oFile.xmlObject)
                        Case OneFile.OneFileType.Tracking
                            'retProc = oShipment.ProcessTrackNotification_E1(oFile.xmlObject, fileId, Orders, refNo)   'sbains changed to E1 version
                            If ProcessReasonCodes Then oShipment.populate_reasoncodes(EnteredDate, "TRACK", , oFile.xmlObject)
                            If ProcessTrackFreightCost Then oShipment.ProcessTrackNotif_ExpectedFreight(oFile.xmlObject, fileId)
                        Case OneFile.OneFileType.Payment
                            'retProc = oShipment.ProcessShipChargeNotif_E1(oFile.xmlObject, fileId, Orders, refNo)     'sbains changed to E1 version

                        Case OneFile.OneFileType.Modified
                            'retProc = oShipment.ProcessModShipNotif_E1(oFile.xmlObject, fileId, Orders, refNo)    'sbains changed to E1 version
                            If ProcessReasonCodes Then oShipment.populate_reasoncodes(EnteredDate, "MOD", , , oFile.xmlObject)

                        Case Else
                            'if we don't know the file, just ignore it
                            ErrObj.log(1, 3, "ProcessQueue", "Unknown file received " & fname)

                    End Select
                Else
                    'unhandled, skip
                    refNo = oFile.RefNo
                    retProc = OneShipments.ProcessReturnType.Skip
                End If



            Catch ex As Exception

                ErrObj.log(1, 0, "Agent.ProcessInboundQueue_E1_TEST", "Could not process fileid " & fileId, ex)

            End Try

            ssql = "UPDATE OneNetFiles SET STATUS = 'C', PROCESSED = ENTERED, REFNO = '" & refNo & "' WHERE ID = " & fileId
            If Not DbConn.ExecuteQuery(ssql, My.Settings.SettingValue("OneNetworkConn")) Then
                ErrObj.log(1, 0, "ProcessQueue", "Could not update file id " & fileId)
                Return False
            End If

            RaiseEvent Progress(i)

        Next


        'just put bar to 100%
        RaiseEvent Progress(100)

        'cleanup
        ds.Dispose()
        cnn.Close()
        cnn.Dispose()

        Return True

    End Function


    Function Update_OutBoundLog_CheckSum(ByRef Orders() As Order_Type, ByVal File_Name As String) As Boolean
        Dim ssql As String
        Dim x As Integer
        Dim drHash As OdbcDataReader

        Try

            'Cycle through orders() array - array data stored in index 1 and up.  Index 1 of array is actually the second element.
            For x = 1 To Orders.Length - 1
                '*******SQL Statement to calculate the SalesDetail Hash for specified Order Demand Location and Order Number****************************************************
                ssql = "select * from OneNetShipments_E1  WHERE TrafficDemand = '" & Orders(x).Demand_Location & "' AND Order_No = " & Orders(x).Order_Number
                '******************************************************************************************************************************************************************
                drHash = DbConn.GetDataReader(ssql, My.Settings.SettingValue("OneNetworkConn"), "Update_OutBoundLog_CheckSum")

                If drHash Is Nothing Then
                    ErrObj.log(1, 0, "Update_OutBoundLog_CheckSum", "Error Accessing Database for Order #:" & Orders(x).Order_Number & "-" & Orders(x).Demand_Location)
                Else
                    'Check if recordset has any data
                    If drHash.HasRows Then
                        Dim da As New Odbc.OdbcDataAdapter
                        Dim ds As New DataSet
                        ssql = "SELECT * FROM OutBoundLog WHERE 1=0 "

                        'get data adapter
                        If Not DbConn.GetUpdateableDataAdapter(ssql, My.Settings.SettingValue("OneNetworkConn"), da) Then
                            Return False
                        End If

                        Dim enterDate As String
                        Dim dr As DataRow = Nothing

                        Try
                            enterDate = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))

                            'open dataset 
                            da.Fill(ds)
                            'create new row
                            dr = ds.Tables(0).NewRow

                            'populate fields
                            With dr
                                .Item("Order_Company") = Orders(x).Demand_Location
                                .Item("Order_No") = Orders(x).Order_Number
                                .Item("Order_type") = drHash.Item("Order_Type")
                                .Item("Last_Modified_Date") = drHash.Item("Julian_Date_Modified")
                                .Item("Last_Modified_time") = drHash.Item("Julian_Time_Modified")
                                .Item("Action_Type") = "U"
                                .Item("Direction") = "IN"
                                .Item("OneNet_Filename") = File_Name
                                .Item("DateTime_Processed") = enterDate
                                .Item("SalesDetail_Hash") = drHash.Item("HashValue")
                                '.Item("SalesDetail_Hash") = drHash.Item("SalesDetail_Hash")
                            End With

                            'add to dataset
                            ds.Tables(0).Rows.Add(dr)

                            'save changes
                            da.Update(ds)
                            ds.AcceptChanges()

                        Catch ex As Exception
                            ErrObj.log(1, 0, "Update_OutBoundLog_CheckSum", "order already deleted or archived therefore no need to update hash for Order #:" & Orders(x).Order_Number & "-" & Orders(x).Demand_Location, ex)
                            If dr.RowState = DataRowState.Modified Then dr.CancelEdit()
                            ds.Dispose()
                            da.Dispose()
                            Return False
                        End Try

                        'cleanup 
                        da.Dispose()
                        ds.Dispose()

                    Else
                        'ErrObj.log(1, 0, "Update_OutBoundLog_CheckSum", "order already deleted or archived therefore no need to update hash for Order #:" & Orders(x).Order_Number & "-" & Orders(x).Demand_Location)
                    End If
                    drHash.Close()
                End If
            Next x

        Catch ex As Exception
            ErrObj.log(1, 0, "Update_OutBoundLog_CheckSum", "could not update outbound log for Order #:" & Orders(x).Order_Number & "-" & Orders(x).Demand_Location, ex)
            Return False
        End Try
        Return True

    End Function

    Function Update_OutBoundLog_Hash(ByRef Orders() As Order_Type) As Boolean
        Dim ssql As String
        Dim x As Integer
        Dim drHash As OdbcDataReader
        Dim Orders_string As String = ""
        Dim da As New Odbc.OdbcDataAdapter
        Dim ds As New DataSet

        Try
            If Orders Is Nothing Then
                Return False
            ElseIf UBound(Orders) < 1 Then
                Return False
            End If
            'Cycle through orders() array - array data stored in index 1 and up.  Index 1 of array is actually the second element.
            For x = 1 To UBound(Orders)
                If Orders_string = "" Then Orders_string = Orders(x).Order_Number Else Orders_string = Orders_string & ", " & Orders(x).Order_Number
            Next x

            '*******SQL Statement to calculate the SalesDetail Hash for specified Order Demand Location and Order Number****************************************************
            ssql = "select * from OneNetShipments_E1  WHERE Order_No IN (" & Orders_string & ") "
            '******************************************************************************************************************************************************************
            drHash = DbConn.GetDataReader(ssql, My.Settings.SettingValue("OneNetworkConn"), "Update_OutBoundLog_Hash")

            If drHash Is Nothing Then
                ErrObj.log(1, 0, "Update_OutBoundLog_Hash", "Error Accessing Database for Orders #:" & Orders_string)
            Else
                'Check if recordset has any data
                If drHash.HasRows Then

                    '---------------------------------------Open Outboundlog table for inserting new log-------------------------------------------------
                    ssql = "SELECT * FROM OutBoundLog WHERE 1=0 "

                    'get data adapter
                    If Not DbConn.GetUpdateableDataAdapter(ssql, My.Settings.SettingValue("OneNetworkConn"), da) Then
                        Return False
                    End If
                    'open dataset 
                    da.Fill(ds)
                    '------------------------------------------------------------------------------------------------------------------------------------

                    Dim enterDate As String = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))

                    'Loop through all the OneNetShipments_E1 records 
                    Do While drHash.Read()
                        Dim dr As DataRow = Nothing

                        Try

                            For x = 1 To UBound(Orders)
                                'If we find a match then insert 
                                If Val(Orders(x).Order_Number) = Val(drHash.Item("Order_No") & "") Then
                                    'create new row
                                    dr = ds.Tables(0).NewRow

                                    'populate fields
                                    With dr
                                        .Item("Order_Company") = Orders(x).Demand_Location
                                        .Item("Order_No") = Orders(x).Order_Number
                                        .Item("Order_type") = drHash.Item("Order_Type")
                                        .Item("Last_Modified_Date") = drHash.Item("Julian_Date_Modified")
                                        .Item("Last_Modified_time") = drHash.Item("Julian_Time_Modified")
                                        .Item("Action_Type") = "U"
                                        .Item("Direction") = "IN"
                                        .Item("OneNet_Filename") = Orders(x).FileName
                                        .Item("DateTime_Processed") = enterDate
                                        .Item("SalesDetail_Hash") = drHash.Item("HashValue")
                                        '.Item("SalesDetail_Hash") = drHash.Item("SalesDetail_Hash")
                                    End With

                                    'add to dataset
                                    ds.Tables(0).Rows.Add(dr)

                                    'we found the match so we don't need to loop through the rest of the orders array
                                    Exit For
                                End If

                            Next x



                        Catch ex As Exception
                            ErrObj.log(1, 0, "Update_OutBoundLog_Hash", "order already deleted or archived therefore no need to update hash for Orders #:" & Orders_string, ex)
                            If dr.RowState = DataRowState.Modified Then dr.CancelEdit()
                            ds.Dispose()
                            da.Dispose()
                            Return False
                        End Try

                    Loop
                    'save changes
                    da.Update(ds)
                    ds.AcceptChanges()

                    'cleanup 
                    da.Dispose()
                    ds.Dispose()

                    ssql = "DELETE FROM outboundlog WHERE PROCESS_ID IN (SELECT PID FROM  (Select min(process_ID) PID,  onenet_filename, order_no from outboundlog group by onenet_filename, order_no having count(*) > 1) A )"
                    If Not DbConn.ExecuteQuery(ssql, My.Settings.SettingValue("OneNetworkConn")) Then
                        ErrObj.log(1, 0, "Update_OutBoundLog_Hash", "Could not remove duplicates from Outboundlog table.")
                        'Return False
                    End If

                Else
                    'ErrObj.log(1, 0, "Update_OutBoundLog_Table", "order already deleted or archived therefore no need to update hash for Order #:" & Orders(x).Order_Number & "-" & Orders(x).Demand_Location)
                End If
                drHash.Close()
            End If

        Catch ex As Exception
            ErrObj.log(1, 0, "Update_OutBoundLog_Hash", "could not update outbound log for Orders #:" & Orders_string, ex)
            Return False
        End Try
        Return True

    End Function

    Public Shared Function InsertOutboundFile(ByVal xmlDir As String, ByVal xmlFile As String, ByVal xmlContent As String) As Boolean

        Dim da As New Odbc.OdbcDataAdapter
        Dim ds As New DataSet
        Dim ssql As String
        'Dim xmlContent As String

        ssql = "SELECT * FROM OneNetFiles WHERE 1=0"

        'get data adapter
        If Not DbConn.GetUpdateableDataAdapter(ssql, My.Settings.SettingValue("OneNetworkConn"), da) Then
            Return False
        End If

        Try

            Dim enterDate As String

            enterDate = DbConn.GetDateSqlServer(My.Settings.SettingValue("OneNetworkConn"))

            'open dataset
            da.Fill(ds)

            Dim dr As DataRow

            'create new row
            dr = ds.Tables(0).NewRow

            'pop fields
            dr("filename") = xmlFile
            dr("content") = xmlContent
            dr("entered") = enterDate
            dr("status") = "P"
            dr("direction") = "out"
            dr("fileseq") = 0

            'add to dataset
            ds.Tables(0).Rows.Add(dr)

            'save changes
            'da.UpdateCommand.CommandTimeout = 6000

            da.Update(ds)
            ds.AcceptChanges()

        Catch ex As Exception
            ErrObj.log(1, 0, "InsertOutFile", "could not insert shipment file " & xmlFile, ex)
            Return False
        End Try

        'cleanup
        da.Dispose()

        Return True

    End Function
    '
    'Function: InsertInboundFile
    'Purpose: insert inbound file into database
    '
    Function InsertInboundFile(ByVal searchPattern As String, ByVal srcDir As String) As Boolean
        Dim di As DirectoryInfo
        Dim fis() As FileInfo


        di = New DirectoryInfo(srcDir)
        fis = di.GetFiles(searchPattern)
        Dim i As Integer = 0

        If fis.Length > 0 Then

            Dim ds As New DataSet
            Dim da As Odbc.OdbcDataAdapter = Nothing
            Dim dr As DataRow
            Dim ssql As String

            Dim fileseq As String

            ssql = "SELECT * FROM OneNetFiles WHERE 1=0"
            DbConn.GetUpdateableDataAdapter(ssql, My.Settings.SettingValue("OneNetworkConn"), da)

            da.Fill(ds)

            For Each fi As FileInfo In fis

                'do some pre-processing

                fileseq = Me.ParseOneDateStampFromFile(fi.Name)

                'read file
                Try

                    Dim objReader As StreamReader
                    Dim strContents As String

                    objReader = New StreamReader(fi.FullName)
                    strContents = objReader.ReadToEnd
                    objReader.Close()

                    If fileseq IsNot Nothing Then

                        'create new row
                        dr = ds.Tables(0).NewRow

                        dr("filename") = fi.Name
                        dr("entered") = Now
                        dr("fileseq") = fileseq
                        dr("status") = "P"
                        dr("content") = strContents
                        dr("direction") = "in"

                        'add row
                        ds.Tables(0).Rows.Add(dr)

                        'commit
                        da.Update(ds)
                        ds.AcceptChanges()

                        'delete file
                        fi.Delete()
                    Else
                        ErrObj.log(1, 0, "InsertFilesIntoTable", "could not parse fileseq " & fi.Name)
                        Return False
                    End If

                Catch ex As Exception
                    ErrObj.log(1, 0, "InsertFilesIntoTable", "Could not insert into table " & fi.Name, ex)
                    Return False
                End Try

            Next

            'CLEANUP
            da.Dispose()

        End If

        Return True

    End Function

    Function ParseOneDateStampFromFile(ByVal filename As String) As String

        Dim strTmp As String
        Dim sIndex As Long
        Dim sLen As Long

        Try
            sIndex = filename.IndexOf("_", filename.IndexOf("_") + 1) + 1
            sLen = filename.IndexOf(".") - sIndex

            'find 1st occurrence of "_"
            strTmp = filename.Substring(sIndex, sLen)

        Catch ex As Exception
            strTmp = Nothing

        End Try

        Return strTmp

    End Function

    Function CheckDir(ByVal srcDir As String) As String
        Dim retval As String = ""

        Try
            Dim oDir As New DirectoryInfo(srcDir)
            If oDir.Exists Then
                retval = oDir.FullName
            End If

        Catch ex As Exception
            retval = ""
        End Try

        Return retval

    End Function

End Class
