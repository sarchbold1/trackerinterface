Imports System
Imports System.Reflection
Imports System.Data.Odbc

'Added By Sarj
Imports System.Data
Imports Oracle.DataAccess.Client
Imports Oracle.DataAccess.Types
Imports System.Text
Imports System.IO
Imports System.Data.SqlClient

Public Class OneMapping

    Private connString As String
    Private E1Conn As String
    Private E1Database As String
    Private OracleConn As String

    Private schemaAssembly As Assembly

    Private errMessage As ErrMapping

    Protected Class ErrMapping

        Private connString As String

        Sub New(ByVal pConn As String)
            connString = pConn
        End Sub

        Sub Add(ByVal desc As String, ByVal functionName As String)

            Dim ssql As String

            Using cnn As New OdbcConnection(connString)
                Try
                    ssql = "SELECT * FROM OneNetError WHERE 1=0"
                    Dim da As New OdbcDataAdapter(ssql, cnn)
                    Dim cmb As New OdbcCommandBuilder(da)
                    Dim ds As New DataSet
                    Dim dr As DataRow

                    cnn.Open()
                    da.Fill(ds)

                    'create new row
                    dr = ds.Tables(0).NewRow

                    dr("type") = 2
                    dr("code") = 0
                    dr("description") = desc
                    dr("source") = functionName
                    dr("logdate") = DateTime.Now

                    'add row & save changes
                    ds.Tables(0).Rows.Add(dr)
                    da.Update(ds)
                    ds.AcceptChanges()

                    'cleanup
                    ds.Dispose()
                    da.Dispose()
                    cmb.Dispose()

                Catch ex As Exception
                    Debug.Print(ex.Message)
                End Try
            End Using

        End Sub

    End Class

    '
    'Name: New
    'Description: Constructor
    'Return: n/a
    '
    'Parameters:
    '
    'n/a
    '
    Sub New(ByRef pConnString As String, ByVal E1Conn As String, ByVal E1Database As String, ByVal OracleConn As String)

        Me.connString = pConnString
        Me.E1Conn = E1Conn
        Me.E1Database = E1Database
        Me.OracleConn = OracleConn
        schemaAssembly = Assembly.GetExecutingAssembly()
        errMessage = New ErrMapping(Me.connString)

    End Sub

    '
    'Name: GetOutboundMapping
    'Description: Retrieve the mapping for a particular section
    'Return: DataSet
    '
    'Parameters:
    '
    'MapSet - name of the set of mappings to return
    '
    Private Function GetOutboundMapping(ByVal mapSet As String, Optional ByVal AppState As String = "") As DataSet

        Dim da As OdbcDataAdapter
        Dim dsMap As New DataSet
        Dim ssql As String

        'get mapping
        If UCase(AppState) = "E1 TO ONENETWORK" Then                               'sbains added
            ssql = "SELECT * FROM OneNetMapE1 WHERE MapSet = ? AND Direction = ?"     'sbains added
        Else
            ssql = "SELECT * FROM OneNetMap WHERE MapSet = ? AND Direction = ?"
        End If

        Try
            da = New OdbcDataAdapter(ssql, Me.connString)
            da.SelectCommand.Parameters.Add("@mapset", OdbcType.VarChar).Value = mapSet
            da.SelectCommand.Parameters.Add("@direction", OdbcType.VarChar).Value = "out"
            da.Fill(dsMap)

        Catch ex As Exception
            dsMap = Nothing
        End Try

        Return dsMap

    End Function

    '
    'Name: GetInboundMapping
    'Description: Retrieve the mapping for a particular section
    'Return: DataSet
    '
    'Parameters:
    '
    'MapSet - name of the set of mappings to return
    '
    Private Function GetInboundMapping(ByVal mapSet As String, Optional ByVal AppState As String = "") As DataSet
        Dim daMap As New OdbcDataAdapter
        Dim dsMap As New DataSet
        Dim ssql As String

        'get mapping
        If UCase(AppState) = "E1 TO ONENETWORK" Then                               'sbains added
            ssql = "SELECT * FROM OneNetMapE1 WHERE MapSet = ? AND Direction = ? Order By SeqNo"     'sbains added

        Else
            ssql = "SELECT * FROM OneNetMap WHERE MapSet = ? AND Direction = ? Order By SeqNo"
        End If
        Try
            daMap = New OdbcDataAdapter(ssql, Me.connString)
            daMap.SelectCommand.Parameters.Add("@mapset", OdbcType.VarChar).Value = mapSet
            daMap.SelectCommand.Parameters.Add("@direction", OdbcType.VarChar).Value = "in"
            daMap.Fill(dsMap)
        Catch ex As Exception
            dsMap = Nothing
        End Try

        Return dsMap

    End Function

    '
    'Name: GetMappingEnum
    'Description: retrieve the mapping for enumerated values (eg. CF -> CUFT)
    'Return: Object
    '
    'Parameters:
    '
    'type - category of enum value (eg. Weight, Volume)
    'lookup - value to lookup in the enum mapping table
    '
    Private Function GetMappingEnum(ByVal type As String, ByVal lookup As String) As Object
        
        Dim retval As Object = Nothing
        Dim ssql As String

        Using cnn As New OdbcConnection(Me.connString)
            Try
                Dim drMapEnum As OdbcDataReader
                Dim cmd As OdbcCommand


                ssql = "SELECT * FROM OneNetMapEnum WHERE type = ? AND  src = ?"
                cmd = New OdbcCommand(ssql, cnn)
                cmd.Parameters.Add("@type", OdbcType.VarChar).Value = type
                cmd.Parameters.Add("@lookup", OdbcType.VarChar).Value = lookup

                cnn.Open()
                drMapEnum = cmd.ExecuteReader

                If drMapEnum IsNot Nothing Then
                    If drMapEnum.Read() Then
                        retval = drMapEnum("dest")
                    End If
                End If

                'cleanup
                drMapEnum.Close()
                cmd.Dispose()

            Catch ex As Exception
                retval = Nothing
            End Try
        End Using
        
        Return retval
    End Function
    '
    'Name: MapField
    'Description: maps fields in datareader to corresponding object
    'Return: Boolean
    '
    'Parameters:
    '
    'rdr - datareader where source data is mapped from
    'objMapTo - object to map datareader object to
    'mapset - mapping of the object
    '
    Public Function MapFields(ByRef rdr As OdbcDataReader, ByRef objMapTo As Object, ByVal mapSet As String, Optional ByRef hasError As Boolean = False, Optional ByVal AppState As String = "", Optional ByVal AuotMark As String = "") As Boolean

        Dim dsMap As DataSet
        Dim PropertyType As String
        Dim PropertySubType As String

        'get mapping
        dsMap = GetOutboundMapping(mapSet, AppState)
        If dsMap Is Nothing Then
            Return False
        End If

        'loop through the mappings retrieved
        For Each rowMap As DataRow In dsMap.Tables(0).Rows
            Dim srcName As Object
            Dim destName As String
            Dim val As Object
            Dim literal As Boolean
            Dim SetToNothing As Boolean = False
            Dim SendToPermanentSite As Boolean = False

            val = Nothing
            destName = ""
            srcName = ""

            Try

                'assign some value vars
                PropertyType = IIf(rowMap("PropertyType") Is DBNull.Value, "", rowMap("PropertyType"))
                PropertySubType = IIf(rowMap("PropertySubType") Is DBNull.Value, "", rowMap("PropertySubType"))

                literal = rowMap("literal")
                destName = rowMap("PropertyName")
                srcName = rowMap("DbField")
                If destName = "CreationEnterprisename" Then
                    Stop
                End If
                'Stop
                ' End If


                'You can either assign xml tags (SHIPTOADDRESS, SHIPTOPARTNERPROFILENAME) OR (SHIPTOSITEINTEGREF) BUT NOT BOTH
                'So for Oncal orders (295, 1244, 1245) that are not transhipments (SO) we want to send up to permanent site (assign SHIPTOSITEINTEGREF and set SHIPTOADDRESS/ SHIPTOPARTNERPROFILENAME = Nothing)
                'So for NON-Oncal orders (<> 295, 1244, 1245) OR for transhipments (<> SO) we want to send up to NON-permanent site (set SHIPTOSITEINTEGREF = Nothing and assign value to SHIPTOADDRESS/ SHIPTOPARTNERPROFILENAME)
                'If UCase(destName) = "SHIPTOADDRESS" Or UCase(destName) = "SHIPTOPARTNERPROFILENAME" Or UCase(destName) = "SHIPTOSITEINTEGREF" Then
                '    'If Trim(rdr("SHIPTOPARTNERPROFILENAME")) <> "COSTCO_ON-VARIOUS" And Not (Trim(rdr("Mfging_Location")) = "295" And Trim(rdr("Order_Customer_No")) = "LESSWI") Then  'Added Jan 25, 2012 - Do Not send Oncal COSTCO orders to permanent site
                '    If Trim(rdr("SHIPTOPARTNERPROFILENAME")) <> "COSTCO_ON-VARIOUS" And Trim(rdr("SHIPTOPARTNERPROFILENAME")) <> "HOMECOM-VARIOUS" And Trim(rdr("SHIPTOPARTNERPROFILENAME")) <> "GRAINGE-VARIOUS" Then  'Added Jan 25, 2012 - Do Not send Oncal COSTCO orders to permanent site
                '        Select Case Trim(rdr("Mfging_Location"))
                '            Case "1295", "1244", "1245"
                '                If Trim(rdr("Order_Type")) = "SO" Then
                '                    SendToPermanentSite = True
                '                ElseIf (Trim(rdr("Order_Type")) = "ST" Or Trim(rdr("Order_Type")) = "SJ") And ((Trim(rdr("ShipToLocation")) = "1295" Or Trim(rdr("ShipToLocation")) = "1244" Or Trim(rdr("ShipToLocation")) = "1245")) Then
                '                    SendToPermanentSite = True
                '                End If
                '        End Select
                '    End If
                'End If

                Select Case UCase(destName)
                    Case "SHIPTOADDRESS", "SHIPTOPARTNERPROFILENAME"
                        SetToNothing = SendToPermanentSite
                    Case "SHIPTOSITEINTEGREF"
                        SetToNothing = Not (SendToPermanentSite)
                End Select

                'check to see if this is a literal value


                If literal Then
                    val = rowMap("LiteralValue")
                ElseIf SetToNothing Then
                    val = Nothing
                Else
                    val = MapPropertyType(PropertyType, PropertySubType, srcName, rdr, AppState, AuotMark)
                End If

                'ProNumber upload Logic Here.
                '  If destName = "ProNumber" Then Stop
                If destName = "ProNumber" Then
                    If Not val Is System.DBNull.Value Then                     'if BillTo/Shipto Combo found don't upload and if no billto/shipto found then check just bill to.
                        If GetProNumExc(rdr("Order_No"), rdr("ShipToLocation")) = True Then
                            val = System.DBNull.Value
                        End If

                        If Not val Is System.DBNull.Value Then
                            Call MarkOrdPro(rdr("Order_No"), val)
                        End If
                    End If
                End If


                'check to see if val is empty
                If val IsNot Nothing Then
                    'try to assign the member
                    If Not SetMember(objMapTo, destName, val) Then
                        'error assigning member, but we don't want to stop the rest of the records

                        errMessage.Add("Error assigning " & val & " to " & destName, "MapFields")
                        hasError = True
                    End If
                End If

            Catch ex As Exception
                'error assigning object, but we need to continue
                errMessage.Add("Error in Try block. Mapping " & mapSet & ",srcName - " & srcName & ", destName - " & destName & vbCrLf & ex.Message, "MapFields")
                hasError = True
            End Try

        Next

        Return True

    End Function

    '
    'Name: SetMember
    'Description: assigns values to fields in object
    'Return: Boolean
    '
    'Parameters:
    '
    'obj - object to assign member values to
    'fldName - Name of field in object to assign value to
    'val - value to assign to field
    '
    Private Function SetMember(ByRef obj As Object, ByVal fldName As String, _
        ByVal val As Object) As Boolean

        Dim fi As FieldInfo

        'look up the property information of the member/field name specified
        fi = obj.GetType.GetField(fldName.Trim)

        ' get a reference to the FieldInfo, exit if no field with that name
        If fi Is Nothing Then
            Return False
        End If

        Try
            If fi.FieldType.IsEnum Then
                'convert string to enum
                val = System.Enum.Parse(fi.FieldType, val)

                'assign value to field
                fi.SetValue(obj, val)

            ElseIf fi.FieldType.IsArray Then

                Dim currentItems As Array
                Dim newArray As Array

                'get all current values in this array
                currentItems = fi.GetValue(obj)

                'see if we get any current array items back
                If currentItems Is Nothing Then
                    'got nothing back, make a new instance to assign
                    newArray = Array.CreateInstance(fi.FieldType.GetElementType, 1)
                    newArray.SetValue(val, 0)
                Else
                    'existing items, preserve it, and add new value
                    newArray = Array.CreateInstance(fi.FieldType.GetElementType, currentItems.Length + 1)
                    Array.Copy(currentItems, newArray, currentItems.Length)
                    newArray.SetValue(val, currentItems.Length)
                End If

                'now assign value to object field/property
                fi.SetValue(obj, newArray)

            Else

                'check for null value
                If Not val Is System.DBNull.Value Then
                    val = Convert.ChangeType(val, fi.FieldType)

                    If val.GetType Is GetType(String) Then
                        val = Trim(val)
                    End If

                    'assign value to field
                    fi.SetValue(obj, val)

                End If
            End If

        Catch ex As Exception
            errMessage.Add("fldname-" & fldName & ",val-" & val.ToString, "SetMember")
            Return False
        End Try

        Return True
    End Function

    '
    'Name: MapPropertyType
    'Description: assigns values to fields in object
    'Return: Boolean
    '
    'Parameters:
    '
    'propertyType
    'propertySubType
    'srcName
    'rdr
    '
    Private Function MapPropertyType(ByVal propertyType As String, ByVal propertySubType As String, _
                            ByVal srcName As Object, ByRef rdr As OdbcDataReader, Optional ByVal AppState As String = "", Optional ByVal AutoDel As String = "") As Object

        Dim RetVal As Object

        'set inital return value to nothing
        RetVal = Nothing

        Dim typeString() As String
        typeString = propertyType.Split(":")

        Select Case typeString(0)
            Case "vc"
                Dim oGeneric As Object

                Dim className As String = typeString(1)

                'if DateTime handle special parsing
                If className = "DateTime" Then

                    'assign catch all
                    If Not srcName Is DBNull.Value And Not srcName = "" Then
                        RetVal = rdr(srcName)
                        RetVal = MapPropertySubType(propertySubType, RetVal)
                    End If

                Else
                    'nested object, handle by creating object

                    'create the new object based on class in table
                    oGeneric = Me.schemaAssembly.CreateInstance(Me.GetType.Assembly.GetName.Name & "." & className)

                    If oGeneric IsNot Nothing Then
                        'recursive call to to map the fields
                        If Not srcName Is DBNull.Value And Not srcName = "" Then
                            If MapFields(rdr, oGeneric, srcName, , AppState) Then
                                RetVal = oGeneric
                            End If
                        Else
                            RetVal = Nothing
                        End If
                    Else
                        'could not create the object, log error
                        errMessage.Add("could not create object " & className, "MapPropertyType")
                    End If

                End If

                Return RetVal

            Case "xs"
                'check to see if srcName is null
                'If srcName = "DELIVER" Then Stop
                If Not srcName Is DBNull.Value And Not srcName = "" Then
                    If srcName.ToString.ToUpper = "TRACKACTIONNAME" And AutoDel = "AMD" Then
                        'Setup this status if ShipConfirmed and has been for 1 full day AND the shipper is on the list of shippers to do this for.
                        RetVal = "DELIVERED"
                    Else
                        Try
                            RetVal = rdr(srcName)
                            RetVal = MapPropertySubType(propertySubType, RetVal)
                        Catch ex As Exception
                            errMessage.Add("could not map " & srcName & " - " & propertySubType & "," & RetVal, "MapPropertyType")
                            RetVal = Nothing
                        End Try
                    End If
                Else

                    RetVal = Nothing
                End If
        End Select

        Return RetVal
    End Function
    '
    'Name: MapPropertySubType
    'Description: Handle special subtypes of property (eg. Date)
    'Return: Object
    '
    'Parameters:
    '
    'PropertySubType - subtype of field
    'val - value to return
    '
    Private Function MapPropertySubType(ByVal propertySubType As String, ByVal val As Object) As Object

        If val Is Nothing Or val Is System.DBNull.Value Then
            Return val
        End If

        'check to see if any formatting/conversion process
        If Not propertySubType = "" Then

            'check to see if date type
            If propertySubType = "yyyymmdd" Then
                val = MacolaDateToOneDate(val)
            Else
                'get the enum mapping
                val = GetMappingEnum(propertySubType, val)
            End If

        End If

        Return val
    End Function

    'sbains Function to Convert Macola Date to Julian for E1
    Public Function ConvertDateToJulian(ByVal InDate As Object) As Integer
        Dim BigDate As Date

        If InDate Is Nothing Or InDate Is System.DBNull.Value Then Return 0
        If IsDate(InDate) = False Then Return 0
        BigDate = CDate(InDate)
        If BigDate > CDate("2899-12-31") Then BigDate = CDate("2899-12-31")

        Return (DateDiff(DateInterval.Year, CDate("1900-01-01"), BigDate) * 1000) + DateDiff(DateInterval.Day, CDate(Format(Year(BigDate), "0000") + "-01-01"), BigDate) + 1
    End Function

    'Name: MapObjectToDataRow
    'Description: map an object to a datarow
    'Return: Object
    '
    'Parameters:
    '
    'obj - source object to get data from
    'dr - datarow to map object values to
    'mapSet - mapping set to use
    '
    Function MapObjectToDataRow(ByVal fileId As Integer, ByRef obj As Object, ByRef dr As DataRow, _
                                ByVal mapSet As String, Optional ByVal mapSubSet As String = "", Optional ByVal AppState As String = "")

        Dim fldName As String = ""
        Dim fldType As String
        Dim dbField As String = ""
        Dim subType As String
        Dim literalVal As Object
        Dim literal As Boolean
        Dim checkField As Object
        Dim CarrierCurr As String = "USD"
        Dim OrderNo As Integer = 0

        Dim dsMap As DataSet
        Dim drMap As DataRow

        dsMap = GetInboundMapping(mapSet, AppState)
        If dsMap Is Nothing Then
            errMessage.Add("Error retrieving mapping " & mapSet & ", mapsubset " & mapSubSet, "MapObjectToDataRow")
            Return False
        End If

        Try
            'loop through all the mapped fields
            For Each drMap In dsMap.Tables(0).Rows
                Try
                    'get field name to assign value to
                    literal = drMap("Literal")
                    literalVal = drMap("LiteralValue")
                    fldName = drMap("PropertyName")
                    fldType = drMap("PropertyType")
                    dbField = drMap("DbField")
                    subType = IIf(drMap("PropertySubType") Is System.DBNull.Value, "", drMap("PropertySubType"))
                    checkField = drMap("CheckMap")
                    Dim modelName() As String = fldType.Split(":")

                    ' If fldName = "MovementNumber" Then Stop
                    'If dbField = "SDPDDJ" Then Stop


                    'If drMap("Mapset").ToString.Trim = "ModShipNotif.Fps" Then Stop
                    Dim whoMe As Type = obj.GetType()
                    Dim fi As FieldInfo
                    Dim OffNetwork As Boolean = False

                    'declare generic object which we will be assigning to
                    Dim oGeneric As Object
                    Dim DateStr As String

                    'get me the field property and assign to object
                    fi = whoMe.GetField(fldName)

                    'see if field or object is not empty
                    If fi Is Nothing Then
                        'do nothing, property does not exist
                        ' Stop
                    ElseIf Trim(drMap("MapSubSet")) <> "" And mapSubSet <> "" And Trim(drMap("MapSubSet").ToString.ToUpper) <> mapSubSet Then
                        'skip this mapping, this is only for specific map subsets
                    ElseIf (fi.FieldType.IsPrimitive Or fi.FieldType Is GetType(String)) And Not (UCase(mapSubSet) = "CANCELLED" And Trim(drMap("MapSubSet").ToString.ToUpper) <> mapSubSet) Then
                        oGeneric = fi.GetValue(obj)

                        If dbField = "SHVR03" Or dbField = "SDVR03" Then
                            oGeneric = Left(Trim(oGeneric), 25)
                        End If

                        If oGeneric Is Nothing And (dbField = "SHCARS" Or dbField = "SDCARS") Then
                            fi = whoMe.GetField("OffNetworkCarrier")
                            If fi IsNot Nothing Then
                                oGeneric = fi.GetValue(obj)
                                OffNetwork = True
                            End If
                        End If

                        Dim assignVal As Object = Nothing

                        If oGeneric IsNot Nothing Then
                            'see if this is a date time field
                            If modelName(1) = "DateTime" Then
                                'need to parse it and return datetime object
                                assignVal = ParseDateTime(oGeneric)
                            ElseIf modelName(1) = "Date" Then     'sbains added
                                assignVal = CStr(ConvertDateToJulian(Left(oGeneric, 10)))
                            ElseIf modelName(1) = "Time" Then    'sbains added
                                DateStr = Left(oGeneric, 19)
                                assignVal = Replace(DateStr.Substring(11), ":", "")
                            Else
                                'no need parse, primitive type
                                assignVal = oGeneric
                            End If

                            'see if there is mapping that needs to be done for this field
                            If Not subType = "" Then
                                'Get Order # to pass into MapInboundSubType (if fieldname is SHCARS then we're looking at the header so get the order number from SHDOCO, else if its SDCARS we're looking at the detail so get order no from  SDDOCO)
                                If dbField = "SHCARS" Then OrderNo = dr("SHDOCO")
                                If dbField = "SDCARS" Then OrderNo = dr("SDDOCO")
                                assignVal = MapInboundSubType(subType, assignVal, fileId, OffNetwork, OrderNo)
                            End If
                        End If

                        'see if literal was specified, will override everything
                        If literal Then
                            assignVal = literalVal
                        End If

                        'assign value
                        If Trim(dbField) <> "" And assignVal IsNot Nothing Then
                            'Validate if it's a string and trim excess characters 

                            'IF the dbfield being mapped to is SDZON or SHZON that means its a note element which must be treated uniquely
                            If UCase(dbField) = "SDZON" Or UCase(dbField) = "SHZON" Then
                                'If we're mapping to the header then dbfield(SHZON) then also write the notes to the blob text attachment
                                If UCase(dbField) = "SHZON" Then WriteBlobData(Trim(oGeneric), CInt(dr("SHDOCO")), Trim(dr("SHDCTO")), Trim(dr("SHKCOO")), "WRITE")

                                'Check to see if the Notes have the word 'DROP' or 'LIVE' in it.  
                                'If 'DROP' then set assigned valed to ZON field is 'D'
                                'If 'LIVE' then set assigned valed to ZON field is 'L'
                                If InStr(UCase(oGeneric), "DROP") > 0 Then
                                    assignVal = "D"
                                ElseIf InStr(UCase(oGeneric), "LIVE") > 0 Then
                                    'assignVal = "L"
                                    assignVal = " "
                                Else
                                    'Else leave blank
                                    assignVal = " "
                                End If

                                'Map the new oGeneric value either SHZON or SDZON
                                'Me.MapObjectToDataRow(fileId, oGeneric, dr, dbField, mapSubSet, AppState)

                            ElseIf LCase(dr(dbField).GetType.Name) = "string" Then
                                assignVal = Left(Trim(assignVal), dr(dbField).length)
                            End If

                            'do some field level checking
                            If Not checkField.Equals(System.DBNull.Value) Then
                                If Me.MapFieldRule(dr, oGeneric, checkField) Then
                                    dr(dbField) = assignVal
                                End If
                            Else
                                'assign to field



                                If fldName.ToUpper = "MOVEMENTNUMBER" Then
                                    If dr(dbField).ToString.Trim = "" Then
                                        dr(dbField) = assignVal
                                    ElseIf (dr(dbField).ToString.Trim.Substring(0, 2) <> "M-" And dr(dbField).ToString.Trim.Substring(0, 2) <> "G-") Then
                                        dr(dbField) = assignVal
                                    End If
                                Else
                                    dr(dbField) = assignVal
                                End If

                            End If

                        End If

                    ElseIf fi.FieldType.IsArray Then
                        'need to deal w/ this separately
                        oGeneric = fi.GetValue(obj)

                        Dim oArray As Array = oGeneric

                        'check to see if we specified an arrayindex in the mapping table
                        If Not drMap("ArrayIndex") Is System.DBNull.Value Then
                            'we did, so get only the array element index specified

                            'make sure this array is not zero length
                            If oArray IsNot Nothing Then

                                If IsNumeric(drMap("ArrayIndex")) Then  'was a numeric value specified for the ArrayIndex in OneNetMap

                                    ' Usually an index of 0 (zero) is specified which means take the first (and usually only) name space for that element
                                    If Val(drMap("ArrayIndex") & "") >= 0 Then
                                        Dim arrIndex() As Integer = {Val(drMap("ArrayIndex") & "")}
                                        oGeneric = oArray.GetValue(arrIndex)
                                    Else  'something like -1 was specified which means take the last index 
                                        Dim arrIndex() As Integer = {oArray.Length - 1}
                                        oGeneric = oArray.GetValue(arrIndex)
                                    End If
                                Else 'otherwise a schema name was provided in the ArrayIndex of OneNetMap
                                    Dim schema_index As Integer = 0
                                    Dim x_index As Integer

                                    'Cycle through names spaces in object and find matching for one specified in ArrayIndex field of OneNetMap table
                                    For x_index = 0 To oArray.GetUpperBound(0)
                                        If oArray.GetValue(x_index).GetType.Name = Trim(drMap("ArrayIndex") & "") Then
                                            schema_index = x_index    'found index so set schema_index value
                                            Exit For
                                        End If
                                    Next x_index

                                    oGeneric = oArray.GetValue(schema_index)
                                End If
                                ''IF the dbfield being mapped to is SDZON or SHZON that means its a note element which must be treated uniquely
                                'If UCase(dbField) = "SDZON" Or UCase(dbField) = "SHZON" Then
                                '    'If we're mapping to the header then dbfield(SHZON) then also write the notes to the blob text attachment
                                '    If UCase(dbField) = "SHZON" Then WriteBlobData(Trim(oGeneric), CInt(dr("SHDOCO")), Trim(dr("SHDCTO")), Trim(dr("SHKCOO")), "WRITE")

                                '    If InStr(UCase(oGeneric), "DROP") > 0 Then
                                '        oGeneric = "D"
                                '    ElseIf InStr(UCase(oGeneric), "LIVE") > 0 Then
                                '        oGeneric = "L"
                                '    Else
                                '        oGeneric = ""
                                '    End If
                                '    'Map the new oGeneric value either SHZON or SDZON
                                '    Me.MapObjectToDataRow(fileId, oGeneric, dr, dbField, mapSubSet, AppState)
                                'Else
                                Me.MapObjectToDataRow(fileId, oGeneric, dr, dbField, mapSubSet, AppState)
                                'End If
                            Else
                                'nothing in array do not do anything
                            End If

                        Else
                            'we're going to have to deal w/ each element in this array
                            'right now we don't handle multiple array elements mapped to the same row
                            'because we only map 1:1

                        End If

                    Else
                        oGeneric = fi.GetValue(obj)
                        'this is a nested object, recursively call function to process its fields
                        If oGeneric IsNot Nothing Then
                            Me.MapObjectToDataRow(fileId, oGeneric, dr, dbField, mapSubSet, AppState)
                        End If

                    End If
                Catch ex As Exception
                    'we want to continue mapping, so we should just let the error go after we log it in the db
                    errMessage.Add("Error mapping Object to datarow " & fldName & "," & dbField, "MapObjectToDataRow, Error: " & ex.Message)

                End Try

            Next

            'cleanup
            dsMap.Dispose()

        Catch ex As Exception
            'we want to continue mapping, so we should just let the error go after we log it in the db
            errMessage.Add("Error mapping Object to datarow " & fldName & "," & dbField, "MapObjectToDataRow, Error: " & ex.Message)

        End Try

        'success
        Return True

    End Function

    Function Get_CarrierNum_Based_On_Currency(ByVal OrderNo As Integer, ByVal lookup As Object) As Integer

        Dim sSql As String
        Dim ReturnVal As Integer = 0
        Dim ShipToCurr As String = ""
        Dim ShipFromCurr As String = ""
        Dim ShipToNo As Integer = 0
        Dim CarrierCurr As String = "USD"
        Dim E1Database As String = Me.E1Database

        Dim cnnE1 As New OracleConnection(Me.OracleConn)

        Try

            cnnE1.Open()

            'Get Ship From (MCU) and Ship To (SHAN) for order number
            sSql = "SELECT SDMCU, SDSHAN FROM (SELECT * FROM " & E1Database & ".F4211 UNION SELECT * FROM " & E1Database & ".F42119) F4211 WHERE SDLTTR <> 980 AND SDLNTY = 'S' AND SDDOCO = " & OrderNo.ToString & " AND ROWNUM =1 "

            Dim cmdE1 As New OracleCommand(sSql, cnnE1)
            cmdE1.CommandType = CommandType.Text

            Dim dr As OracleDataReader = cmdE1.ExecuteReader

            'see if we get any records back
            If dr IsNot Nothing Then
                If dr.HasRows Then
                    dr.Read()

                    'Get default currency for ship from/to
                    ShipFromCurr = Get_Default_Currency(Trim(dr("SDMCU")).ToString)
                    ShipToCurr = Get_Default_Currency(Trim(dr("SDSHAN")).ToString)
                    ShipToNo = dr("SDSHAN")

                    'If either ship from or ship to is USD then set currency to USD otherwise both are Canadian so set to CAD
                    If ShipFromCurr = "USD" Or ShipToCurr = "USD" Then
                        CarrierCurr = "USD"
                    Else
                        If ShipToNo = 1258 Then   'Exception added for Buffalo - Buffalo Set up as CAD(expected) but payment file will be in USD so must set Currcode to USD (Oct 4, 2010)
                            CarrierCurr = "USD"
                        Else
                            CarrierCurr = "CAD"   'Added Rule as per discussion with Barry and Han on March 25 - if CAD then demand = 100
                        End If

                    End If

                    dr.Close()
                    dr = Nothing

                    Using cnn As New OdbcConnection(Me.connString)
                        Try
                            'Get the carrier number for lookup/Currency combination - if none exists then we return 0
                            sSql = "SELECT E1AN8 FROM CarrierMap WHERE UPPER(Carrier) = '" & UCase(Trim(lookup.ToString)) & "' AND UPPER(Country) = '" & UCase(CarrierCurr) & "' "
                            cnn.Open()
                            Dim cmd As New OdbcCommand(sSql, cnn)
                            ReturnVal = Val(cmd.ExecuteScalar & "")
                            cnn.Close()

                        Catch ex As Exception
                            errMessage.Add("Error retrieving Carrier # from 'Carrier' table for Order:" & OrderNo.ToString & "(" & ex.Message & ")", "Get_CarrierNum_Bas\ed_On_Currency")
                            cnn.Close()
                            Return ReturnVal
                        End Try
                    End Using

                End If
            End If
        Catch ex As Exception
            errMessage.Add("Error retrieving Carrier # for Order:" & OrderNo.ToString & "(" & ex.Message & ")", "Get_CarrierNum_Based_On_Currency")
            cnnE1.Close() : cnnE1.Dispose()
            Return ReturnVal
        End Try
        cnnE1.Close() : cnnE1.Dispose()
        Return ReturnVal
    End Function

    Function Get_Default_Currency(ByVal LocationCode As String) As String
        Dim sSql As String
        Dim ReturnVal As String = "USD"   'Set default to USD
        Dim cnn As New OracleConnection(Me.OracleConn)
        Try
            cnn.Open()
            sSql = "SELECT AICRCD FROM " & Me.E1Database & ".F03012 WHERE ROWNUM <= 1 AND AIAN8 = " & Val(LocationCode).ToString
            Dim cmd As New OracleCommand(sSql, cnn)
            cmd.CommandType = CommandType.Text
            ReturnVal = UCase(Trim(cmd.ExecuteScalar.ToString & ""))

            If Trim(ReturnVal) = "" Then ReturnVal = "USD"

        Catch ex As Exception
            errMessage.Add("Error retrieving Default Currency for Location:" & LocationCode & "(" & ex.Message & ")", "Get_Default_Currency")
            Return ReturnVal
        End Try
        Return ReturnVal
    End Function

    Private Function MapInboundSubType(ByVal type As String, ByVal lookup As Object, ByVal fileId As Integer, ByVal OffNetwork As Boolean, ByVal OrderNo As Integer, Optional ByRef nestcount As Integer = 0) As Object

        Dim ssql As String
        Dim retval As Object = Nothing
        Dim CarrierNo As Integer = 0

        Using cnn As New OdbcConnection(Me.connString)
            Dim dr As OdbcDataReader

            ssql = "SELECT * FROM IncomingFieldMap WHERE type = ? AND lookup = ?"

            Dim cmd As New OdbcCommand(ssql, cnn)
            cmd.Parameters.Add("@type", OdbcType.VarChar).Value = Trim(type).Replace("'", "''")
            cmd.Parameters.Add("@lookup", OdbcType.VarChar).Value = Trim(lookup).Replace("'", "")
            'cmd.Parameters.Add("@lookup", OdbcType.VarChar).Value = Trim(lookup)

            cnn.Open()
            dr = cmd.ExecuteReader

            If dr IsNot Nothing Then
                If dr.HasRows Then
                    If Trim(LCase(type)) = "carrier" Then
                        retval = dr("value")
                        ''If carrier has multiple address book entries (US and Canadiant) then use the currency to pick the right one
                        'cmd.CommandText = "SELECT E1AN8 FROM CarrierMap WHERE UPPER(Carrier) = '" & UCase(Trim(lookup.ToString)) & "' AND UPPER(Country) = '" & UCase(CarrierCurr) & "' "
                        'dr.Close()
                        'CarrierNo = Val(cmd.ExecuteScalar & "")
                        CarrierNo = Get_CarrierNum_Based_On_Currency(OrderNo, lookup)
                        If CarrierNo > 0 Then
                            retval = CarrierNo
                        End If
                    Else
                        retval = dr("value")
                    End If
                Else
                    If Trim(LCase(type)) = "carrier" Then
                        'Check to see of the Carrier Name has CPU in it. If so then it's a pickup and get address number for 'CPU'
                        If InStr(UCase(lookup), "CPU") >= 1 Or InStr(UCase(lookup), "OTHER") >= 1 Then
                            If nestcount <= 2 Then
                                'retval = MapInboundSubType(type, "CPU", fileId, nestcount + 1)
                                retval = MapInboundSubType(type, "OTHER CARRIER", fileId, OffNetwork, OrderNo, nestcount + 1)
                            Else
                                retval = Nothing
                            End If
                        Else
                            'Else it's a delivery for an 'Other Carrier' so get address number for 'OTHER CARRIER'
                            If nestcount <= 2 Then
                                retval = MapInboundSubType(type, "OTHER CARRIER", fileId, OffNetwork, OrderNo, nestcount + 1)
                                'Only generate an error if the carrier name is not other
                                If UCase(Trim(lookup)) <> "OTHER" And OffNetwork = False Then
                                    errMessage.Add("No match found for carrier '" & lookup & "'.  'OTHER CARRIER' will be used instead for File ID:" & fileId.ToString, "MapInboundSubType")
                                End If
                            Else
                                retval = Nothing
                            End If
                        End If
                    Else
                        retval = lookup
                    End If
                End If
            Else
                retval = Nothing
            End If

            'cleanup
            dr.Close()
            cmd.Dispose()
        End Using

        Return retval

    End Function

    Private Function MapFieldRule(ByRef drContext As DataRow, ByVal srcFieldVal As Object, ByVal checkMap As String) As Boolean
        Dim ssql As String
        Dim doMap As Boolean = True

        Using cnn As New OdbcConnection(Me.connString)
            Try
                cnn.Open()

                ssql = "SELECT * FROM FieldRuleMap WHERE MapField = ?"

                Dim cmd As New OdbcCommand(ssql, cnn)
                cmd.Parameters.Add("@mapfield", OdbcType.VarChar).Value = checkMap

                Dim rdr As OdbcDataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)

                'loop through any map fields
                While rdr.Read And doMap.Equals(True)
                    Dim true_condition As String = rdr("true_condition")
                    Dim fldname As String = rdr("fldname")
                    Dim fldtype As Object = rdr("fldtype")
                    Dim fldvalue As Object

                    'get value of field from passed reader
                    fldvalue = drContext(fldname)

                    Select Case true_condition
                        Case "isnull"
                            'don't do mapping if it's not null
                            doMap = drContext(fldname).Equals(System.DBNull.Value)
                        Case "=0"
                            doMap = drContext(fldname) = 0
                        Case "<=100"
                            doMap = drContext(fldname) <= 100
                        Case "=1295"
                            doMap = drContext(fldname) = "1295"
                        Case "<>1295"
                            doMap = drContext(fldname) <> "1295"
                        Case "isdifferent"

                            'if the dates are the same, we want to set false because our true condition is "isdifferent"

                            'make sure it's not null
                            If Not drContext(fldname).Equals(System.DBNull.Value) Then
                                Dim compareFrom As Object = Nothing
                                Dim compareTo As Object = Nothing

                                'try to convert to date time object
                                System.DateTime.TryParse(srcFieldVal, compareTo)

                                'need to do some extra conversion for dates
                                If fldtype.Equals("date") Then

                                    'see if the dates are identical
                                    If drContext(fldname).Date.compareto(compareTo) = 0 Then
                                        doMap = False
                                    End If

                                ElseIf fldtype.Equals("macoladate") Then
                                    'convert to yyyyMMdd format
                                    compareTo = CDate(compareTo).ToString("yyyyMMdd")

                                    If drContext(fldname).Equals(CInt(compareTo)) Then
                                        doMap = False
                                    End If

                                Else
                                    doMap = Not drContext(fldname).Equals(srcFieldVal)
                                End If
                            End If
                    End Select

                End While
                'cleanup
                rdr.Close()
                cmd.Dispose()

            Catch ex As Exception
                doMap = False
            End Try

        End Using

        Return doMap

    End Function

    Function MacolaDateToOneDate(ByVal macolaDate As String) As String
        Dim retDate As DateTimeOffset
        Dim macDate As DateTime

        'do some integ checking
        If macolaDate.Length = 8 Then

            macDate = New DateTime(CInt(macolaDate.Substring(0, 4)), CInt(macolaDate.Substring(4, 2)), CInt(macolaDate.Substring(6, 2)), 8, 0, 0)
            retDate = New DateTimeOffset(macDate, New TimeSpan(-5, 0, 0))

            Return retDate.ToString("yyyy-MM-ddThh:mm:sszzz")
        End If

        Return Nothing

    End Function

    Function ParseDateTime(ByVal sDateTime As String) As DateTime

        Dim retDate As DateTime
        Dim arrDateTime As String()
        Dim arrDate As String()
        Dim arrTime As String()
        Dim arrStartTime, arrEndTime As String()

        'split date/time string from OneNetwork
        arrDateTime = sDateTime.Split("T")

        'now split the date portion
        arrDate = arrDateTime(0).Split("-")

        'now split the time, the format is hh:mm:dd-hh:mm:dd
        'which is the start time and end time
        arrTime = arrDateTime(1).Split("-")

        'now split the start/end time for their hh:mm:ss
        arrStartTime = arrTime(0).Split(":")
        If arrTime.GetUpperBound(0) = 1 Then
            arrEndTime = arrTime(1).Split(":")
        End If
        'now assign to vars to pass as params
        Dim year, month, day, hour, minute, second As Integer
        year = arrDate(0)
        month = arrDate(1)
        day = arrDate(2)
        hour = arrStartTime(0)
        minute = arrStartTime(1)
        second = arrStartTime(2)

        retDate = New DateTime(year, month, day, hour, minute, second)

        Return retDate

    End Function


    Function CarrierMap_Lookup(ByRef CarrierNo As String, ByRef Carrier As String) As Long
        Dim retval As Long = Nothing
        Dim ssql As String

        Using cnn As New OdbcConnection(Me.connString)
            Try
                Dim drCarrierMap As OdbcDataReader
                Dim cmd As OdbcCommand


                ssql = "SELECT * FROM CarrierMap WHERE CarrierNo = ? AND  Carrier = ?"
                cmd = New OdbcCommand(ssql, cnn)
                cmd.Parameters.Add("@CarrierNo", OdbcType.VarChar).Value = CarrierNo
                cmd.Parameters.Add("@Carrier", OdbcType.VarChar).Value = Carrier

                cnn.Open()
                drCarrierMap = cmd.ExecuteReader

                If drCarrierMap IsNot Nothing Then
                    If drCarrierMap.Read() Then
                        retval = drCarrierMap("E1AN8")
                    End If
                End If

                'cleanup
                drCarrierMap.Close()
                cmd.Dispose()

            Catch ex As Exception
                retval = Nothing
            End Try
        End Using

        Return retval

    End Function

    'Converts Oracle Blob Data to string  (E1 Sales Order Notes are stored as Blob data type in Oracle)
    Public Sub WriteBlobData(ByVal Notes As String, ByVal Order_No As String, ByVal Order_Type As String, ByVal KCOO As String, Optional ByVal TestCase As String = "")
        Dim retval As Boolean = False
        Dim ssql As String = ""
        'Dim conn As OracleConnection = New OracleConnection("User Id=kikapps;Password=custom;Data Source=KIKE1DB1")
        Dim cmd As New OracleCommand
        Dim returnstring As String = ""
        cmd.Connection = New OracleConnection(Me.OracleConn)
        cmd.Connection.Open()

        'Add a comment to end of notes to let the user know these notes are used by OneNetwork and will be overwritten
        Notes = Notes & ControlChars.CrLf & "(These notes are overwritten by OneNetwork)"

        Try
            Select Case UCase(TestCase)
                Case "READONLY"
                    cmd.CommandText = "SELECT GDTXKY FROM " & Me.E1Database & ".F00165 WHERE GDOBNM = 'GT4201A' AND GDTXKY = '" & Order_No & "|" & Order_Type & "|" & KCOO & "' AND GDGTITNM = 'ONENETWORK' "
                    Dim Notes_Str As String = System.Text.Encoding.Unicode.GetString(cmd.ExecuteScalar())

                Case Else
                    cmd.CommandText = "SELECT Max(GDMOSEQN) FROM " & Me.E1Database & ".F00165 WHERE GDOBNM = 'GT4201A' AND GDTXKY = '" & Order_No & "|" & Order_Type & "|" & KCOO & "' AND GDGTITNM = 'ONENETWORK'"
                    Dim MaxOneNetLineNum As Object = cmd.ExecuteScalar()

                    'If MaxOneNetLineNum has no value then there is no existing with the label 'ONENETWORK' so we have to get the next line number and insert a new record
                    If MaxOneNetLineNum.Equals(System.DBNull.Value) Then
                        cmd.CommandText = "SELECT Max(GDMOSEQN) FROM " & Me.E1Database & ".F00165 WHERE GDOBNM = 'GT4201A' AND GDTXKY = '" & Order_No & "|" & Order_Type & "|" & KCOO & "' "
                        MaxOneNetLineNum = cmd.ExecuteScalar()
                        If MaxOneNetLineNum.Equals(System.DBNull.Value) Then MaxOneNetLineNum = 1 Else MaxOneNetLineNum = MaxOneNetLineNum + 1
                        Dim blob As Byte() = System.Text.Encoding.Unicode.GetBytes(Notes)
                        Dim param As OracleParameter = cmd.Parameters.Add("blobtodb", OracleDbType.Blob)
                        param.Value = blob
                        param.Direction = ParameterDirection.Input

                        cmd.CommandText = "INSERT INTO " & Me.E1Database & ".F00165 (GDOBNM,GDTXKY,GDMOSEQN,GDGTMOTYPE,GDLNGP,GDUSER,GDUPMJ,GDTDAY,GDGTITNM,GDQUNAM,GDGTFILENM,GDGTFUTS1,GDGTFUTS2,GDGTFUTS3,GDGTFUTS4,GDGTFUTM1,GDGTFUTM2,GDTXFT) VALUES ('GT4201A','" & Order_No & "|" & Order_Type & "|" & KCOO & "'," & MaxOneNetLineNum.ToString & ",0,' ','ONENETWORK',KIKDATETOJUL(CURRENT_DATE),to_char(Current_Date, 'HHmmss'),'ONENETWORK',' ',' ',' ',' ',' ',' ',0,0,:blobtodb)"
                        cmd.ExecuteNonQuery()
                    Else
                        'Otherwise if MaxOneNetLineNum has a value then we just need to update the record with line number  = MaxOneNetLineNum


                        Dim blob As Byte() = System.Text.Encoding.Unicode.GetBytes(Notes)
                        Dim param As OracleParameter = cmd.Parameters.Add("blobtodb", OracleDbType.Blob)
                        param.Value = blob
                        param.Direction = ParameterDirection.Input

                        cmd.CommandText = "UPDATE " & Me.E1Database & ".F00165 SET GDTXFT = :blobtodb WHERE GDOBNM = 'GT4201A' AND GDGTITNM = 'ONENETWORK' AND GDTXKY =  '" & Order_No & "|" & Order_Type & "|" & KCOO & "' AND GDMOSEQN = " & MaxOneNetLineNum.ToString
                        cmd.ExecuteNonQuery()

                    End If

                    'Notes = Notes & ControlChars.CrLf & "(These notes are overwritten by OneNetwork)"
                    'Dim blob As Byte() = System.Text.Encoding.Unicode.GetBytes(Notes)
                    'Dim param As OracleParameter = cmd.Parameters.Add("blobtodb", OracleDbType.Blob)
                    'param.Value = blob
                    'param.Direction = ParameterDirection.Input

                    'cmd.CommandText = "UPDATE " & My.Settings.E1Database & ".F00165 SET GDGTITNM = 'ONENETWORK', GDTXFT = :blobtodb WHERE GDOBNM = 'GT4201A' AND GDTXKY = '17936|SO|00200' AND GDMOSEQN = 2 "
                    'cmd.ExecuteNonQuery()
            End Select

            cmd.Connection.Close()

            cmd.Connection.Dispose()
            cmd.Dispose()
            cmd = Nothing
            retval = True
            'Byte[] byteBLOBData = new Byte[0];
            'byteBLOBData = (Byte[])(row["DOCLIST"]);
            'Dim s As String = System.Text.Encoding.UTF8.GetString(byteBLOBData)

        Catch ex As Exception
            retval = False
            cmd.Connection.Close()

            cmd.Connection.Dispose()
            cmd.Dispose()
            cmd = Nothing
        End Try
    End Sub

    Private Function GetProNumExc(ByVal Order_no As String, ByVal ShipTo As String) As Boolean
        'Function to check if the current order falls into the exception rule for PRoNumber upload functionality.
        Dim da As OdbcDataAdapter
        Dim dsProNum As New DataSet
        Dim dsBillTo As New DataSet
        Dim dsProBill As New DataSet
        Dim dsLoadPO As New DataSet
        Dim ssql As String
        Dim Found As Boolean = False
        Dim BillTo As String = ""
        'Step 1 Get BillTo via the Order_No
        If My.Settings.Application_Mode = "LIVE" Then
            ssql = "SELECT SHAN8 FROM " & My.Settings.E1Database & ".F4201  WHERE SHDOCO = ?"
        Else
            ssql = "SELECT SHAN8 FROM " & My.Settings.E1Database_TEST & ".F4201  WHERE SHDOCO = ?"
        End If

        Try
            da = New OdbcDataAdapter(ssql, My.Settings.E1Conn)
            da.SelectCommand.Parameters.Add("@SDDOCO", OdbcType.Numeric).Value = Order_no.Trim
            If da.Fill(dsBillTo) = 0 Then
                BillTo = ""
            Else
                BillTo = dsBillTo.Tables(0).Rows(0).Item("SHAN8").ToString
            End If

        Catch ex As Exception
            errMessage.Add("Error in Try block on Order: " & Order_no & " | Env: " & My.Settings.E1Database & " | ConStr: " & My.Settings.E1Conn & vbCrLf & ex.Message, "GetProNumExc")
            dsBillTo = Nothing
            da = Nothing
        End Try

        dsBillTo = Nothing


        'Step 2 Check BillTo/ShipTo
        ssql = "SELECT * FROM ProNumberEXP WHERE BillTo = ? and ShipTo = ?"
        Try
            da = New OdbcDataAdapter(ssql, Me.connString)
            da.SelectCommand.Parameters.Add("@BillTo", OdbcType.VarChar).Value = BillTo.Trim
            da.SelectCommand.Parameters.Add("@ShipTo", OdbcType.VarChar).Value = ShipTo.Trim

            If da.Fill(dsProNum) > 0 Then
                Found = True
            Else
                Found = False
            End If

        Catch ex As Exception
            dsProNum = Nothing
        End Try
        dsProNum = Nothing

        'if nothing is found check just bill to.

        If Found = False Then
            ssql = "SELECT * FROM ProNumberEXP WHERE BillTo = ? and ltrim(RTrim(ShipTo)) = ''"
            Try
                da = New OdbcDataAdapter(ssql, Me.connString)
                da.SelectCommand.Parameters.Add("@BillTo", OdbcType.VarChar).Value = BillTo.Trim
                da.SelectCommand.Parameters.Add("@ShipTo", OdbcType.VarChar).Value = ShipTo.Trim
                If da.Fill(dsProBill) > 0 Then
                    Found = True
                Else
                    Found = False
                End If

            Catch ex As Exception
                dsProBill = Nothing
            End Try
            dsProBill = Nothing
        End If

        'for Walmart We only send up the the ProNumber once to OneNetwork so we track this in a table in Onenetwork, check that table to insure that we have not already sent this info up.
        If BillTo.Trim = "29666" Or BillTo.Trim = "4225" Or BillTo.Trim = "3355" Or BillTo.Trim = "1556" Then
            ssql = "SELECT * FROM ProNumHist WHERE Order_ID = ?"
            Try
                da = New OdbcDataAdapter(ssql, Me.connString)
                da.SelectCommand.Parameters.Add("@Order_ID", OdbcType.VarChar).Value = Order_no.Trim
                If da.Fill(dsLoadPO) > 0 Then
                    Found = True
                End If
            Catch ex As Exception
                dsLoadPO = Nothing
            End Try
            dsLoadPO = Nothing
        End If
        Return Found

    End Function
    Private Sub MarkOrdPro(ByVal Order_ID As String, ByVal ProNo As String)
        'This will update the OrderNo and ProNo to a tracking table for walmart.
        Dim con As New SqlConnection
        Dim cmd As New SqlCommand
        Dim CMD2 As OdbcCommand
        Dim Con2 As New OdbcConnection
        Try
            Con2.ConnectionString = Me.connString
            Con2.Open()
            CMD2 = New OdbcCommand("INSERT INTO ProNumHist (Order_ID, ProNumber,DateUpload) VALUES (?,?,?)", Con2)
            CMD2.Connection = Con2
            CMD2.CommandText = "INSERT INTO ProNumHist (Order_ID, ProNumber,DateUpload) VALUES (?,?,?)"
            CMD2.Parameters.AddWithValue("@Order_ID", OdbcType.VarChar).Value = Order_ID.Trim
            CMD2.Parameters.AddWithValue("@ProNumber", OdbcType.VarChar).Value = ProNo.Trim
            CMD2.Parameters.AddWithValue("@DateUpload", OdbcType.VarChar).Value = Date.Now
            CMD2.ExecuteNonQuery()
        Catch ex As Exception
            errMessage.Add("Error Writting Pronumber to ProNumHist Table: (" & ex.Message & ")", "MarkOrdPro")
        End Try

    End Sub

End Class
